LINKIT-H3I
======

Installation
======
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash

sudo EXTERNAL_URL="http://gitlab.example.com" yum install -y gitlab-ce

Git global setup
======

1. git config --global user.name "Wilie Wahyu Hidayat"
2. git config --global user.email "wilie.hidayat87@gmail.com"

Link a SSH key to server
======

Check : cat ~/.ssh/id_rsa.pub

Add : ssh-keygen -t rsa -C "wilie.hidayat87@gmail.com"

Create a new repository
======

git clone git@gitlab.com:wilie.hidayat87/linkit-h3i.git

cd /app/xmp2012/interface/moh3i

touch README.md

git add README.md

git commit -m "add README"

git push -u origin master


Push an existing folder
======

cd /app/xmp2012/interface/moh3i

git init

git remote add origin git@gitlab.com:wilie.hidayat87/linkit-h3i.git

1. git add /home/production/.
2. git add /home/willy/.
3. git add /app/xmp2021/interface/default/.
4. git add /app/xmp2021/interface/moh3i/bin/.
5. git add /app/xmp2021/interface/moh3i/broadcast/.
6. git add /app/xmp2021/interface/moh3i/charging/.
7. git add /app/xmp2021/interface/moh3i/config/.
8. git add /app/xmp2021/interface/moh3i/content/.
9. git add /app/xmp2021/interface/moh3i/mo/.
10. git add /app/xmp2021/interface/moh3i/mt/.
11. git add /app/xmp2021/interface/moh3i/service/.
12. git add /app/xmp2021/interface/moh3i/user/.
13. git add /app/xmp2021/interface/moh3i/www/.
14. git add /app/xmp2021/interface/moh3i/www/api/.
15. git add /app/xmp2021/interface/moh3i/www/cms/.
16. git add /app/xmp2021/interface/moh3i/www/data/.
17. git add /app/xmp2021/interface/moh3i/www/dr/.
18. git add /app/xmp2021/interface/moh3i/www/mo/.
19. git add /app/xmp2021/interface/moh3i/www/sms/.
20. git add /app/xmp2021/interface/moh3i/www/techcheck/.
21. git add /app/xmp2021/interface/moh3i/www/push_statistic.php
22. git add /app/xmp2021/interface/moh3i/www/techcheck.php
23. git add /app/xmp2021/interface/moh3i/www/techcheck.php
24. git add /app/xmp2021/interface/moh3i/xmp.php

git commit -m "Initial commit"

git push -u origin master


Push an existing Git repository
======

cd /app/xmp2012/interface/moh3i

git remote rename origin old-origin

git remote add origin git@gitlab.com:wilie.hidayat87/linkit-h3i.git

git push -u origin --all

git push -u origin --tags


Pull repo to production
======

git remote add origin git@gitlab.com:wilie.hidayat87/linkit-h3i.git

git fetch origin

git status

git pull -u origin master

Revert pull request
======

git revert <commit sha1>

HOW TO
======

-- Add PS/SPC Keyword

1. Add PS/SPC Keyword in this path configuration /app/xmp2012/interface/moh3i/config/mt.php
2. Add config keyword in xmp.mechanism table
3. Add data lock in this path /DATA/app/xmp2012/moh3i/lock/DP_master
4. Add keyword in table dbpush.push_schedule field notes
5. Add tpm setting in this path 
    5.1. /app/xmp2012/interface/moh3i/bin/broadcast/broadcast_renewal_daily_priority.sh
    5.1. /app/xmp2012/interface/moh3i/bin/broadcast/broadcast_renewal_daily_nonpriority.sh
    etc...
6. Add setting for auto tpm in /app/xmp2012/interface/moh3i/config/tpmall.json

-- Running nohup

1. nohup sh /app/xmp2012/interface/moh3i/bin/charging/nohup_token.sh &
2. nohup sh /app/xmp2012/interface/moh3i/bin/broadcast/nohup_erase_mo_lock.sh &
3. nohup sh /app/xmp2012/interface/moh3i/bin/mo/nohup_moprocess.sh &
4. nohup sh /app/xmp2012/interface/moh3i/bin/mo/nohup_mospcprocess.sh &
5. nohup sh /app/xmp2012/interface/moh3i/bin/mo/nohup_moretryprocess.sh &
6. nohup sh /app/xmp2012/interface/moh3i/bin/mt/nohup_mtdelay.sh &
