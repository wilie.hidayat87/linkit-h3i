<?php

class database_mysql implements database_interface {

    public $resource;
    public $connProfile;
    public $numRows;

    public function connect($connProfile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($connProfile)));

        $this->resource = @mysql_connect($connProfile ['host'], $connProfile ['username'], $connProfile ['password']);
        if (!is_resource($this->resource)) {
            $log->write(array('level' => 'error', 'message' => "Resource not found : " . mysql_error()));
            return false;
        } else {
            $log->write(array('level' => 'debug', 'message' => "Resource found : " . print_r($this->resource, 1)));
        }

        $selectDb = @mysql_select_db($connProfile ['database'], $this->resource) or $log->write(array('level' => 'error', 'message' => mysql_error($this->resource)));
        if ($selectDb === false) {
            return false;
        }

        $this->connProfile = $connProfile;
        return true;
    }

    public function reconnect($connProfile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($connProfile)));

        $this->resource = @mysql_connect($connProfile ['host'], $connProfile ['username'], $connProfile ['password'], true);
        if (!is_resource($this->resource)) {
            $log->write(array('level' => 'error', 'message' => "Resource not found : " . mysql_error($this->resource)));
            return false;
        } else {
            $log->write(array('level' => 'debug', 'message' => "Resource found : " . print_r($this->resource, 1)));
        }

        $selectDb = @mysql_select_db($connProfile ['database'], $this->resource) or $log->write(array('level' => 'error', 'message' => mysql_error($this->resource)));
        if ($selectDb === false) {
            return false;
        }

        $this->connProfile = $connProfile;
        return true;
    }

    public function set_charset() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        return @mysql_set_charset('utf8', $this->resource);
    }

    public function query($sql) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $sql));

        $this->check_connection($this->connProfile);

        $log->write(array('level' => 'debug', 'message' => "Resource : " . print_r($this->resource, 1)));
        $query = @mysql_query($sql, $this->resource) or $log->write(array('level' => 'error', 'message' => "Resource not found : " . @mysql_error($this->resource)));

        $this->numRows = $this->numRows();
        return $query;
    }

    public function fetch($sql) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        $row2 = array();
        $query = $this->query($sql);
        while ($row = @mysql_fetch_array($query, MYSQL_BOTH)) {
            $row2 [] = $row;
        }
        $this->numRows = $this->numRows();
        if (count($row2) == 0) {
            $log->write(array('level' => 'debug', 'message' => "Data not found"));
        }
        return $row2;
    }

    public function check_connection($connProfile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($connProfile)));

        $log->write(array('level' => 'debug', 'message' => "Resource : " . print_r($this->resource, 1)));
        if (@!mysql_ping($this->resource)) {
            $log->write(array('level' => 'info', 'message' => "Lost connection : " . mysql_error($this->resource)));
            return $this->reconnect($connProfile);
        }
    }

    public function numRows() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        return @mysql_affected_rows($this->resource);
    }

    public function last_insert_id() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        return @mysql_insert_id($this->resource);
    }

}