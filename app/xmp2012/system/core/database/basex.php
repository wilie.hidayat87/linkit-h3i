<?php
class database_basex {
	
	private static $instance;
	private $connectionBase;
	
	private function __construct(){}
	
	public static function getInstance() 
	{           
		if (! self::$instance) 
		{
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function load($connProfile, $reload = FALSE) 
	{
        $dbConfig	= loader_config::getInstance()->getConfig('database');
		$profile	= $dbConfig->profile[$connProfile];
		$className	= 'database_' . $profile['driver'] . 'x'; // Change This to the right class
		
		if(class_exists($className)) {
			$dbClass = new $className();
			$connect = ($reload == FALSE) ? $dbClass->connect($profile) : $dbClass->reconnect($profile);
		} else {
			$dbClass = FALSE;
		}
		
		return $dbClass;
	}
}