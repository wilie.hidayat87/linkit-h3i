<?php 

class xlsdp_functions {

	public function removeSubscriber($data) {
		
		$user = loader_data::get('user');
		$user->msisdn = $data['msisdn'];
		$user->adn = $data['adn'];
		$user->service = $data['service'];
		$user->operator = $data['operatorName'];
		$user->active = '2';
		
		$user_manager = user_manager::getInstance();
		$subscribe = $user_manager->getUserException($user);
		
		if ($subscribe) {
			if ($subscribe->active == '1') {
				$subscribe->channel_unsubscribe = 'SMS';
				$subscribe->transaction_id_unsubscribe = $data['trxid'];
				$subscribe->active = '2';
				$user_update = user_manager::getInstance();
				$user_update->updateUserData($subscribe);
			}
		}	
	}
	
	public function addSubscriber($data) {
		
		$user = loader_data::get('user');
		$user->msisdn = $data['msisdn'];
		$user->adn = $data['adn'];
		$user->service = $data['service'];
		$user->operator = $data['operatorName'];
		$user->active = '2';

		$user_manager = user_manager::getInstance();
		$subscribe = $user_manager->getUserException($user);

		if ($subscribe === false) {
			$user->transaction_id_subscribe = $data['trxid'];
			$user->channel_subscribe = 'SMS';
			$user->active = '1';
			
        	$user_update = user_manager::getInstance();
        	$user_update->addUserData($user);
        	
        	return true;
		} else {
			if ($subscribe->active == '1') {
				// blm tau mau ngapain karena ga ngirim sesuatu
			}
			return false;
		}		

	}
	
	public function sendFirstPush($data) {
		
		$log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => "First Push For : " . serialize($data)));		
		
		$service_config = loader_config::getInstance()->getConfig('service');
		$ini_reader = ini_reader::getInstance();
		$ini_data = loader_data::get('ini');
		$ini_data->file = $service_config->iniPath . "reg.ini";
	
		$firstpush = loader_data::get('mt');
		$firstpush->inReply = $mo_data->id;
		$firstpush->msgId = date('YmdHis') . str_replace('.', '', microtime(true));
		$firstpush->adn = $data['adn'];
		$firstpush->operatorId = $this->getOperatorID($data['operator']);
		$firstpush->operatorName = $data['operator'];
		$firstpush->service = $data['msisdn'];
		//$firstpush->subject = strtoupper('MT;PUSH;' . $mo_data->channel . ';SIM');
		$firstpush->subject = strtoupper('MT;PUSH;SMS;REG');
		$firstpush->msisdn = $data['msisdn'];
		$firstpush->channel = 'SMS';
		$firstpush->partner = '';
		$firstpush->type = 'mtpush';
		$firstpush->mo = NULL;
		
		$ini_data->type = 'push_after_reg';
		$ini_data->section = 'REPLY';
		$firstpush->msgData = $ini_reader->get($ini_data);
		$ini_data->section = 'CHARGING';
		$firstpush->price = $ini_reader->get($ini_data);
		
		$mt_processor = new manager_mt_processor ();
		$mt_processor->saveToQueue($firstpush);
		
		return true;
	}

	public function saveRequest($data) {
		$model_xlsdp = loader_model::getInstance ()->load ( 'xlsdp', 'connDatabase1' );
		$save_res = $model_xlsdp->save_request($data);
		if ($save_res)
			return $save_res;
		else
			return false;
	}
	
	public function getServiceName($serviceID) {
		$model_xlsdp = loader_model::getInstance ()->load ( 'xlsdp', 'connDatabase1' );
		$service_name = $model_xlsdp->get_service_name ( $serviceID );
		if ($service_name)
			return $service_name;
		else
			return false;
	}
	
	public function getOperatorID($name) {
		$model_xlsdp = loader_model::getInstance ()->load ( 'xlsdp', 'connDatabase1' );
		$operator = $model_xlsdp->get_operator_id($name);
		if ($operator)
			return $operator;
		else
			return false;
	}	
		
	public function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
	
		if (is_array($d)) {
			/*
				* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}
	
	public function arrayToObject($d) {
		if (is_array($d)) {
			/*
				* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return (object) array_map(__FUNCTION__, $d);
		}
		else {
			// Return object
			return $d;
		}
	}
}


?>