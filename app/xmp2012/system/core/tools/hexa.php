<?php
class tools_hexa{
    
    public static function hex_encode($text, $hex_sign){
        $log = manager_logging::getInstance ();
        $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
        
        $ret    =   "";
        $len    = strlen($text);
        for($i=0; $i<$len; $i++){
            $ret    .= sprintf("%s%02X", $hex_sign, ord($text{$i}));
        }
        return $ret;
    }
    
}