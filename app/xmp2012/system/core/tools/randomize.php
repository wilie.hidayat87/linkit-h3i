<?php

class tools_randomize {

    public static function get($len = 4) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $chars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $token = '';
        for ($i = 0; $i < $len; $i++)
            $token .= $chars [mt_rand(0, strlen($chars)-1)];
        return $token;
    }

    public function getPin($len = 4) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $token = '';
        for ($i = 0; $i < $len; $i++)
            $token .= rand(0, 9);
        return $token;
    }

}