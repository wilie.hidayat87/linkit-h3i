<?php

class tools_mail {

    public function sends(mail_data $data) {
        require_once CORE_PATH . "/library/swift/swift_required.php";
        $transport = Swift_SmtpTransport::newInstance($data->smtpHost, $data->smtpPort, $data->smtpType)
                ->setUsername($data->smtpUser)
                ->setPassword($data->smtpPwd);

        $mailer = Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance($data->subject)
                ->setFrom($data->from)
                ->setTo($data->sender)
                ->setBody($data->content);
        return $mailer->send($message);
    }

}