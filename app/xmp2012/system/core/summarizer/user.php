<?php
class summarizer_user implements summarizer_interface {
	public function execute(summarizer_data $summarizer){
                $log = manager_logging::getInstance ();
                $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
                
		$loader_model = loader_model::getInstance();
		
		$from_model = $loader_model->load("user", $summarizer->from);
		$from_data = $from_model->getSummary($summarizer);
		
		$to_model = $loader_model->load("report", $summarizer->to);
		$to_data = $to_model->saveSummaryUser($from_data, $summarizer);
		
		return true;
	}
}