<?php
$smarty_dir = loader_config::getInstance ()->getConfig ( 'smarty' )->libraryPath;
require_once ($smarty_dir . '/Smarty.class.php');

class library_smarty extends Smarty {
	public function __construct() {
		parent::__construct ();
		
		#load path from config
		$config_smarty = loader_config::getInstance ()->getConfig ( 'smarty' );
		//$smarty_dir = $config_smarty->libraryPath;
		$view_dir = $config_smarty->templatePath;
		
		$this->template_dir = $view_dir . '/templates/';
		$this->compile_dir = $view_dir . '/templates_c/';
		$this->config_dir = $view_dir . '/configs/';
		$this->cache_dir = $view_dir . '/cache/';
		
	//** un-comment the following line to show the debug console
	//$smarty->debugging = true;
	

	//$this->caching = Smarty::CACHING_LIFETIME_CURRENT;
	}

}