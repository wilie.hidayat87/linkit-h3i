<?php

class charging_mcp {

    public static function charge(mt_data $mt_data) {
        $log = manager_logging::getInstance();

        $loader_config = loader_config::getInstance();
        $config_mcp = $loader_config->getConfig('mcp');

        $get = 'phone=' . $mt_data->msisdn;
        $get .= '&msgid=' . $mt_data->msgId;

        $url = $config_mcp->billingUrl;

        $hit = http_request::get($url, $get);

        $result = explode('|', $hit);
        if (count($result) > 1) {
            if ($result [0] == 'BILLED') {
                return array('status' => 'OK', 'response' => $result [1]);
            } else {
                return array('status' => 'NOK', 'response' => $result [0]);
            }
        } else {
            return array('status' => 'NOK', 'response' => $hit);
        }
    }

}