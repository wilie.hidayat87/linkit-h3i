<?php

class charging_spring {

    public static function charge(mt_data $mt_data) {
        $log = manager_logging::getInstance();

        $loader_config = loader_config::getInstance();
        $config_spring = $loader_config->getConfig('spring');

        $get = 'phone=' . $mt_data->msisdn;
        $get .= '&msgid=' . $mt_data->msgId;
        $get .= '&user=' . $mt_data->charging->username;
        $get .= '&pwd=' . $mt_data->charging->password;
        $get .= '&appid=' . $mt_data->charging->chargingId;
        $get .= '&tariff_class=' . $mt_data->charging->tariffClass;
        $get .= '&content_type=014';
        if (strtoupper($mt_data->channel) == 'SMS') {
            $channelQuickHack = '003';
        } elseif (strtoupper($mt_data->channel) == 'WAP') {
            $channelQuickHack = '001';
        } else {
            $channelQuickHack = '002';
        }

        //$get .= '&buy_channel=' . $mt_data->channel;
        $get .= '&buy_channel=' . $channelQuickHack;

        $url = $config_spring->billingUrl;

        $hit = http_request::get($url, $get);

        $result = explode('|', $hit);
        if (count($result) > 1) {
            if ($result [0] == 'BILLED') {
                return array('status' => 'OK', 'response' => $result [1]);
            } else {
                return array('status' => 'NOK', 'response' => $result [0]);
            }
        } else {
            return array('status' => 'NOK', 'response' => $hit);
        }
    }

}