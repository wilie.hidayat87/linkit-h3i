<?php
class wap_reg_session_data {
	public $id;
	public $token;
	public $service;
	public $operator;
	public $adn;
	public $msisdn;
	public $status;
	public $date_created;
	public $date_modified;
}