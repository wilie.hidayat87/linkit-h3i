<?php

class http_request {

    public static function get($url, $getfields = '', $timeout = 10) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . (!empty($getfields) ? '?' . $getfields : ""));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $output = curl_exec($ch);
        $header = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $log->write(array('level' => 'info', 'message' => "Hit url : " . $url . (!empty($getfields) ? "?" . $getfields : ""), 'response' => "Header: " . $header . ". Body: " . $output));
        return $output;
    }

    public static function post($url, $postfields, $timeout = 10, $arrCustom = array()) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

        foreach ($arrCustom as $key => $val) {
            curl_setopt($ch, $key, $val);
        }

        $output = curl_exec($ch);
        $header = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $log->write(array('level' => 'info', 'message' => "Hit url : " . $url . "?" . $postfields, 'response' => "Header: " . $header . ". Body: " . $output));
        return $output;
    }

	/* public static function requestPost($req) 
	{		
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		$start = time();
		
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_URL, $req->url);
		
		if($req->port > 0)
			@curl_setopt($ch, CURLOPT_PORT, $req->port);
		
		if(!empty($req->login))
		{
			@curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			@curl_setopt($ch, CURLOPT_USERPWD, $req->login); 
		}
		
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $req->ssl);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_TIMEOUT, $req->timeout);
        @curl_setopt($ch, CURLOPT_POST, 1);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $req->body);
		@curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		
		if(!empty($req->headers))
			@curl_setopt($ch, CURLOPT_HTTPHEADER, $req->headers);

        $output = @curl_exec($ch);
        $header_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $header_out = @curl_getinfo($ch, CURLINFO_HEADER_OUT);
        @curl_close($ch);

		$elapse = (int)time() - (int)$start;

		echo "Request Header: ".json_encode($req->headers).", \nRequest Body : ".$req->body . "\n\n";
		echo $header_out;		
		echo "Response Code : " . $header_code . "\n";
		echo "Response Body : " . $output . "\n\n";
		echo "Time Executed : ".date("Y-m-d H:i:s")."\n\n";
		echo "Elapse : {$elapse} second\n\n";
		
		$log->write(array('level' => 'info', 'message' => "Request hit url : " . $req->url . ", Header : " . json_encode($req->headers) . ", Body : " . $req->body, 'response' => "Header: " . $header_code . ", Body: " . json_encode($output) . ", Elapse: " . $elapse));
		
		$resp = new stdClass();
		$resp->header_code = $header_code;
		$resp->output = $output;
		
        return $resp;
    } */
	
	public static function requestGet($url, $getfields = '', $timeout = 10) {
        $log = manager_logging::getInstance();
        
        $start = time();

        $which_php = shell_exec("which php");

        $log->write(array('level' => 'debug', 'message' => "Start : " . $which_php));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . (!empty($getfields) ? $getfields : ""));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $output = curl_exec($ch);
        $header = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $elapse = (int)time() - (int)$start;

        $log->write(array('level' => 'info', 'message' => "Hit url ({$which_php}) : " . $url . (!empty($getfields) ? $getfields : ""), 'response' => "Header: " . $header . ". Body: " . $output . ", Elapse: " . $elapse));
        return $output;
    }
	
	public static function requestPost($req, $part) {
	
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start Request {$part}"));
		
		$start = time();
			
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_URL, $req['url']);
		
		if($req['port'] > 0)
			@curl_setopt($ch, CURLOPT_PORT, $req['port']);
		
		if(!empty($req['login']))
		{
			@curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			@curl_setopt($ch, CURLOPT_USERPWD, $req['login']); 
		}
		
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $req['ssl']);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_TIMEOUT, $req['timeout']);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $req['body']);
		@curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		
		if(!empty($req['headers']))
			@curl_setopt($ch, CURLOPT_HTTPHEADER, $req['headers']);

		$output = @curl_exec($ch);
		$header_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_out = @curl_getinfo($ch, CURLINFO_HEADER_OUT);
		@curl_close($ch);

		$elapse = (int)time() - (int)$start;

		/* logg("Request ".strtoupper($part).": \n");
		logg("========================\n\n");
		logg("Request URL : " . $req['url'] . "\n");
		logg("Request Login : " . $req['login'] . "\n");
		logg("Request Header : " . json_encode($req['headers']) . ", \nRequest Body : " . $req['body'] . "\n");
		logg("========================\n\n");
		logg("Response Header : " . $header_out."\n");
		logg("Response Code : " . $header_code . "\n");
		logg("Response Body : " . $output);
		logg("========================\n\n");
		logg("Time Executed : " . date("Y-m-d H:i:s") . "\n");
		logg("Elapse : {$elapse} second\n");
		logg("========================\n\n\r\r"); */
		
		$resp = array();
		$resp['header_code'] = $header_code;
		$resp['output'] = $output;
		
		$log->write(array('level' => 'info', 'message' => "Request ".$part." Hit URL : " . $req['url'] . ", Header : " . json_encode($req['headers']) . ", Body : " . json_encode($req['body']), 'response' => "Header: " . $header_code . ", Body: " . json_encode($output) . ", Elapse: " . $elapse));
		
		return $resp;
	}
	
    public static function postBody($url, $body, $timeout = 10) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        return self::post($url, $body, $timeout);
    }

    public static function getRealIpAddr() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
