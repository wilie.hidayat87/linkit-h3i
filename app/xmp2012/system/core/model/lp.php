<?php

class model_lp extends model_base {

	public function countingLP($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = "INSERT INTO lp (id,hash,date_send,time_send,flag,landing,mo) VALUES 
		('','".mysql_real_escape_string($data->_hash)."',
		 '".mysql_real_escape_string($data->date_send)."',
		 '".mysql_real_escape_string($data->time_send)."',
		 '".mysql_real_escape_string($data->flag)."',
		 '".mysql_real_escape_string($data->landing)."',
		 '".mysql_real_escape_string($data->mo)."');";
		//echo $sql;die;
		$query = $this->databaseObj->query($sql);

        if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
			return false;
		}
    }
	
	public function getMoLP($lid)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        $log->write(array('level' => 'debug', 'message' => "Data : " . serialize($lid)));

        $sql = sprintf("SELECT mo FROM lp where flag=1 and id=".$lid." LIMIT 1");
		
        $data = $this->databaseObj->fetch($sql);
		
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
	}
	
	public function getLP()
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = sprintf("SELECT id FROM lp where flag=0 ORDER BY id ASC LIMIT 500;");
		
        $data = $this->databaseObj->fetch($sql);
		
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
	}
	
	public function updateLPFlag($data) {
		
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        $log->write(array('level' => 'debug', 'message' => "Data : " . serialize($data)));

        $sql = sprintf("UPDATE lp SET flag=1,mo=".$data->mo." WHERE id IN (".$data->id.");");
		
        $query = $this->databaseObj->query($sql);
		
		return true;
	}
	
    public function CountingLandingPage($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = "INSERT INTO lp (msisdn,hash,date_send,time_send,flag,landing,mo) VALUES 
		('".mysql_real_escape_string($data->msisdn)."',
		 '".mysql_real_escape_string($data->_hash)."',
		 '".mysql_real_escape_string($data->date_send)."',
		 '".mysql_real_escape_string($data->time_send)."',
		 '".mysql_real_escape_string($data->flag)."',
		 '".mysql_real_escape_string($data->landing)."',
		 '".mysql_real_escape_string($data->mo)."')";
		
		$query = $this->databaseObj->query($sql);

        if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
			return false;
		}
    }
	
	public function getByMO($data) {
		
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        $log->write(array('level' => 'debug', 'message' => "Data : " . serialize($data)));

        $sql = sprintf("SELECT id FROM lp where flag=".$data->flag." and mo=".$data->mo);
		
        $data = $this->databaseObj->fetch($sql);
		
        return $data;
	}
	
	public function getByLanding($data) {
		
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        $log->write(array('level' => 'debug', 'message' => "Data : " . serialize($data)));

        $sql = sprintf("SELECT id FROM lp where flag=".$data->flag);
		
        $data = $this->databaseObj->fetch($sql);
		
        return $data;
	}
	
	public function getByDataLP($data) {
		
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        $log->write(array('level' => 'debug', 'message' => "Data : " . serialize($data)));

        $sql = sprintf("SELECT id FROM lp where flag=".$data->flag." and landing=".$data->landing." and mo=".$data->mo);
		
        $data = $this->databaseObj->fetch($sql);
		
        if (count($data) > 0) {
            return $data;
        } else {
            return 0;
        }
	}
	
	public function updateFlag($lastID) {
		
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        $log->write(array('level' => 'debug', 'message' => "Data : " . serialize($lastID)));

        $sql = sprintf("UPDATE lp SET flag=1 WHERE id <= " . $lastID);
        //$sql = sprintf("DELETE FROM lp WHERE id <= " . $lastID);
		
        $query = $this->databaseObj->query($sql);
		
		return true;
	}
}

?>
