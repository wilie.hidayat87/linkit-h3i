<?php

class model_pushretry extends model_base {

	public function qSql($sql = '') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return array();
        }
    }
	
	public function eSql($sql = '') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$this->databaseObj->query($sql);
		return true;
    }
}