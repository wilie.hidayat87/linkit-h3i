<?php

class model_user extends model_base {

    public function populateUser($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

        $sql = sprintf("SELECT * 
				FROM xmp.subscription 
				WHERE service = '%s' 
				AND adn = '%s' 
				AND operator = '%s' 
				AND active = '1'
        		AND subscribed_from <= NOW() 
				AND subscribed_until >= NOW() 
        		", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function get($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "SELECT * FROM xmp.subscription WHERE 1=1 ";
        if (!empty($user_data->service))
            $sql .= " AND service = '" . mysql_real_escape_string($user_data->service) . "' ";

        if (!empty($user_data->adn))
            $sql .= " AND adn = '" . mysql_real_escape_string($user_data->adn) . "' ";

        if (!empty($user_data->operator))
            $sql .= " AND operator = '" . mysql_real_escape_string($user_data->operator) . "' ";

        if (!empty($user_data->active))
            $sql .= " AND active = '" . mysql_real_escape_string($user_data->active) . "' ";

        if (!empty($user_data->msisdn))
            $sql .= " AND msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "' ";

        //$sql = rtrim($sql, 'AND');
        $sql .= " ORDER BY ID DESC LIMIT 1";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result [0];
        } else {
            return false;
        }
    }
 
	public function getException($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $active = (!empty($user_data->active)) ? $user_data->active : '0';
        $sql = "SELECT * FROM xmp.subscription WHERE `active` <> " . mysql_real_escape_string($active);

        if (!empty($user_data->service))
            $sql .= " AND service = '" . mysql_real_escape_string($user_data->service) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        if (!empty($user_data->operator))
            $sql .= " AND operator = '" . mysql_real_escape_string($user_data->operator) . "'";

        if (!empty($user_data->msisdn))
            $sql .= " AND msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'";

        $sql .= " ORDER BY ID DESC LIMIT 1";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result [0];
        } else {
            return false;
        }
    }
	
    public function getUserException($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        //$active = (!empty($user_data->active)) ? $user_data->active : '0';
        $sql = "SELECT * FROM xmp.subscription WHERE `active` IN (" . $user_data->active . ")";

        if (!empty($user_data->service))
            $sql .= " AND service = '" . mysql_real_escape_string($user_data->service) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        if (!empty($user_data->operator))
            $sql .= " AND operator = '" . mysql_real_escape_string($user_data->operator) . "'";

        if (!empty($user_data->msisdn))
            $sql .= " AND msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'";

        $sql .= " ORDER BY ID DESC LIMIT 1";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result [0];
        } else {
            return false;
        }
    }

    public function add($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $subscribed_from = "";
        $transaction_id_subscribe = "";
        if ($user_data->active == '1') {
            $subscribed_from = date('Y-m-d H:i:s');
            $transaction_id_subscribe = $user_data->transaction_id_subscribe;
        }
        if (empty($user_data->channel_subscribe))
            $channel_subscribe = 'sms';
        else
            $channel_subscribe = $user_data->channel_subscribe;
		
		//$subscribed_to = (isset($user_date->subscribed_to)) ? $user_date->subscribed_to : "";
		
		if($user_data->operator == 'moh3i' && isset($user_data->active_until))
		{
			$sql = sprintf("INSERT INTO xmp.subscription (transaction_id_subscribe, msisdn, service, adn, operator, channel_subscribe, subscribed_from, subscribed_until, partner, active, time_created, time_updated, active_until, renewal_date
						) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',NOW(),NOW(),'%s','%s');", mysql_real_escape_string($transaction_id_subscribe), mysql_real_escape_string($user_data->msisdn), mysql_real_escape_string($user_data->service), mysql_real_escape_string($user_data->adn), mysql_real_escape_string($user_data->operator), mysql_real_escape_string($channel_subscribe), mysql_real_escape_string($subscribed_from), mysql_real_escape_string($user_data->subscribed_to), mysql_real_escape_string($user_data->partner), mysql_real_escape_string($user_data->active), mysql_real_escape_string($user_data->active_until), mysql_real_escape_string($user_data->renewal_date));
		}
		else
		{
			$sql = sprintf("INSERT INTO xmp.subscription (transaction_id_subscribe, msisdn, service, adn, operator, channel_subscribe, subscribed_from, subscribed_until, partner, active, time_created, time_updated
						) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',NOW(),NOW());", mysql_real_escape_string($transaction_id_subscribe), mysql_real_escape_string($user_data->msisdn), mysql_real_escape_string($user_data->service), mysql_real_escape_string($user_data->adn), mysql_real_escape_string($user_data->operator), mysql_real_escape_string($channel_subscribe), mysql_real_escape_string($subscribed_from), mysql_real_escape_string($user_data->subscribed_to), mysql_real_escape_string($user_data->partner), mysql_real_escape_string($user_data->active));
		}
		
        $this->databaseObj->query($sql);
        return $this->databaseObj->last_insert_id();
    }

    public function update(user_data $user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

		$loaderConfig = loader_config::getInstance();
		$configMain = $loaderConfig->getConfig('main');
		
        $sql = sprintf("UPDATE xmp.subscription SET active = '%s', time_updated = NOW()", mysql_real_escape_string($user_data->active));
        if (empty($user_data->channel_subscribe))
            $sql .= sprintf(", channel_subscribe = 'sms'");
        else
            $sql .= sprintf(", channel_subscribe = '%s'", mysql_real_escape_string($user_data->channel_subscribe));
        if (empty($user_data->channel_unsubscribe))
            $sql .= sprintf(", channel_unsubscribe = 'sms'");
        else
            $sql .= sprintf(", channel_unsubscribe = '%s'", mysql_real_escape_string($user_data->channel_unsubscribe));
       
	    if ($user_data->active == '1')
            $sql .= sprintf(", subscribed_from = '%s', transaction_id_subscribe = '%s'", date('Y-m-d H:i:s'), mysql_real_escape_string($user_data->transaction_id_subscribe));
		
        /* if ($user_data->active != '1')
            $sql .= sprintf(", subscribed_until = '%s', transaction_id_unsubscribe = '%s'", date('Y-m-d H:i:s'), mysql_real_escape_string($user_data->transaction_id_unsubscribe));
		*/
		
		if($user_data->operator == 'moh3i')
		{
			// if (!empty($user_data->subscribed_to))
				// $sql .= sprintf(", subscribed_until = '%s'", mysql_real_escape_string($user_data->subscribed_to));
			
			if (!empty($user_data->subscribed_to))
				$sql .= sprintf(", subscribed_until = '%s'", date("Y-m-d H:i:s", strtotime($configMain->subscribed_until)));
			
			if (!empty($user_data->active_until))
				$sql .= sprintf(", active_until = '%s'", date("Y-m-d H:i:s", strtotime($configMain->active_until)));

            if (!empty($user_data->success) || $user_data->success > 0)
			    $sql .= ", success = success + 1";

            $sql .= sprintf(", priority = '%s'", mysql_real_escape_string($user_data->priority));
			
			if (!empty($user_data->renewal_date))
				$sql .= sprintf(", renewal_date = '%s'", $user_data->renewal_date);
		}
		
        $sql .= sprintf(" WHERE id = '%s';", mysql_real_escape_string($user_data->id));
		
		$log->write(array('level' => 'debug', 'message' => "SQL UPDATE MO (".date("Y-m-d H:i:s", strtotime($configMain->subscribed_until)).") : " . $sql));
		
        $this->databaseObj->query($sql);
        return true;
    }

	public function updateByMsisdn(user_data $user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = sprintf("UPDATE xmp.subscription SET active = '%s', time_updated = NOW()", mysql_real_escape_string($user_data->active));
		
        if (empty($user_data->channel_subscribe))
            $sql .= sprintf(", channel_subscribe = 'sms'");
        else
            $sql .= sprintf(", channel_subscribe = '%s'", mysql_real_escape_string($user_data->channel_subscribe));
		
        if (empty($user_data->channel_unsubscribe))
            $sql .= sprintf(", channel_unsubscribe = 'sms'");
        else
            $sql .= sprintf(", channel_unsubscribe = '%s'", mysql_real_escape_string($user_data->channel_unsubscribe));
       
	    if (!empty($user_data->subscribed_from))
				$sql .= sprintf(", subscribed_from = '%s'", mysql_real_escape_string($user_data->subscribed_from));
			
	    if ($user_data->active == '1')
            $sql .= sprintf(", transaction_id_subscribe = '%s'", mysql_real_escape_string($user_data->transaction_id_subscribe));
		
        /* if ($user_data->active != '1')
            $sql .= sprintf(", subscribed_until = '%s', transaction_id_unsubscribe = '%s'", date('Y-m-d H:i:s'), mysql_real_escape_string($user_data->transaction_id_unsubscribe)); */
		
		if($user_data->operator == 'moh3i')
		{
			if (!empty($user_data->subscribed_to))
				$sql .= sprintf(", subscribed_until = '%s'", mysql_real_escape_string($user_data->subscribed_to));
			
			if (!empty($user_data->active_until))
				$sql .= sprintf(", active_until = '%s'", mysql_real_escape_string($user_data->active_until));

			$sql .= sprintf(", priority = '%s'", mysql_real_escape_string($user_data->priority));

            if (!empty($user_data->success))
				$sql .= ", success = success + 1";
		}
		
        $sql .= sprintf(" WHERE msisdn = '%s';", mysql_real_escape_string($user_data->msisdn));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateActivationByMsisdn(user_data $user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $extSQL = "";

        if ($user_data->priority <> 0)
			$extSQL = sprintf(", priority = '%s'", mysql_real_escape_string($user_data->priority));
        else
            $extSQL = sprintf(", priority = '%s'", 0);

        if (!empty($user_data->success) || $user_data->success > 0)
			$extSQL .= ", success = success + 1";
			
		if (!empty($user_data->renewal_date))
			$extSQL .= sprintf(", renewal_date = '%s'", mysql_real_escape_string($user_data->renewal_date));
				
        $sql = sprintf("UPDATE xmp.subscription SET active = '%s', time_updated = NOW() ".$extSQL." WHERE msisdn = '%s';", mysql_real_escape_string($user_data->active), mysql_real_escape_string($user_data->msisdn));
        $this->databaseObj->query($sql);
        return true;
    }
	
    public function getSpringUser($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "
				SELECT
					u.id AS id,
					u.adn AS adn,
					s.id AS package_id,
					u.service AS package_name,
					p.protocol AS protocol_number,
					u.active AS status,
					u.subscribed_from AS datetime_subscription,
					u.subscribed_until AS datetime_last_cancel,
					u.channel_subscribe AS channel
				FROM
					xmp.subscription AS u
				INNER JOIN
					xmp.service AS s ON u.service = s.name AND u.adn = s.adn
				INNER JOIN
					xmp.spring_protocol AS p ON p.subscription_id = u.id
				WHERE
					u.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND u.adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        if (!empty($user_data->protocolType))
            $sql .= " AND p.protocol_type = '" . mysql_real_escape_string($user_data->protocolType) . "'";

        $sql .= " ORDER BY u.id DESC ";

        if (!empty($user_data->limit))
            $sql .= " LIMIT " . mysql_real_escape_string($user_data->limit);

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSpringUserHistory($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $wapDB = loader_config::getInstance()->getConfig('database')->profile ['connWap'] ['database'];

        $sql = "
                       SELECT          
                                u.id,   
                                u.subscribed_from,
                                u.subscribed_until,
                                u.adn,  
                                s.id AS package_id,
                                s.name AS package_name,
                                p.protocol,
                                dl.price tariff,
                                u.channel_subscribe, 
                                u.channel_unsubscribe,
                                u.active
                        FROM    
                                xmp.subscription u
                        INNER JOIN
                                xmp.service s ON u.adn = s.adn AND u.service = s.name
                        INNER JOIN
                                xmp.spring_protocol p ON p.subscription_id = u.id
                        LEFT JOIN
                            (SELECT wd.price, ws.DateCreated,ws.Msisdn, ws.Operator, ws.Service FROM " . mysql_real_escape_string($wapDB) . ".wap_session ws
                        INNER JOIN
                            " . mysql_real_escape_string($wapDB) . ".wap_download_log wd ON wd.SessionID = ws.ID WHERE ws.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "') dl ON dl.Msisdn = u.msisdn AND dl.Operator = u.operator AND dl.Service = u.service AND dl.DateCreated between u.subscribed_from and u.subscribed_until
                        WHERE
                                u.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'
                        ";
        if (!empty($user_data->subscribed_from))
            $sql .= " AND date(u.subscribed_from) >= '" . mysql_real_escape_string($user_data->subscribed_from) . "'";

        if (!empty($user_data->subscribed_until))
            $sql .= " AND date(u.subscribed_until) <= '" . mysql_real_escape_string($user_data->subscribed_until) . "'";

        if (!empty($user_data->service))
            $sql .= " AND u.service = '" . mysql_real_escape_string($user_data->service) . "' ";

        if (!empty($user_data->adn))
            $sql .= " AND u.adn = '" . mysql_real_escape_string($user_data->adn) . "' ";

        $sql .= ' ORDER BY 
					u.id DESC';

        if (!empty($user_data->limit))
            $sql .= " LIMIT " . mysql_real_escape_string($user_data->limit);

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSpringUserProtocol($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "SELECT p.id, u.transaction_id_subscribe, u.transaction_id_unsubscribe, u.adn AS large_account, s.id AS package_id, u.service AS package_name, p.protocol AS protocol_number, u.msisdn, p.protocol_type AS type_id, u.active, u.subscribed_from, u.subscribed_until, p.mt_time AS datetime_mt, p.mo_time AS datetime_mo, p.msg_data AS message_mo, wd.Price AS tariff_value, p.protocol_type AS tariff_type_id, u.channel_subscribe AS activation_channel, u.channel_unsubscribe AS cancelation_channel, wd.DateCreated AS datetime_purchase 
                FROM spring_protocol AS p
                INNER JOIN subscription AS u ON p.subscription_id = u.id 
                INNER JOIN service AS s ON u.adn = s.adn AND u.service = s.name 
                LEFT JOIN wap.wap_download_log AS wd ON p.id = wd.ProtocolID 
                WHERE p.protocol = '" . mysql_real_escape_string($user_data->protocol) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND u.adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        $sql .= ' LIMIT 1';
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result [0];
        } else {
            return false;
        }
    }

    public function getSummary(summarizer_data $summarizer) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($summarizer)));

        $sql = sprintf("select DATE(subscribed_from) as date_subscribed, operator, adn, service, channel_subscribe as channel, count(*) as total 
					from %s 
					where date(subscribed_from) = '%s' group by adn, service, channel_subscribe, operator", mysql_real_escape_string($summarizer->tableFrom), mysql_real_escape_string($summarizer->date));
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSpringDownloadHistory($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $wapDB = loader_config::getInstance()->getConfig('database')->profile ['connWap'] ['database'];

        $sql = "
			SELECT
                            wd.id,
                            us.adn,
                            p.protocol, 
                            p.mo_time, 
                            p.mt_time, 
                            p.msg_data,
                            wd.price tariff,
                            us.channel_subscribe,
                            us.channel_unsubscribe,
                            wd.status_charging,
                            wc.ID contentId,
                            wc.Title contentName,
                            wc.Code,
                            wd.DateCreated purchase_time,
                            p.protocol_type tariffId
                        FROM
                            " . mysql_real_escape_string($wapDB) . ".wap_session ws
                        INNER JOIN
                            " . mysql_real_escape_string($wapDB) . ".wap_download_log wd ON wd.SessionID = ws.ID
                        INNER JOIN
                            " . mysql_real_escape_string($wapDB) . ".wap_content wc ON wc.Code = wd.ContentCode
                        LEFT JOIN
                            spring_protocol p ON p.id = wd.ProtocolID
                        INNER JOIN
                            (select * from subscription where msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "' order by id desc limit 1)us
                             ON ws.Msisdn = us.msisdn AND ws.Operator = us.operator AND ws.Service = us.service
                        WHERE
                            ws.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "' AND ws.Status = '1'";

        if (!empty($user_data->subscribed_from))
            $sql .= " AND date(wd.DateCreated) >= '" . mysql_real_escape_string($user_data->subscribed_from) . "'";

        if (!empty($user_data->subscribed_until))
            $sql .= " AND date(wd.DateCreated) <= '" . mysql_real_escape_string($user_data->subscribed_until) . "'";

        if (!empty($user_data->service))
            $sql .= " AND us.service = '" . mysql_real_escape_string($user_data->service) . "' ";

        if (!empty($user_data->adn))
            $sql .= " AND us.adn = '" . mysql_real_escape_string($user_data->adn) . "' ";

        $sql .= ' ORDER BY wd.id DESC';

        if (!empty($user_data->limit))
            $sql .= " LIMIT " . mysql_real_escape_string($user_data->limit);

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSpringRetry($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "
            SELECT * 
            FROM subscription
            WHERE msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        if (!empty($user_data->active))
            $sql .= " AND active = '" . mysql_real_escape_string($user_data->active) . "'";

        $sql .= ' ORDER BY id DESC LIMIT 1';

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function execUser($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

        //$sql = sprintf("SELECT msisdn FROM subscription WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '1' ORDER BY priority DESC", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator));
	$sql = sprintf("SELECT msisdn FROM xmp.subscription WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '1'", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
    
	public function execUserModDate($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

		$sql = sprintf("SELECT msisdn FROM xmp.subscription WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '1' AND DATE_ADD(date(subscribed_from), INTERVAL ".mysql_real_escape_string($broadcast_data->subdate)." DAY) = CURRENT_DATE()", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator));
		//echo $sql;
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
    public function getSpringexecUser($broadcast_data, $afterDay) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

        $sql = sprintf("SELECT * FROM xmp.subscription WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '1' AND DATEDIFF(DATE(NOW()),DATE(subscribed_from)) = '%s' AND DATE(subscribed_from) <> DATE(NOW())", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator), mysql_real_escape_string($afterDay));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function updateSubscription($transactionId, $msisdn) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start Transaction Id : " . $transactionId . ". msisdn : " . $msisdn));

        $sql = sprintf("UPDATE xmp.subscription SET transaction_id_subscribe = '%s'", mysql_real_escape_string($transactionId));
        $sql .= sprintf(" WHERE msisdn = '%s';", mysql_real_escape_string($msisdn));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateStatusSubscription($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE xmp.subscription SET active = '%s' WHERE msisdn = '%s' AND service = '%s' AND operator = '%s'", mysql_real_escape_string($data->status), mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->service), mysql_real_escape_string($data->operator));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function getMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("SELECT * FROM xmp.membership WHERE service = '%s' AND operator = '%s' AND msisdn = '%s' LIMIT 1", mysql_real_escape_string($data->service), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->msisdn));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }
	
	public function insertMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("INSERT INTO xmp.membership (id,msisdn,active,created_date,modify_date,operator,service,password,sent) values ('','%s','%s','%s','%s','%s','%s','%s','%s')", mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->active), mysql_real_escape_string($data->created_date), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->service), mysql_real_escape_string($data->password), mysql_real_escape_string($data->sent));
        $this->databaseObj->query($sql);
        return true;
    }

	public function updateActiveMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE xmp.membership SET active = '%s',modify_date = '%s',sent = '%s' WHERE msisdn = '%s' and operator = '%s' and service = '%s'", mysql_real_escape_string($data->active), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->sent), mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->service));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updatePasswordMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE xmp.membership SET active = '%s',password = '%s',modify_date = '%s',sent = '%s' WHERE msisdn = '%s' and operator = '%s' and service = '%s'", mysql_real_escape_string($data->active), mysql_real_escape_string($data->password), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->sent), mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->service));
		//echo $sql;die;
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateMembershipSent($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE xmp.membership SET sent = '%s',active = '%s', modify_date = '%s' WHERE msisdn = '%s' and operator = '%s' and service = '%s'", mysql_real_escape_string($data->sent), mysql_real_escape_string($data->active), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->service));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function getREGTransact($p) {
		$log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($p)));
		$sql = sprintf("SELECT MSGDATA, SUBJECT, SERVICE, ADN FROM xmp.tbl_msgtransact WHERE msisdn = '%s' AND msgindex = '%s' ORDER BY id DESC LIMIT 1;", mysql_real_escape_string($p->msisdn), mysql_real_escape_string($p->msgId));
		
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result[0];
		} else {
			return false;
		}
	}
	
	public function plusIntervalWeeklyCharge($data) {
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));
		
		/* $interval = '1';
		if(!empty($data->interval))  */
			$interval = $data->interval;
		
		// SALAH QUERY
        //$sql = sprintf("UPDATE subscription SET subscribed_from = CONCAT(DATE_ADD(date(subscribed_from), INTERVAL %s DAY), ' 00:00:00') WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '%s' AND msisdn = '%s' LIMIT 1", mysql_real_escape_string($interval), mysql_real_escape_string($data->service), mysql_real_escape_string($data->adn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->active), mysql_real_escape_string($data->msisdn));
		
		if($interval == '0')
		{
			$sql = sprintf("UPDATE xmp.subscription SET subscribed_from = CONCAT(current_date, ' 00:00:00') WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '%s' AND msisdn = '%s' LIMIT 1", mysql_real_escape_string($data->service), mysql_real_escape_string($data->adn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->active), mysql_real_escape_string($data->msisdn));
		}
		else
		{
			$sql = sprintf("UPDATE xmp.subscription SET subscribed_from = CONCAT(DATE_ADD(current_date, INTERVAL %s DAY), ' 00:00:00') WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '%s' AND msisdn = '%s' LIMIT 1", mysql_real_escape_string($interval), mysql_real_escape_string($data->service), mysql_real_escape_string($data->adn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->active), mysql_real_escape_string($data->msisdn));
		}
		
        $this->databaseObj->set_charset();
        $this->databaseObj->query($sql);
        return $this->databaseObj->numRows;
	}
	
	public function plusIntervalRetryNextDay($data) {
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));
		
		$interval = '6';
		if(!empty($data->interval)) 
			$interval = $data->interval;
		
        $sql = sprintf("UPDATE xmp.subscription SET subscribed_from = CONCAT(DATE_SUB(current_date, INTERVAL %s DAY), ' 00:00:00') WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '%s' AND msisdn = '%s' LIMIT 1", mysql_real_escape_string($interval), mysql_real_escape_string($data->service), mysql_real_escape_string($data->adn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->active), mysql_real_escape_string($data->msisdn));
        $this->databaseObj->set_charset();
        $this->databaseObj->query($sql);
        return $this->databaseObj->numRows;
	}
	
	public function isBlacklisted($msisdn) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn));

        $sql = sprintf("SELECT * FROM xmp.blacklist_msisdn WHERE msisdn = '%s' LIMIT 1", mysql_real_escape_string($msisdn));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return true;
        } else {
            return false;
        }
    }
	
	public function isWhitelisted($msisdn, $keyword) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn . ", keyword = " . $keyword));

        $sql = sprintf("SELECT * FROM xmp.whitelist_msisdn WHERE msisdn = %s AND UPPER(keyword) = '%s' LIMIT 1", mysql_real_escape_string($msisdn), mysql_real_escape_string($keyword));
		
		$log->write(array('level' => 'info', 'message' => "SQL : " . $sql));
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return true;
        } else {
            return false;
        }
    }
	
	public function isWhitelistedV2($msisdn) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn));

        $sql = sprintf("SELECT * FROM xmp.whitelist_msisdn WHERE msisdn = '%s'", mysql_real_escape_string($msisdn));
		
		$log->write(array('level' => 'info', 'message' => "SQL : " . $sql));
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }
	
	public function insertMsisdnSubject($ms) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start msisdn : " . $ms['msisdn']));

        $sql = sprintf("INSERT INTO xmp.msisdn_subject VALUE (default,'%s','%s','%s','%s')", 
					mysql_real_escape_string($ms['msisdn']), 
					mysql_real_escape_string($ms['service']), 
					mysql_real_escape_string($ms['subject']), 
					mysql_real_escape_string($ms['s2']));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateMsisdnSubject($ms) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start msisdn : " . $ms['msisdn']));

        $sql = sprintf("UPDATE xmp.msisdn_subject SET subject = '%s', s2 = '%s' WHERE msisdn = '%s' AND service = '%s';", mysql_real_escape_string($ms['subject']), mysql_real_escape_string($ms['s2']), mysql_real_escape_string($ms['msisdn']), mysql_real_escape_string($ms['service']));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function checkMsisdnSubject($ms) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start msisdn : " . $ms['msisdn']));

        $sql = sprintf("SELECT subject, s2 FROM xmp.msisdn_subject WHERE msisdn = '%s' AND service = '%s' LIMIT 1", mysql_real_escape_string($ms['msisdn']), mysql_real_escape_string($ms['service']));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }
	
	public function updateMsisdnSubjectNotif($ms) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start msisdn : " . $ms['msisdn']));

        $sql = sprintf("UPDATE xmp.msisdn_subject SET notif = '%s' WHERE msisdn = '%s' AND service = '%s';", mysql_real_escape_string($ms['notif']), mysql_real_escape_string($ms['msisdn']), mysql_real_escape_string($ms['service']));
		
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateSubsFlag($msisdn, $flag = "FAILED") {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start msisdn : " . $msisdn));

        $sql = sprintf("UPDATE xmp.subscription SET flag = '%s'", mysql_real_escape_string($flag));
        $sql .= sprintf(" WHERE msisdn = '%s';", mysql_real_escape_string($msisdn));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function emergencyPopulateUser($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$sql = "SELECT msisdn FROM xmp.only_msisdn";
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function getRenewalDaily($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

		//SELECT s.msisdn, ms.service, ms.subject FROM subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = 'gemezz' AND s.adn = '99876' AND s.operator = 'moh3i' AND s.active = '1' AND ms.subject = 'DAILY' and CURRENT_DATE < DATE(s.active_until)
		
		//$addon = " AND s.msisdn = 6289655913961";
		//$addon = " AND s.msisdn = 62895423478415";
		//$addon = " AND s.msisdn = 628978659181";
		
		$sql = sprintf("SELECT s.time_updated, s.subscribed_until, s.msisdn, ms.service, ms.subject, ms.s2, s.priority, s.success FROM xmp.subscription s INNER JOIN xmp.msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = '%s' AND s.adn = '%s' AND s.operator = '%s' AND s.active = '1' AND ms.subject IN (%s) AND CURRENT_DATE < DATE(s.active_until) AND CURRENT_DATE < DATE(s.subscribed_until) AND DATE(s.time_created) <> CURRENT_DATE AND DATE(s.subscribed_from) <> CURRENT_DATE {$addon};", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator), strtoupper($broadcast_data->notes));

		//echo $sql;
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function getRenewalMonthly($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

		//SELECT s.msisdn, ms.service, ms.subject FROM subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = 'gemezz' AND s.adn = '99876' AND s.operator = 'moh3i' AND s.active = '1' AND ms.subject = 'DAILY' and CURRENT_DATE < DATE(s.active_until)
		
		//$addon = " AND s.msisdn = 628978659181";
		//$curdate = "CURRENT_DATE";
        //$curdate = "'2019-09-14'";

		$sql = sprintf("SELECT s.time_updated, s.subscribed_until, s.msisdn, ms.service, ms.subject, ms.s2, s.priority, s.success FROM xmp.subscription s INNER JOIN xmp.msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = '%s' AND s.adn = '%s' AND s.operator = '%s' AND s.active = '2' AND ms.subject IN (%s) AND CURRENT_DATE = DATE(s.active_until) AND CURRENT_DATE < (s.subscribed_until) {$addon};", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator), strtoupper($broadcast_data->notes));

        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function getActiveUntil($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

		//SELECT s.msisdn, ms.service, ms.subject FROM subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = 'gemezz' AND s.adn = '99876' AND s.operator = 'moh3i' AND s.active = '1' AND date(s.active_until) = current_date
		
		//$addon = " AND s.msisdn = 628978659181";
		
		$sql = sprintf("SELECT s.time_updated, s.subscribed_until, s.msisdn, ms.service, ms.subject, ms.s2, s.priority, s.success FROM xmp.subscription s INNER JOIN xmp.msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = '%s' AND s.adn = '%s' AND s.operator = '%s' AND s.active IN ('1') AND DATE(s.active_until) = CURRENT_DATE {$addon};", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator));
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function getRetryMonthly($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

		//SELECT s.msisdn, ms.service, ms.subject FROM subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = 'gemezz' AND s.adn = '99876' AND s.operator = 'moh3i' AND s.active = 1 AND ms.subject = 'MONTHLY' AND date(s.active_until) < CURRENT_DATE
		
		//$addon = " AND s.msisdn IN (6289655913961,6289678365676,6289655913547)";
		
		$sql = sprintf("SELECT s.time_updated, s.subscribed_until, s.msisdn, ms.service, ms.subject, ms.s2, s.priority, s.success FROM xmp.subscription s INNER JOIN xmp.msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = '%s' AND s.adn = '%s' AND s.operator = '%s' AND s.active = 1 AND ms.subject IN (%s) AND CURRENT_DATE < DATE(s.active_until) {$addon};", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator), strtoupper($broadcast_data->notes));
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function get180dayD($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

		//SELECT s.msisdn, ms.service, ms.subject FROM subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = 'gemezz' AND s.adn = '99876' AND s.operator = 'moh3i' AND s.active = 1 AND DATE(s.subscribed_until) = CURRENT_DATE AND ms.subject = 'DAILY'
		
		//$addon = " AND s.msisdn = 6289678365676";
		
		$sql = sprintf("SELECT s.time_updated, s.subscribed_until, s.msisdn, ms.service, ms.subject, ms.s2, s.priority, s.success FROM xmp.subscription s INNER JOIN xmp.msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = '%s' AND s.adn = '%s' AND s.operator = '%s' AND s.active IN (1,2) AND DATE(s.subscribed_until) = CURRENT_DATE AND ms.subject IN (%s) {$addon};", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator), strtoupper($broadcast_data->notes));
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function get180dayM($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

		//SELECT s.msisdn, ms.service, ms.subject FROM subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = 'gemezz' AND s.adn = '99876' AND s.operator = 'moh3i' AND s.active = 1 AND DATE(s.subscribed_until) = CURRENT_DATE AND ms.subject = 'DAILY'
		
		//$addon = " AND s.msisdn = 6289655913547";
		
		$sql = sprintf("SELECT s.time_updated, s.subscribed_until, s.msisdn, ms.service, ms.subject, ms.s2, s.priority, s.success FROM xmp.subscription s INNER JOIN xmp.msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = '%s' AND s.adn = '%s' AND s.operator = '%s' AND s.active IN (1,2) AND DATE(s.subscribed_until) = CURRENT_DATE AND ms.subject IN (%s) {$addon};", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator), strtoupper($broadcast_data->notes));
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function getConfirmMechanismByMsisdn($msisdn) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn));

		$sql = "SELECT * FROM xmp.confirm_mechanism WHERE expire_date > NOW() AND msisdn = '" .mysql_real_escape_string($msisdn). "' AND status = 0 ORDER BY id DESC LIMIT 1";
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function getConfirmMechanism() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$sql = "SELECT cm.*, ms.service, ms.subject FROM xmp.confirm_mechanism cm INNER JOIN xmp.msisdn_subject ms ON cm.msisdn = ms.msisdn WHERE DATE(cm.expire_date) > CURRENT_DATE AND cm.status = 0";
		
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function insertConfirmMechanism($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("INSERT INTO xmp.confirm_mechanism VALUE (default, '%s', '%s', 0, '%s', '%s')", mysql_real_escape_string($data['msisdn']), mysql_real_escape_string($data['datetime']), mysql_real_escape_string($data['type']), mysql_real_escape_string($data['message']));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateConfirmMechanism($msisdn) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start msisdn : " . $msisdn));

        $sql = sprintf("UPDATE xmp.confirm_mechanism SET status = 1 WHERE msisdn = '%s'", mysql_real_escape_string($msisdn));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function getConfirmMechanismWithExpiredDate() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$sql = "SELECT * FROM xmp.confirm_mechanism WHERE expire_date < NOW() AND status = 0 AND type = '180day' ORDER BY id DESC;";
		//echo $sql;
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function getVSubs($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "SELECT s.active, ms.subject FROM xmp.subscription s INNER JOIN xmp.msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'";

        if (!empty($user_data->service))
            $sql .= " AND s.service = '" . mysql_real_escape_string($user_data->service) . "'";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result [0];
        } else {
            return array();
        }
    }
	
	public function getBySubjectData($msisdn) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn));

        $sql = "SELECT * FROM xmp.msisdn_subject WHERE msisdn = '" . $msisdn . "'";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return array();
        }
    }
}
