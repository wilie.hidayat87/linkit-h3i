<?php

class model_service extends model_base {

    public function get() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = sprintf("SELECT * FROM service GROUP BY name");
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
    public function getServices($plain_service) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		$sql = sprintf("SELECT s.name FROM service s INNER JOIN mechanism m ON s.`id` = m.`service_id` WHERE m.`pattern` = '%s' LIMIT 1", 
			mysql_real_escape_string($plain_service));
			
		$log->write(array('level' => 'debug', 'message' => "Execute sql : " . $sql));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
}
