<?php

class model_tblmsgtransact extends model_base {

    public function saveMO($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

		$msgdata = (!empty($mo_data->s2) ? $mo_data->msgData." ".$mo_data->s2 : $mo_data->msgData);
		
        $sql = sprintf("
            INSERT INTO xmp.tbl_msgtransact (
                IN_REPLY_TO, MSGINDEX, MSGTIMESTAMP, ADN, MSISDN, OPERATORID, MSGDATA, MSGLASTSTATUS, 
                MSGSTATUS, CLOSEREASON, SERVICEID, MEDIA, CHANNEL, SERVICE, PARTNER, SUBJECT, PRICE, ISR 
            ) VALUES (
                %d , '%s', '%s', '%s', '%s', '%d', '%s', NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, 0
            )", mysql_real_escape_string($mo_data->inReply), mysql_real_escape_string($mo_data->msgId), mysql_real_escape_string($mo_data->incomingDate), mysql_real_escape_string($mo_data->adn), mysql_real_escape_string($mo_data->msisdn), mysql_real_escape_string($mo_data->operatorId), mysql_real_escape_string($msgdata), mysql_real_escape_string($mo_data->msgStatus), mysql_real_escape_string($mo_data->closeReason), mysql_real_escape_string($mo_data->serviceId), mysql_real_escape_string($mo_data->media), mysql_real_escape_string($mo_data->channel), mysql_real_escape_string($mo_data->service), mysql_real_escape_string($mo_data->partner), mysql_real_escape_string($mo_data->subject), mysql_real_escape_string($mo_data->price));
        $this->databaseObj->set_charset();

        $query = $this->databaseObj->query($sql);
        if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
            $config_retry = loader_config::getInstance()->getConfig('retry');

            $filename = uniqid() . ".sql";
            $path = $config_retry->bufferPathMysql . "/" . $filename;
            
            $retry = loader_data::get('retry');
            $retry->profile = $this->databaseObj->connProfile['database'];
            $retry->query = $sql;

            $buffer = buffer_file::getInstance();


            return $buffer->save($path, $retry);
        }
    }

    public function saveMT($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt_data)));

        $sql = sprintf("
            INSERT INTO xmp.tbl_msgtransact (
                IN_REPLY_TO, MSGINDEX, MSGTIMESTAMP, ADN, MSISDN, OPERATORID, MSGDATA, MSGLASTSTATUS, 
                MSGSTATUS, CLOSEREASON, SERVICEID, MEDIA, CHANNEL, SERVICE, PARTNER, SUBJECT, PRICE, ISR 
            ) VALUES (
                %d, '%s', NOW(), '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, '0'
            )", mysql_real_escape_string($mt_data->inReply), mysql_real_escape_string($mt_data->msgId), mysql_real_escape_string($mt_data->adn), mysql_real_escape_string($mt_data->msisdn), mysql_real_escape_string($mt_data->operatorId), mysql_real_escape_string($mt_data->msgData), mysql_real_escape_string($mt_data->msgLastStatus), mysql_real_escape_string($mt_data->msgStatus), mysql_real_escape_string($mt_data->closeReason), mysql_real_escape_string($mt_data->serviceId), mysql_real_escape_string($mt_data->media), mysql_real_escape_string($mt_data->channel), mysql_real_escape_string($mt_data->service), mysql_real_escape_string($mt_data->partner), mysql_real_escape_string($mt_data->subject), mysql_real_escape_string($mt_data->price));

        //$log->write(array('level' => 'debug', 'message' => "SQL : " . $sql));

        $this->databaseObj->set_charset();
        $query = $this->databaseObj->query($sql);
        if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
            $config_retry = loader_config::getInstance()->getConfig('retry');

            $filename = uniqid() . ".sql";
            $path = $config_retry->bufferPathMysql . "/" . $filename;
            
            $retry = loader_data::get('retry');
            $retry->profile = $this->databaseObj->connProfile['database'];
            $retry->query = $sql;

            $buffer = buffer_file::getInstance();

            return $buffer->save($path, $retry);
        }
    }

    public function saveBulkMT($valuesData, $init) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if($init == "dev")
            $tbl = "xmp.tbl_msgtransact_bulk";
        else if($init == "prod")
            $tbl = "xmp.tbl_msgtransact";

        $sql = "INSERT INTO {$tbl} (
                    ID, IN_REPLY_TO, MSGINDEX, MSGTIMESTAMP, ADN, MSISDN, OPERATORID, MSGDATA, MSGLASTSTATUS, 
                    MSGSTATUS, CLOSEREASON, SERVICEID, MEDIA, CHANNEL, SERVICE, PARTNER, SUBJECT, PRICE, ISR 
                ) VALUES {$valuesData}";

        $log->write(array('level' => 'info', 'message' => "SQL Bulk : " . substr($sql, 0, 200)));

        $this->databaseObj->set_charset();
        $query = $this->databaseObj->query($sql);
        
        return true;
    }

    public function setStatus($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt_data)));
	$serviceid = (!empty($mt_data->serviceId)) ? "AND serviceid ='". $mt_data->serviceId ."'" : '';
	//error_log(date('Ymd His')." ".print_r($mt_data,1)." ",3,"/tmp/mt");
        $sql = sprintf("UPDATE 
		xmp.tbl_msgtransact 
		SET msgstatus = '%s', 
		closereason = '%s' 
		WHERE msgindex = '%s' 
		AND adn = '%s' 
		AND msisdn = '%s' 
		%s ORDER BY id desc 
		LIMIT 1", mysql_real_escape_string($mt_data->status), mysql_real_escape_string($mt_data->closeReason), mysql_real_escape_string($mt_data->msgId), mysql_real_escape_string($mt_data->adn), mysql_real_escape_string($mt_data->msisdn),$serviceid);
        $this->databaseObj->set_charset();
        $this->databaseObj->query($sql);
        return $this->databaseObj->numRows;
    }

    public function get($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt_data)));

        $sql = sprintf("SELECT * 
		FROM xmp.tbl_msgtransact 
		WHERE MSGINDEX = '%s' 
		AND adn = '%s'
		AND msisdn = '%s' 
		LIMIT 1", mysql_real_escape_string($mt_data->msgId), mysql_real_escape_string($mt_data->adn), mysql_real_escape_string($mt_data->msisdn));
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getDRTransact($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt_data)));

        $sql = sprintf("SELECT a.*, b.id chargingId, b.sender_type 
		FROM xmp.tbl_msgtransact a
                INNER JOIN charging b ON a.serviceid = b.charging_id
		WHERE a.msgindex = '%s' 
		AND a.adn = '%s'
		AND a.msisdn = '%s' 
		ORDER BY a.id DESC LIMIT 1", mysql_real_escape_string($mt_data->msgId), mysql_real_escape_string($mt_data->adn), mysql_real_escape_string($mt_data->msisdn));
		echo $sql;
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSummary(summarizer_data $summarizer) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($summarizer)));

        $config_main = loader_config::getInstance()->getConfig('main');
        $operator_name = $config_main->operator;
        $model_operator = loader_model::getInstance()->load('operator', 'connDatabase1');
        $operator = $model_operator->getOperatorId($operator_name);

        $sql = sprintf("
                SELECT 
                        DATE(msgtimestamp) AS sumdate,
                        adn,
                        operatorid,
                        serviceid,
                        price,
                        partner,
                        service,
                        subject,
                        msgstatus,
                        count(*) as total
                FROM
                        %s
                WHERE
                        msgtimestamp between '%s 00:00:00' AND '%s 23:59:59' AND operatorid='%s'
                GROUP BY
                        DATE(msgtimestamp), subject, operatorid, partner, service, price, msgstatus
                ORDER BY
                        DATE(msgtimestamp), subject, operatorid, partner, service, price, msgstatus", mysql_real_escape_string($summarizer->tableFrom), mysql_real_escape_string($summarizer->date), mysql_real_escape_string($summarizer->date), mysql_real_escape_string($operator));
	//exit($sql);	
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getByMsgId($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt_data)));

        $sql = sprintf("SELECT * 
		FROM tbl_msgtransact 
		WHERE MSGINDEX = '%s' 
		LIMIT 1", mysql_real_escape_string($mt_data->msgId));
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result[0];
        } else {
            return false;
        }
    }
	        public function getRedeemPoint($msgdata,$redeem_date,$service) {
                //$log = manager_logging::getInstance();
                //$log->write(array('level' => 'debug', 'message' => "Start Redem" . serialize($msgdata)));

                $sql = sprintf("SELECT *
                        FROM tbl_msgtransact
                        WHERE MSGDATA IN ('%s') AND msgtimestamp >= '%s 00:00:00' AND msgtimestamp <= '%s 23:59:59' AND service='%s' ", mysql_real_escape_string($msgdata),  mysql_real_escape_string($redeem_date), mysql_real_escape_string($redeem_date), mysql_real_escape_string($service));

                //exit($sql);
                $result = $this->databaseObj->fetch($sql);
                //var_dump($result);
                        if (count($result) > 0) {
                            return $result;
                        } else {
                            return false;
                        }

        }

	public function allowSOASubs($mt) {
		$log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt)));
		$sql = sprintf("SELECT COUNT(msisdn) m FROM subscription WHERE msisdn = '%s' AND service = '%s' AND active in (0,1,2) GROUP BY msisdn ORDER BY id DESC LIMIT 1;", mysql_real_escape_string($mt->msisdn), mysql_real_escape_string($mt->service));
		
		$result = $this->databaseObj->fetch($sql);
		if ($result[0]['m'] < 2) {
			return true;
		} else {
			return false;
		}
	}
	
	public function saveTransact($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . json_encode($data)));

        $sql = sprintf("
            INSERT INTO xmp.tbl_msgtransact (
                IN_REPLY_TO, MSGINDEX, MSGTIMESTAMP, ADN, MSISDN, OPERATORID, MSGDATA, MSGLASTSTATUS, 
                MSGSTATUS, CLOSEREASON, SERVICEID, MEDIA, CHANNEL, SERVICE, PARTNER, SUBJECT, PRICE, ISR 
            ) VALUES (
                %d , '%s', '%s', '%s', '%s', '%d', '%s', NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, 0
            )", 1, mysql_real_escape_string($data['sessionID']), mysql_real_escape_string($data['msgtimestamp']), '', mysql_real_escape_string($data['msisdn']), mysql_real_escape_string($data['operatorId']), mysql_real_escape_string($data['msgData']), mysql_real_escape_string($data['msgStatus']), mysql_real_escape_string($data['closeReason']), '', '1', mysql_real_escape_string($data['channel']), mysql_real_escape_string($data['service']), mysql_real_escape_string($data['partner']), mysql_real_escape_string($data['subject']), mysql_real_escape_string($data['price']));
			
        $this->databaseObj->set_charset();

        $query = $this->databaseObj->query($sql);
        return $this->databaseObj->last_insert_id();
    }
}
