<?php class model_pixel extends model_base {
    public function setPixel($pixel,$operator,$service = 'none', $campurl = 'none') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start Set Pixel to DB"));
        
	$find_str = strpos($pixel,'?');
	if($find_str !== false){
	   $pixel = substr($pixel,0,strpos($pixel,'?'));
	}
	if(substr($pixel, 0, 3) == "ADT") {
	   $data['partner']='adsterdam';
	}
	else if(substr($pixel, 0, 3) == "102") {
	    $partner='pocketmedia';
	}
	else{
	if(strlen($pixel) == 30) {
	   $partner='kissads';
	}else if(strlen($pixel) == 55) {
	   $partner='kimia';
	}else if(strlen($pixel) == 23) {
	   $partner='mobusi';
	}else if(strpos(strtolower($pixel),'573360aa5d9d0')) {
              $partner='mobipium';
	}else {
	   if(preg_match('/^cd/',strtolower($pixel))) {
	      $partner='cd';
	      $pixel = str_replace('cd','',$pixel);
	   } else {
	      $partner='blank';
	   }
	}
	}
	$query = false;
	if($partner!=='blank'){
	//$query = false;
        $sql = sprintf("INSERT INTO pixel_storage (pixel, operator, partner, service, campurl) 
values('%s','%s','%s','%s','%s')",mysql_real_escape_string($pixel),mysql_real_escape_string($operator),mysql_real_escape_string($partner),mysql_real_escape_string($service),mysql_real_escape_string($campurl));
	$query = $this->databaseObj->query($sql);
	}
        if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
	    return false;
	}
    }
    public function getPixel($operator,$partner) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start get Pixel in DB"));
	
        $sql = sprintf("SELECT pixel FROM pixel_storage WHERE operator = '%s' AND partner = '%s' AND date_time > NOW() - INTERVAL 3 HOUR AND is_used = 0 ORDER 
BY RAND() LIMIT 1",mysql_real_escape_string($operator),mysql_real_escape_string($partner));
	
	$query = $this->databaseObj->fetch($sql);
	
        if (count($query)>0) {
            $this->updatePixel($query[0]['pixel']);
            return $query[0]['pixel'];
        } else {
  	   return false;
	}
    }
    public function showPixel($id){
	$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start show Pixel in DB"));
        $sql = "SELECT pixel FROM pixel_storage WHERE id = ".$id;
        $query = $this->databaseObj->fetch($sql);
        if (count($query)>0) {
            $this->updatePixel($query[0]['pixel']);
            return $query[0]['pixel'];
        } else {
           return false;
        }
    }
	
    public function updatePixel($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start Update Pixel to DB"));
        $sql = sprintf("UPDATE pixel_storage set is_used = 1 where pixel = '%s'",mysql_real_escape_string($data));
	$query = $this->databaseObj->query($sql);
        if ($query) {
            return true;
        } else {
 	    return false;
	}
    }
}
?>
