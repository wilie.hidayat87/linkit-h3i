<?php

class model_mechanism extends model_base {

    public function readAll($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $sql = sprintf("
            SELECT  
                m.id, 
                m.pattern,  
                m.handler,
                s.name 
            FROM  
                xmp.mechanism m 
            INNER JOIN  
                xmp.service s ON  m.service_id = s.id 
            WHERE 
                m.status <> '0' 
            AND 
                s.adn = '%s' 
            AND 
            	m.operator_id = '%s'
            ORDER BY m.pattern DESC;", mysql_real_escape_string($mo_data->adn), mysql_real_escape_string($mo_data->operatorId));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function readAllModule($patternId) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $patternId));

        $sql = sprintf("
            SELECT 
                r.*, 
                m.handler, 
                m.name AS moduleName, 
                c.message_type,
				c.gross,
				c.netto
            FROM 
                xmp.reply r
            INNER JOIN 
                xmp.module m ON r.module_id = m.id
            INNER JOIN 
                xmp.charging c ON r.charging_id = c.id
            WHERE 
                m.status = '1' 
            AND 
                r.mechanism_id = '%s'", mysql_real_escape_string($patternId));

        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function getMechanism($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $sql = sprintf("SELECT m.pattern,s.name FROM xmp.mechanism m INNER JOIN xmp.service s ON m.service_id = s.id WHERE m.pattern = '%s' LIMIT 1;", mysql_real_escape_string(strtolower($mo_data->rawSMS)));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }

	public function getMoRevByRaw($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $sql = sprintf("SELECT morev,modrev FROM xmp.mechanism WHERE pattern = '%s' LIMIT 1;", mysql_real_escape_string(strtolower($mo_data->rawSMS)));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }
	
	public function updateMoRevByRaw($mo_data) {
        $log = manager_logging::getInstance();

        $rawSMS = mysql_real_escape_string(strtolower($mo_data->rawSMS));

        $log->write(array('level' => 'info', 'message' => "Mechanism Update MO : ".$rawSMS));

        $sql = sprintf("UPDATE xmp.mechanism SET morev = ((1 * morev) + charging) WHERE pattern = '%s';", $rawSMS);
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function getDpRevByRaw($rawSMS) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $sql = sprintf("SELECT dprev,dpdrev FROM xmp.mechanism WHERE pattern = '%s' LIMIT 1;", mysql_real_escape_string(strtolower($rawSMS)));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }
	
	public function updateDpRevByRaw($msisdn, $subject, $status, $rawSMS) {
        $log = manager_logging::getInstance();

        $rawSMS = mysql_real_escape_string(strtolower($rawSMS));
        
        $log->write(array('level' => 'info', 'message' => "Mechanism Update DP : ".$rawSMS.", [MSISDN] : ".$msisdn.", [SUBJECT] : ".$subject.", [STATUS] : " . $status));

        $sql = sprintf("UPDATE xmp.mechanism SET dprev = ((1 * dprev) + charging) WHERE pattern = '%s';", $rawSMS);
		
		$log->write(array('level' => 'debug', 'message' => "Mechanism Update DP : " . $sql));
		
        $this->databaseObj->query($sql);
        return true;
    }
}
