<?php

class model_retry extends model_base {

    public function save($query) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $query));

        return $this->databaseObj->query($query);
    }

	public function saveBuffer($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

		$sql = "INSERT INTO ".$data->table." (id,message,slot) VALUE ('','".$data->message."',".$data->slot.")";
		
        $this->databaseObj->query($sql);
        return $this->databaseObj->last_insert_id();
    }
	
	public function getBuffer($data)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));
		
		$sql = "SELECT * FROM ".$data->table." WHERE slot = ".$data->slot." ORDER BY id ASC LIMIT ".$data->limit.";";
        $result = $this->databaseObj->fetch($sql);
		
        if (count($result) > 0) {
            return $result;
        } else {
            return array();
        }
	}
	
	public function deleteBuffer($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

		$sql = "DELETE FROM ".$data->table." WHERE id = ".$data->id.";";
		
        $this->databaseObj->query($sql);
        return true;
    }
}