<?php

class model_modelay extends model_base {

    public function add(mo_delay_data $mo_delay) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_delay)));

        $sql = sprintf("
                    INSERT INTO xmp.mo_delay (
                            id, reg_type, subject, subject2, service, adn, msisdn, created, expired, status, closereason, priority, thread_id, obj
                    ) VALUES (
                            DEFAULT, '%s', '%s', '%s', '%s', '%s', '%s', NOW(), DATE_ADD(NOW(),INTERVAL %d SECOND), %d, '%s', %d, %d, '%s'
                    )", mysql_real_escape_string($mo_delay->reg_type), mysql_real_escape_string($mo_delay->subject), mysql_real_escape_string($mo_delay->subject2), mysql_real_escape_string($mo_delay->service), mysql_real_escape_string($mo_delay->adn), mysql_real_escape_string($mo_delay->msisdn), mysql_real_escape_string($mo_delay->date_expired), mysql_real_escape_string($mo_delay->status), mysql_real_escape_string($mo_delay->closereason), mysql_real_escape_string($mo_delay->priority), mysql_real_escape_string($mo_delay->thread_id), mysql_real_escape_string(trim($mo_delay->obj)));
        $query = $this->databaseObj->query($sql);
        return $this->databaseObj->last_insert_id();
    }

    public function update($params) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($params)));

        $sql = sprintf("UPDATE xmp.mo_delay SET status = '%s' WHERE id = '%s'", mysql_real_escape_string($params['status']), mysql_real_escape_string($params['id']));
        return $this->databaseObj->query($sql);
    }

    public function updateRetry($params) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_delay)));

        $where = "";
        if(isset($params['subject2']) && !empty($params['subject2']))
        {
            $where .= " AND subject2 = '".mysql_real_escape_string($params['subject2'])."'";
        }

        $sql = sprintf("UPDATE xmp.mo_delay SET status = %d, closereason = '%s', priority = %d WHERE DATE(created) = CURRENT_DATE AND msisdn = '%s' AND subject = '%s' {$where} ORDER BY id DESC LIMIT 1", mysql_real_escape_string($params['status']), mysql_real_escape_string($params['closereason']), mysql_real_escape_string($params['priority']), mysql_real_escape_string($params['msisdn']), mysql_real_escape_string($params['subject']));
        return $this->databaseObj->query($sql);
    }

    public function getMOBuffer(mo_delay_data $mo_delay) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_delay)));

        //$sql = sprintf("SELECT * FROM xmp.mo_delay WHERE DATE(created) = CURRENT_DATE AND status = %d AND thread_id = %d", mysql_real_escape_string($mo_delay->status), mysql_real_escape_string($mo_delay->thread_id));
        $sql = sprintf("SELECT * FROM xmp.mo_delay WHERE status = %d AND thread_id = %d", mysql_real_escape_string($mo_delay->status), mysql_real_escape_string($mo_delay->thread_id));

        if(isset($mo_delay->priority))
        {
            $sql .= sprintf(" AND priority = %d", mysql_real_escape_string($mo_delay->priority));
        }

        if(isset($mo_delay->reg_type))
        {
            $sql .= sprintf(" AND reg_type = '%s'", mysql_real_escape_string($mo_delay->reg_type));
        }

        if(isset($mo_delay->closereason))
        {
            $sql .= sprintf(" AND closereason = '%s'", mysql_real_escape_string($mo_delay->closereason));
        }
        
        $sql .= sprintf(" ORDER BY priority ASC LIMIT %d", mysql_real_escape_string($mo_delay->limit));
        
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function manualUnsub($params) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($params)));

        $sql = sprintf("UPDATE xmp.subscription SET active = %d WHERE msisdn = '%s'", mysql_real_escape_string($params['active']), mysql_real_escape_string($params['msisdn']));

        $log->write(array('level' => 'debug', 'message' => "Start : " . mysql_error()));

        $this->databaseObj->query($sql);

        return true; 
    }

}
