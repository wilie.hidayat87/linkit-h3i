<?php

class model_pushbuffer extends model_base {

    public function save(model_data_pushbuffer $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("
                INSERT INTO
                    dbpush.{$data->tablename}(
                        pid,
                        src,
                        dest,
                        oprid,
                        service,
                        subject,
                        message,
                        price,
                        stat,
                        created,
                        tid,
                        obj,
                        thread_id,
                        priority
                    )
                VALUES(
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        NOW(),
                        '%s',
                        '%s',
                         %d,
                         %d
                )", mysql_real_escape_string($data->pid), mysql_real_escape_string($data->src), mysql_real_escape_string($data->dest), mysql_real_escape_string($data->oprid), mysql_real_escape_string($data->service), mysql_real_escape_string($data->subject), mysql_real_escape_string($data->message), mysql_real_escape_string($data->price), mysql_real_escape_string($data->stat), mysql_real_escape_string($data->tid), mysql_real_escape_string(trim($data->obj)), mysql_real_escape_string($data->thread_id), mysql_real_escape_string($data->priority));

        $query = $this->databaseObj->query($sql);
        return $this->databaseObj->last_insert_id();
    }

    public function update(model_data_pushbuffer $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE dbpush.{$data->tablename} SET stat = '%s' where id = '%s'", $data->stat, $data->id);
        return $this->databaseObj->query($sql);
    }

    public function execPushbuffer($pid, $service, $operatorId) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : [" . $pid . "][" . $service . "]"));

        $sql = sprintf("SELECT id,obj FROM dbpush.push_buffer WHERE pid = '%s' AND service = '%s' AND oprid = '%s' AND stat = 'ON_QUEUE' limit 20000", mysql_real_escape_string($pid), mysql_real_escape_string($service), mysql_real_escape_string($operatorId));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function execPushbufferWithThread($params = array()) 
    {
        //$pid, $service, $operatorId, $subject, $thread_id, $limit=1000

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : [" . serialize($params) . "]"));

        $now = time();

		if($now >= $params['startPriority'] && $now <= $params['endPriority'])
		{
            $sql = sprintf(
                "SELECT t.* FROM (SELECT * FROM dbpush.".$params['tablename']." 
                WHERE 
                DATE(created) = CURRENT_DATE AND 
                pid = '%s' AND 
                service = '%s' AND 
                oprid = '%s' AND 
                stat = 'ON_QUEUE' AND
                SPLIT_STRING(LOWER(subject),';',4) = '%s' AND
                thread_id = %d AND 
                priority IN (".mysql_real_escape_string($params['priority_flag']).") 
                LIMIT %d) AS t ORDER BY t.priority DESC 
                ", 
                mysql_real_escape_string($params['pid']), 
                mysql_real_escape_string($params['service']), 
                mysql_real_escape_string($params['operatorId']),
                mysql_real_escape_string($params['subject']),
                mysql_real_escape_string($params['thread_id']),
                mysql_real_escape_string($params['limit'])	        
            );
        }
        else
        {
            $sql = sprintf(
                "SELECT * FROM dbpush.".$params['tablename']." 
                WHERE 
                DATE(created) = CURRENT_DATE AND 
                pid = '%s' AND 
                service = '%s' AND 
                oprid = '%s' AND 
                stat = 'ON_QUEUE' AND 
                SPLIT_STRING(LOWER(subject),';',4) = '%s' AND 
                thread_id = %d AND 
                priority IN (".mysql_real_escape_string($params['priority_flag']).") 
                LIMIT %d  
                ", 
                mysql_real_escape_string($params['pid']), 
                mysql_real_escape_string($params['service']), 
                mysql_real_escape_string($params['operatorId']),
                mysql_real_escape_string($params['subject']),
                mysql_real_escape_string($params['thread_id']),
                mysql_real_escape_string($params['limit'])	        
            );
        }

		echo $sql;
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function execPushbufferWithThreadMicro($params = array()) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($params)));

        $now = time();

		if($now >= $params['startPriority'] && $now <= $params['endPriority'])
		{
            $sql = sprintf(
                "SELECT t.* FROM (SELECT * FROM dbpush.".$params['tablename']." 
                WHERE 
                DATE(created) = CURRENT_DATE AND 
                pid = '%s' AND 
                service = '%s' AND 
                oprid = '%s' AND 
                stat = 'ON_QUEUE' AND
                subject = '%s' AND 
                thread_id = %d AND 
                priority IN (".mysql_real_escape_string($params['priority_flag']).") 
                LIMIT %d) AS t ORDER BY t.priority DESC   
                ", 
                mysql_real_escape_string($params['pid']), 
                mysql_real_escape_string($params['service']), 
                mysql_real_escape_string($params['operatorId']),
                mysql_real_escape_string($params['full_subject']),
                mysql_real_escape_string($params['thread_id']),
                mysql_real_escape_string($params['limit'])	        
            );
        }
        else
        {
            $sql = sprintf(
                "SELECT * FROM dbpush.".$params['tablename']." 
                WHERE 
                DATE(created) = CURRENT_DATE AND 
                pid = '%s' AND 
                service = '%s' AND 
                oprid = '%s' AND 
                stat = 'ON_QUEUE' AND 
                subject = '%s' AND 
                thread_id = %d AND 
                priority IN (".mysql_real_escape_string($params['priority_flag']).") 
                LIMIT %d  
                ", 
                mysql_real_escape_string($params['pid']), 
                mysql_real_escape_string($params['service']), 
                mysql_real_escape_string($params['operatorId']),
                mysql_real_escape_string($params['full_subject']),
                mysql_real_escape_string($params['thread_id']),
                mysql_real_escape_string($params['limit'])	        
            );
        }
		echo $sql;
        $log->write(array('level' => 'info', 'message' => "SQL : [" . $sql . "]"));

		//echo $sql;
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
        
    public function clearBuffer($deleteList) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($deleteList)));

        $deleteList = implode(",", $deleteList);
        $sql = sprintf("DELETE FROM dbpush.push_buffer where id in(%s)", $deleteList);
        return $this->databaseObj->query($sql);
    }
	
	public function checkOnQueueBuffer() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = "SELECT id FROM dbpush.push_buffer WHERE stat = 'ON_QUEUE' AND DATE(created) = CURRENT_DATE LIMIT 1";
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return false;
        } else {
            return true;
        }
    }
	
	public function updateToOnQueueBuffer($msisdn) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn));

        $sql = "UPDATE dbpush.push_buffer SET stat = 'ON_QUEUE' WHERE subject LIKE '%DAILYPUSH%' AND DATE(created) = CURRENT_DATE AND dest = '".mysql_real_escape_string($msisdn)."'";
        return $this->databaseObj->query($sql);
    }
	
	public function isPushRetrySchedule($date = "", $status = "") {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$w = '';
		if(!empty($date))
			$w .= " AND created = '{$date}'";
		if(!empty($status))
			$w .= " AND status = '{$status}'";
		
        $sql = "SELECT * FROM dbpush.push_retry_schedule WHERE 1=1 ".$w." LIMIT 1";
		//echo $sql;
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return true;
        } else {
            return false;
        }
    }
	
	public function insertRetrySchedule($date, $status) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = "INSERT dbpush.push_retry_schedule VALUE ('".mysql_real_escape_string($date)."', '".mysql_real_escape_string($status)."')";
        return $this->databaseObj->query($sql);
    }
	
	public function updateRetrySchedule($status) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = "UPDATE dbpush.push_retry_schedule SET status = '".mysql_real_escape_string($status)."' WHERE created = CURRENT_DATE;";
        return $this->databaseObj->query($sql);
    }
}
