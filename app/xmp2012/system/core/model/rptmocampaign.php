<?php

class model_rptmocampaign extends model_base {

    public function getRptMOCampaign($data) {
		
		$sql = sprintf("SELECT * FROM rpt_mo_campaign WHERE date_send = '%s' AND publisher = '%s' AND type = '%s' AND params = '%s'", mysql_real_escape_string($data['date_send']), mysql_real_escape_string($data['publisher']), mysql_real_escape_string($data['type']), mysql_real_escape_string($data['params']));
	
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return array();
		}	
	}
	
	public function insertRptMOCampaign($data){

        $sql = sprintf("INSERT INTO rpt_mo_campaign VALUE ('','%s','%s','%s','%s','%s')", mysql_real_escape_string($data['date_send']), mysql_real_escape_string($data['publisher']), mysql_real_escape_string($data['type']), mysql_real_escape_string($data['total']), mysql_real_escape_string($data['params']));
		
		//echo $sql;die;
		
		$query = $this->databaseObj->query($sql);
        if ($query) {
            return true;
        } else {
			return false;
		}
	}
	
	public function updateRptMOCampaign($data){

		$date_send = mysql_real_escape_string($data['date_send']);
		$publisher = mysql_real_escape_string($data['publisher']);
		$type = mysql_real_escape_string($data['type']);
		$total = mysql_real_escape_string($data['total']);
		$total = (int)$total;
		$params = mysql_real_escape_string($data['params']);
		
		$uptMO = "total = ".$total;
		if($publisher == 'kims')
			$uptMO = "total = total+".$total;
			
        $sql = "UPDATE rpt_mo_campaign SET $uptMO WHERE date_send = '$date_send' AND publisher = '$publisher' AND type = '$type' AND params = '$params'";
		
		//echo $sql."\n";
		
		$query = $this->databaseObj->query($sql);
        if ($query) {
            return true;
        } else {
			return false;
		}
	}
	
	public function getRptNewMOCampaign($data) {
		
		$tableLP = mysql_real_escape_string($data['tableLP']);
		$sDate = mysql_real_escape_string($data['sDate']);
		$eDate = mysql_real_escape_string($data['eDate']);
		$type = mysql_real_escape_string($data['type']);
		
		$sql = "SELECT mo.date_send AS Date, mo.publisher AS Publisher, l.Total AS Landing, mo.total AS Total, ((mo.total/l.Total)*100) AS CR FROM (SELECT date_send, CASE WHEN LENGTH(hash) = 23 THEN 'mobusi' WHEN LENGTH(hash) = 55 THEN 'kimia' ELSE hash END AS publisher, COUNT(id) AS Total FROM $tableLP WHERE date_send BETWEEN '$sDate' AND '$eDate' AND landing = 1 GROUP BY publisher,date_send) AS l INNER JOIN (SELECT * FROM rpt_mo_campaign WHERE date_send BETWEEN '$sDate' AND '$eDate' AND type = '$type') AS mo ON l.publisher = mo.publisher AND l.date_send = mo.date_send ORDER BY mo.date_send ASC;";
		
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return array();
		}	
	}
	
	public function getNegatifStatusBy7PastDay($data) {
		
		$fTable = $data['ft'];
		$sTable = $data['st'];
		$interval = $data['interval'];
		$having = $data['having'];
		$suspend = mysql_real_escape_string($data['suspend']);
		$unknown = mysql_real_escape_string($data['unknown']);
		
		// $sql = "SELECT msisdn, COUNT(id) AS total FROM $table WHERE msgtimestamp BETWEEN DATE_ADD(now(),INTERVAL ".$interval.") AND now() AND closereason IN ('".$suspend."','".$unknown."') GROUP BY msisdn;";
		$sql = "SELECT ft.msisdn, (ft.total+st.total) as total from (SELECT msisdn, COUNT(id) AS total FROM $fTable WHERE msgtimestamp BETWEEN DATE_ADD(now(),INTERVAL $interval) AND now() AND closereason IN ('Account is suspended:SUSP','Unkown subscriber:UNKS') GROUP BY msisdn) as ft INNER JOIN (SELECT msisdn, COUNT(id) AS total FROM $sTable WHERE msgtimestamp BETWEEN DATE_ADD(now(),INTERVAL $interval) AND now() AND closereason IN ('Account is suspended:SUSP','Unkown subscriber:UNKS') GROUP BY msisdn) as st ON ft.msisdn = st.msisdn having total > $having;";
		//echo $sql;die;
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return array();
		}	
	}
	
	public function updateNegatifStatus($data){
		
		$msisdn = mysql_real_escape_string($data['msisdn']);
		$status = mysql_real_escape_string($data['status']);
		
		$sql = "UPDATE subscriber SET status = '".$status."', time_updated = '".date("Y-m-d H:i:s")."' WHERE msisdn = '".$msisdn."';";
		
		$this->databaseObj->query($sql);
		return true;
	}
	
	public function insertNegatifSubscriber($data){
		
		$msisdn = mysql_real_escape_string($data['msisdn']);
		
		$sql = "INSERT INTO negatif_subscriber value ('".$msisdn."','".date("Y-m-d H:i:s")."');";
		
		$this->databaseObj->query($sql);
		return true;
	}
	
	public function getLPSet() {
		
		$sql = sprintf("SELECT * FROM lp_set");
	
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return array();
		}	
	}
}
