<?php

class model_module extends model_base {

    public function readMechanism($id) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $id));

        return 'char';
    }
	
	public function getAnomalyMT() {
		$log = manager_logging::getInstance();
		
		$sql = "SELECT TRIM(REPLACE(REPLACE(REPLACE(msgstatus,'\t',''),'\n',''),'\r','')) AS status, closereason, COUNT(id) AS total FROM tbl_msgtransact WHERE subject = 'MT;PUSH;SMS' AND DATE(MSGTIMESTAMP) = CURRENT_DATE() GROUP BY status,closereason ORDER BY status ASC;";
		
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return false;
		}	
	}

	public function getAnomalyMTByMSISDN($status) {
		$log = manager_logging::getInstance();
		
		$sql = "SELECT msisdn, TRIM(REPLACE(REPLACE(REPLACE(msgstatus,'\t',''),'\n',''),'\r','')) AS status, closereason FROM tbl_msgtransact WHERE subject = 'MT;PUSH;SMS' AND DATE(MSGTIMESTAMP) = CURRENT_DATE() AND TRIM(REPLACE(REPLACE(REPLACE(msgstatus,'\t',''),'\n',''),'\r','')) = '".$status."' ORDER BY status DESC LIMIT 1;";
		
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return false;
		}	
	}
}