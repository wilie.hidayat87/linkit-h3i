<?php

class model_redeemhistory extends model_base {

        public function insert($msisdn,$redeem_date) {

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn));

        $sql = sprintf("INSERT INTO redeem_point_history(msisdn,redeem_date) VALUES('%s','%s')", mysql_real_escape_string($msisdn),mysql_real_escape_string($redeem_date));
        $this->databaseObj->query($sql);
        return true;
    }

        public function insertRedeemHistory($msisdn,$redeem_date,$redeemPoint) {

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn));

        $sql = sprintf("INSERT INTO redeem_point_history(msisdn,redeem_date,point) VALUES('%s','%s','%s')", mysql_real_escape_string($msisdn),mysql_real_escape_string($redeem_date),mysql_real_escape_string($redeemPoint));
        $this->databaseObj->query($sql);
        return true;
    }
}

?>

