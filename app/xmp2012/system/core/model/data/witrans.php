<?php
class model_data_witrans {
    public $id;
    public $in_reply_to;
    public $msgindex;
    public $msgtimestamp;
    public $adn;
    public $msisdn;
    public $operatorid;
    public $msgdata;
    public $msglaststatus;
    public $msgstatus;
    public $closereason;
    public $serviceid;
    public $media;
    public $channel;
    public $service;
    public $partner;
    public $subject;
    public $price;
    public $isr;
}
