<?php

class model_xlsdp extends model_base {
	
	public function get_service_name($serviceID) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "XL SDP Get Service Name : " . serialize ( $serviceID ) ) );
		
		$sql = sprintf(
			"SELECT name
			 FROM service 
			 WHERE serviceID = '%s'", 
			mysql_real_escape_string($serviceID)
		);
		
		$data = $this->databaseObj->fetch($sql);
		if (count($data) > 0) return $data[0]; else return false;		
	}
	
	public function get_operator_id($name) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "XL SDP Get Operator ID : " . serialize ( $serviceID ) ) );
		
		$sql = sprintf(
			"SELECT id
			 FROM operator 
			 WHERE name = '%s'", 
			mysql_real_escape_string($name)
		);
		
		$data = $this->databaseObj->fetch($sql);
		if (count($data) > 0) return $data[0]; else return false;		
	}
	
	public function save_request($data) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "XL SDP Save Request : " . serialize ( $data ) ) );
		
		$sql = sprintf(
			"INSERT INTO `xlsdp_reg_log` (
				`msisdn`,
				`trxid`, 
				`service`, 
				`operator`, 
				`action`,
				`act_desc`,
  				`channel`,
			    `created`,
			    `modified`				
			) VALUES (
				'%s',
				'%s',
				'%s',
				'%s',
				%d,
				'%s',
  				'%s',
			    now(),
				now()
			 )",
			mysql_real_escape_string($data->msisdn),
			mysql_real_escape_string($data->trxid),
			mysql_real_escape_string($data->service),
			mysql_real_escape_string($data->operator),
			mysql_real_escape_string($data->action),
			mysql_real_escape_string($data->act_desc),
			mysql_real_escape_string($data->channel)
		);
		
		//$log->write ( array ('level' => 'debug', 'message' => "SQL : " . $sql));
			
		$query = $this->databaseObj->query($sql);
		return $this->databaseObj->last_insert_id();		
		
	}
	

	
}