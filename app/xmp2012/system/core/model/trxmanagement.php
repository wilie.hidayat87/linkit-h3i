<?php

class model_trxmanagement extends model_base {

    public function getTrxManagement($data) {
		$log = manager_logging::getInstance();
		
		$sql = sprintf("SELECT * FROM xmp.trxmanagement WHERE setting_name = '%s'", mysql_real_escape_string($data['setting_name']));
		
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return false;
		}	
	}
	
	public function updateTrxManagement($data){
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = sprintf("update xmp.trxmanagement set setting_value = '%s' WHERE setting_name = '%s'",mysql_real_escape_string($data['setting_value']),mysql_real_escape_string($data['setting_name']));
		
		$query = $this->databaseObj->query($sql);
        if ($query) {
            return true;
        } else {
			return false;
		}
	}
	
	public function cleanPushBuffer($data){
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $sql = sprintf("truncate table xmp.push_buffer_recurring;");
		
		$query = $this->databaseObj->query($sql);
        if ($query) {
            return true;
        } else {
			return false;
		}
	}
}
