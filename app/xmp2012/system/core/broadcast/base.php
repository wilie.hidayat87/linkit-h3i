<?php

class broadcast_base implements broadcast_interface {

    /**
     * @param $broadcast_data
     */
    public function push($broadcast_data) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $main_config = loader_config::getInstance ()->getConfig('main');
        $loader_queue = loader_queue::getInstance ();

        $operator_name = $main_config->operator;
        $class_name = $operator_name . "_broadcast_" . $broadcast_data->handlerFile;
        $class_default = "default_broadcast_" . $broadcast_data->handlerFile;
        if (class_exists($class_name)) {
            $broadcast = new $class_name ( );
        } elseif (class_exists($class_default)) {
            $log->write(array('level' => 'info', 'message' => "Class Doesn't Exist : " . $class_name));
            $broadcast = new $class_default ( );
        } else {
            $log->write(array('level' => 'error', 'message' => "Class Doesn't Exist : " . $class_name . " & " . $class_default));
            return false;
        }

        $users_data = $this->populateUser($broadcast_data);

        $content_manager = content_manager::getInstance ()->getBroadcastContent($broadcast_data);
        $broadcast_content = $content_manager->getBroadcastContent($broadcast_data);

        $adn = $main_config->adn;
        $operator_name = $main_config->operator;
        $model_operator = loader_model::getInstance ()->load('operator', 'connDatabase1');

        // create object mt data for get charging
        $mt_data = loader_data::get('mt');
        $mt_data->service = $broadcast_data->service;
        $mt_data->adn = $adn;
        $mt_data->type = 'dailypush';
        $mt_data->operatorId = $model_operator->getOperatorId($operator_name);
        $mt_data->price = $broadcast_data->price;

        $charging_data = charging_manager::getInstance ()->getCharging($mt_data);

        foreach ($users_data as $user_data) {
            // create mt object then save to queue
            $mt = loader_data::get('mt');
            $mt->inReply = NULL;
            $mt->msgId = date("YmdHis") . str_replace('.', '', microtime(true));
            $mt->adn = $adn;
            $mt->msgData = $broadcast_data->content;
            $mt->price = $broadcast_data->price;
            $mt->operatorId = $mt_data->operatorId;
            $mt->service = $broadcast_data->service;
            $mt->subject = strtoupper('MT;PUSH;SMS;BROADCAST');
            $mt->operatorName = $operator_name;
            $mt->serviceId = $charging_data->serviceId;
            $mt->msisdn = $user_data->msisdn;
            $mt->type = 'dailypush';

            $slot = substr($mt->msisdn, - 1, 1);

            // save to queue
            $profile = 'text';
            $queue = $loader_queue->load($profile);

            $data = loader_data::get('queue');
            $data->channel = $operator_name . '_dailypush' . $slot;
            $data->value = serialize($mt);
            $queue->put($data);
        }
    }

    /**
     * @param $broadcast_data
     */
    protected function populateUser($broadcast_data) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $model_user = loader_model::getInstance ()->load('schedule', 'connBroadcast');
        return $model_user->populateUser($broadcast_data);
    }

    /**
     * @param $broadcast_data
     * @param $user_data
     */
    protected function createMT($broadcast_data, $user_data) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        
    }

}