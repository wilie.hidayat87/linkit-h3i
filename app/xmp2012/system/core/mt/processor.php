<?php
interface mt_processor {
	public function saveToQueue($mt_data);
	public function process($slot);
}