<?php

class api_callcenter_historysubs {

    public function process($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        $check = $this->checkParams($GET);

        if ($check != 'OK') {
            return $check;
        }

        $user_data = new user_data ();
        $user_data->msisdn = $GET ['phone'];
        if (!empty($GET ['largeAccount']))
            $user_data->adn = $GET ['largeAccount'];
        if (!empty($GET ['start']))
            $user_data->subscribed_from = $GET ['start'];
        if (!empty($GET ['end']))
            $user_data->subscribed_until = $GET ['end'];
        if (!empty($GET ['package']))
            $user_data->service = $GET ['package'];
        $user_data->active = 1;
        if (!empty($GET ['limit']))
            $user_data->limit = $GET ['limit'];

        $model_user = loader_model::getInstance()->load('user', 'connDatabase1');
        $spring_user = $model_user->getSpringUserHistory($user_data);

        if ($spring_user !== false and count($spring_user) > 0)
            return $this->createXML($spring_user);
        else
            return 'NOT_FOUND';
    }

    protected function checkParams($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        if (empty($GET ['phone'])) {
            return 'MISSING_PARAMETERS';
        } else if (!is_numeric($GET ['phone']) || (!empty($GET ['largeAccount']) && !is_numeric($GET ['largeAccount']))) {
            return 'INVALID_PARAMETERS';
        } else if (!empty($GET ['start'])) {
            if (!$this->is_valid_date($GET ['start']))
                return 'INVALID_PARAMETERS';
        } else if (!empty($GET ['end'])) {
            if (!$this->is_valid_date($GET ['end']))
                return 'INVALID_PARAMETERS';
        }
        return 'OK';
    }

    protected function createXML($users) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($users)));

        $config_spring = loader_config::getInstance()->getConfig('spring');
        $date = date("Y-m-d") . "T" . date("H:i:sO");
        $size = count($users);
        $xml = '
			<subscriptions>
				<datetime-request>' . $date . '</datetime-request>
				<size>' . $size . '</size>';

        foreach ($users as $user) {
            #set channel
            if (!empty($user ['channel_subscribe']))
                $channel = $user ['channel_subscribe'];
            else
                $channel = $user ['channel_unsubscribe'];

            # set status and active
            if ($user ['active'] == 1) {
                $status = 'SIGNED';
                $active = 'Y';
            } else {
                $status = 'CANCEL';
                $active = 'N';
            }

            #set ip
            if (strtolower($user ['channel_subscribe']) == 'sms')
                $ip = $config_spring->ip;
            else
                $ip = $config_spring->ip;

            $xml .= '
				<subscription>
					<id>' . $user ['id'] . '</id>
					<datetime-event>' . $this->set_date($user ['subscribed_from']) . '</datetime-event>
					<portal-id>' . $config_spring->portalId . '</portal-id>
					<portal-name>' . $config_spring->portalName . '</portal-name>
					<large-account>' . $user ['adn'] . '</large-account>
					<package-id>' . $user ['package_id'] . '</package-id>
					<package-name>' . $user ['package_name'] . '</package-name>
					<protocol-number>' . $user ['protocol'] . '</protocol-number>
					<tariff-value>' . ($user ['tariff']?$user ['tariff']:"0") . '</tariff-value>
					<channel>' . strtoupper($channel) . '</channel>
					<status>' . $status . '</status>
					<active>' . $active . '</active>
					<ip>' . $ip . '</ip>
				</subscription>';
        }

        $xml .= '</subscriptions>';
        return $xml;
    }

    private function set_date($date) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $date));

        $date = explode(" ", $date);
        return $date [0] . "T" . $date [1] . date("O");
    }

    private function is_valid_date($value, $format = 'yyyy-mm-dd') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start value : " . $value . " format : " . $format));

        if (strlen($value) >= 6 && strlen($format) == 10) {
            // find separator. Remove all other characters from $format
            $separator_only = str_replace(array('m', 'd', 'y'), '', $format);
            $separator = $separator_only [0]; // separator is first character


            if ($separator && strlen($separator_only) == 2) {
                // make regex
                $regexp = str_replace('mm', '(0?[1-9]|1[0-2])', $format);
                $regexp = str_replace('dd', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp);
                $regexp = str_replace('yyyy', '(19|20)?[0-9][0-9]', $regexp);
                $regexp = str_replace($separator, "\\" . $separator, $regexp);

                //if ($regexp != $value && preg_match ( '/' . $regexp . '\z/', $value )) {
                // check date
                $arr = explode($separator, $value);
                $day = $arr [2];
                $month = $arr [1];
                $year = $arr [0];
                if (@checkdate($month, $day, $year))
                    return true;
                //}
            }
        }
        return false;
    }

}