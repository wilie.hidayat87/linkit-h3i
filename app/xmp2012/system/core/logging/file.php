<?php

class logging_file {

    protected $configArray = array();
    protected $startTime = NULL;
    protected $level = array();
    protected static $unique_id;

    public function __construct($profileArray) {
        $this->config = loader_config::getInstance()->getConfig('logging');
        $this->configArray = $profileArray;
        self::$unique_id = uniqid();
    }
    
    public function writeDefault($processType=null,$logData) {
        $configArray = $this->config->profile['default'];
    	$path = $configArray ['path'] . "/" . $configArray ['filename'] . "_" . date('Ymd');
    	$content = date("Y-m-d H:i:s") . "|" . $processType . "|" . serialize($logData). "|" . $this->getRunTime() . "\r\n";
    	
    	//chmod($path, 0666);    	
    	return @file_put_contents($path, $content, FILE_APPEND);
    }

    public function write($loggingArr) {
    
        if ($this->config->loglevel==9) return; // add by erad to skip logging process, if loglevel = 9 (silent mode)
    
        $trace = $this->backtrace();
        $loggingArr ['file'] = $trace ['file'];
        $loggingArr ['class'] = $trace ['class'];
        $loggingArr ['function'] = $trace ['function'];

        $content = $this->generateContent($loggingArr);

        switch ($loggingArr ['level']) {
            case 'error' :
                $debugLevel = 0;
                break;
            case 'info' :
                $debugLevel = 1;
                break;
            case 'debug' :
                $debugLevel = 2;
                break;
            default :
                $debugLevel = 2;
                break;
        }

        if ($debugLevel <= $this->config->loglevel) {
            $path = $this->configArray ['path'] . "/" . $this->configArray ['filename'] . "_" . date('Ymd');

            $mailConfig = loader_config::getInstance()->getConfig('mail');
            if ($mailConfig->sendLog && $mailConfig->level >= $debugLevel) {
                $mail_data = loader_data::get("mail");
                $mail_data->smtpUser = $mailConfig->smtpUser;
                $mail_data->subject = $mailConfig->subject . " [" . $loggingArr ['level'] . "] " . date('Y-m-d H:i:s');
                $mail_data->content = $path . "\r\n" . $content;
                tools_mail::sends($mail_data);
            }

            //chmod($path, 0666);
            return @file_put_contents($path, $content, FILE_APPEND);
        }
    }

    protected function backtrace() {
        $debug = debug_backtrace();
        $trace ['file'] = $debug [2] ['file'];
        $trace ['class'] = $debug [3] ['class'];
        $trace ['function'] = $debug [3] ['function'];
        return $trace;
    }

    protected function getRunTime() {
        return substr((microtime() - $this->startTime), 0, $this->config->timeDigit);
    }

    protected function generateContent($loggingArr) {

        if (empty($loggingArr ['response']))
            $loggingArr ['response'] = "";

        if (empty($loggingArr ['forking']))
            $loggingArr ['forking'] = "";
        if (empty($loggingArr ['pid']))
            $loggingArr ['pid'] = "";

        $content = str_replace(array('{uniqueId}', '{datetime}', '{exectime}', '{level}', '{file}', '{class}', '{function}', '{message}', '{response}'), array(self::$unique_id . $loggingArr ['pid'], date("Y-m-d H:i:s"), $this->getRunTime(), $loggingArr ['level'], $loggingArr ['file'], $loggingArr ['class'], $loggingArr ['function'], preg_replace(array('(\s+)', '(\t+)'), ' ', $loggingArr ['message']), str_replace("\n", "", $loggingArr ['response'])), str_replace(" ", "\t", $this->config->lineFormat));
        $content .= "\r\n";
        return $content;
    }

    public static function writeDBFile(file_data $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));
        $content = $data->content;
        $content .= "\r\n";
        $is_success_save = file_put_contents($data->path, $content, FILE_APPEND);
        if ($is_success_save === false) {
            $log->write(array('level' => 'error', 'message' => "Failed to write a file : " . $data->path));
        }
        if (!$is_success_save && !is_writable($data->path)) {
            chmod($data->path, 0777);
        }

        return $is_success_save;
    }

}
