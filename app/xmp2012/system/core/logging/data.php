<?php
class logging_data {
	public $id;
	public $level;
	public $path;
	public $message;
	public $msisdn;
	public $subject;
	public $msgId;
	public $operator;
	public $response;
	public $messageType;
	public $lineFormat;
	public $timeFormat;
	public $logLevel;
	public $runTime;
}