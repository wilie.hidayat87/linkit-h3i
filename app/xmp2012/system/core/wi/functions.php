<?php

class wi_functions {
	
	public $GET;
	
	public function safe_b64encode($string) {
		$data = base64_encode ( $string );
		$data = str_replace ( array ('+', '/', '=' ), array ('-', '_', '' ), $data );
		return $data;
	}
	
	public function encode($value, $key) {
		if (! $value) {
			return false;
		}
		$text = $value;
		$iv_size = mcrypt_get_iv_size ( MCRYPT_3DES, MCRYPT_MODE_ECB );
		$iv = mcrypt_create_iv ( $iv_size, MCRYPT_RAND );
		$crypttext = mcrypt_encrypt ( MCRYPT_3DES, $key, $text, MCRYPT_MODE_ECB, $iv );
		
		return trim ( $this->safe_b64encode ( $crypttext ) );
	}
	
	public function gen_reqid($length = 5) {
		/*
	    //$characters = "0123456789abcdefghijklmnopqrstuvwxyz";
	    $characters = "0123456789";
	    $string = date("YmdHis")." ";
	    for ($p = 0; $p < $length; $p++) {
	        $string .= $characters[mt_rand(0, strlen($characters))];
	    }
	 	*/
		$string = date ( 'ymdHis' ) . str_pad ( mt_rand ( 0, 99999 ), 5, '0' );
		return $string;
	}
	
	public function gen_url($configWi, $data) {
		$eData = $this->encode ( $data, $configWi->cppass );
		$base_url = $configWi->wi_url ['request'];
		$base_url = str_replace ( '%CPCODE%', $configWi->cpcode, $base_url );
		$base_url = str_replace ( '%DATA%', $eData, $base_url );
		return $base_url;
	
	}
	
	public function save_wi_info($data) {
		$model_wi = loader_model::getInstance ()->load ( 'wi', 'connDatabase1' );
		$insert_id = $model_wi->save_wi_info ( $data );
		if ($insert_id)
			return true;
		else
			return false;
	}
	
	public function read_wi_info($data) {
		$model_wi = loader_model::getInstance ()->load ( 'wi', 'connDatabase1' );
		$wi_data = $model_wi->read_wi_info ( $data );
		if ($wi_data)
			return $wi_data;
		else
			return false;
	}
	
	public function save_wi_tracker($data) {
		$model_wi = loader_model::getInstance ()->load ( 'wi', 'connDatabase1' );
		$insert_id = $model_wi->save_wi_tracker ( $data );
		if ($insert_id)
			return true;
		else
			return false;
	}

	public function update_wi_info($data) {
		$model_wi = loader_model::getInstance ()->load ( 'wi', 'connDatabase1' );
		return $model_wi->update_wi_info ( $data );
	}

	public function save_wi_trans($data) {
		$model_wi = loader_model::getInstance ()->load ( 'wi', 'connDatabase1' );
		return $model_wi->save_wi_trans( $data );
	}
	
	public function read_wi_instid($data) {
		$model_wi = loader_model::getInstance ()->load ( 'wi', 'connDatabase1' );
		$wi_data = $model_wi->read_wi_instid( $data );
		if ($wi_data)
			return $wi_data;
		else
			return false;
	}
	
}

?>
