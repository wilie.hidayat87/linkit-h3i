<?php
class  queue_delay implements queue_interface{
    public $slot;
    public $channel;
    function connect($profile){}
    
    public function put(queue_data $data){
        $log = manager_logging::getInstance ();
        $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
        
        $buffer_file    = buffer_file::getInstance();
        $load_config    = loader_config::getInstance();
        $config_mo      = $load_config->getConfig('mo');
        $path           = $this->generate_file_name($data);
        
        if($buffer_file->save($path, $data->value))
            return $config_mo->returnCode['OK'];
        else
            return $config_mo->returnCode['NOK'];
        
    }
    
    private function generate_file_name($data){
        $log = manager_logging::getInstance ();
        $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
        
        $config_delay = loader_config::getInstance ()->getConfig ( 'delay' );
		
        $bufferPath = $config_delay->bufferPath;
        $delayTime  = $config_delay->delayTime;
        $throttle   = $config_delay->throttle;

        $path = $bufferPath . '/'.$data->channel.'/';
        if (! is_dir ( $path ))
                mkdir ( $path, 0777, TRUE );

        return $path . uniqid () . '.MT';
    }
    
    public function pop() {
        $log = manager_logging::getInstance ();
        $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
        
        $profile            = "delaypush";
        $load_config        = loader_config::getInstance ();
        
        $config_main        = $load_config->getConfig ( 'main' );
        $config_delay       = $load_config->getConfig ( 'delay' );

        $channel            = $this->channel;
        $path               = $config_delay->bufferPath . "/" . $this->channel;
        $limit              = $config_delay->throttle;
        $buffer_file        = buffer_file::getInstance();
        return $buffer_file->read($path, $limit, 'mt');
        
    }
    
    public function setSlot($slot, $channel) {
            $log = manager_logging::getInstance ();
            $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
            
            $this->slot     = $slot;
            $this->channel = $channel;
           
    }
        
        
}