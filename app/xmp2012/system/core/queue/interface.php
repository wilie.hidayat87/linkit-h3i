<?php

interface queue_interface {
	public function connect($profile);
	public function put(queue_data $data);
	public function pop();
}