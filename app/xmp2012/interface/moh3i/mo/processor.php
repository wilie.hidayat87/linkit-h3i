<?php

class moh3i_mo_processor extends default_mo_processor {

    /**
     * @param $arrData
     */
    public function saveToFile($arrData) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $log->write(array('level' => 'info', 'message' => "Info : " . print_r($arrData, TRUE)));

        $load_config = loader_config::getInstance();
        $config_main = $load_config->getConfig('main');
		$configMT = $load_config->getConfig('mt');
		$profile = 'text';
		
        $ip = $_SERVER ['REMOTE_ADDR'];

		$config_mo = $load_config->getConfig('mo');
        $mo_data = loader_data::get('mo');
		$mo_data->msisdn = $arrData ['msisdn'];
        $mo_data->msgId = $arrData ['transid'];
        $mo_data->rawSMS = trim($arrData ['sms']);		
		$mo_data->adn = $arrData['sc'];
        $mo_data->operatorName = $config_main->operator;

        if (!empty($arrData ['trx_time']))
            $mo_data->incomingDate = $arrData ['trx_time'];
        else
            $mo_data->incomingDate = date("Y-m-d H:i:s");
        
        $mo_data->incomingIP = http_request::getRealIpAddr();
        $mo_data->type = 'mo';
        $mo_data->channel = $arrData ['channel'];
		
		if (!empty($arrData ['bundling']))
            $mo_data->bundling = $arrData ['bundling'];
		
		if (!empty($arrData ['subsType']))
		{
            $mo_data->subsType = $arrData ['subsType'];
			
			// Add product name into media column
			if (!empty($arrData ['product_name']))
			{
				$mo_data->subsType .= "-" . $arrData ['product_name'];
			}
		}

		if (!empty($arrData ['rereg']) || $arrData ['rereg'] == true)
            $mo_data->rereg = $arrData ['rereg'];

		$subKeyword = $this->splitKeyword($mo_data->rawSMS);
		$mo_data->service = $subKeyword['service'];
		$mo_data->customService = $subKeyword['2ndK'];
		$mo_data->s2 = $subKeyword['3rdK'];
		
		return $this->saveToBuffer($mo_data);
    }

	private function saveToBuffer($mo_data)
	{
		$log = manager_logging::getInstance();
		
		$load_config = loader_config::getInstance();
		$config_mo = $load_config->getConfig('mo');
		$buffer_file = buffer_file::getInstance();

		if(strpos(strtoupper($mo_data->customService), "SPC") !== FALSE && substr($mo_data->customService,0,3) == 'SPC')
		{
			// Buffer path for SPC MO only
			$path = $buffer_file->generate_file_name($mo_data, 'mospc');
		}
		else
		{
			// Buffer path for original mo
			$path = $buffer_file->generate_file_name($mo_data);
		}
		
		if((strpos(strtoupper($mo_data->rawSMS), 'UNREG') !== FALSE))
		{
			$log->write(array('level' => 'debug', 'message' => 'Object MO write at: ' . $path . ' response : ' . $config_mo->returnCode['OK']));

			$save_file = $buffer_file->save($path, $mo_data);
			
			return $config_mo->returnCode ['OK'];
		}
		else
		{
			$date = date("Ymd");
			$lockPath = "/DATA/app/xmp2012/logs/moh3i/lock/{$date}";
			if (!file_exists($lockPath)) 
			{
				shell_exec("cp -pa /DATA/app/xmp2012/logs/moh3i/lock/master {$lockPath}");
			}
			
			$string = $this->clean($mo_data->rawSMS);
			
			$lockPath .= "/filter_" . $mo_data->msisdn . "_" . $string;
			
			if(!file_exists($lockPath))
			{
				if(touch($lockPath))
				{
					$save_file = $buffer_file->save($path, $mo_data);
					
					$log->write(array('level' => 'debug', 'message' => 'Object MO write at: ' . $path . ' response : ' . $config_mo->returnCode['OK']));
					return $config_mo->returnCode ['OK'];
				}
			}
			else
			{
				$log->write(array('level' => 'error', 'message' => 'Too many request with the same value, response : ' . $config_mo->returnCode['NOK']));
				return $config_mo->returnCode ['NOK'];
			}
		}
	}
	
	private function clean($string) 
	{
		$string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '_', $string); // Removes special chars.

		return preg_replace('/-+/', '_', $string); // Replaces multiple hyphens with single one.
	}

	private function hitMT($data)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $log->write(array('level' => 'debug', 'message' => "Info : " . print_r($data, TRUE)));

        $load_config = loader_config::getInstance();
		$config_main = $load_config->getConfig('main');
		$configMT = $load_config->getConfig('mt');
		$profile = 'text';
		
		if(strlen($data['content']) > 160)
		{
			$param .= '&UDH=1';
		}
		else
		{
			if(strlen($data['content']) == 0)
				$param .= '&UDH=1';
			else
				$param .= '&UDH=0';
		}

		$param .= '&MOBILENO=' . $data['msisdn'];
		$param .= '&MESSAGE=' . urlencode($data['content']);
		
		if($config_main->toggle_env == "PRODUCTION") 
		{
			$url = $configMT->profile [$profile] ['sendUrl'] [0];
		} 
		else if($config_main->toggle_env == "DEVELOPMENT") 
		{
			$url = $configMT->profile [$profile] ['sendUrlDev'] [0];
		}
		
		if($configMT->profile [$profile] ['hit']){
			
			$hit = http_request::requestGet($url, $param, $configMT->profile [$profile] ['SendTimeOut']);
			$log->write ( array ('level' => 'debug', 'message' => "MT Url:" . $url . $param . ', Result:' . $hit ) );
			
		}else{
			
			$log->write ( array ('level' => 'debug', 'message' => "Disable MT" ) );
			
		}
		
		return true;
	}
	
	private function getVSubs($msisdn, $conn) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($msisdn)));

        $sql = "SELECT s.active, ms.subject FROM xmp.subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.msisdn = '" . mysql_real_escape_string($msisdn) . "'";

        $log->write(array('level' => 'debug', 'message' => "SQL : " . $sql));

        $result = mysql_query($sql, $conn);

        if (mysql_num_rows($result) > 0) {
            return mysql_fetch_array($result);
        } else {
            return array();
        }
    }

	private function subsChecking($data)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $log->write(array('level' => 'debug', 'message' => "Info : " . print_r($data, TRUE)));
		
		$xmp_model = loader_model::getInstance ();
		$model_user = $xmp_model->load ( 'user', 'connDatabase1' );
			
		//$subcribe = $model_user->getUserException($data);
		$subcribe = $model_user->getVSubs($data);
							
		$isContinued = true;
		
		if($subcribe === false)
			$isContinued = true;
		else
		{
			//$findThis = strpos(strtoupper($subcribe['subject']), "SPC");
			
			$request_keyword = substr(strtoupper($data->subject),0,2); // request keyword
			$existing_keyword = substr(strtoupper($subcribe['subject']),0,3); // existing keyword in a database
			
			$log->write(array('level' => 'info', 'message' => "Request Keyword : ".strtoupper($data->subject).", Existing Keyword : ".strtoupper($subcribe['subject']) . ", Current active : " . $subcribe['active']));

			if($request_keyword == "PS" && $existing_keyword == 'SPC')
			{
				$userDB = loader_model::getInstance()->load ( 'user', 'connDatabase1' );
	
				$switchUser = loader_data::get('user');
				$switchUser->status = '0';
				$switchUser->msisdn = $data->msisdn;
				$switchUser->service = strtolower($data->service);
				$switchUser->operator = $data->operator;
				
				$userDB->updateStatusSubscription($switchUser);

				$subcribe['active'] = 0;
			}

			if ((int) $subcribe['active'] == 1 || (int) $subcribe['active'] == 2)
				$isContinued = false;
			else if ((int) $subcribe['active'] == 0)
				$isContinued = true;
		}
		
		return $isContinued;
	}
	
	private function splitKeyword($str)
	{
		$customService = ""; $_3rdKeyword = "";
		
		$split_raw_sms = explode(" ", $str);
		if(count($split_raw_sms) > 0){
			$trigger = $split_raw_sms[0]; // REG
			$service = $split_raw_sms[1]; // SERVICE NAME
			$cs = !empty($split_raw_sms[2]) ? $split_raw_sms[2] : ""; // SUB KEYWORD
			$_3rdKeyword = !empty($split_raw_sms[3]) ? $split_raw_sms[3] : ""; // 3RD KEYWORD
			
			if((strpos(strtoupper($str), 'REG') !== FALSE))
				$customService = $cs;
			else
				$customService = $service;
			
			// if trigger is not common format
			if(!in_array(strtoupper($trigger), array("REG", "UNREG")))
			{ 
				$trigger = ""; // REG
				$service = $split_raw_sms[0]; // SERVICE NAME
				$customService = $split_raw_sms[1];
				$_3rdKeyword = !empty($split_raw_sms[2]) ? $split_raw_sms[2] : ""; // 3RD KEYWORD
			}
		}
		
		return array("trigger" => $trigger, "service" => $service, "2ndK" => $customService, "3rdK" => $_3rdKeyword);
	}
	
    private function setDate($char) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $log->write(array('level' => 'debug', 'message' => "Info : " . $char));

        if (empty($char)) {
            return date("Y-m-d H:i:s");
        } else {
            $y = substr($char, 0, 4);
            $m = substr($char, 4, 2);
            $d = substr($char, 6, 2);
            $h = substr($char, 8, 2);
            $i = substr($char, 10, 2);
            $s = substr($char, 12, 2);
            $datetime = $y . '-' . $m . '-' . $d . ' ' . $h . ':' . $i . ':' . $s;
            return $datetime;
        }
    }
	
	private function confirmConf($main, $sub, $cur = 'this')
	{
		$load_config = loader_config::getInstance();
		$configMT = $load_config->getConfig('mt');
		
		return $configMT->profile ['text'] ['mtProcess'] ['confirmation'] [$main] [strtolower($sub)] [strtolower($cur)];
	}

	private function aldosRequestPostback($req = array())
	{
		$trxid = ((!empty($req['trxid'])) ? $req['trxid'] : date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) ));
		$keyword = strtolower($req['keyword']);
		$msisdn = $req['msisdn'];
		$sms = $req['sms'];
		
		if(substr($sms,0,3) == "REG" && in_array($keyword,array('ps50','ps51','ps52','ps53','ps54','ps55','ps56','ps57','ps58','ps59','ps60')))
		{
			$log = manager_logging::getInstance();
			$log->write(array('level' => 'debug', 'message' => "Start"));
			
			$params = array(
				"trxid=".$trxid
				,"serv_id=gemezz" . $keyword
				,"partner=linkh3i"
				,"msisdn=".$msisdn
				,"px=" . $keyword
			);

			$fullurl = "http://kbtools.net/linkh3i.php?";

			$hit = http_request::requestGet ( $fullurl, implode("&", $params), 10 );
		}
		
		// New reporting Airpay
		
		if(in_array($keyword,array('ps59')))
		{
			//MO : http://149.129.252.221:8028/app/api/waki_in.php?msisdn=6289536625&operator=4&sdc=99876&sms=REG+Gemezz+PS59&trx_id=628273293273209809809809890&service_type=2&trx_date=20100120103012
			
			$params = array(
				"msisdn=".$msisdn
			   ,"operator=4"
			   ,"sdc=99876"
			   ,"sms=".urlencode($sms)
			   ,"trx_id=".$trxid
			   ,"service_type=2"
			   ,"trx_date=".date("YmdHis")
			);

			$fullurl = "http://149.129.252.221:8028/app/api/waki_in.php?";

			//$hit = http_request::requestGet ( $fullurl, implode("&", $params), 10 );
		}
		
		return true;
	}
	
	public function moProcess($mo)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start MO : " . serialize($mo)));
		
		$load_config = loader_config::getInstance();
        $config_main = $load_config->getConfig('main');
		$config_mo = $load_config->getConfig('mo');
		$configMT = $load_config->getConfig('mt');
		$profile = 'text';
		
		$user_manager = user_manager::getInstance();
		
		$trough = true;
		
		// Blacklist
		if($user_manager->isBlacklisted($mo->msisdn)) $trough = false;
		
		$log->write(array('level' => 'info', 'message' => "Blacklist check [{$mo->msisdn}] : " . (($trough == true) ? "OK" : "NOK")));
		
		// Whitelist OLD
		//if($user_manager->isWhitelisted($mo->msisdn, trim($mo->rawSMS))) $trough = false;
		
		if(strpos(strtoupper(trim($mo->rawSMS)), "STOP") !== false || strpos(strtoupper(trim($mo->rawSMS)), "UNREG") !== false)
		{
			$trough = true; // If UNREG no need to check into whitelist
		}
		else
		{
			// Whitelist NEW LOGIC
			$whitelisted = $user_manager->isWhitelistedV2($mo->msisdn);
		
			if(count($whitelisted) > 0)
			{
				$trough = false;
				
				$request_keyword = trim($mo->rawSMS);
			
				for($w=0; $w < count($whitelisted); $w++)
				{
					if(strtoupper($request_keyword) == strtoupper($whitelisted[$w]['keyword']))
					{
						$trough = true;
					}
				}
			}
		
			$log->write(array('level' => 'info', 'message' => "Whitelist check [{$mo->msisdn}, ".trim($mo->rawSMS)."] : " . (($trough == true) ? "OK" : "NOK")));
		}
		
		if($trough)
		{
			// New Feature - Aldo's request postback for campaign
			// Using keyword : PS50 - PS60

			$this->aldosRequestPostback(array(
				 'keyword' 	=> $mo->customService
				,'msisdn'	=> $mo->msisdn	
				,'trxid'	=> $mo->msgId	
				,'sms'		=> strtoupper($mo->rawSMS)
			));

			if($mo->channel == 'sms')
			{
				$xmp_model = loader_model::getInstance ();
				$model_mechanism = $xmp_model->load ( 'mechanism', 'connDatabase1' );
				$model_user = $xmp_model->load ( 'user', 'connDatabase1' );
				
				$model_pushcontent = loader_model::getInstance ()->load ( 'pushcontent', 'connBroadcast' );
				
				// Allow dynamic in 3rd keyword
				if(in_array(strtoupper($mo->customService), $config_main->dynamic3rdKeyword))
				{
					$log->write(array('level' => 'info', 'message' => "Dynamic 3rd keyword [{$mo->customService}] : OK"));
					
					$mo->rawSMS = strtolower("reg " . $mo->service . " " . $mo->customService);
				}
				
				$mechanism = $model_mechanism->getMechanism($mo);
						
				$reply_pattern = array('YES','YA','Y');
				
				if(count($mechanism) > 0 || in_array(strtoupper($mo->rawSMS), $reply_pattern)) // ======= INVALID KEYWORD PROCESS ====== //
				{
					// ======= CONFIRMATION SWITCH ON/OFF ============ //
					
					$useConfirmation = false;
					
					if($configMT->profile [$profile] ['mtProcess'] ['confirmation'] ['global']) $useConfirmation = true;
					
					if($useConfirmation) // GLOBAL SETTING
					{
						// DAILY / MONTHLY SERVICE CONFIRMATION SETTING
						if(!empty($mo->customService))
						{
							//DAILY SET CONFIRMATION
							if(in_array(strtolower($mo->customService), $configMT->profile [$profile] ['mtProcess'] ['confirmation'] ['daily'] ['subject']))
							{
								$useConfirmation = $this->confirmConf('daily', $mo->customService, 'this');

								if(!empty($mo->s2))
								{
									if(in_array(strtolower($mo->s2), $configMT->profile [$profile] ['mtProcess'] ['confirmation'] ['daily'] ['s2']))
									{
										$useConfirmation = $this->confirmConf('daily', $mo->customService, $mo->s2);
									}
								}
							}
							
							//MONTHLY SET CONFIRMATION
							if(in_array(strtolower($mo->customService), $configMT->profile [$profile] ['mtProcess'] ['confirmation'] ['monthly'] ['subject']))
							{
								$useConfirmation = $this->confirmConf('monthly', $mo->customService, 'this');

								if(!empty($mo->s2))
								{
									if(in_array(strtolower($mo->s2), $configMT->profile [$profile] ['mtProcess'] ['confirmation'] ['monthly'] ['s2']))
									{
										$useConfirmation = $this->confirmConf('monthly', $mo->customService, $mo->s2);
									}
								}
							}
						}
					}
					
					// ======= PROCESSING REPLYING YA/YES/Y ======== //
					
					if(in_array(strtoupper($mo->rawSMS), $reply_pattern))
					{
						$checkConfirm = $model_user->getConfirmMechanismByMsisdn($mo->msisdn);
						
						if(count($checkConfirm) > 0)
						{
							$model_user->updateConfirmMechanism($mo->msisdn);
						
							if(!empty($checkConfirm[0]['type']))
							{
								switch($checkConfirm[0]['type'])
								{
									case '180day' :
										$message = unserialize($checkConfirm[0]['message']);
										$mo->replied_180d = $message['replied'];
										$mo->customService = ((!empty($message['msisdn_subject'])) ? $message['msisdn_subject'] : '');
										$mo->s2 = ((!empty($message['s2'])) ? $message['s2'] : '');
										
										$arr = array("REG", strtoupper($message['service']), $mo->customService, $mo->s2);
										$rawSMS = trim(implode(" ", $arr));
										
										/* $rawSMS = "REG " . strtoupper($message['service']) . ((isset($message['msisdn_subject'])) ? ' ' . $message['msisdn_subject'] : ''); */
									break;
									
									case 'daily' :
									case 'monthly' :
										$message = unserialize($checkConfirm[0]['message']);
										$rawSMS = $message->rawSMS;
										
										$subKeyword = $this->splitKeyword($message->rawSMS);
										$mo->customService = $subKeyword['2ndK'];
										$mo->s2 = $subKeyword['3rdK'];
									break;
								}
								
								$mo->rawSMS = trim($rawSMS);
								
								if(!in_array(strtoupper($mo->rawSMS), $reply_pattern))
								{
									return $mo;
								}
								else
								{
									$log->write(array('level' => 'error', 'message' => 'Raw SMS is : '.$mo->rawSMS.', can not proceed!.'));
									return false;
								}
							}
						}
						else
						{
							$log->write(array('level' => 'error', 'message' => 'Data not found, can not proceed!.'));
							return false;
						}
					}
					else
					{
						if((strpos(strtoupper($mo->rawSMS), 'UNREG') !== FALSE) || (strpos(strtoupper($mo->rawSMS), 'STOP') !== FALSE))
						{
							// ======= PROCESSING UNREG REQUEST ======== //
							return $mo;
						}
						else
						{						
							if(count($mechanism) > 0)
							{
								// ======= SUBS CHECKING REQUEST ======== //
								
								$user = loader_data::get('user');
								$user->msisdn = $mo->msisdn;
								$user->adn = $mo->adn;
								$user->service = $mechanism[0]['name'];
								$user->operator = $mo->operatorName;
								$user->active = '0,1,2';
								$user->subject = strtoupper($mo->customService);
								$user->s2 = strtoupper($mo->s2);
								
								$isContinued = $this->subsChecking($user);
								
								$log->write(array('level' => 'debug', 'message' => 'IS continued : ' . $isContinued . ', pattern : ' . strtoupper($mechanism[0]['pattern']) . ', useConfirmation : ' . $useConfirmation));
								
								// KET : PULL SERVICE TIDAK PERLU SMS KONFIRMASI HANYA SERVICE REG
								
								if((strpos(strtoupper($mechanism[0]['pattern']), 'REG') !== FALSE) && $useConfirmation == TRUE) // REG SERVICE
								{
									// ======= PROCESSING REG/1ST PUSH REQUEST ======== //
								
									if($isContinued)
									{
										// ======= SUBSCRIPTION NOT ACTIVE OR NOT REGISTERED ====== //
										
										$log->write(array('level' => 'debug', 'message' => 'Request REG IS : ' . $config_mo->returnCode['OK']));
										
										$dailyListSubject = $configMT->profile ['text'] ['mtProcess'] ['confirmation'] ['daily'] ['subject'];
										
										$daily_or_monthly = (!empty($mo->customService) ? ((in_array(strtolower($mo->customService),$dailyListSubject)) ? 'daily' : 'monthly') : 'daily');
										
										$confirm = array();
										$confirm['msisdn'] = $mo->msisdn;
										$confirm['datetime'] = date("Y-m-d H:i:s", strtotime("+5 minutes"));
										$confirm['type'] = $daily_or_monthly;
										$confirm['message'] = serialize($mo);
										
										$model_user->insertConfirmMechanism($confirm);
										
										$dt = new moh3i_broadcast_data ();
										$dt->service = $mechanism[0]['name'];
										$dt->contentLabel = 'confirmation';
										$dt->notes = $daily_or_monthly;
										$pushContent = $model_pushcontent->getContentByLabel ( $dt );

										if($useConfirmation)
										{
											$this->hitMT(array(
												'msisdn'  => $mo->msisdn,
												'content' => $pushContent[0]['content']
											));
										}
									}
									else
									{
										// ======= SUBSCRIPTION ACTIVE OR REGISTERED ====== //
										
										$log->write(array('level' => 'debug', 'message' => 'Request REG REJECTED Subs Active : ' . $config_mo->returnCode['NOK']));
										
										$dt = new moh3i_broadcast_data ();
										$dt->service = $mechanism[0]['name'];
										$dt->contentLabel = 'subs_still_active';
										$dt->notes = 'notify';
										$pushContent = $model_pushcontent->getContentByLabel ( $dt );

										$findThis = strpos(strtoupper($mo->customService), "SPC");
									
										if($findThis !== FALSE)
										{
											//do nothing
										}
										else
										{
											$this->hitMT(array(
												'msisdn'  => $mo->msisdn,
												'content' => $pushContent[0]['content']
											));
										}
									}
									
									// ======= DO MT MESSAGE ====== //
									
									/*
									$findThis = strpos(strtoupper($mo->customService), "SPC");
									
									if($findThis !== FALSE)
									{
										//do nothing
									}
									else
									{
										$this->hitMT(array(
											'msisdn'  => $mo->msisdn,
											'content' => $pushContent[0]['content']
										));
									}
									*/
									
									return false;
								}
								else // REG WITH NO CONFIRMATION
								{
									if($isContinued)
									{
										// ======= SUBSCRIPTION NOT ACTIVE OR NOT REGISTERED ====== //

										$log->write(array('level' => 'info', 'message' => 'Registration ['.strtoupper($mo->customService).'] Request is : ' . $config_mo->returnCode['OK']));
										return $mo;
									}
									else
									{
										// ======= SUBSCRIPTION ACTIVE OR REGISTERED ====== //
										
										$dt = new moh3i_broadcast_data ();
										$dt->service = $mechanism[0]['name'];
										$dt->contentLabel = 'subs_still_active';
										$dt->notes = 'notify';
										$pushContent = $model_pushcontent->getContentByLabel ( $dt );
										
										// ======= DO MT MESSAGE ====== //
										
										$findThis = strpos(strtoupper($mo->customService), "SPC");
										
										if($findThis !== FALSE)
										{
											//do nothing
										}
										else
										{
											$this->hitMT(array(
												'msisdn'  => $mo->msisdn,
												'content' => $pushContent[0]['content']
											));
										}

										/*
										if($this->useMT($mo->customService))
										{
											$this->hitMT(array(
												'msisdn'  => $mo->msisdn,
												'content' => $pushContent[0]['content']
											));
										}
										*/

										$log->write(array('level' => 'debug', 'message' => 'Request PULL REJECTED Subs Active : ' . $config_mo->returnCode['NOK']));
										return false;
									}
								}
							}
							else
							{
								$log->write(array('level' => 'debug', 'message' => 'Request is : ' . $config_mo->returnCode['NOK']));
								return false;
							}
						}
					}
				}
				else
				{
					// ======= INVALID KEYWORD MT ====== //
					
					$dt = new moh3i_broadcast_data ();
					$dt->service = 'gemezz';
					$dt->contentLabel = 'invalid_keyword';
					$dt->notes = 'notify';
					$pushContent = $model_pushcontent->getContentByLabel ( $dt );
		
					$findThis = strpos(strtoupper($mo->customService), "SPC");
									
					if($findThis !== FALSE)
					{
						//do nothing
					}
					else
					{
						$this->hitMT(array(
							'msisdn'  => $mo->msisdn,
							'content' => $pushContent[0]['content']
						));
					}
					
					return false;
				}
			}
			else
			{
				if((strpos(strtoupper($mo->rawSMS), 'UNREG') !== FALSE) || (strpos(strtoupper($mo->rawSMS), 'STOP') !== FALSE))
				{
					// ======= PROCESSING UNREG REQUEST ======== //
					
					return $mo;
				}
				else
				{
					// ======= CONVERT MONTHLY SUBS TO MO PS3 DAILY SUBS ======= //
					
					if(strtoupper($mo->customService) == 'PS3')
					{
						$userDB = loader_model::getInstance()->load ( 'user', 'connDatabase1' );
			
						$switchUser = loader_data::get('user');
						$switchUser->status = '0';
						$switchUser->msisdn = $mo->msisdn;
						$switchUser->service = strtolower($mo->service);
						$switchUser->operator = $mo->operatorName;
						
						$userDB->updateStatusSubscription($switchUser);
					}
					
					// ======= PORTAL SUBSCRIPTION OR NOT SMS CHANNEL ====== //
					$user = loader_data::get('user');
					$user->msisdn = $mo->msisdn;
					$user->adn = $mo->adn;
					$user->service = strtolower($mo->service);
					$user->operator = $mo->operatorName;
					$user->active = '0,1,2';
					$user->subject = strtoupper($mo->customService);
					$user->s2 = strtoupper($mo->s2);
					
					if($this->subsChecking($user))
					{
						// ======= SUBSCRIPTION NOT ACTIVE OR NOT REGISTERED ====== //
						
						return $mo;
					}
					else
					{
						// ======= SUBSCRIPTION ACTIVE OR REGISTERED ====== //
						
						$model_pushcontent = loader_model::getInstance ()->load ( 'pushcontent', 'connBroadcast' );
						
						$dt = new moh3i_broadcast_data ();
						$dt->service = strtolower($mo->service);
						$dt->contentLabel = 'subs_still_active';
						$dt->notes = 'notify';
						$pushContent = $model_pushcontent->getContentByLabel ( $dt );
						
						// ======= DO MT MESSAGE ====== //
						
						$findThis = strpos(strtoupper($mo->customService), "SPC");
									
						if($findThis !== FALSE)
						{
							//do nothing
						}
						else
						{
							$this->hitMT(array(
								'msisdn'  => $mo->msisdn,
								'content' => $pushContent[0]['content']
							));
						}
						
						$log->write(array('level' => 'debug', 'message' => 'Subs Still Active : ' . $config_mo->returnCode['NOK']));
						return false;
					}
				}
			}
		}
		else return false;
	}

	public function initprocess($params) 
	{ 
		$configname = (!empty($params['configname']) ? $params['configname'] : 'moretry');
		$lockname = (!empty($params['lockname']) ? $params['lockname'] : 'moretry_processor');
		$log_profile = (!empty($params['logname']) ? $params['logname'] : 'mo_retry_processor');

        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $config_main = loader_config::getInstance ()->getConfig('main');
        $slot = loader_config::getInstance ()->getConfig($configname)->bufferSlot;

        for ($i = 0; $i < $slot; $i++) {
            if ($config_main->use_forking) {
                switch ($pid = pcntl_fork ()) {
                    case - 1 :
                        $log->write(array('level' => 'error', 'message' => "Forking failed"));
                        die('Forking failed');
                        break;
                    case 0 :
                        $this->moprocessing(
							$i, 
							array(
								"configname" 	=> $configname, 
				  				"logname" 		=> $log_profile,
								"lockname" 		=> $lockname,
							)
						);
                        exit ();
                        break;
                    default :
                        //pcntl_waitpid ( $pid, $status );
                        break;
                }
            } else {
                $this->moprocessing(
							$i, 
							array(
								"configname" 	=> $configname, 
				  				"logname" 		=> $log_profile,
								"lockname" 		=> $lockname,
							)
						);
            }
        }
        return true;
	}
	
	public function moprocessing($slot, $params) 
	{
        $lock = new library_lockfile($params['lockname']);
        $lock->create($slot);

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => " Start : " . $slot));

        $buffer_file = buffer_file::getInstance();
        $load_config = loader_config::getInstance();
        $config_mo = $load_config->getConfig($params['configname']);

        $path = $config_mo->bufferPath . '/' . $slot;
        $limit = $config_mo->bufferThrottle;
        $result = $buffer_file->read($path, $limit);

        if ($result !== false) {
            $service_listener = manager_service_listener::getInstance();
            foreach ($result as $val) {
                foreach ($val as $moDataPath => $content) {
                    $log->write(array('level' => 'debug', 'message' => serialize($content)));
                    
                    $log->writeDefault($params['logname'],$content);

                    if (is_object($content)) {
						
						$date = date("Ymd");
						$lockPath = "/DATA/app/xmp2012/logs/moh3i/lock/{$date}";
						
						$string = $this->clean($content->rawSMS);
						
						$lockPath .= "/moproc_" . $content->msisdn . "_" . $string;
						
						if(!file_exists($lockPath))
						{
							touch($lockPath);

							$buffer_file->delete($moDataPath);
							
							$mo_processing = new moh3i_mo_processor ( );
							$response = $mo_processing->moProcess($content);
							
							if($response !== FALSE)
							{
								$obj = $service_listener->notify($response);
								$log->write(array('level' => 'debug', 'message' => "Done Retry: " . serialize($obj)));
							}
						}
						
                    } else {
                        $log->write(array('level' => 'error', 'message' => "buffer MO is not an object"));
                        $buffer_file->delete($moDataPath);
                        $lock->delete($slot);
                        return false;
                    }
                }
            }
            $lock->delete($slot);
            return true;
        } else {
            $lock->delete($slot);
            return false;
        }
    }
}


