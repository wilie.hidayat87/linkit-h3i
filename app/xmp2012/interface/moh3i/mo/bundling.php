<?php

class moh3i_mo_bundling {

    /**
     * @param $data
     */
    public function process($data) 
    {
        $proceed = false;

        error_reporting(1);

        /* $xml = <<<XML
<?xml version="1.0" encoding="UTF-8"?><ns0:Request xmlns:ns0="http://www.tibco.com/schemas/TEST/Schema2.xsd2"><ns0:MSISDN>628978002847</ns0:MSISDN><ns0:timeStamp>2017-01-31 18:33:47</ns0:timeStamp><ns0:subsChannel>ESME</ns0:subsChannel><ns0:partnerName>GENFLIX1</ns0:partnerName><ns0:productName>300MB_Genflix1</ns0:productName><ns0:subsType>1</ns0:subsType><ns0:TransactionId>EAI201701311814895</ns0:TransactionId><ns0:Signature>1234</ns0:Signature><ns0:reserved1/><ns0:reserved2/><ns0:reserved3/><ns0:statusCode/><ns0:statusDescription/></ns0:Request>
XML; */

		$loaderConfig = loader_config::getInstance();
		$config_main = $loaderConfig->getConfig('main');
		
        $log_profile = 'mo_receiver';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'info', 'message' => "Start bundling : " . $data));

        $dataXML = array(
            'msisdn' 	        => ''
            ,'timeStamp'        => date("Y-m-d H:i:s")
            ,'subsChannel'      => 'sms'
            ,'channel'          => 'sms'
            ,'partnerName'      => ''
            ,'productName'      => ''
            ,'subsType'         => ''
            ,'TransactionId'    => date("YmdHis") . time()
            ,'Signature'        => ''
            ,'code'             => ''
            ,'codeDesc'         => ''
        );
		
        $parseXML = @simplexml_load_string($data);
		
        if($parseXML === false)
        {
            //echo (string)$parseXML->children('http://www.tibco.com/schemas/TEST/Schema2.xsd2')->MSISDN;
            $proceed = false;

            $dataXML['code'] = '4';
            $dataXML['codeDesc'] = 'xml parse error';
        }
        else
        {
            $proceed = true;

            $xmlRoot = 'http://www.tibco.com/schemas/TEST/Schema2.xsd2';

            /*
            $dataXML = array(
            'msisdn' 	=> (string)$parseXML->children($xmlRoot)->MSISDN
            ,'timeStamp' => (string)$parseXML->children($xmlRoot)->timeStamp
            ,'subsChannel' => (string)$parseXML->children($xmlRoot)->subsChannel
            ,'partnerName' => (string)$parseXML->children($xmlRoot)->partnerName
            ,'productName' => (string)$parseXML->children($xmlRoot)->productName
            ,'subsType' => (string)$parseXML->children($xmlRoot)->subsType
            ,'TransactionId' => (string)$parseXML->children($xmlRoot)->TransactionId
            ,'Signature' => (string)$parseXML->children($xmlRoot)->Signature
            ,'reserved1' => (string)$parseXML->children($xmlRoot)->reserved1
            ,'reserved2' => (string)$parseXML->children($xmlRoot)->reserved2
            ,'reserved3' => (string)$parseXML->children($xmlRoot)->reserved3
            ,'statusCode' => (string)$parseXML->children($xmlRoot)->statusCode
            ,'statusDescription' => (string)$parseXML->children($xmlRoot)->statusDescription
            );
            */

            $dataXML['msisdn'] = (string)$parseXML->children($xmlRoot)->MSISDN;
            $dataXML['timeStamp'] = (string)$parseXML->children($xmlRoot)->timeStamp;
            $dataXML['subsChannel'] = (string)$parseXML->children($xmlRoot)->subsChannel;
            $dataXML['channel'] = 'sms';
            $dataXML['partnerName'] = (string)$parseXML->children($xmlRoot)->partnerName;
            $dataXML['productName'] = (string)$parseXML->children($xmlRoot)->productName;
            $dataXML['subsType'] = (string)$parseXML->children($xmlRoot)->subsType;
            $dataXML['TransactionId'] = (string)$parseXML->children($xmlRoot)->TransactionId;
            $dataXML['Signature'] = (string)$parseXML->children($xmlRoot)->Signature;
			
			/* $dataXML['msisdn'] = "62897854321";
            $dataXML['timeStamp'] = "2021-10-27 12:26:47";
            $dataXML['subsChannel'] = "ESME";
            $dataXML['channel'] = 'sms';
            $dataXML['partnerName'] = "GENFLIX1";
            $dataXML['productName'] = "300MB_Genflix1";
            $dataXML['subsType'] = "S3";
            $dataXML['TransactionId'] = "EAI201701311814895";
            $dataXML['Signature'] = "1234"; */

            /*
            $key = '';
            $divider = "-";
            $SignatureMath = 
                md5(
                    $dataXML['msisdn']."{$divider}".$dataXML['subsType']."{$divider}".$dataXML['TransactionId']."{$divider}".$key
                );

            if ($dataXML['Signature'] == $SignatureMath)
            {
                $proceed = false;

                $dataXML['code'] = '3';
                $dataXML['codeDesc'] = 'invalid signature';
            }
            */

            // /if (!in_array($dataXML['subsType'], array('S1')))
            if (!in_array($dataXML['subsType'], array('S1','S3','S7','S7T','S14','S30','S30T','S7F')))
            {
                $proceed = false;

                $dataXML['code'] = '6';
                $dataXML['codeDesc'] = 'invalid subsType (only 1 or 3)';
            }
        }

        if($proceed)
        {
            $params = array();
            $params['sc'] = '99876';
            $params['msisdn'] = $dataXML['msisdn'];

            switch($dataXML['subsType'])
            {
                case 'S1'   : $params['sms'] = 'REG GEMEZZ PS7'; break;
                case 'S3'   : $params['sms'] = 'REG GEMEZZ PS8'; break;
                case 'S7'   : $params['sms'] = 'REG GEMEZZ PS9'; break;
                case 'S7T'  : $params['sms'] = 'REG GEMEZZ PS10'; break;
                case 'S14'  : $params['sms'] = 'REG GEMEZZ PS11'; break;
                case 'S30'  : $params['sms'] = 'REG GEMEZZ PS12'; break;
                case 'S30T' : $params['sms'] = 'REG GEMEZZ PS13'; break;
                case 'S7F'  : $params['sms'] = 'REG GEMEZZ PS14'; break;
            }

            $params['transid'] = $dataXML['trxid'];
            $params['channel'] = $dataXML['channel'];
            $params['trx_time'] = $dataXML['timestamp'];

            $params['bundling'] = true;
            $params['subsType'] = $dataXML['subsType'];
			$params['product_name'] = $dataXML['productName'];

            $log->write(array('level' => 'info', 'message' => "Params : ". serialize($params)));

			require_once("/app/xmp2012/interface/moh3i/config/database.php");
			
			$dbConf = new config_database();
			
            $conn = mysql_connect($dbConf->profile['connDatabase1']['host'],$dbConf->profile['connDatabase1']['username'],$dbConf->profile['connDatabase1']['password']);
            mysql_select_db($dbConf->profile['connDatabase1']['database']);

            //$nonBundlingSubject = array('DAILY','PS','PS1','PS2','PS3','PS4','PS5','PS6','FS','MS','MS1','MS2','MS4','MS5','SPC3','SPC4');
            //$bundlingSubject = array('PS7','PS8','PS9','PS10','PS11','PS12','PS13','PS14','PS15');

            $result = false; //$exceptional_result = false;

            $user = $this->getVSubs($dataXML['msisdn'], $conn);
			
			//print_r($user);
			
            $log->write(array('level' => 'debug', 'message' => "User Data : " . serialize($user)));

            if(count($user) < 1)
            {
                $result = true;
            }
            else
            {
				// OLD : Allow for keyword bundling only
				// NEW : Disable bundling cek, allow all keyword enable active and replace to bundling keyword
				
				/*
                if(in_array((int)$user['active'], array(0,1,2)))
                {					
					
                    if(in_array($user['subject'], $config_main->nonBundlingAllSubject) || in_array($user['subject'], $config_main->bundlingSubject))
                    {
					*/
                        //if((int)$user['active'] == 1 && in_array($user['subject'], $config_main->bundlingSubject))
                        //{
							$log->write(array('level' => 'debug', 'message' => "Request Rereg allowed, before[".$user['subject']."], after[" .$params['sms']."]" ));
							
                            $params['rereg'] = true;
							
							// Result when subject is listed as non bundling subject and current active subs (rereg)
							$this->updateInActiveSubs($dataXML['msisdn'], '0', $conn);
                        //}
 
                        $result = true;
                    //}
                //}
            }

            if($result)
            {
                $dataXML['code'] = '0';
                $dataXML['codeDesc'] = 'success';

                $moProcessor = new manager_mo_processor ( );
                $moProcess = $moProcessor->saveToFile($params);

                if($moProcess == 'NOK')
                {
                    $log->write(array('level' => 'info', 'message' => 'Threshold reached msisdn : ' . $dataXML['msisdn']));

                    /*
                    if(count($user) > 0)
                    {
                        $this->updateInActiveSubs($dataXML['msisdn'], $user['active'], $conn);
                    }
                    */
                    
                    $dataXML['code'] = '9';
                    $dataXML['codeDesc'] = 'threshold';
                }
            }
            else
            {
                $dataXML['code'] = '7';
                $dataXML['codeDesc'] = 'trxid already exist';
            }
        }

        $xml = new DomDocument("1.0", "UTF-8");
        $response = $xml->createElement("response");

        $msisdn_element = $xml->createElement("msisdn");
        $msisdn_element->nodeValue=$dataXML['msisdn'];
        $response->appendChild($msisdn_element);

        $timestamp_element = $xml->createElement("timestamp");
        $timestamp_element->nodeValue=$dataXML['timestamp'];
        $response->appendChild($timestamp_element);

        $subsType_element = $xml->createElement("subsType");
        $subsType_element->nodeValue=$dataXML['subsType'];
        $response->appendChild($subsType_element);

        $channel_element = $xml->createElement("channel");
        $channel_element->nodeValue=$dataXML['subsChannel'];
        $response->appendChild($channel_element);

        $trxid_element = $xml->createElement("trxid");
        $trxid_element->nodeValue=$dataXML['trxid'];
        $response->appendChild($trxid_element);

        $status_element = $xml->createElement("status");
        $status_element = $response->appendChild($status_element);

        $status_element->appendChild($xml->createElement('code',$dataXML['code']));
        $status_element->appendChild($xml->createElement('desc',$dataXML['codeDesc']));

        $xml->appendChild($response);
        $xml->formatOutput = TRUE;

        $output = $xml->saveXML();
        $log->write(array('level' => 'info', 'message' => "Output : ".$output));

		mysql_close($conn);
		
        return $output;
    }

    private function getVSubs($msisdn, $conn) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($msisdn)));

        $sql = "SELECT s.active, ms.subject FROM xmp.subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.msisdn = '" . mysql_real_escape_string($msisdn) . "'";

        $log->write(array('level' => 'debug', 'message' => "SQL : " . $sql));

        $result = mysql_query($sql, $conn);

        if (mysql_num_rows($result) > 0) {
            return mysql_fetch_array($result);
        } else {
            return array();
        }
    }

    private function updateInActiveSubs($msisdn, $active = 0, $conn) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($msisdn)));

        $sql = sprintf("UPDATE xmp.subscription SET active = '%s', time_updated = now() WHERE msisdn = '%s';", mysql_real_escape_string($active), mysql_real_escape_string($msisdn));

        $log->write(array('level' => 'debug', 'message' => "SQL : " . $sql));

        $result = mysql_query($sql, $conn);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}
