<?php
class moh3i_broadcast_contentlabel {
	
	public function execute($params) {
		
		$contentLabel = $params['contentLabel'];
		$price = $params['price'];
		
        $log_profile = 'broadcast';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));

		/* $url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=uxtbgl&passkey=w4k1MS&nohp=6285798614406&pesan=".urlencode("Push Weekly Indosat LinkIT started - $content_label");
		$arrUrl = explode('?',$url);
		$url = $arrUrl[0];
		$prm = isset($arrUrl[1]) ? $arrUrl[1] : '';		
		http_request::get($url, $prm, 30); */
		
        $schedules = $this->populateScheduleByContentLabel($params);
		//print_r($schedules);//die;
        $total = count($schedules);

        $main_config = loader_config::getInstance()->getConfig('main');
        $operator_name = $main_config->operator;

		$db = array(
			'hostname' => 'vasdb.cdzdyl8xksm8.eu-central-1.rds.amazonaws.com',
			'username' => 'linkit',
			'password' => 'l1nk1t360_db',
			'database' => 'xmp');

		$conn = mysql_connect($db['hostname'],$db['username'],$db['password']);
		mysql_select_db($db['database']);

        if ($total > 0) {
            foreach ($schedules as $broadcast_data) {
                $model_schedule = loader_model::getInstance()->load('schedule', 'connBroadcast');

				$broadcast_data->contentLabel = $contentLabel;
				$broadcast_data->status = 1;
				$broadcast_data->price = $price;
                $model_schedule->setStatus($broadcast_data); //set status to 1
		
				//print_r($broadcast_data);
				//echo "\n\r\n\r";
				// HIT PUSH
                $this->pushModif($broadcast_data, $conn);
			
				$broadcast_data->contentLabel = $contentLabel;
                $broadcast_data->status = 2;
				$broadcast_data->price = $price;
                $model_schedule->setStatus($broadcast_data); // set status to 2
            }
        }

		mysql_close($conn);

        return true;
    }
	
	public function populateScheduleByContentLabel($params) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $main_config = loader_config::getInstance()->getConfig('main');
        $operator_name = $main_config->operator;
	
        $model_schedule = loader_model::getInstance()->load('schedule', 'connBroadcast');
        $broadcast = new moh3i_broadcast_data ();

		$contentLabel = $params['contentLabel'];
		$price = $params['price'];
		
        $broadcast->operator = $operator_name;
        $broadcast->status = 0;
        $broadcast->contentLabel = $contentLabel;
		$broadcast->price = $price;
        $data = $model_schedule->getByContentLabel($broadcast);

		//print_r($data);
        //create object broadcast data
        $broadcast_data = array();
        if (!empty($data)) {
            foreach ($data as $val) {
                $dt = new moh3i_broadcast_data ();

                $dt->id = $val ['id'];
                $dt->service = $val ['service'];
                $dt->operator = $val ['operator'];
                $dt->adn = $val ['adn'];
                $dt->recurringType = $val ['recurring_type'];
                $dt->handlerFile = $val ['handlerfile'];
                $dt->pushTime = $val ['push_time'];
                $dt->status = $val ['status'];
                $dt->contentLabel = $val ['content_label'];
                $dt->contentSelect = $val ['content_select'];
                $dt->lastContentId = $val ['last_content_id'];
                $dt->price = $val ['price'];
				
				/* if(strpos($val ['notes'], ",") !== FALSE)
				{
					$notes = explode(",", $val ['notes']);
					foreach($notes as $note)
						$dt->notes .= "'" . $note . "',";
						
					$dt->notes = rtrim($dt->notes, ",");
				}
				else $dt->notes = $val ['notes']; */
					
				$dt->notes = $val ['notes'];
                $dt->created = $val ['created'];
                $dt->modified = $val ['modified'];
				//$dt->amount_prioritize = $val ['amount_prioritize'];
                $broadcast_data [] = $dt;
            }
        }
        return $broadcast_data;
    }
	
	/**
	 * @param $broadcast_data
	 */
	public function pushModif(moh3i_broadcast_data $broadcast_data, $conn) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		//if($broadcast_data->contentLabel <> 'retry_monthly')
		//{
			$content_manager = content_manager::getInstance ();
			$broadcast_content = $content_manager->getBroadcastContent ( $broadcast_data );

			if (empty($broadcast_content)) {
				return false;
			}
		//}
		
		$model_operator = loader_model::getInstance ()->load ( 'operator', 'connDatabase1' );

		$users = $this->populateUserByContentLabel ( $broadcast_data );
		//print_r($users);die;

		if ($users !== false) {
			$operator_name = $broadcast_data->operator;
			$operator_id = $model_operator->getOperatorId ( $operator_name );
		
			//print_r($users);	
			$pushproject_data = new model_data_pushproject ();
			$pushproject_data->sid = $broadcast_data->id;
			$pushproject_data->src = $broadcast_data->adn;
			$pushproject_data->oprid = $operator_id;
			$pushproject_data->service = $broadcast_data->service;
			
			//SET SUBJECT KEYWORD
			$pushproject_data->subject = $this->setSubject($broadcast_data->contentLabel);
				
			$pushproject_data->message = serialize($broadcast_content);
			$pushproject_data->price = $broadcast_data->price;
			$pushproject_data->amount = 0;
			
			$mPushProject = loader_model::getInstance ()->load ( 'pushproject', 'connBroadcast' );
			$pid = $mPushProject->save ( $pushproject_data );
			//echo "base populateSave \n";
			
			$amount = 0; //$ren_daily = 0; $ren_monthly = 0; $ret_monthly = 0;

			foreach ( $users as $users_data ) {
				
				if(!$this->isBlacklisted($users_data ['msisdn']))
				{
					$pushbuffer_data = new model_data_pushbuffer ();
					$pushbuffer_data->pid = $pid;
					$pushbuffer_data->src = $broadcast_data->adn;
					$pushbuffer_data->dest = $users_data ['msisdn'];
					$pushbuffer_data->oprid = $operator_id;
					$pushbuffer_data->service = $broadcast_data->service;
					
					//SET SUBJECT KEYWORD
					//$pushbuffer_data->subject = $this->setSubject($broadcast_data->contentLabel) . (($users_data ['subject'] <> 'DAILY') ? ';' . $users_data ['subject'] : '') . ((trim($users_data ['s2']) <> '') ? ';' . $users_data ['s2'] : '');
					
					// Disable sub2 keyword
					//$pushbuffer_data->subject = $this->setSubject($broadcast_data->contentLabel) . (($users_data ['subject'] <> '') ? ';' . $users_data ['subject'] : '') . ((trim($users_data ['s2']) <> '') ? ';' . $users_data ['s2'] : '');
					
					$pushbuffer_data->subject = $this->setSubject($broadcast_data->contentLabel) . (($users_data ['subject'] <> '') ? ';' . $users_data ['subject'] : '');
					
					if(strtoupper($users_data ['subject']) == 'PS7')
					{
						$ps7_hardcode_content = array(
							"failed" 	=> 'Pulsa kamu tidak cukup'
						   //,"success" 	=> 'selamat kamu dapat akses gratis layanan gemezz 1 hari. join kompetisi dapatkan hadiah grandprize sebuah mobil s/d (tanggal masa aktif bundling) di tri.gemezz.mobi. tarif data berlaku.'
						   ,"success" 	=> 'Gemezz + kuota 100MB telah aktif. Join kompetisi di tri.gemezz.mobi dan dapatkan hadiah grandprize sebuah mobil s/d 17 Januari 2022 dari LinkIT. Berhenti Berlangganan STOP GEMEZZ100AR ke 234. CS: 089655913961'
						);
						$pushbuffer_data->message = serialize($ps7_hardcode_content);
					} 
					else
					{
						$pushbuffer_data->message = serialize($broadcast_content);
					}
					
					$pushbuffer_data->price = $broadcast_data->price;
					$pushbuffer_data->stat = "ON_QUEUE";
					$pushbuffer_data->tid = date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) );
					$pushbuffer_data->thread_id = substr ( $pushbuffer_data->dest, strlen ( $pushproject_data->dest ) - 1, 1 );
					
					$mt_data = $this->createModifyMT ( $pushbuffer_data, $broadcast_data, $users_data, $conn );
					
					$pushbuffer_data->obj = serialize ( $mt_data );

					$pushbuffer_data->priority = $users_data ['priority'];

					$tbl_push_buffer = 'push_buffer';
					
					$success = (int)$users_data ['success'];

					if($success > 0 && $pushbuffer_data->priority == 1)
					{
						$pushbuffer_data->priority = 1;

						$pushbuffer_data->tablename = $tbl_push_buffer . "_priority";
					}
					else
					{
						// OVERRIDE priority even priority = 1 and success = 0 then priority will always set to = 0
						$pushbuffer_data->priority = 0;

						// FEATURE :
						// 1. SUCCESS COUNT = 0, 
						// 2. PRIORITY = 0, AND
						// 3. CONSECUTIVE CHARGE FAILED MORE THAN 7 DAY IN RENEWAL DAILY & RETRY MONTHLY (H-1 FAILED)
						
						if (in_array($broadcast_data->contentLabel, array('renewal_daily','retry_monthly')))
						{
							list($latest_date, $latest_time) = explode(" ", $users_data ['time_updated']);
							$latest_date = trim($latest_date);

							$datetime1 = new DateTime($latest_date); 
							$datetime2 = new DateTime(); // CUR DATE
							
							$difference = $datetime1->diff($datetime2); 

							$diff = $difference->format('%a'); 

							if($success < 1 && (int)$diff > 7)
							{
								$pushbuffer_data->tablename = $tbl_push_buffer . "_nonpriority_more7day";
							}
							else
							{
								$pushbuffer_data->tablename = $tbl_push_buffer . "_nonpriority";
							}
						}
						else
						{
							$pushbuffer_data->tablename = $tbl_push_buffer . "_nonpriority";
						}
					}
					
					$mPushBuffer = loader_model::getInstance ()->load ( 'pushbuffer', 'connBroadcast' );
					
					if ($mPushBuffer->save ( $pushbuffer_data )) {
						$amount ++;
					}
				}
				//print_r($pushbuffer_data);die;
			}
			
			$pushproject_data = new model_data_pushproject ();
			$pushproject_data->pid = $pid;
			$pushproject_data->amount = $amount;
			$mPushProject->update ( $pushproject_data );
		}

		return true;
	}
	
	/**
	 * @param $broadcast_data
	 * @param $user_data
	 */
	public function createModifyMT(model_data_pushbuffer $pushbuffer_data, moh3i_broadcast_data $broadcast_data, $users_data, $conn) {

		$loader_config = loader_config::getInstance();
		$configMain = $loader_config->getConfig('main');

		$log = manager_logging::getInstance ();
		
		$log->write ( array ('level' => 'debug', 'message' => "Start " . serialize($broadcast_data) ) );
		
		$mt_data = loader_data::get ( 'mt' );
		$mt_data->inReply = NULL;
		$mt_data->msgId = date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) );
		$mt_data->adn = $pushbuffer_data->src;
		$mt_data->msgData = $pushbuffer_data->message;
		$mt_data->price = $pushbuffer_data->price;
		$mt_data->operatorId = $pushbuffer_data->oprid;
		$mt_data->channel = "sms";
		$mt_data->service = $pushbuffer_data->service;
		$mt_data->subject = $pushbuffer_data->subject;
		$mt_data->operatorName = $broadcast_data->operator;
		$mt_data->msisdn = $pushbuffer_data->dest;
		$mt_data->type = ((strpos(strtoupper($broadcast_data->notes),'DAILY') !== FALSE) ? 'daily' : 'monthly'); // daily | monthly
		$mt_data->msisdn_subject = (($users_data['subject'] <> 'DAILY') ? $users_data['subject'] : '');
		$mt_data->s2 = trim($users_data['s2']);

		//$bundlingSubject = array('PS7','PS8','PS9','PS10','PS11','PS12','PS13','PS14');

		if(in_array($users_data['subject'], $configMain->bundlingSubject))
		{
			/*
			list($sudate, $sutime) = explode(" ", $users_data['subscribed_until']);
			
			list($suy, $sum, $sud) = explode("-", $sudate);
			list($suh, $sui, $sus) = explode("-", $sutime);

			$mt_data->subscribe_until = date('Y-m-d H:i:s', mktime($suh,$sui,$sus,$sum,($sud-90),$suy));
			*/
			$subscribed_until = $users_data['subscribed_until'];

			if(!empty($subscribed_until))
			{
				$getVSubs = $this->getVSubs($pushbuffer_data->dest, $conn);
				$subscribed_until = $getVSubs['subscribed_until'];
			}

			list($sudate, $sutime) = explode(" ", $subscribed_until);

			$val_90day = 90;
			
			$dateG=date_create($sudate);
			date_add($dateG,date_interval_create_from_date_string("-{$val_90day} days"));
			$mt_data->subscribe_until = date_format($dateG,"Y-m-d") . " " . $sutime;
		}
		
		return $mt_data;
	}
	
	/**
	 * @param $broadcast_data
	 */
	public function populateUserByContentLabel(moh3i_broadcast_data $broadcast_data) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$model_user = loader_model::getInstance ()->load ( 'user', 'connDatabase1' );
		
		switch($broadcast_data->contentLabel){
			
			case "renewal_daily" : 
				$users = $model_user->getRenewalDaily ( $broadcast_data );
			break;
			
			case "renewal_monthly" : 
				$users = $model_user->getRenewalMonthly ( $broadcast_data );
			break;
			
			case "active_until"	: 
				$users = $model_user->getActiveUntil ( $broadcast_data );
			break;
			
			case "retry_monthly" : 
				$users = $model_user->getRetryMonthly ( $broadcast_data );
			break;
			
			case "180dayM" : 
				$users = $model_user->get180dayM ( $broadcast_data );
			break;
			
			case "180dayD" : 
				$users = $model_user->get180dayD ( $broadcast_data );
			break;
		}
		
		return $users;
	}
	
	/**
	 * @param $broadcast_data
	 */
	public function isBlacklisted($msisdn) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$model_user = loader_model::getInstance ()->load ( 'user', 'connDatabase1' );
		
		return $model_user->isBlacklisted ( $msisdn );
	}
	
	public function setSubject($label)
	{
		switch($label){
			
			case "renewal_daily" 	: $subject = strtoupper ( "MT;CHARGE;SMS;RENEWAL_DAILY" ); break;
			case "renewal_monthly" 	: $subject = strtoupper ( "MT;CHARGE;SMS;RENEWAL_MONTHLY" ); break;
			case "active_until" 	: $subject = strtoupper ( "MT;CHECK;SMS;ACTIVE_UNTIL" ); break;
			case "retry_monthly" 	: $subject = strtoupper ( "MT;CHARGE;SMS;RETRY_MONTHLY" ); break;
			case "180dayM" 			: $subject = strtoupper ( "MT;NOTIF;SMS;180DAYM" ); break;
			case "180dayD" 			: $subject = strtoupper ( "MT;NOTIF;SMS;180DAYD" ); break;
		}
		
		return $subject;
	}

	private function getVSubs($msisdn, $conn) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($msisdn)));

        $sql = "SELECT s.subscribed_until, s.active, ms.subject FROM xmp.subscription s INNER JOIN msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.msisdn = '" . mysql_real_escape_string($msisdn) . "'";

        $log->write(array('level' => 'debug', 'message' => "SQL : " . $sql));

        $result = mysql_query($sql, $conn);

        if (mysql_num_rows($result) > 0) {
            return mysql_fetch_array($result);
        } else {
            return array();
        }
    }
}
