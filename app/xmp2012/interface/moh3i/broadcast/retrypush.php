<?php
class moh3i_broadcast_retrypush {
	
	public $startBackup;
	public $endBackup;
	public $startRetry;
	public $endRetry;
	public $startFixing;
	public $endFixing;
	
	public function __construct()
	{
		// RETRY
		$this->startRetry = strtotime("00:00:00");
		$this->endRetry = strtotime("23:30:59");
		// FIX REPORT
		$this->startFixing = strtotime("01:25:00");
		$this->endFixing = strtotime("01:40:00");
		//$this->startFixing = strtotime("14:25:00");
		//$this->endFixing = strtotime("15:40:00");
	}
	
	//Retry Timeout file base H3I
	public function retryTimeoutFileBase($params)
	{
		$allowed_retry = $params['retry_attempt'];
		$tablename = $params['tablename'];

		$allowed_retry = (!empty($allowed_retry) ? $allowed_retry : 1);

		$lockPath = '/tmp/reserve_moh3i_broadcast_retry_filebase_timeout_'.$tablename;

		$pushretry_model = loader_model::getInstance ()->load ( 'pushretry', 'connDatabase1' );

		$ri = 0;
		$priority = 3;
		
		$checkRetry = $pushretry_model->qSql("SELECT stat, COUNT(1) as total FROM dbpush.{$tablename} WHERE priority >= {$priority} AND DATE(created) = CURRENT_DATE AND stat IN ('ON_QUEUE', 'PUSHED') GROUP BY stat ORDER BY stat ASC");

		if(count($checkRetry) < 1)
		{
			$ri = 1; // means 1st time retry
			unlink($lockPath);
		}
		else
		{
			$arr_stat = array("ON_QUEUE");
			
			for($r=0; $r<count($checkRetry); $r++)
			{
				if(!in_array($checkRetry[$r]['stat'], $arr_stat))
				{
					$riD = @file_get_contents($lockPath, true);
					list($dateR, $ri) = explode("|", $riD);

					$ri = (int)$ri + 1; 
					$priority = ($priority + $ri) - 1;
					
					if($ri <= $allowed_retry)
					{
						unlink($lockPath);
					}

					break;
				}
			}
		}

		if(file_exists($lockPath)) {
			echo "NOK - Lock File Exist on $lockPath \n";
			exit;
		} else {
			touch($lockPath);
			chmod($lockPath, 0777);
			file_put_contents($lockPath, date("Ymd") . "|" . $ri);
		}

		$logging = "dev";

		$log_profile = 'broadcast';
		$log = manager_logging::getInstance();
		$log->setProfile($log_profile);
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );

		$arrpath = array(
			"/DATA/app/xmp2012/logs/moh3i/retry/" . date("Ymd") . "_200_3_109.txt",
			"/DATA/app/xmp2012/logs/moh3i/retry/" . date("Ymd") . "_400.txt",
			"/DATA/app/xmp2012/logs/moh3i/retry/" . date("Ymd") . "_5012.txt",
			//"/DATA/app/xmp2012/logs/moh3i/retry/" . date("Ymd") . "_4012.txt",
		);

		foreach($arrpath as $path)
		{
			$handle = @fopen($path, "r");

			if ($handle) 
			{
				@rename($path, $path . ".retry" . $ri);

				$x=0; $msisdns = "";

				while (($buffer = fgets($handle, 4096)) !== false) 
				{
					$log = trim($buffer);
					$arr_msisdn = explode("/", str_replace(".lock","",$log));

					$msisdn = $arr_msisdn[count($arr_msisdn)-1];
					
					// Temporary Feature
					// START {
						
					$checkRC_5030_4010_200_3_3 = $pushretry_model->qSql("SELECT msisdn FROM xmp.rc_takeout_5030_4010_200_3_3 WHERE msisdn = {$msisdn} LIMIT 1");
					
					if(empty($checkRC_5030_4010_200_3_3[0]['msisdn']))
					{
						$msisdns .= $msisdn . ",";
					}
					
					// END }
					
					//$msisdns .= $msisdn . ",";
	
					// Remove locking file by msisdn
					unlink($log);

					if($x >= 2000 && !empty($msisdns))
					{
						$msisdns = rtrim($msisdns, ",");
					
						$uSQL = sprintf("UPDATE dbpush.{$tablename} SET stat = '%s', priority = '%s' WHERE DATE(created) = '%s' AND dest IN (%s);", "ON_QUEUE", $priority, date("Y-m-d"), $msisdns);

						echo $uSQL." ...\n\r\n\r";

						$pushretry_model->eSql($uSQL);

						$msisdns = "";
						$x=0;
					}

					$x++;
				}

				if(!empty($msisdns))
				{
					$msisdns = rtrim($msisdns, ",");
					
					$uSQL = sprintf("UPDATE dbpush.{$tablename} SET stat = '%s', priority = '%s' WHERE DATE(created) = '%s' AND dest IN (%s);", "ON_QUEUE", $priority, date("Y-m-d"), $msisdns);

					echo $uSQL." ...\n\r\n\r";

					$pushretry_model->eSql($uSQL);
					
					//$log->write ( array ('level' => 'debug', 'message' => "uSQL : " . $uSQL ) );

					$msisdns = "";
				}

				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}

				fclose($handle);
			}
		}

		//unlink($lockPath);

		return true;
	}

	//Retry Timeout H3I
	public function retryTimeout($limit = 50, $bulk = 10, $logging = "prod")
	{
		$now = time();
		
		if($now >= $this->startRetry && $now <= $this->endRetry)
		{
			$sdate = date("Y-m-d") . " 00:00:00";
			$edate = date("Y-m-d") . " 23:30:59";
			$closereason = "'200_3_109','200_3_4'";
			$subject = "'MT;CHARGE;SMS;RETRY_MONTHLY;SPC4','MT;CHARGE;SMS;RETRY_MONTHLY;SPC3','MT;CHARGE;SMS;RETRY_MONTHLY;MS;SPC2','MT;CHARGE;SMS;RETRY_MONTHLY;MS;SPC','MT;CHARGE;SMS;RETRY_MONTHLY;MS','MT;CHARGE;SMS;RENEWAL_MONTHLY;MS;SPC2','MT;CHARGE;SMS;RENEWAL_MONTHLY;MS;SPC','MT;CHARGE;SMS;RENEWAL_MONTHLY;MS','MT;CHARGE;SMS;RENEWAL_MONTHLY;SPC4','MT;CHARGE;SMS;RENEWAL_MONTHLY;SPC3','MT;CHARGE;SMS;RENEWAL_DAILY;PS6','MT;CHARGE;SMS;RENEWAL_DAILY;PS5','MT;CHARGE;SMS;RENEWAL_DAILY;PS4','MT;CHARGE;SMS;RENEWAL_DAILY;PS3','MT;CHARGE;SMS;RENEWAL_DAILY;PS1;SPC','MT;CHARGE;SMS;RENEWAL_DAILY;PS','MT;CHARGE;SMS;RENEWAL_DAILY;PS7','MT;CHARGE;SMS;RENEWAL_DAILY;PS8','MT;CHARGE;SMS;RENEWAL_DAILY;PS9','MT;CHARGE;SMS;RENEWAL_DAILY;PS10','MT;CHARGE;SMS;RENEWAL_DAILY;PS11','MT;CHARGE;SMS;RENEWAL_DAILY;PS12','MT;CHARGE;SMS;RENEWAL_DAILY;PS13','MT;CHARGE;SMS;RENEWAL_DAILY;PS14','MT;CHARGE;SMS;RENEWAL_DAILY;PS15','MT;CHARGE;SMS;RENEWAL_DAILY;DAILY'";

			$log_profile = 'broadcast';
			$log = manager_logging::getInstance();
			$log->setProfile($log_profile);
			$log->write(array('level' => (($logging == "prod") ? "debug" : "info"), 'message' => "Start"));
		
			$pushretry_model = loader_model::getInstance ()->load ( 'pushretry', 'connDatabase1' );

			$sql = sprintf("SELECT msisdn, subject FROM xmp.tbl_msgtransact WHERE msgtimestamp BETWEEN '%s' AND '%s' and closereason in (%s) and subject in (%s) LIMIT %d;", $sdate, $edate, $closereason, $subject, $limit);

			echo $sql." ...\n\r\n\r";

			$log->write(array('level' => (($logging == "prod") ? "debug" : "info"), 'message' => "SQL : " . $sql));

			$trxs = $pushretry_model->qSql($sql);

			$x = 0; $msisdns = ""; 

			$base_msisdn_lock = "/DATA/app/xmp2012/logs/moh3i/lock/DP_" . date("Ymd");

			for($i=0; $i<count($trxs); $i++)
			{
				list($init, $charge, $sms, $subject, $msisdn_subject, $s2) = explode(";", strtolower($trxs[$i]['subject']));

				$msisdn_subject_folder = (!empty($msisdn_subject) ? "/" . strtoupper($msisdn_subject) : "") . (!empty($s2) ? "_" . strtoupper($s2) : "");

				$msisdn_lock = $base_msisdn_lock . "/" . $subject . $msisdn_subject_folder . "/" . $trxs[$i]['msisdn'] . ".lock";
				
				unlink($msisdn_lock);

				$msisdns .= $trxs[$i]['msisdn'] . ",";
	
				if($x >= $bulk && !empty($msisdns))
				{
					$msisdns = rtrim($msisdns, ",");
					
					//$dSQL = sprintf("DELETE FROM xmp.tbl_msgtransact WHERE msgtimestamp BETWEEN '%s' AND '%s' AND msisdn IN (%s) AND subject in (%s);", $sdate, $edate, $msisdns, $subject);

					//echo $dSQL." ...\n\r\n\r";

					//$pushretry_model->eSql($dSQL);

					//$log->write(array('level' => (($logging == "prod") ? "debug" : "info"), 'message' => "dSQL : " . $dSQL));

					$uSQL = sprintf("UPDATE dbpush.push_buffer SET stat = '%s' WHERE created BETWEEN '%s' AND '%s' AND dest IN (%s) AND subject in (%s);", "ON_QUEUE", $sdate, $edate, $msisdns, $subject);

					echo $uSQL." ...\n\r\n\r";

					$pushretry_model->eSql($uSQL);
					
					$log->write(array('level' => (($logging == "prod") ? "debug" : "info"), 'message' => "uSQL : " . $uSQL));

					$msisdns = "";
					$x=0;
				}
				
				$x++;
			}

			if(!empty($msisdns))
			{
				$msisdns = rtrim($msisdns, ",");
					
				//$dSQL = sprintf("DELETE FROM xmp.tbl_msgtransact WHERE msgtimestamp BETWEEN '%s' AND '%s' AND msisdn IN (%s) AND subject in (%s);", $sdate, $edate, $msisdns, $subject);

				//echo $dSQL." ...\n\r\n\r";

				//$pushretry_model->eSql($dSQL);

				//$log->write(array('level' => (($logging == "prod") ? "debug" : "info"), 'message' => "dSQL : " . $dSQL));

				$uSQL = sprintf("UPDATE dbpush.push_buffer SET stat = '%s' WHERE created BETWEEN '%s' AND '%s' AND dest IN (%s) AND subject in (%s);", "ON_QUEUE", $sdate, $edate, $msisdns, $subject);

				echo $uSQL." ...\n\r\n\r";

				$pushretry_model->eSql($uSQL);
				
				$log->write(array('level' => (($logging == "prod") ? "debug" : "info"), 'message' => "uSQL : " . $uSQL));

				$msisdns = "";
			}
		}
		
		return true;
	}

	//BACKUP DATA TRX
	public function pushRetryTransactBackup()
	{
		$log_profile = 'broadcast';
		$log = manager_logging::getInstance();
		$log->setProfile($log_profile);
		$log->write(array('level' => 'debug', 'message' => "Start"));
	
		$pushretry_model = loader_model::getInstance ()->load ( 'pushretry', 'connDatabase1' );
		
		$master_trx_backup_tbl = "transact_pushbackup";
		$trx_backup_tbl = $master_trx_backup_tbl . "_" . date("Ym");
		
		$checking = $pushretry_model->qSql("SHOW TABLES LIKE '{$trx_backup_tbl}';");
		
		if(count($checking) < 1)
			$pushretry_model->eSql("CREATE TABLE $trx_backup_tbl LIKE {$master_trx_backup_tbl};");
		
		$arrObj = array();
		$arrObj['table'] = $trx_backup_tbl;
		$arrObj['subject'] = 'DAILYPUSH';
		//$arrObj['closereason'] = '4';
		//$arrObj['msgstatus'] = 'FAILED';
		//$arrObj['msisdn'] = '6281510808873';
		$arrObj['randomize'] = false; // random it if limit applied
		$arrObj['limit'] = -1; // -1 for unlimited
		
		$pushretry_model->transactPushBackup($arrObj);
		return true;
	}
	
	// FLOW STEP CRON 1
	// **
	/*
	1. get negatif trx ( closereason  = 4, status = failed )
	2. check flag on table current date is retry already done or still running
	3. when empty or done running, empty the temporary table trx retry
	4. insert all trx to temp table
	5. delete all msisdn corresponding to the fetched data
	6. enable msisdn on push buffer to on_queue
	7. when still running then hold this code up
	*/
	// **
	
	public function retryInsufficientBalance()
	{
		$date = date("Y-m-d");
		$now = time();
		
		if($now >= $this->startRetry && $now <= $this->endRetry)
		{
			$log_profile = 'broadcast';
			$log = manager_logging::getInstance();
			$log->setProfile($log_profile);
			$log->write(array('level' => 'debug', 'message' => "Start"));
		
			$pushretry_model = loader_model::getInstance ()->load ( 'pushretry', 'connDatabase1' );
			
			$arrObj = array();
			$arrObj['subject'] = 'DAILYPUSH';
			$arrObj['closereason'] = '4';
			$arrObj['msgstatus'] = 'FAILED';
			//$arrObj['msisdn'] = '6281510808873';
			$arrObj['randomize'] = true; // random it if limit applied
			$arrObj['limit'] = 300; // -1 for unlimited
			
			$msisdns = $pushretry_model->getMsisdnNegatifStatus($arrObj);
			$cnt = count($msisdns);
			//echo $cnt;
			//die;
			if($cnt > 0){
				
				$tableFlag = 'dbpush.push_retry_schedule';
				
				if($pushretry_model->flagChecking($tableFlag, $date, 'RUNNING'))
				{
					return false;
				}
				else
				{
					if($pushretry_model->flagChecking($tableFlag, $date)){
						$pushretry_model->updateFlag($tableFlag, 'RUNNING');
					}
					else
					{
						$pushretry_model->insertFlag($tableFlag, $date, 'RUNNING');
						$pushretry_model->emptyTable('xmp.trx_retry');
					}
					
					for($i=0; $i<count($msisdns); $i++)
					{
						$data = array(
							"trxid"			=> $msisdns[$i]['ID']
						   ,"IN_REPLY_TO"	=> $msisdns[$i]['IN_REPLY_TO']
						   ,"MSGTIMESTAMP"	=> $msisdns[$i]['MSGTIMESTAMP']
						   ,"ADN"			=> $msisdns[$i]['ADN']
						   ,"MSISDN"		=> $msisdns[$i]['MSISDN']
						   ,"OPERATORID"	=> $msisdns[$i]['OPERATORID']
						   ,"MSGDATA"		=> $msisdns[$i]['MSGDATA']
						   ,"MSGLASTSTATUS"	=> $msisdns[$i]['MSGLASTSTATUS']
						   ,"MSGSTATUS"		=> $msisdns[$i]['MSGSTATUS']
						   ,"CLOSEREASON"	=> $msisdns[$i]['CLOSEREASON']
						   ,"SERVICEID"		=> $msisdns[$i]['SERVICEID']
						   ,"MEDIA"			=> $msisdns[$i]['MEDIA']
						   ,"CHANNEL"		=> $msisdns[$i]['CHANNEL']
						   ,"SERVICE"		=> $msisdns[$i]['SERVICE']
						   ,"PARTNER"		=> $msisdns[$i]['PARTNER']
						   ,"SUBJECT"		=> $msisdns[$i]['SUBJECT']
						   ,"PRICE"			=> $msisdns[$i]['PRICE']
						   ,"ISR"			=> $msisdns[$i]['ISR']
						   ,"STATUSRETRY"	=> '0'
						);
						
						$arr = array(
							"limit"			=> 1
						   ,"status_retry"	=> 0
						   ,"date"			=> date("Y-m-d")
						   ,"msisdn"		=> $data['MSISDN']
						);
						$cnt = count($pushretry_model->trxRetry($arr));
						
						if($cnt < 1)
							$pushretry_model->pushTrxRetry($data);
						
						//echo $trxid . "\n\r";
						//$pushretry_model->deleteMsisdnWithSystemErrorByID($data['trxid']);
						
						$sql = "DELETE FROM xmp.tbl_msgtransact WHERE DATE(msgtimestamp) = CURRENT_DATE AND ID = '".$data['trxid']."'";
						$pushretry_model->eSql($sql);
					}
					
					for($i=0; $i<count($msisdns); $i++)
					{
						$msisdn = $msisdns[$i]['MSISDN'];
						//echo $msisdn . "\n\r";
						//$pushretry_model->updateToOnQueueBuffer($msisdn);
						
						$sql = "UPDATE dbpush.push_buffer SET stat = 'ON_QUEUE' WHERE subject LIKE '%DAILYPUSH%' AND DATE(created) = CURRENT_DATE AND dest = '{$msisdn}'";
						$pushretry_model->eSql($sql);
					}
					
					return true;
				}
			}
			else { return false; }
			
		}else return false;
	}
	
	// FLOW STEP CRON 2
	// **
	/*
	1. check all msisdn on temp table is all flag update to '1' ( done processed ), while msisdn updated on dr processor to update to temp table msisdn by msisdn
	2. when already processed then flag table write to 'done'
	*/
	// **
	
	public function checkRetryInsufficientBalance()
	{
		$now = time();
		
		if($now >= $this->startRetry && $now <= $this->endRetry)
		{
			$log_profile = 'broadcast';
			$log = manager_logging::getInstance();
			$log->setProfile($log_profile);
			$log->write(array('level' => 'debug', 'message' => "Start"));
		
			$pushretry_model = loader_model::getInstance ()->load ( 'pushretry', 'connDatabase1' );
			
			$data = array(
							"limit"			=> -1
						   ,"status_retry"	=> 0
						   ,"date"			=> date("Y-m-d")
						);
			$cnt = count($pushretry_model->trxRetry($data));
			
			if($cnt < 1)
			{
				if($pushretry_model->updateFlag('dbpush.push_retry_schedule','DONE')) return true;
				else return false;
			}
			else return false;
		}
		else return false;
	}
	
	// FLOW STEP CRON 3
	// **
	/*
	1. create flag to prevent multiple process
	2. truncate trx temp backup table
	3. compare data trx temp table when not in the transact data in H-1 data
		sql = select * from trx_retry where msisdn not in (select msisdn from tbl_msgtransact where date(msgtimestamp) = '2018-09-18');
	4. insert to trx temp backup table from selected data from poin 2
		sql = insert into trx_retry_temp select id,in_reply_to,msgindex,msgtimestamp,adn,msisdn,operatorid,msgdata,msglaststatus,msgstatus,closereason,serviceid,media,channel,service,partner,subject,price,isr from trx_retry where msisdn not in (select msisdn from tbl_msgtransact where date(msgtimestamp) = '2018-09-18');
	5. insert into transact production from trx temp backup table
		sql = insert into tbl_msgtransact (in_reply_to,msgindex,msgtimestamp,adn,msisdn,operatorid,msgdata,msglaststatus,msgstatus,closereason,serviceid,media,channel,service,partner,subject,price,isr) select in_reply_to,msgindex,msgtimestamp,adn,msisdn,operatorid,msgdata,msglaststatus,msgstatus,closereason,serviceid,media,channel,service,partner,subject,price,isr from trx_retry_temp;
	*/
	// **
	
	public function autoFixReportAfterPushRetry()
	{
		$now = time();
		$date = date('Y-m-d');
		$H_min1Day = date('Y-m-d',strtotime("-1 days"));
		
		if($now >= $this->startFixing && $now <= $this->endFixing)
		{
			$log_profile = 'broadcast';
			$log = manager_logging::getInstance();
			$log->setProfile($log_profile);
			$log->write(array('level' => 'debug', 'message' => "Start"));
		
			$pushretry_model = loader_model::getInstance ()->load ( 'pushretry', 'connDatabase1' );
			
			$tableFlag = 'xmp.auto_fix_report_after_retry';
			
			if($pushretry_model->flagChecking($tableFlag, $date, 'FIXED'))
			{
				return false;
			}
			else
			{
				$pushretry_model->insertFlag($tableFlag, $date, 'FIXED');
				
				$pushretry_model->emptyTable('xmp.trx_retry_temp');
				
				$master_trx_backup_tbl = "transact_pushbackup";
				$trx_backup_tbl = $master_trx_backup_tbl . "_" . date("Ym");
				//echo $trx_backup_tbl."\n\n";
				$checking = $pushretry_model->qSql("SHOW TABLES LIKE '{$trx_backup_tbl}';");
		
				if(count($checking) < 1){
					$trx_backup_tbl = $master_trx_backup_tbl . "_" . date("Ym", strtotime("-1 month"));
					//$pushretry_model->eSql("CREATE TABLE {$trx_backup_tbl} LIKE {$master_trx_backup_tbl};");
				}
				
				//$sql = "SELECT * FROM xmp.trx_retry WHERE msisdn NOT IN (SELECT msisdn FROM xmp.tbl_msgtransact WHERE DATE(msgtimestamp) = '{$H_min1Day}' AND subject like '%DAILYPUSH%');";
				
				$sql = "SELECT * FROM {$trx_backup_tbl} WHERE DATE(msgtimestamp) = '{$H_min1Day}' AND LEFT(subject, 7) = 'MT;PUSH' AND SPLIT_STRING(subject,';',4) = 'DAILYPUSH' AND msisdn NOT IN (SELECT msisdn FROM xmp.tbl_msgtransact WHERE DATE(msgtimestamp) = '{$H_min1Day}' and left(subject, 7) = 'MT;PUSH' AND SPLIT_STRING(subject,';',4) = 'DAILYPUSH');";
				//echo $sql."\n\n";
				$compare = $pushretry_model->qSql($sql);
				
				if(count($compare) > 0)
				{
					//$sql = "INSERT INTO xmp.trx_retry_temp SELECT id,in_reply_to,msgindex,msgtimestamp,adn,msisdn,operatorid,msgdata,msglaststatus,msgstatus,closereason,serviceid,media,channel,service,partner,subject,price,isr FROM xmp.trx_retry WHERE msisdn NOT IN (SELECT msisdn FROM xmp.tbl_msgtransact WHERE DATE(msgtimestamp) = '{$H_min1Day}' AND subject like '%DAILYPUSH%');";
					
					$sql = "INSERT INTO xmp.trx_retry_temp SELECT id,in_reply_to,msgindex,msgtimestamp,adn,msisdn,operatorid,msgdata,msglaststatus,msgstatus,closereason,serviceid,media,channel,service,partner,subject,price,isr FROM {$trx_backup_tbl} WHERE DATE(msgtimestamp) = '{$H_min1Day}' AND LEFT(subject, 7) = 'MT;PUSH' AND SPLIT_STRING(subject,';',4) = 'DAILYPUSH' AND msisdn NOT IN (SELECT msisdn FROM xmp.tbl_msgtransact WHERE DATE(msgtimestamp) = '{$H_min1Day}' and left(subject, 7) = 'MT;PUSH' AND SPLIT_STRING(subject,';',4) = 'DAILYPUSH');";
					//echo $sql."\n\n";
					$exe = $pushretry_model->eSql($sql);
					
					if($exe)
					{
						$sql = "INSERT INTO xmp.tbl_msgtransact (in_reply_to,msgindex,msgtimestamp,adn,msisdn,operatorid,msgdata,msglaststatus,msgstatus,closereason,serviceid,media,channel,service,partner,subject,price,isr) SELECT in_reply_to,msgindex,msgtimestamp,adn,msisdn,operatorid,msgdata,msglaststatus,msgstatus,closereason,serviceid,media,channel,service,partner,subject,price,isr FROM xmp.trx_retry_temp;";
						//echo $sql;
						$pushretry_model->eSql($sql);
					}
					
					return true;
				}
				else return false;
			}
			
		}
		else return false;
	}
}
