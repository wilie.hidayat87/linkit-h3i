<?php
class moh3i_broadcast_sentcontent {
	
	public function process(){
    	//test
        $log_profile = 'broadcast';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $config_main = loader_config::getInstance ()->getConfig('main');
        $slot = loader_config::getInstance ()->getConfig('contentbuffer')->bufferSlot;

        //forking process slot

        for ($i = 0; $i < $slot; $i++) {
            if ($config_main->use_forking) {
                switch ($pid = pcntl_fork ()) {
                    case - 1 :
                        $log->write(array('level' => 'error', 'message' => "Forking failed"));
                        die('Forking failed');
                        break;
                    case 0 :
                        $this->processSentContent($i);
                        exit ();
                        break;
                    default :
                        //pcntl_waitpid ( $pid, $status );
                        break;
                }
            } else {
                $this->processSentContent($i);
            }
        }
        return true;
    }
	
	public function processSentContent($slot) 
	{
        $log = manager_logging::getInstance ();
		$log->write(array('level' => 'debug', 'message' => 'Start : ' . $slot));

		$load_config = loader_config::getInstance();
		$buffer_file = buffer_file::getInstance();

		$contentbuffer = $load_config->getConfig('contentbuffer');

		$path = $contentbuffer->bufferPath . '/' . $slot;
		$limit = $contentbuffer->bufferThrottle;
		$result = $buffer_file->read($path, $limit, 'cb');

		if ($result !== false) {
			foreach ($result as $val) {
				foreach ($val as $crDataPath => $data) {
					$log->write(array('level' => 'debug', 'message' => "path : " . $crDataPath . " content : " . serialize($data)));

					if (is_object($data)) 
					{
						$buffer_file->delete($crDataPath);
						
						$crSave = $this->portal_hit($data);
						
					} else {
						$log->write(array('level' => 'error', 'message' => "buffer is not an object"));
						$buffer_file->delete($crDataPath);
					}
				}
			}
		}
    }
	
	public function sentResponse($rawdata)
	{	
		if (!empty($rawdata) && count($rawdata) != 0) 
		{
			$log = manager_logging::getInstance ();
			$log->write(array('level' => 'debug', 'message' => "Start"));
			
			$mainConfig = loader_config::getInstance ()->getConfig('main');
				
			$loader_model = loader_model::getInstance();
			$user = $loader_model->load('user', 'connDatabase1');
			
			$trxdata = new stdClass();
			$trxdata->msisdn=$rawdata['msisdn'];
			$trxdata->operator=$rawdata['operator'];
			$trxdata->service=$rawdata['service'];
					
			$rec = $user->getMembership($trxdata);
			
			$api = new manager_api();
			
			if(count($rec) > 0)
				return array('status' => 'OK', 'q' => $api->__encode($rec[0]));
			else
				return array('status' => 'NOK');
		}
		else return array('status' => 'NOK');
	}
	
	function sent($data, $timeout = 10) {
		
		$log = manager_logging::getInstance ();
		$log->write(array('level' => 'debug', 'message' => "Start"));
		
		$mainConfig = loader_config::getInstance ()->getConfig('main');
		$loader_model = loader_model::getInstance();
		$user = $loader_model->load('user', 'connDatabase1');
			
		$start_hit = time();
		
		$trxdata = new stdClass();
		$trxdata->msisdn=$data->msisdn;
		$trxdata->operator=$data->operator;
		$trxdata->service=$data->service;
		$trxdata->modify_date=date("Y-m-d H:i:s");
		$trxdata->sent=1;
		$trxdata->active=(int)$data->active;
				
		$user->updateMembershipSent($trxdata);
		
		$rec = $user->getMembership($trxdata);
		
		$data = array(
			  'msisdn'=>$trxdata->msisdn,
			  'modify_date'=>$trxdata->modify_date,
			  'operator'=>$trxdata->operator,
			  'service'=>$trxdata->service,
			  'active'=>$data->active,
			  'sent'=>1,
			  'password'=>$rec[0]['password']
			  );
		
		if(strtolower($trxdata->service) == 'gold')
		{
			$api = new manager_api();
			
			$dataArray = $api->__encode($data);
			$dataArray = "?q=" . $dataArray;
				
			$url = $mainConfig->contentURL . $dataArray;
		}
		else if(strtolower($trxdata->service) == 'gemezz')
		{
			if($trxdata->active == 1)
				$url = $mainConfig->gemezzREGURL . "&msisdn=" . $trxdata->msisdn . "&password=" . $rec[0]['password'];
			else
				$url = $mainConfig->gemezzUNREGURL . "&msisdn=" . $trxdata->msisdn;
		}
		else if(strtolower($trxdata->service) == 'gemezz1')
		{
			$url = $mainConfig->gemezz1PULLURL . "&msisdn=" . $trxdata->msisdn . "&password=" . $rec[0]['password'];
		}
		
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_URL, $url);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $output = @curl_exec($ch);
        $header = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        @curl_close($ch);

		//print_r($output);die;
        $end_hit = time();
		
		$log->write(array('level' => 'info', 'message' => "Hit Sent Content : " . $url . ", Header : " . $header . ", Content(Msisdn : ".$trxdata->msisdn.", Password : ".$rec[0]['password']."), TimeHit=".((int)$end_hit-(int)$start_hit)));
		
        return $output;
    }
	
	public function processUnreg($mo_data){
		
		$log = manager_logging::getInstance ();
		$log->write(array('level' => 'debug', 'message' => "Start"));
		
		$mainConfig = loader_config::getInstance ()->getConfig('main');
		$loader_model = loader_model::getInstance();
		$user = $loader_model->load('user', 'connDatabase1');
		
		if(strtolower($mo_data->keyword) == "gold")
		{
			if($mainConfig->openCustomSms)
			//if(true)
			{						
				if(
					$mo_data->channel == 'sms' 
				 && $mo_data->adn == '99876' 
				 && $mo_data->msgData == 'unreg gold' 
				 && $mo_data->keyword == 'gold' 
				 && $mo_data->operatorName == 'moh3i' 
				 && $mo_data->requestType == 'unreg' 
					)
				{
					$trxdata = new stdClass();
					$trxdata->msisdn=$mo_data->msisdn;
					$trxdata->modify_date=date("Y-m-d H:i:s");
					$trxdata->operator=$mo_data->operatorName;
					$trxdata->service= $mo_data->keyword;
					$trxdata->active=0; 
					$trxdata->sent=0;
					
					$user->updateActiveMembership($trxdata);
					
					$buffer_file = buffer_file::getInstance ();
											
					$path = $buffer_file->generate_file_name ( $trxdata, "contentbuffer", "cb" );
					
					if ($buffer_file->save ( $path, $trxdata )) {
						$log->write(array('level' => 'debug', 'message' => "Buffer OK"));
						//return array('level' => 'debug', 'message' => "Buffer OK");
					} else {
						$log->write(array('level' => 'debug', 'message' => "Buffer NOK"));
						//return array('level' => 'debug', 'message' => "Buffer NOK");
					}
				}
			}
		}
	}
	
	public function processSentNoReplied180D() 
	{
		$log_profile = 'broadcast';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		$load_config = loader_config::getInstance();
		 
		$xmp_model = loader_model::getInstance ();
		$model_user = $xmp_model->load ( 'user', 'connDatabase1' );
		$checkConfirm = $model_user->getConfirmMechanismWithExpiredDate();
		
		$configMT = $load_config->getConfig('mt');
		$profile = 'text';
		
		$url = $configMT->profile [$profile] ['sendUrl'] [0];
		
		if(count($checkConfirm) > 0)
		{
			foreach($checkConfirm as $dt)
			{
				$model_user->updateConfirmMechanism($dt['msisdn']);
				
				$param = '&MOBILENO=' . $dt['msisdn'];
				$message = unserialize($dt['message']);			
				$param .= '&MESSAGE=' . urlencode($message['no_replied']);
				
				$keyword = strtolower($message['msisdn_subject']);
				
				//DAILY SET CONFIRMATION
				if(in_array($keyword, $configMT->profile[$profile]['mtProcess']['mt']['broadcast']['renewal_daily']['subject']))
				{
					$sent_confirm_180 = $configMT->profile[$profile]['mtProcess']['mt']['broadcast']['renewal_daily'][$keyword]['this'];
				}
				
				//MONTHLY SET CONFIRMATION
				if(in_array($keyword, $configMT->profile[$profile]['mtProcess']['mt']['broadcast']['renewal_monthly']['subject']))
				{
					$sent_confirm_180 = $configMT->profile[$profile]['mtProcess']['mt']['broadcast']['renewal_monthly'][$keyword]['this'];
				}
				
				if($configMT->profile [$profile] ['hit'] && $sent_confirm_180 == TRUE){
					
					$hit = http_request::requestGet($url, $param, $configMT->profile [$profile] ['SendTimeOut']);
					$log->write ( array ('level' => 'debug', 'message' => "MT Url:" . $url . $param . ', Result:' . $hit ) );
					
				}else{
					
					$log->write ( array ('level' => 'debug', 'message' => "Disable MT For Keyword : " . $keyword ) );
					
				}
			}
		}
	}

	public function portal_hit($mt)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt)));
		
		$start_hit = time();

		$loader_config = loader_config::getInstance();
		$config_main = $loader_config->getConfig('main');
		$configMT = $loader_config->getConfig('mt');
		$profile = 'text';
		
		//TSID=201812031319521543817992&MSISDN=6289526716941&STATUS=SUCCESS&EVENT=FIRSTPUSH&Type=DAILY
		$param = "";
		$param .= '?TSID=' . $mt->msgId;
		$param .= '&MSISDN=' . $mt->msisdn;
		
		if($mt->type == 'unreg'){
			$param .= '&STATUS=SUCCESS';
			$param .= '&EVENT=UNSUB';
		}
		else{
			$param .= '&STATUS=' . (($mt->msgStatus == 'DELIVERED') ? 'SUCCESS' : 'FAILED');
			$param .= '&EVENT=' . ((strpos(strtoupper($mt->subject), ';TEXT') !== FALSE) ? 'FIRSTPUSH' : 'RENEWAL');
		}
		
		$param .= '&Type=' . strtoupper($mt->type);
		
		if($mt->channel == 'portal' || $mt->channel == 'lp1' || $mt->channel == 'lp2') $param .= '&SOURCE=' . strtoupper($mt->channel);
		
		if($config_main->toggle_env == "PRODUCTION") 
		{
			$url = $configMT->profile [$profile] ['sendUrl'] [1];
		} 
		else if($config_main->toggle_env == "DEVELOPMENT") 
		{
			$url = $configMT->profile [$profile] ['sendUrlDev'] [1];
		}
		
		$hit = http_request::requestGet($url, $param, $configMT->profile [$profile] ['SendTimeOut']);

		$end_hit = time();

		$log->write ( array ('level' => 'debug', 'message' => "Portal Hit Url:" . $url . $param . ', Result:' . $hit . ', TimeHit='.((int)$end_hit-(int)$start_hit)) );
	}
}
?>