<?php
$tpm_path_file = "/app/xmp2012/interface/moh3i/config/tpmall.json";

$bFile = new buffer_file();
$tpm_data = json_decode($bFile->get($tpm_path_file));

if(isset($_POST['Submit_rdp']) && $_POST['Submit_rdp'] == "SUBMIT")
{
	foreach($_POST['rdp'] as $rdp_key => $rdp_val)
	{
		list($subject_post, $init_post, $tpm) = explode("_", $rdp_key);
		
		$tpm_data->renewal_daily->_1->gemezz->_1000->$subject_post = $subject_post . "_rdp_" . $rdp_val[0];
	}
	
	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}

if(isset($_POST['Submit_rdnp']) && $_POST['Submit_rdnp'] == "SUBMIT")
{
	foreach($_POST['rdnp'] as $rdnp_key => $rdnp_val)
	{
		list($subject_post, $init_post, $tpm) = explode("_", $rdnp_key);
		
		$tpm_data->renewal_daily->_0->gemezz->_1000->$subject_post = $subject_post . "_rdnp_" . $rdnp_val[0];
	}
	
	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}

if(isset($_POST['Submit_rmp_1000']) && $_POST['Submit_rmp_1000'] == "SUBMIT")
{
	foreach($_POST['rmp_1000'] as $rmp_1000_key => $rmp_1000_val)
	{
		list($init_post, $price, $tpm) = explode("_", $rmp_1000_key);

		$tpm_data->renewal_monthly->_1->gemezz->_1000->$init_post = $init_post . "_" . $price . "_" . $rmp_1000_val[0];
	}
	
	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}

if(isset($_POST['Submit_rmp_2000']) && $_POST['Submit_rmp_2000'] == "SUBMIT")
{
	foreach($_POST['rmp_2000'] as $rmp_2000_key => $rmp_2000_val)
	{
		list($init_post, $price, $tpm) = explode("_", $rmp_2000_key);
		
		$tpm_data->renewal_monthly->_1->gemezz->_2000->global_value = $init_post . "_" . $price . "_" . $rmp_2000_val[0];
	}
	
	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}

if(isset($_POST['Submit_rmnp_1000']) && $_POST['Submit_rmnp_1000'] == "SUBMIT")
{
	foreach($_POST['rmnp_1000'] as $rmnp_1000_key => $rmnp_1000_val)
	{
		list($init_post, $price, $tpm) = explode("_", $rmnp_1000_key);

		$tpm_data->retry_monthly->_1->gemezz->_1000->$init_post = $init_post . "_" . $price . "_" . $rmnp_1000_val[0];
	}
	
	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}

if(isset($_POST['Submit_rmnp_2000']) && $_POST['Submit_rmnp_2000'] == "SUBMIT")
{
	$tpm_data = json_decode($bFile->get($tpm_path_file), true);
	
	foreach($_POST['rmnp_2000'] as $rmnp_2000_key => $rmnp_2000_val)
	{
		//list($init_post, $price, $tpm) = explode("_", $rmnp_2000_key);
		
		//$tpm_data->renewal_monthly->_0->gemezz->_2000->global_value = $init_post . "_" . $price . "_" . $rmnp_2000_val[0];
		
		list($subject_post, $session, $initpost, $tpm) = explode("_", $rmnp_2000_key);
		
		$tpm_data['retry_monthly']['_34567']['gemezz']['_1000'][$subject_post]['sesi_'.$session] = $subject_post . "_" . $session . "_rsdp_" . $rmnp_2000_val[0];
	}
	
	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}

if(isset($_POST['Submit_rsdp']) && $_POST['Submit_rsdp'] == "SUBMIT")
{
	$tpm_data = json_decode($bFile->get($tpm_path_file), true);
	
	//print_r($_POST['rsdp']);
	foreach($_POST['rsdp'] as $rsdp_key => $rsdp_val)
	{
		//PS7_5_rsdp_35
		list($subject_post, $session, $initpost, $tpm) = explode("_", $rsdp_key);
		//print_r($subject_post . "_" . $session . "_rsdp_" . $rsdp_val[0]);die;

		$tpm_data['renewal_daily']['_34567']['gemezz']['_1000'][$subject_post]['sesi_'.$session] = $subject_post . "_" . $session . "_rsdp_" . $rsdp_val[0];
	}

	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}

if(isset($_POST['Submit_rtmnp_1000']) && $_POST['Submit_rtmnp_1000'] == "SUBMIT")
{
	$tpm_data = json_decode($bFile->get($tpm_path_file), true);
	
	//print_r($_POST['rsdp']);
	foreach($_POST['rtmnp_1000'] as $rtmnp_1000_key => $rtmnp_1000_val)
	{
		//PS7_5_rsdp_35
		list($subject_post, $session, $initpost, $tpm) = explode("_", $rtmnp_1000_key);
		//print_r($subject_post . "_" . $session . "_rsdp_" . $rsdp_val[0]);die;

		$tpm_data['retry_monthly']['_0']['gemezz']['_1000'][$subject_post][$session] = $subject_post . "_" . $session . "_rtmnp_" . $rtmnp_1000_val[0];
	}

	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}

if(isset($_POST['Submit_rtmnp_2000']) && $_POST['Submit_rtmnp_2000'] == "SUBMIT")
{
	$tpm_data = json_decode($bFile->get($tpm_path_file), true);
	
	//print_r($_POST['rsdp']);
	foreach($_POST['rtmnp_2000'] as $rtmnp_2000_key => $rtmnp_2000_val)
	{
		//PS7_5_rsdp_35
		list($subject_post, $session, $initpost, $tpm) = explode("_", $rtmnp_2000_key);
		//print_r($subject_post . "_" . $session . "_rsdp_" . $rsdp_val[0]);die;

		$tpm_data['retry_monthly']['_0']['gemezz']['_2000'][$subject_post][$session] = $subject_post . "_" . $session . "_rtmnp_" . $rtmnp_2000_val[0];
	}

	$bFile->saveString($tpm_path_file, json_encode($tpm_data));
	$tpm_data = json_decode($bFile->get($tpm_path_file));
}
?>
<p>
  <label for="amount" style="font-weight: bold; font-size: 20px;">GRAND SUMMARIZE ALL TPM :</label>
  <input type="text" id="amount" readonly style="font-size: 20px; border:0; color:#f6931f; font-weight:bold;">
</p>
<form method="post" action="">
<input type="hidden" name="iddeftab" value="defchangetpm" />
<fieldset><legend>RENEWAL DAILY - GEMEZZ - 1000.00 - PRIORITY TPM, TOTAL = <input type="text" id="rdp_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->renewal_daily->_1->gemezz->_1000 as $rdp)
{
	list($subject,$initdp,$tpmSubject) = explode("_", $rdp);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rdp[<?=$rdp?>][]" id="<?=$rdp?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rdp">
		<?=$subject?>
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$rdp?>" class="slider-id">
			<div id="custom-handle-<?=$rdp?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rdp" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>

<fieldset><legend>RENEWAL DAILY - GEMEZZ - 1000.00 - NON PRIORITY TPM, TOTAL = <input type="text" id="rdnp_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->renewal_daily->_0->gemezz->_1000 as $rdnp)
{
	list($subject,$initdp,$tpmSubject) = explode("_", $rdnp);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rdnp[<?=$rdnp?>][]" id="<?=$rdnp?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rdnp">
		<?=$subject?>
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$rdnp?>" class="slider-id">
			<div id="custom-handle-<?=$rdnp?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rdnp" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>

<fieldset><legend>RENEWAL MONTHLY - GEMEZZ - 1000.00 - PRIORITY TPM, TOTAL = <input type="text" id="rmp_1000_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->renewal_monthly->_1->gemezz->_1000 as $rmp_1000)
{
	list($subject,$price,$tpmSubject) = explode("_", $rmp_1000);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rmp_1000[<?=$rmp_1000?>][]" id="<?=$rmp_1000?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rmp_1000">
		<?=$subject?>
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$rmp_1000?>" class="slider-id">
			<div id="custom-handle-<?=$rmp_1000?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rmp_1000" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>

<fieldset><legend>RENEWAL MONTHLY - GEMEZZ - 2000.00 - PRIORITY TPM, TOTAL = <input type="text" id="rmp_2000_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->renewal_monthly->_1->gemezz->_2000 as $rmp_2000)
{
	list($initdp,$price,$tpmSubject) = explode("_", $rmp_2000);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rmp_2000[<?=$rmp_2000?>][]" id="<?=$rmp_2000?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rmp_2000">
		GLOBAL
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$rmp_2000?>" class="slider-id">
			<div id="custom-handle-<?=$rmp_2000?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rmp_2000" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>

<fieldset><legend>RETRY MONTHLY - GEMEZZ - 1000.00 - PRIORITY TPM ( H-1 MO ), TOTAL = <input type="text" id="rmnp_1000_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->retry_monthly->_1->gemezz->_1000 as $rmnp_1000)
{
	list($subject,$price,$tpmSubject) = explode("_", $rmnp_1000);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rmnp_1000[<?=$rmnp_1000?>][]" id="<?=$rmnp_1000?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rmnp_1000">
		<?=$subject?>
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$rmnp_1000?>" class="slider-id">
			<div id="custom-handle-<?=$rmnp_1000?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rmnp_1000" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>

<?php
$sesi = 0;
//if(time() > strtotime("00:00:00") && time() < strtotime("23:59:59")) { $st = "00:00:00"; $en = "23:59:59"; $sesi = 1; }
if(time() > strtotime("00:00:00") && time() < strtotime("14:00:00")) { $st = "00:00:00"; $en = "14:00:00"; $sesi = 1; }
if(time() > strtotime("14:00:00") && time() < strtotime("16:00:00")) { $st = "15:00:00"; $en = "16:00:00"; $sesi = 2; }
if(time() > strtotime("16:00:00") && time() < strtotime("18:00:00")) { $st = "17:00:00"; $en = "18:00:00"; $sesi = 3; }
if(time() > strtotime("18:00:00") && time() < strtotime("21:00:00")) { $st = "20:00:00"; $en = "21:00:00"; $sesi = 4; }
if(time() > strtotime("21:00:00") && time() < strtotime("24:00:00")) { $st = "22:00:00"; $en = "23:00:00"; $sesi = 5; }
?> 

<fieldset><legend>RETRY MONTHLY - GEMEZZ - 1000.00 - PRIORITY TPM ( H-1 MO ) RETRY SAME DAY SESI <?=$sesi?>, RANGE TIME ( START = <?=$st?> - END = <?=$en?> ), TOTAL = <input type="text" id="rmnp_2000_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->retry_monthly->_34567->gemezz->_1000 as $rmnp_2000)
{
	if($sesi == 1) $persession = $rmnp_2000->sesi_1;
	if($sesi == 2) $persession = $rmnp_2000->sesi_2;
	if($sesi == 3) $persession = $rmnp_2000->sesi_3;
	if($sesi == 4) $persession = $rmnp_2000->sesi_4;
	if($sesi == 5) $persession = $rmnp_2000->sesi_5;

	list($subject,$session,$initdp,$tpmSubject) = explode("_", $persession);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rmnp_2000[<?=$persession?>][]" id="<?=$persession?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rmnp_2000">
		<?=$subject?>
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$persession?>" class="slider-id">
			<div id="custom-handle-<?=$persession?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rmnp_2000" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>

<fieldset><legend>RENEWAL DAILY - GEMEZZ - 1000.00 - PRIORITY TPM RETRY SAME DAY SESI <?=$sesi?>, RANGE TIME ( START = <?=$st?> - END = <?=$en?> ), TOTAL = <input type="text" id="rsdp_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->renewal_daily->_34567->gemezz->_1000 as $rsdp)
{
	//PS7_1_rsdp_35
	
	if($sesi == 1) $persession = $rsdp->sesi_1;
	if($sesi == 2) $persession = $rsdp->sesi_2;
	if($sesi == 3) $persession = $rsdp->sesi_3;
	if($sesi == 4) $persession = $rsdp->sesi_4;
	if($sesi == 5) $persession = $rsdp->sesi_5;

	list($subject,$session,$initdp,$tpmSubject) = explode("_", $persession);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rsdp[<?=$persession?>][]" id="<?=$persession?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rsdp">
		<?=$subject?>
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$persession?>" class="slider-id">
			<div id="custom-handle-<?=$persession?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rsdp" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>

<?php
$sesi = "";
if(time() > strtotime("00:00:00") && time() < strtotime("23:59:59")) { $sesi = "LOW"; }
if(time() > strtotime("12:00:00") && time() < strtotime("18:00:00")) { $sesi = "FAIR"; }
if(time() > strtotime("18:00:00") && time() < strtotime("24:00:00")) { $sesi = "HIGH"; }
?> 

<fieldset><legend>RETRY MONTHLY - GEMEZZ - 1000.00, NON PRIORITY TPM <?=$sesi?> TOTAL = <input type="text" id="rtmnp_1000_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->retry_monthly->_0->gemezz->_1000 as $rtmnp)
{
	if($sesi == "LOW") $persession = $rtmnp->low;
	if($sesi == "FAIR") $persession = $rtmnp->middle;
	if($sesi == "HIGH") $persession = $rtmnp->high;
	
	list($subject,$sessionname,$initdp,$tpmSubject) = explode("_", $persession);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rtmnp_1000[<?=$persession?>][]" id="<?=$persession?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rtmnp_1000">
		<?=$subject?>
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$persession?>" class="slider-id">
			<div id="custom-handle-<?=$persession?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rtmnp_1000" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>

<fieldset><legend>RETRY MONTHLY - GEMEZZ - 2000.00, TPM <?=$sesi?> TOTAL = <input type="text" id="rtmnp_2000_tot" class="tot_tpm" readonly></legend>
<table cellspacing=0 cellpadding=0 border=0 style="width: 100%;">
<?php
foreach($tpm_data->retry_monthly->_0->gemezz->_2000 as $rtmnp)
{
	if($sesi == "LOW") $persession = $rtmnp->low;
	if($sesi == "FAIR") $persession = $rtmnp->middle;
	if($sesi == "HIGH") $persession = $rtmnp->high;
	
	list($subject,$sessionname,$initdp,$tpmSubject) = explode("_", $persession);
?>
<tr>
	<td style="width: 30px; padding:3px;">
		<input name="rtmnp_2000[<?=$persession?>][]" id="<?=$persession?>" value=<?=$tpmSubject?> type="hidden" class="subamount_rtmnp_2000">
		<?=$subject?>
	</td>
	<td style="width: 100%; padding:3px;">
		<div style="margin-left: 20px;" id="slider-range-max-<?=$persession?>" class="slider-id">
			<div id="custom-handle-<?=$persession?>" style="width: 1em; height: 0.5em; margin-top: 0em;text-align: center;line-height: 0.6em; padding: 5px; top: -4px; left: 17.0854%;font-size: 13px;" class="ui-slider-handle"></div>
		</div>
	</td>
</tr>
<?php
}
?>
<tr>
<td style="width: 30px; padding:3px;">&nbsp;</td><td align="left" style="width: 100%; padding:3px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30px; padding:3px;"><input type="submit" name="Submit_rtmnp_2000" value="SUBMIT" /></td><td align="left" style="width: 100%; padding:3px;"></td>
</tr>
</table>
</fieldset>
</form>
