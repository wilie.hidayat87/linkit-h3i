<form method="post" action="">
<input type="hidden" name="iddeftab" value="defrev" />
<table cellspacing="0" cellpadding="0">
<thead>
	<tr>
		<th align="left">Subject Type</th>
		<th align="left">
			<?php
			$subject_type = (isset($_POST['subject_type']) ? trim($_POST['subject_type']) : "CHARGE");
			?>
			<select name="subject_type">
				<option value="CHARGE" <?=(($subject_type == "CHARGE") ? "selected" : "")?>>- CHARGE / All Rev -</option>
				<option value="CHARGE;SMS;R" <?=(($subject_type == "CHARGE;SMS;R") ? "selected" : "")?>>- CHARGE;SMS;R / Renewal Rev Only -</option>
			</select>
		</th>
	</tr>
	<tr>
		<th align="left">- Check Per Days !</th>
		<th align="left">
			<select name="checkdays">
				<option value="2" <?=(($_POST['checkdays'] == 2) ? "selected" : "selected")?>>- Last 2 Days -</option>
				<option value="3" <?=(($_POST['checkdays'] == 3) ? "selected" : "")?>>- Last 3 Days -</option>
				<option value="4" <?=(($_POST['checkdays'] == 4) ? "selected" : "")?>>- Last 4 Days -</option>
				<option value="5" <?=(($_POST['checkdays'] == 5) ? "selected" : "")?>>- Last 5 Days -</option>
				<option value="6" <?=(($_POST['checkdays'] == 6) ? "selected" : "")?>>- Last 6 Days -</option>
				<option value="7" <?=(($_POST['checkdays'] == 7) ? "selected" : "")?>>- Last 7 Days -</option>
			</select>
		</th>
	</tr>
	<tr>
		<th align="left">Hour Minute</th>
		<th align="left">
			- From -  
			<select name="fromhourminute">
				<?php
				$default = "04:00";
				$fromhourminute = (isset($_POST['fromhourminute']) ? trim($_POST['fromhourminute']) : "04:00");
	
				for($h = 0; $h < 24; $h++)
				{
					for($m = 0; $m < 60; $m++)
					{
						$h = ((strlen($h) == 1) ? "0".$h : $h);
						$m = ((strlen($m) == 1) ? "0".$m : $m);
						$v = $h.":".$m;
						
						?>
							<option value="<?=$v?>" <?=(($fromhourminute == $v) ? 'selected' : '')?>>- <?=$v?> -</option>
						<?php
					}
				}
				?>
			</select>
			- To - 
			<select name="tohourminute">
				<?php
				$default = date("H:i");
				$tohourminute = (isset($_POST['tohourminute']) ? trim($_POST['tohourminute']) : $default);
				
				for($h = 0; $h < 24; $h++)
				{
					for($m = 0; $m < 60; $m++)
					{
						$h = ((strlen($h) == 1) ? "0".$h : $h);
						$m = ((strlen($m) == 1) ? "0".$m : $m);
						$v = $h.":".$m;
						
						?>
							<option value="<?=$v?>" <?=(($tohourminute == $v) ? 'selected' : '')?>>- <?=$v?> -</option>
						<?php
					}
				}
				?>
			</select>
		</th>
	</tr>
	<tr>
		<th colspan="2" align="left"><input type="submit" name="submit" value="Show" /></th>
	</tr>
</thead>
</table>
</form>
<br />
<?php
if(isset($_POST['submit']) && $_POST['submit'] == "Show")
{
	$subject_type = (isset($_POST['subject_type']) ? trim($_POST['subject_type']) : "CHARGE");
	$checkdays = (isset($_POST['checkdays']) ? (int)$_POST['checkdays'] : 5);
	$fromhourminute = (isset($_POST['fromhourminute']) ? trim($_POST['fromhourminute']) : "05:00");
	$tohourminute = (isset($_POST['tohourminute']) ? trim($_POST['tohourminute']) : "22:00");
		
	$link = new mysqli("vasdb.cdzdyl8xksm8.eu-central-1.rds.amazonaws.com", "linkit", "l1nk1t360_db", "xmp");
	
	$exSql = "";
	
	for($d=1; $d <= $checkdays; $d++)
	{
		$dateTbl = date("Ymd", strtotime("-".($d-1)." days"));
		
		$tblDate = "tbl_msgtransact_".$dateTbl;
		$columnDate = date("Y-m-d", strtotime("-".($d)." days"));
		
		if($dateTbl == "20201202")
		{
			$tblDate = "tbl_msgtransact";
			$columnDate = "2020-12-02";
		}
		
		$exSql .= " UNION
		SELECT t.date as date, FORMAT(SUM(t.price*t.total), 2) AS rev FROM (
		SELECT DATE(msgtimestamp) AS DATE, price, COUNT(1) AS total FROM xmp.`".$tblDate."` 
		WHERE msgtimestamp BETWEEN '".$columnDate." {$fromhourminute}:00' AND '".$columnDate." {$tohourminute}:00' AND SUBJECT LIKE '%{$subject_type}%' AND msgstatus = 'DELIVERED' GROUP BY DATE, price) AS t";
		
		if(($d+1) == $checkdays) break;
	}
	
	$sql = "SELECT trx.* FROM (
	SELECT t.date as date, FORMAT(SUM(t.price*t.total), 2) AS rev FROM (
	SELECT DATE(msgtimestamp) AS DATE, price, COUNT(1) AS total FROM xmp.`tbl_msgtransact` 
	WHERE msgtimestamp BETWEEN '".date("Y-m-d")." {$fromhourminute}:00' AND '".date("Y-m-d")." {$tohourminute}:00'   
	AND SUBJECT LIKE '%{$subject_type}%' AND msgstatus = 'DELIVERED' GROUP BY DATE, price) AS t
	{$exSql}
	) AS trx;";

	$result = $link->query($sql);

	?>
	<table cellspacing="0" cellpadding="0" border=1>
	<thead>
		<tr>
		<th align="left">No. </th>
		<th align="left">Date</th>
		<th align="left">Revenue</th>
		</tr>
	</thead>
	</tbody>
	<?php
	$no = 1;
	while($r = $result->fetch_assoc())
	{
		?>
		<tr>
		<td><?=$no?>. </td>
		<td><?=$r['date']?></td>
		<td><?=$r['rev']?></td>
		</tr>
		<?php
		$no++;
	}

	?>
	</tbody>
	</table>
	</br /><br />
	<span>
	SQL Queries = <?=$sql?>
	</span>
	<?php
	$result->free();
	$link->close();
}
?>