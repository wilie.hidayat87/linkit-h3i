<form method="post" action="">
<input type="hidden" name="iddeftab" value="deftechcheck" />
<input type="checkbox" name="code[]" value="400" <?=(isset($_POST['code'][0]) ? "checked" : "checked")?> />
- Timeout Check Attempt ( code : 400 ) - <br /><br />
<input type="checkbox" name="code[]" value="200_3_109" <?=(isset($_POST['code'][1]) ? "checked" : "")?> />
- Max Throttle Check Attempt ( code : 200_3_109 ) - <br /><br />
<input type="checkbox" name="insuff" value="4012" />
- Insufficient Balance Check Attempt ( code : 4012 ) - <br /><br />
- Check Per Days !
<select name="checkdays">
	<option value="2" <?=(($_POST['checkdays'] == 2) ? "selected" : "")?>>- Last 2 Days -</option>
	<option value="3" <?=(($_POST['checkdays'] == 3) ? "selected" : "")?>>- Last 3 Days -</option>
	<option value="4" <?=(($_POST['checkdays'] == 4) ? "selected" : "")?>>- Last 4 Days -</option>
	<option value="5" <?=(($_POST['checkdays'] == 5) ? "selected" : "")?>>- Last 5 Days -</option>
	<option value="6" <?=(($_POST['checkdays'] == 6) ? "selected" : "")?>>- Last 6 Days -</option>
	<option value="7" <?=(($_POST['checkdays'] == 7) ? "selected" : "")?>>- Last 7 Days -</option>
</select>
<br /><br />
<fieldset style="border: 1px solid #ccc;">
<legend>Note</legend>
	<span style="color: red"> Exlude Insufficient Balance code ( 4012 ) check when checking simultaneously with other code status!. </span>
</fieldset><br />
<input type="submit" name="submit" value="Check" />
</form>
<?php
if(isset($_POST['submit']) && $_POST['submit'] == "Check")
{
$logpath = "/DATA/app/xmp2012/logs/moh3i";

$code = (isset($_POST['code']) ? $_POST['code'] : "");
$insuff = (isset($_POST['insuff']) ? $_POST['insuff'] : "");
$checkdays = (isset($_POST['checkdays']) ? trim($_POST['checkdays']) : 2);
$retry_attempt = 5;

if(count($code) > 0)
{
	$retry_path = $logpath . "/retry/";
	echo "Check in path : {$retry_path}<br />";
}

$output = "";

foreach($code as $c)
{
	ob_start();
	
	for($d=0; $d < $checkdays; $d++)
	{
		$date_check = (($d > 0) ? date("Ymd", strtotime("-{$d} days")) : date("Ymd"));
		
		for($r=1; $r <= $retry_attempt; $r++)
		{
			$initfile = $date_check . "_{$c}.txt.retry{$r}";
			$filecheck = $retry_path . $initfile;
			
			$retryHour = "";
			
			if($r == 1)
				$retryHour = "01pm";
			else if($r == 2)
				$retryHour = "03pm";
			else if($r == 3)
				$retryHour = "05pm";
			else if($r == 4)
				$retryHour = "08pm";
			else if($r == 5)
				$retryHour = "10pm";
				
			
			if(file_exists($filecheck))
			{
				$result = shell_exec("cat {$filecheck} | wc -l");
				echo "Retry Hour {$retryHour} - {$initfile} - Total = " . $result . "<br />";
			}
			else
			{
				echo "File {$filecheck} not existed, execute in {$retryHour}!<br />";
			}
		}
		
		echo "<br /><br />";
	}
	
	if($c == '400'){
		$codename_timeout = $c;
		$timeout = ob_get_clean();
	}
	else if($c == '200_3_109'){
		$codename_max_throttle = $c;
		$max_throttle = ob_get_clean();
	}
}
	
if(!empty($timeout))
{
	?>
	<br />
	<fieldset style="border: 1px solid #ccc;">
		<legend>Code Check : <?=$codename_timeout?></legend>
	<?=$timeout?>
	</fieldset>
	<?php
}

if(!empty($max_throttle))
{
	?>
	<br />
	<fieldset style="border: 1px solid #ccc;">
		<legend>Code Check : <?=$codename_max_throttle?></legend>
	<?=$max_throttle?>
	</fieldset>
	<?php
}

if(!empty($insuff))
{
	$logpath = $logpath . "/broadcast/";
	echo "Check in path : {$logpath}<br />";
	
	?>
	<br />
	<fieldset style="border: 1px solid #ccc;">
		<legend>Code Check : <?=$insuff?></legend>
	<?php
		for($d=0; $d < $checkdays; $d++)
		{
			$date_check = (($d > 0) ? date("Ymd", strtotime("-{$d} days")) : date("Ymd"));
			$date_logcheck = (($d > 0) ? date("Y-m-d", strtotime("-{$d} days")) : date("Y-m-d"));
			
			if($d > 1) {
				$initfile = "broadcast_" . $date_check . ".gz";
				$filecheck = $logpath . $initfile;
				$comm_exe = "zcat {$filecheck}";
			}
			else{
				$initfile = "broadcast_" . $date_check;
				$filecheck = $logpath . $initfile;
				$comm_exe = "cat {$filecheck}";
			}
			
			if(file_exists($filecheck))
			{
				$result = shell_exec($comm_exe . " {$filecheck} | grep 'Request Payment Hit' | grep '4012' | grep '{$date_logcheck} 0[5-9]' | wc -l");
				echo "Check range hour in [ 05am - 09am ] - {$initfile} - Total = " . $result . "<br />";
				
				$result = shell_exec($comm_exe . " {$filecheck} | grep 'Request Payment Hit' | grep '4012' | grep '{$date_logcheck} 1[0-9]' | wc -l");
				echo "Check range hour in [ 10am - 09pm ] - {$initfile} - Total = " . $result . "<br />";
				
				$result = shell_exec($comm_exe . " {$filecheck} | grep 'Request Payment Hit' | grep '4012' | grep '{$date_logcheck} 2[0-3]' | wc -l");
				echo "Check range hour in [ 8pm - 12pm ] - {$initfile} - Total = " . $result . "<br /><br />";
			}
			else
			{
				echo "File {$filecheck} not existed!<br />";
			}
			
		}
	?>
	</fieldset>
	<?php
}
}
?>