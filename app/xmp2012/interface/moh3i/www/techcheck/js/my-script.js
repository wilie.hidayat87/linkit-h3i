$( function() 
{	
	// TOTAL TPM RENEWAL DAILY PRIORITY
	
	var rdp_tot_tpm = $( "#rdp_tot" );
	var rdp_tot_value = 0;

	$('.subamount_rdp').each(function(){
		rdp_tot_value += parseFloat(this.value);
	});
	
	rdp_tot_tpm.val( rdp_tot_value );
	
	// TOTAL TPM RENEWAL DAILY NON PRIORITY
	
	var rdnp_tot_tpm = $( "#rdnp_tot" );
	var rdnp_tot_value = 0;

	$('.subamount_rdnp').each(function(){
		rdnp_tot_value += parseFloat(this.value);
	});
	
	rdnp_tot_tpm.val( rdnp_tot_value );
	
	// TOTAL TPM RENEWAL DAILY RETRY SAME DAY PRIORITY
	
	var rsdp_tot_tpm = $( "#rsdp_tot" );
	var rsdp_tot_value = 0;

	$('.subamount_rsdp').each(function(){
		rsdp_tot_value += parseFloat(this.value);
	});
	
	rsdp_tot_tpm.val( rsdp_tot_value );
	
	// TOTAL TPM RENEWAL MONTHLY PRIORITY PRICE 1000
	
	var rmp_1000_tot_tpm = $( "#rmp_1000_tot" );
	var rmp_1000_tot_value = 0;

	$('.subamount_rmp_1000').each(function(){
		rmp_1000_tot_value += parseFloat(this.value);
	});
	
	rmp_1000_tot_tpm.val( rmp_1000_tot_value );
	
	// TOTAL TPM RENEWAL MONTHLY PRIORITY PRICE 2000
	
	var rmp_2000_tot_tpm = $( "#rmp_2000_tot" );
	var rmp_2000_tot_value = 0;

	$('.subamount_rmp_2000').each(function(){
		rmp_2000_tot_value += parseFloat(this.value);
	});
	
	rmp_2000_tot_tpm.val( rmp_2000_tot_value );
	
	// TOTAL TPM RENEWAL MONTHLY NON PRIORITY PRICE 1000
	
	var rmnp_1000_tot_tpm = $( "#rmnp_1000_tot" );
	var rmnp_1000_tot_value = 0;

	$('.subamount_rmnp_1000').each(function(){
		rmnp_1000_tot_value += parseFloat(this.value);
	});
	
	rmnp_1000_tot_tpm.val( rmnp_1000_tot_value );
	
	// TOTAL TPM RENEWAL MONTHLY PRIORITY PRICE 2000
	
	var rmnp_2000_tot_tpm = $( "#rmnp_2000_tot" );
	var rmnp_2000_tot_value = 0;

	$('.subamount_rmnp_2000').each(function(){
		rmnp_2000_tot_value += parseFloat(this.value);
	});
	
	rmnp_2000_tot_tpm.val( rmnp_2000_tot_value );
	
	// TOTAL TPM RETRY MONTHLY PRIORITY PRICE 1000
	
	var rtmnp_1000_tot_tpm = $( "#rtmnp_1000_tot" );
	var rtmnp_1000_tot_value = 0;

	$('.subamount_rtmnp_1000').each(function(){
		rtmnp_1000_tot_value += parseFloat(this.value);
	});
	
	rtmnp_1000_tot_tpm.val( rtmnp_1000_tot_value );
	
	// TOTAL TPM RETRY MONTHLY PRIORITY PRICE 2000
	
	var rtmnp_2000_tot_tpm = $( "#rtmnp_2000_tot" );
	var rtmnp_2000_tot_value = 0;

	$('.subamount_rtmnp_2000').each(function(){
		rtmnp_2000_tot_value += parseFloat(this.value);
	});
	
	rtmnp_2000_tot_tpm.val( rtmnp_2000_tot_value );
	
	// GRAND TOTAL TPM
	
	var grand_tot_value = 0;
	
	$('.tot_tpm').each(function(){
		grand_tot_value += parseFloat(this.value);
	});
	
	var alltpm = $( "#amount" );
	alltpm.val( grand_tot_value );
	
	// SET SCRIPT SLIDER RENEWAL DAILY PRIORITY
	
	$( ".subamount_rdp" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RENEWAL DAILY PRIORITY
			rdp_tot_value = 0;
		
			$('.subamount_rdp').each(function(){
				rdp_tot_value += parseFloat(this.value);
			});
			
			rdp_tot_tpm.val( rdp_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
	
	// SET SCRIPT SLIDER RENEWAL DAILY PRIORITY
	
	$( ".subamount_rdnp" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RENEWAL DAILY PRIORITY
			rdnp_tot_value = 0;
		
			$('.subamount_rdnp').each(function(){
				rdnp_tot_value += parseFloat(this.value);
			});
			
			rdnp_tot_tpm.val( rdnp_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
	
	// SET SCRIPT SLIDER RENEWAL DAILY RETRY SAME DAY PRIORITY
	
	$( ".subamount_rsdp" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RENEWAL DAILY RETRY SAME DAY PRIORITY
			rsdp_tot_value = 0;
		
			$('.subamount_rsdp').each(function(){
				rsdp_tot_value += parseFloat(this.value);
			});
			
			rsdp_tot_tpm.val( rsdp_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
	
	// SET SCRIPT SLIDER RENEWAL MONTHLY PRIORITY PRICE 1000
	
	$( ".subamount_rmp_1000" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RENEWAL MONTHLY PRIORITY PRICE 1000
			rmp_1000_tot_value = 0;
		
			$('.subamount_rmp_1000').each(function(){
				rmp_1000_tot_value += parseFloat(this.value);
			});
			
			rmp_1000_tot_tpm.val( rmp_1000_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
	
	// SET SCRIPT SLIDER RENEWAL MONTHLY PRIORITY PRICE 2000
	
	$( ".subamount_rmp_2000" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RENEWAL MONTHLY PRIORITY PRICE 2000
			rmp_2000_tot_value = 0;
		
			$('.subamount_rmp_2000').each(function(){
				rmp_2000_tot_value += parseFloat(this.value);
			});
			
			rmp_2000_tot_tpm.val( rmp_2000_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
	
	// SET SCRIPT SLIDER RENEWAL MONTHLY NON PRIORITY PRICE 1000
	
	$( ".subamount_rmnp_1000" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RENEWAL MONTHLY NON PRIORITY PRICE 1000
			rmnp_1000_tot_value = 0;
		
			$('.subamount_rmnp_1000').each(function(){
				rmnp_1000_tot_value += parseFloat(this.value);
			});
			
			rmnp_1000_tot_tpm.val( rmnp_1000_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
	
	// SET SCRIPT SLIDER RENEWAL MONTHLY PRIORITY PRICE 2000
	
	$( ".subamount_rmnp_2000" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RENEWAL MONTHLY NON PRIORITY PRICE 2000
			rmnp_2000_tot_value = 0;
		
			$('.subamount_rmnp_2000').each(function(){
				rmnp_2000_tot_value += parseFloat(this.value);
			});
			
			rmnp_2000_tot_tpm.val( rmnp_2000_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
	
	// SET SCRIPT SLIDER RETRY MONTHLY PRIORITY PRICE 1000
	
	$( ".subamount_rtmnp_1000" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RETRY MONTHLY PRICE 1000
			rtmnp_1000_tot_value = 0;
		
			$('.subamount_rtmnp_1000').each(function(){
				rtmnp_1000_tot_value += parseFloat(this.value);
			});
			
			rtmnp_1000_tot_tpm.val( rtmnp_1000_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
	
	// SET SCRIPT SLIDER RETRY MONTHLY PRIORITY PRICE 2000
	
	$( ".subamount_rtmnp_2000" ).each(function() {
		// read initial values from markup and remove that
		var disSlider = $( this );
		var sliderName = disSlider.attr("id");
		var value = parseInt(disSlider.val() , 10 );
		var sliderObj = $( "#slider-range-max-" + sliderName );
		
		var handle = $( "#custom-handle-" + sliderName );
		sliderObj.slider({
		  range: "max",
		  min: 0,
		  max: 200,
		  value: value,
		  create: function() {
			handle.text( sliderObj.slider( "value" ) );
		  },
		  slide: function( event, ui ) {
			handle.text( ui.value );
		  },
		  stop: function( event, ui ) {
			disSlider.val( ui.value );
			
			// SUM CURRENT RETRY MONTHLY PRICE 2000
			rtmnp_2000_tot_value = 0;
		
			$('.subamount_rtmnp_2000').each(function(){
				rtmnp_2000_tot_value += parseFloat(this.value);
			});
			
			rtmnp_2000_tot_tpm.val( rtmnp_2000_tot_value );
			
			// SUM CURRENT GRAND TOTAL
			
			grand_tot_value = 0;
		
			$('.tot_tpm').each(function(){
				grand_tot_value += parseFloat(this.value);
			});
			
			alltpm.val( grand_tot_value );
		  }
		});
		handle.text( sliderObj.slider( "value" ) );
	});
});