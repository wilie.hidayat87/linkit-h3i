<?php
class buffer_file {

    public function saveString($path, $content) 
	{
		if (!is_writable($path)) {
            chmod($path, 0777);
        }
		
        $result = (boolean) file_put_contents($path, $content);

        if ($result === false) {
            return false;
        } else {
            return true;
        }
    }

	public function get($pathfile)
	{
		$file = file_get_contents($pathfile, true);
		
		return $file;
	}
}
?>