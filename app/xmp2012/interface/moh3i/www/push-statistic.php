<?php
	date_default_timezone_set('Asia/Jakarta');

	$db_host        = 'vasdb.cdzdyl8xksm8.eu-central-1.rds.amazonaws.com';
	$db_user        = 'linkit';
	$db_pass        = 'l1nk1t360_db';
	$db_database    = 'xmp';

	$con = mysqli_connect($db_host, $db_user, $db_pass, $db_database) or die(mysqli_error());

	if(mysqli_connect_error()){
		die(mysqli_connect_error());
	}

	$yesterday = false;
	if(!isset($argv[1])) {
		$date = date('Y-m-d');	
	} else {
		$date = date('Y-m-d',time()+(60*60*24*$argv[1]));
		$yesterday = true;
	}

	// MO SPC
	// TOTAL QUEUE
	error_reporting(0);
	$data = array();
	$data['MO_SPC']['ON_QUEUE'];
	$data['FAILED_STAT'] = array();
	$data['RESPONS_TIME'] = array();
	$data['TPH'] = 0;

	echo 'PRIO_PS'.PHP_EOL;
	// ------------------
	$data['PRIO_PS']['ON_QUEUE'] = 0;
	
	$sql = "SELECT COUNT(1) AS total FROM dbpush.push_buffer_priority WHERE subject LIKE 'MT;CHARGE;SMS;RENEWAL_DAILY%' AND priority='1' AND stat = 'ON_QUEUE'";	
	$result = mysqli_query($con,$sql) or die('1. '.mysqli_error($con));
	
	$row=mysqli_fetch_assoc($result);
	if(!empty($row))
		$data['PRIO_PS']['ON_QUEUE'] = $row['total'];
	
	$data['PRIO_PS']['PUSHED'] = '0';
	
	$sql = "SELECT COUNT(1) AS total FROM dbpush.push_buffer_priority WHERE subject LIKE 'MT;CHARGE;SMS;RENEWAL_DAILY%' AND stat = 'PUSHED'";	
	$result = mysqli_query($con,$sql) or die('1. '.mysqli_error($con));
	
	$row=mysqli_fetch_assoc($result);
	if(!empty($row))
		$data['PRIO_PS']['PUSHED'] = $row['total'];
	
	$data['PRIO_PS']['SUCCESS'] = '0';
		
	while($row=mysqli_fetch_assoc($result)) $data['PRIO_PS'][strtoupper($row['stat'])] = $row['total'];	
	// SUCCESS 
	$sql = "SELECT COUNT(1) AS total FROM xmp.tbl_msgtransact 
			WHERE subject LIKE 'MT;CHARGE;SMS;RENEWAL_DAILY%' AND msgstatus = 'DELIVERED' AND msisdn IN 
		   (SELECT dest FROM dbpush.push_buffer_priority WHERE subject LIKE 'MT;CHARGE;SMS;RENEWAL_DAILY%' AND stat = 'PUSHED');";	
	$result = mysqli_query($con,$sql) or die('2. '.mysqli_error($con));
	while($row=mysqli_fetch_assoc($result)) $data['PRIO_PS']['SUCCESS'] = $row['total'];	

	echo 'NON_PRIO_PS'.PHP_EOL;
	// NON PRIO PS
	// total QUEUE & PUSHED
	$data['NON_PRIO_PS']['ON_QUEUE'] = '0';
	$data['NON_PRIO_PS']['PUSHED'] = '0';
	$data['NON_PRIO_PS']['SUCCESS'] = '0';
		
	$sql = "SELECT stat, COUNT(1) AS total FROM dbpush.push_buffer_nonpriority WHERE subject LIKE 'MT;CHARGE;SMS;RENEWAL_DAILY%' GROUP BY stat";	
	$result = mysqli_query($con,$sql) or die('3. '.mysqli_error($con));
	while($row=mysqli_fetch_assoc($result)) $data['NON_PRIO_PS'][strtoupper($row['stat'])] = $row['total'];	

	// SUCCESS 
	$sql = "SELECT COUNT(1) AS total FROM xmp.tbl_msgtransact WHERE subject LIKE 'MT;CHARGE;SMS;RENEWAL_DAILY%' AND msgstatus = 'DELIVERED' 
	AND msisdn IN (SELECT dest FROM dbpush.push_buffer_nonpriority WHERE subject LIKE 'MT;CHARGE;SMS;RENEWAL_DAILY%' AND stat = 'PUSHED')";	
	$result = mysqli_query($con,$sql) or die('4. '.mysqli_error($con));
	while($row=mysqli_fetch_assoc($result)) $data['NON_PRIO_PS']['SUCCESS'] = $row['total'];	

	echo 'PRIO_SPC'.PHP_EOL;
	// PRIO SPC
	$sql = "SELECT stat, COUNT(1) AS total FROM dbpush.push_buffer_priority WHERE subject 
			LIKE 'MT;CHARGE;SMS;RETRY_MONTHLY%' AND priority = 1 GROUP BY stat";	
	$result = mysqli_query($con,$sql) or die('5. '.mysqli_error($con));
		
	$data['PRIO_SPC']['ON_QUEUE'] = '0';
	$data['PRIO_SPC']['PUSHED'] = '0';
	$data['PRIO_SPC']['SUCCESS'] = '0';
		
	while($row=mysqli_fetch_assoc($result)) $data['PRIO_SPC'][strtoupper($row['stat'])] = $row['total'];	

	// SUCCESS 
	$sql = "SELECT COUNT(1) AS total FROM xmp.tbl_msgtransact WHERE 
			subject LIKE 'MT;CHARGE;SMS;RETRY_MONTHLY%' AND msgstatus = 'DELIVERED' AND msisdn IN 
			(SELECT dest FROM dbpush.push_buffer_priority WHERE subject LIKE 'MT;CHARGE;SMS;RETRY_MONTHLY%' AND priority = 1 AND stat = 'PUSHED')";	
	$result = mysqli_query($con,$sql) or die('6. '.mysqli_error($con));
	while($row=mysqli_fetch_assoc($result)) $data['PRIO_SPC']['SUCCESS'] = $row['total'];	
		
	echo 'NON_PRIO_SPC'.PHP_EOL;
	// NON PRIO SPC
	// total QUEUE & PUSHED
	$data['NON_PRIO_SPC']['ON_QUEUE'] = '0';
	$data['NON_PRIO_SPC']['PUSHED'] = '0';
	$data['NON_PRIO_SPC']['SUCCESS'] = '0';
		
	$sql = "SELECT stat, COUNT(1) AS total FROM dbpush.push_buffer_nonpriority WHERE subject LIKE 'MT;CHARGE;SMS;RETRY_MONTHLY%' GROUP BY stat";	
	$result = mysqli_query($con,$sql) or die('7. '.mysqli_error($con));
	while($row=mysqli_fetch_assoc($result)) $data['NON_PRIO_SPC'][strtoupper($row['stat'])] = $row['total'];	

	// SUCCESS 
	$sql = "SELECT COUNT(1) AS total FROM xmp.tbl_msgtransact WHERE 
			subject LIKE 'MT;CHARGE;SMS;RETRY_MONTHLY%' AND msgstatus = 'DELIVERED' AND msisdn IN 
			(SELECT dest FROM dbpush.push_buffer_nonpriority WHERE subject LIKE 'MT;CHARGE;SMS;RETRY_MONTHLY%' AND stat = 'PUSHED')";	
	$result = mysqli_query($con,$sql) or die('8. '.mysqli_error($con));
	while($row=mysqli_fetch_assoc($result)) $data['NON_PRIO_SPC']['SUCCESS'] = $row['total'];

	
	// MO SPC
	// QUEUE
	$data['MO_SPC']['ON_QUEUE'] = '0';
	$data['MO_SPC']['PUSHED'] = '0';
	$data['MO_SPC']['SUCCESS'] = '0';
	
	$data['MO_PS']['ON_QUEUE'] = '0';
	$data['MO_PS']['PUSHED'] = '0';
	$data['MO_PS']['SUCCESS'] = '0';

	$result = '';
	// PUSHED 
	echo 'MO_SPC PUSHED'.PHP_EOL;

	$sql = "SELECT COUNT(1) AS total FROM xmp.tbl_msgtransact WHERE channel = 'SPECIAL' AND 
		    subject LIKE 'MT;CHARGE;SPECIAL;TEXT%' AND date(MSGTIMESTAMP)='$date'";
	$result = mysqli_query($con,$sql) or die('9. '.mysqli_error($con));
	while($row=mysqli_fetch_assoc($result)) $data['MO_SPC']['PUSHED'] = $row['total'];	

	$sql = "SELECT COUNT(1) AS total FROM xmp.tbl_msgtransact WHERE channel = 'sms' AND 
			subject LIKE 'MT;CHARGE;SMS;TEXT%' AND date(MSGTIMESTAMP)='$date'";
	$result = mysqli_query($con,$sql) or die('9. '.mysqli_error($con));
	while($row=mysqli_fetch_assoc($result)) $data['MO_PS']['PUSHED'] = $row['total'];	
	
	// SUCCESS
	$sql = "SELECT COUNT(1) AS total FROM xmp.tbl_msgtransact WHERE channel = 'SPECIAL' AND 
			subject LIKE 'MT;CHARGE;SPECIAL;TEXT%' AND msgstatus = 'DELIVERED' AND date(MSGTIMESTAMP)='$date'";
	$result = mysqli_query($con,$sql) or die('10. '.mysqli_error($con));
    while($row=mysqli_fetch_assoc($result)) $data['MO_SPC']['SUCCESS'] = $row['total'];

	$sql = "SELECT COUNT(1) AS total FROM xmp.tbl_msgtransact WHERE channel = 'sms' AND 
			subject LIKE 'MT;CHARGE;SMS;TEXT%' AND msgstatus = 'DELIVERED' AND date(MSGTIMESTAMP)='$date'";
	$result = mysqli_query($con,$sql) or die('10. '.mysqli_error($con));
    while($row=mysqli_fetch_assoc($result)) $data['MO_PS']['SUCCESS'] = $row['total'];
	// ------------------
	
	echo 'MO_SPC ON_QUEUE'.PHP_EOL;
	$command = 'ls -l /DATA/buffer/mospc/[0-9] | wc -l';
	$data['MO_SPC']['ON_QUEUE'] = intval(trim(exec($command))) - 29;
	
	$command = 'ls -l /DATA/buffer/mo/[0-9] | wc -l';
	$data['MO_PS']['ON_QUEUE'] = intval(trim(exec($command))) - 29;
	echo 'FAILED_STAT'.PHP_EOL;

	/*
	root@vas-app ~]# cat /DATA/app/xmp2012/logs/moh3i/collection/reason_20201210
     15
  42818 \":\"200_3_109\",\"
     93 \":\"200_3_3\",\"
    470 \":\"4010\",\"
  66220 \":\"4012\",\"
    395 \":\"5030\",\"
    448 \":\"5031\",\"
   1327 \":\"\",\"
[root@vas-app ~]# cat /DATA/app/xmp2012/logs/moh3i/collection/elapse_20201210
   1044 0
  37717 1
     34 10
  37583 2
  19354 3
   9299 4
   4131 5
   1727 6
    639 7
    194 8
     64 9
[root@vas-app ~]# cat /DATA/app/xmp2012/logs/moh3i/collection/tph_20201210
111786
	*/
	//$command = 'sh /home/production/breakdown_reason_cur_1_pasthour.sh';
	
	
	// Temp Shutdown Process
	/*
	$command = 'cat /DATA/app/xmp2012/logs/moh3i/collection/reason_'.date('Ymd');
	$output = array();
	exec($command,$output);
	
	$i = 0;
	foreach($output as $output) {
		$out = trim($output);
		$array = explode(':',$out);
		if($i==0 && count($array)==1) {
			$data['FAILED_STAT'][] = array('CODE' => 'TIMEOUT','VALUE' => $array[0]);	
		} else {
			$array[1] = trim(trim($array[1],'\"'));
			$array[1] = trim($array[1],'\",');
			if($array[1]) {
				$data['FAILED_STAT'][] = array('CODE' => $array[1],'VALUE' => trim(trim($array[0],'\"')));
			} else {
				$data['FAILED_STAT'][] = array('CODE' => 'SUCCESS','VALUE' => trim(trim($array[0],'\"')));
			}
		}
		$i++;
	}

	echo 'RESPONS TIME'.PHP_EOL;
	//$command = 'sh /home/production/elapse_cur_1_pasthour.sh';
	$command = 'cat /DATA/app/xmp2012/logs/moh3i/collection/elapse_'.date('Ymd');
	$output = array();
	exec($command,$output);
	
	foreach($output as $output) {
		$array = explode(' ',trim($output));
		$data['RESPONS_TIME'][] = array('SECOND' => trim($array[1]),'VALUE'=>trim($array[0]));
	}
	
	echo 'TPH'.PHP_EOL.PHP_EOL;
	//$command = 'sh /home/production/TPH_check.sh';
	$command = 'cat /DATA/app/xmp2012/logs/moh3i/collection/tph_'.date('Ymd');
	$output = array();
	exec($command,$output); //print_r($output);exit;
	
	foreach($output as $output) {
		$data['TPH'] = trim($output);
	}
	*/
	
	$data['date'] = $date;
	$url = 'http://103.77.79.130:2020/h3i/cstools_admin/add_push_statistic.php';
	$post_data = json_encode($data);
	 
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,array('data'=>$post_data));
	// Receive server response ...
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec($ch);

	curl_close ($ch);
//	print_r(json_encode($output));
//	exit;
//	echo json_encode($data).PHP_EOL;
	echo 'OUTPUT = '.$server_output;
	