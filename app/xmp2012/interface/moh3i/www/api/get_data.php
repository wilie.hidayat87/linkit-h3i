<?php

function countData($date, $folder, $arr, $init, $status, $single = 0)
{
    $price_1000 = array("P", "S", "D");
    $price_2000 = array("M");

    $price = 0;

    if($single <> 0)
    {
        $dp_path = $folder . "/" . $init . "/" . $arr . "/" . $status . "/";

        $count = shell_exec("ls {$dp_path} | wc -l");
        $count = str_replace("\n", "", $count);

        if (in_array(strtoupper(substr($arr[$i], 0, 1)), $price_1000)) $price = 1000;

        if (in_array(strtoupper(substr($arr[$i], 0, 1)), $price_2000)) $price = 2000;

        echo "Subject [".strtoupper(substr($arr[$i], 0, 1))."], Price = ".$price.", Counting data [".$dp_path."] = " . $count . "\n\r";

        $result[] = array($arr => $count, "price" => $price);
    }
    else
    {
        for($i=0; $i<count($arr); $i++)
        {
            $dp_path = $folder[$init] . "/" . $arr[$i] . "/" . $status . "/";

            $count = shell_exec("ls {$dp_path} | wc -l");
            $count = str_replace("\n", "", $count);

            if (in_array(strtoupper(substr($arr[$i], 0, 1)), $price_1000)) $price = 1000;

            if (in_array(strtoupper(substr($arr[$i], 0, 1)), $price_2000)) $price = 2000;

            echo "Subject [".strtoupper(substr($arr[$i], 0, 1))."], Price = ".$price.", Counting data [".$dp_path."] = " . $count . "\n\r";

            $result[] = array($arr[$i] => $count, "price" => $price);
        }
    }

    return array("date" => $date, "result" => $result);
}

/*
$_REQUEST['subject'] = "PS4"; 
$_REQUEST['init'] = "renewal_daily"; 
$_REQUEST['date'] = "2019-11-10";
$_REQUEST['single'] = 1;
*/

$date = (!empty($_REQUEST['date'])) ? $_REQUEST['date'] : date('Y-m-d');
$folder_date = str_replace("-","",$date);

$folder = array(
	"rd" 	=> "/DATA/app/xmp2012/logs/moh3i/lock/DP_{$folder_date}/renewal_daily",
	"rnm" 	=> "/DATA/app/xmp2012/logs/moh3i/lock/DP_{$folder_date}/renewal_monthly",
	"rm" 	=> "/DATA/app/xmp2012/logs/moh3i/lock/DP_{$folder_date}/retry_monthly"
);

$rd_subject = array('DAILY','PS','PS1','PS3','PS4','PS5','PS6','PS7','PS8','PS9','PS10','PS11','PS12','PS13','PS14','PS15');
$rnm_subject = array('MS','MS_SPC','MS_SPC2','SPC3','SPC4');
$rm_subject = array('MS','MS_SPC','MS_SPC2','SPC3','SPC4');

if(isset($_REQUEST['subject']) == true && isset($_REQUEST['init']) == true)
{
    $result[] = countData(
        $date,
        "/DATA/app/xmp2012/logs/moh3i/lock/DP_{$folder_date}", 
        $_REQUEST['subject'], 
        $_REQUEST['init'], 
        'DELIVERED', 
        $_REQUEST['single']
    );
}
else
{
    $result[] = countData($date, $folder, $rd_subject, 'rd', 'DELIVERED');
    $result[] = countData($date, $folder, $rnm_subject, 'rnm', 'DELIVERED');
    $result[] = countData($date, $folder, $rm_subject, 'rm', 'DELIVERED');
}

echo json_encode($result);
