<?php
$subject = (isset($_GET['subject']) ? $_GET['subject'] : "partner");
$sumdate = (isset($_GET['sumdate']) ? urldecode($_GET['sumdate']) : date("Y-m-d"));
$by = (isset($_GET['by']) ? $_GET['by'] : "ISJ;rpt_subject;REG");
$trx_backup_table = (isset($_GET['trx_backup_table']) ? $_GET['trx_backup_table'] : "tbl_msgtransact");

$rpt = mysql_connect('10.10.13.85','reports','reportsp455w0rd');
$db_selected1 = mysql_select_db('reports',$rpt);

$xmp = mysql_connect('10.10.13.85','xmp','xmpp455w0rd');
$db_selected2 = mysql_select_db('xmp',$xmp);

$cmp = mysql_connect('10.10.13.85','cmp','cmpp455w0rd');
$db_selected3 = mysql_select_db('cmp',$cmp);

switch($subject)
{
	case "rpt_operator" :
	
		$sql = "SELECT o.operator_alias AS Operator, s.sumdate AS SumDate, SUM(s.total) AS Total, SUM(s.total)*s.gross AS Revenue, DATE_FORMAT(NOW(), '%d/%m/%Y %h.%i %p') AS last_update FROM rpt_service2 s INNER JOIN tbl_operator o ON s.operator = o.id WHERE s.sumdate = '{$sumdate}' AND s.msgstatus = 'DELIVERED' AND s.subject <> 'MO;PULL;SMS;ERROR' AND s.gross <> 0;";
		
		$query = mysql_query($sql, $rpt);
		while($row = mysql_fetch_array($query)){
			$data[] = array(
				"operator" => $row['Operator']
			   ,"sumdate" => $row['SumDate']
			   ,"total" => $row['Total']
			   ,"revenue" => $row['Revenue']
			   ,"last_update" => $row['last_update']
			   );
		}
	
	break;
	
	case "rpt_service" :
	
		$sql = "SELECT o.operator_alias AS Operator, s.sumdate AS SumDate, o.shortcode AS SDC, s.service, CASE WHEN UPPER(SPLIT_STRING(s.subject,';',4)) = 'TEXT' THEN 'REG' ELSE UPPER(SPLIT_STRING(s.subject,';',4)) END AS MainSubject, CASE WHEN SPLIT_STRING(s.subject,';',5) IS NULL THEN SPLIT_STRING(s.subject,';',4) ELSE SPLIT_STRING(s.subject,';',5) END AS SubKeyword, s.gross AS price, SUM(s.total) AS Total, SUM(s.total)*s.gross AS Revenue, DATE_FORMAT(NOW(), '%d/%m/%Y %h.%i %p') AS last_update FROM rpt_service2 s INNER JOIN tbl_operator o ON s.operator = o.id WHERE s.sumdate = '{$sumdate}' AND s.msgstatus = 'DELIVERED' AND s.subject <> 'MO;PULL;SMS;ERROR' GROUP BY s.service, MainSubject, SubKeyword;";
		
		$query = mysql_query($sql, $rpt);
		while($row = mysql_fetch_array($query)){
			
			$service = $row['service'];
			$Sub_Keyword = strtoupper((string)$row['SubKeyword']);
			
			if($Sub_Keyword == 'TEXT')
				$subkeyword = "NORMAL";
			else
				$subkeyword = $row['SubKeyword'];
			
			$sql = "SELECT COUNT(DISTINCT s.msisdn) AS sub_active FROM msisdn_subject ms JOIN subscription s ON ms.msisdn=s.msisdn WHERE s.active='1' AND s.service = '{$service}' AND ms.subject = '{$subkeyword}';";
			
			$sql = mysql_query($sql, $xmp);
			$subactive = mysql_fetch_array($sql);
			
			$data[] = array(
				"operator" => $row['Operator']
			   ,"sumdate" => $row['SumDate']
			   ,"sdc" => $row['SDC']
			   ,"service" => $service
			   ,"mainsubject" => $row['MainSubject']
			   ,"subkeyword" => ($subkeyword == 'NORMAL') ? '' : $subkeyword
			   ,"price" => $row['price']
			   ,"subactive" => (count($subactive) > 0) ? $subactive['sub_active'] : 0
			   ,"total" => $row['Total']
			   ,"revenue" => $row['Revenue']
			   ,"last_update" => $row['last_update']
			   );
		}
	
	break;
	
	case "rpt_subject" :
	
		if($by == "REG")
		{
			//$sql = "SELECT DISTINCT o.operator_alias AS operator, d.subject, IF(SPLIT_STRING(d.subject,';',4)='TEXT','REG','') as mainsubject, SPLIT_STRING(d.subject,';',5) as subkeyword, d.service, d.msgstatus, d.closereason, d.total, s.gross AS price, IF(d.msgstatus='DELIVERED',d.total*s.gross,0) AS revenue FROM rpt_detail d INNER JOIN tbl_operator o ON o.ID = d.operator INNER JOIN rpt_service2 s ON s.service = d.service AND s.subject = d.subject WHERE d.sumdate = '{$sumdate}' AND d.subject LIKE 'MT;PUSH;SMS;TEXT%' OR d.subject LIKE 'MT;PUSH;UMB;TEXT%';";
			
			$sql = "select s.sumdate, o.operator_alias AS operator, 'REG' AS mainsubject, SPLIT_STRING(s.subject,';',5) as subkeyword, s.service, s.msgstatus, d.closereason, IF(s.msgstatus='DELIVERED',s.total,d.total) AS total, s.gross AS price, IF(s.msgstatus='DELIVERED',(s.gross*d.total),'') AS revenue from rpt_service2 s INNER JOIN rpt_detail d ON s.sumdate = d.sumdate and s.subject = d.subject and s.service = d.service and d.msgstatus = s.msgstatus INNER JOIN tbl_operator o ON o.ID = d.operator WHERE s.sumdate = '{$sumdate}' and left(s.subject, 7) = 'MT;PUSH' AND SPLIT_STRING(s.subject,';',4) = 'TEXT';";
			
			$query = mysql_query($sql, $rpt);
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => $row['operator']
					   ,"mainsubject" => $row['mainsubject']
					   ,"subkeyword" => $row['subkeyword']
					   ,"service" => $row['service']
					   ,"msgstatus" => $row['msgstatus']
					   ,"closereason" => $row['closereason']
					   ,"price" => $row['price']
					   ,"revenue" => $row['revenue']
					   ,"total" => $row['total']
					   );
				}
				//print_r($data);
			}
		}
		
		if($by == "UNREG")
		{
			//$sql = "SELECT o.operator_alias AS operator, 'UNREG' as mainsubject, '' as subkeyword, d.service, d.msgstatus, d.closereason, d.total FROM rpt_detail d INNER JOIN tbl_operator o ON o.ID = d.operator WHERE d.sumdate = '{$sumdate}' AND d.subject = 'MO;PULL;SMS;HANDLERCREATOR';";
			//$sql = "SELECT IF(UPPER(o.name)='INDOSAT','ISAT','XL') AS operator, 'UNREG' AS mainsubject, ms.subject As subkeyword, s.name AS service, t.msgstatus AS msgstatus, t.closereason AS closereason, count(distinct t.msisdn) as total FROM reply r INNER JOIN charging c ON r.charging_id = c.id INNER JOIN mechanism m ON m.id = r.mechanism_id INNER JOIN service s ON s.id = m.service_id INNER JOIN tbl_msgtransact t ON t.serviceid = c.charging_id INNER JOIN msisdn_subject ms ON ms.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE t.subject='MO;PULL;SMS;HANDLERCREATOR' and UPPER(t.msgdata) = CONCAT('UNREG',' ',s.name) AND date(msgtimestamp) = '{$sumdate}' GROUP BY operator, mainsubject, subkeyword, service, msgstatus, closereason;";
			
			//$sql = "SELECT IF(UPPER(o.name)='INDOSAT','ISAT','XLSDP') AS operator, 'UNREG' AS mainsubject, ms.subject As subkeyword, s.name AS service, t.msgstatus AS msgstatus, t.closereason AS closereason, count(distinct t.msisdn) as total FROM reply r INNER JOIN charging c ON r.charging_id = c.id INNER JOIN mechanism m ON m.id = r.mechanism_id INNER JOIN service s ON s.id = m.service_id INNER JOIN {$trx_backup_table} t ON t.serviceid = c.charging_id INNER JOIN msisdn_subject ms ON ms.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE t.subject='MO;PULL;SMS;HANDLERCREATOR' AND t.msgdata regexp 'UNREG|Unreg|unreg' AND date(t.msgtimestamp) = '{$sumdate}' GROUP BY operator, mainsubject, subkeyword, service, msgstatus, closereason;";
			
			$sql = "SELECT IF(UPPER(o.name)='INDOSAT','ISAT','XLSDP') AS operator, 'UNREG' AS mainsubject, ms.subject As subkeyword, t.service, t.msgstatus AS msgstatus, t.closereason AS closereason, count(distinct t.msisdn) as total FROM {$trx_backup_table} t INNER JOIN msisdn_subject ms ON ms.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE t.subject='MO;PULL;SMS;HANDLERCREATOR' AND t.msgdata regexp 'UNREG|Unreg|unreg' AND date(t.msgtimestamp) = '{$sumdate}' GROUP BY operator, mainsubject, subkeyword, service, msgstatus, closereason;";
			
			$query = mysql_query($sql, $xmp);
			
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => $row['operator']
					   ,"mainsubject" => $row['mainsubject']
					   ,"subkeyword" => $row['subkeyword']
					   ,"service" => $row['service']
					   ,"msgstatus" => $row['msgstatus']
					   ,"closereason" => $row['closereason']
					   ,"price" => 0
					   ,"revenue" => 0
					   ,"total" => $row['total']
					   );
				}
			}
		}
		
		if($by == "DAILYPUSH")
		{
			//$sql = "SELECT subject, service, msgstatus, closereason, COUNT(id) AS total FROM tbl_msgtransact WHERE DATE(msgtimestamp) = '{$sumdate}' AND subject LIKE 'MT;PUSH;SMS;DAILYPUSH%' GROUP BY subject, service, msgstatus, closereason;";
			
			//$sql = "SELECT o.operator_alias AS operator, IF(SPLIT_STRING(d.subject,';',4)='DAILYPUSH','DAILYPUSH','') as mainsubject, SPLIT_STRING(d.subject,';',5) as subkeyword, d.service, d.msgstatus, d.closereason, d.total, s.gross AS price, IF(d.msgstatus='DELIVERED',d.total*s.gross,0) AS revenue  FROM rpt_detail d INNER JOIN tbl_operator o ON o.ID = d.operator INNER JOIN rpt_service2 s ON s.service = d.service AND s.subject = d.subject WHERE d.sumdate = '{$sumdate}' AND d.subject LIKE 'MT;PUSH;SMS;DAILYPUSH%' OR d.subject LIKE 'MT;PUSH;UMB;DAILYPUSH%';";
			
			$sql = "select s.sumdate, o.operator_alias AS operator, 'DAILYPUSH' AS mainsubject, SPLIT_STRING(s.subject,';',5) as subkeyword, s.service, s.msgstatus, d.closereason, IF(s.msgstatus='DELIVERED',s.total,d.total) AS total, s.gross AS price, IF(s.msgstatus='DELIVERED',(s.gross*d.total),'') AS revenue from rpt_service2 s INNER JOIN rpt_detail d ON s.sumdate = d.sumdate and s.subject = d.subject and s.service = d.service and d.msgstatus = s.msgstatus INNER JOIN tbl_operator o ON o.ID = d.operator WHERE s.sumdate = '{$sumdate}' and left(s.subject, 7) = 'MT;PUSH' AND SPLIT_STRING(s.subject,';',4) = 'DAILYPUSH';";
			
			$query = mysql_query($sql, $rpt);
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => $row['operator']
					   ,"mainsubject" => $row['mainsubject']
					   ,"subkeyword" => $row['subkeyword']
					   ,"service" => $row['service']
					   ,"msgstatus" => $row['msgstatus']
					   ,"closereason" => $row['closereason']
					   ,"price" => $row['price']
					   ,"revenue" => $row['revenue']
					   ,"total" => $row['total']
					   );
				}
			}
		}
		
		if($by == "LANDING")
		{
			$sql = "SELECT servicename, UPPER(operator) AS operator, COUNT(id) AS total FROM pixel_storage WHERE DATE(date_time) = '{$sumdate}' GROUP BY servicename, operator;";
		
			$query = mysql_query($sql, $cmp);
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => "ISAT"
					   ,"mainsubject" => $by
					   ,"subkeyword" => ""
					   ,"service" => $row['servicename']
					   ,"msgstatus" => ''
					   ,"closereason" => ''
					   ,"price" => $row['price']
					   ,"revenue" => $row['Revenue']
					   ,"total" => $row['total']
					   );
				}
			}
		}
		
		if($by == "BLACKLIST")
		{
			$sql = "select count(distinct msisdn) AS total from blacklist_msisdn;";
		
			$query = mysql_query($sql, $xmp);
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => "ISAT"
					   ,"mainsubject" => "BLACKLIST"
					   ,"subkeyword" => ""
					   ,"service" => ""
					   ,"msgstatus" => ''
					   ,"closereason" => ''
					   ,"price" => $row['price']
					   ,"revenue" => $row['Revenue']
					   ,"total" => $row['total']
					   );
				}
			}
		}
	
	break;
	
	case "partner" :
	
		list($partner, $table, $by) = explode(";", $by);
		
		switch($table)
		{
			case "rpt_service" :
				
				$sql = "SELECT r.*, IF(r.msgstatus='DELIVERED',(r.total*r.price),'0') AS revenue, DATE_FORMAT(NOW(), '%d/%m/%Y %h.%i %p') AS last_update FROM (SELECT 'ISAT' AS operator, DATE(msgtimestamp) AS sumdate, t.adn AS sdc, t.service, 'REG' AS mainsubject, msp.subject as subkeyword, t.price, t.msgstatus, COUNT(1) AS total FROM {$trx_backup_table} t INNER JOIN msisdn_subject_partner msp ON msp.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE DATE(msgtimestamp) = '{$sumdate}' AND msp.subject IN ('{$partner}') GROUP BY t.msgstatus, t.closereason, msp.subject) AS r;";
				
				$query = mysql_query($sql, $xmp);
				
				$i=0; $totsubactive = 0;
				while($row = mysql_fetch_array($query)){
					
					if($i==0)
					{
						$sql = "SELECT COUNT(1) AS total FROM msisdn_subject_partner msp INNER JOIN subscription s ON msp.msisdn = s.msisdn WHERE s.active = 1 AND msp.subject = '{$partner}' LIMIT 1;";
						
						$sql = mysql_query($sql, $xmp);
						$subactive = mysql_fetch_array($sql);
						$totsubactive = (count($subactive) > 0) ? $subactive['total'] : 0;
					}
					
					$data[] = array(
						"operator" => $row['operator']
					   ,"sumdate" => $row['sumdate']
					   ,"sdc" => $row['sdc']
					   ,"service" => $row['service']
					   ,"mainsubject" => $row['mainsubject']
					   ,"subkeyword" => $row['subkeyword']
					   ,"price" => $row['price']
					   ,"subactive" => $totsubactive
					   ,"total" => $row['total']
					   ,"revenue" => $row['revenue']
					   ,"last_update" => $row['last_update']
					   );
					   
					$i++;
				}
				
				//print_r($data);die;
		
			break;
			case "rpt_subject" :
			
			if($by == "REG")
			{
				//$sql = "SELECT DISTINCT o.operator_alias AS operator, d.subject, IF(SPLIT_STRING(d.subject,';',4)='TEXT','REG','') as mainsubject, SPLIT_STRING(d.subject,';',5) as subkeyword, d.service, d.msgstatus, d.closereason, d.total, s.gross AS price, IF(d.msgstatus='DELIVERED',d.total*s.gross,0) AS revenue FROM rpt_detail d INNER JOIN tbl_operator o ON o.ID = d.operator INNER JOIN rpt_service2 s ON s.service = d.service AND s.subject = d.subject WHERE d.sumdate = '{$sumdate}' AND d.subject LIKE 'MT;PUSH;SMS;TEXT%' OR d.subject LIKE 'MT;PUSH;UMB;TEXT%';";
				
				$sql = "SELECT r.*, IF(r.msgstatus='DELIVERED',(r.total*r.price),'0') AS revenue FROM (SELECT DATE(msgtimestamp) AS sumdate, 'ISAT' AS operator, 'REG' AS mainsubject, msp.subject as subkeyword, t.service, t.msgstatus, t.closereason, t.price, COUNT(1) AS total FROM {$trx_backup_table} t INNER JOIN msisdn_subject_partner msp ON msp.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE DATE(msgtimestamp) = '{$sumdate}' AND msp.subject IN ('{$partner}') AND LEFT(t.subject, 7) = 'MT;PUSH' AND SPLIT_STRING(t.subject,';',4) = 'TEXT' GROUP BY sumdate, operator, mainsubject, subkeyword, t.service, t.msgstatus, t.closereason, t.price) AS r;";
				
				$query = mysql_query($sql, $xmp);
				if(mysql_num_rows($query) > 0)
				{
					while($row = mysql_fetch_array($query)){
						
						$data[] = array(
						   "sumdate" => $sumdate
						   ,"operator" => $row['operator']
						   ,"mainsubject" => $row['mainsubject']
						   ,"subkeyword" => $row['subkeyword']
						   ,"service" => $row['service']
						   ,"msgstatus" => $row['msgstatus']
						   ,"closereason" => $row['closereason']
						   ,"price" => $row['price']
						   ,"revenue" => $row['revenue']
						   ,"total" => $row['total']
						   );
					}
					//print_r($data);
				}
			}
			
			if($by == "DAILYPUSH")
			{
				//$sql = "SELECT DISTINCT o.operator_alias AS operator, d.subject, IF(SPLIT_STRING(d.subject,';',4)='TEXT','REG','') as mainsubject, SPLIT_STRING(d.subject,';',5) as subkeyword, d.service, d.msgstatus, d.closereason, d.total, s.gross AS price, IF(d.msgstatus='DELIVERED',d.total*s.gross,0) AS revenue FROM rpt_detail d INNER JOIN tbl_operator o ON o.ID = d.operator INNER JOIN rpt_service2 s ON s.service = d.service AND s.subject = d.subject WHERE d.sumdate = '{$sumdate}' AND d.subject LIKE 'MT;PUSH;SMS;TEXT%' OR d.subject LIKE 'MT;PUSH;UMB;TEXT%';";
				
				$sql = "SELECT r.*, IF(r.msgstatus='DELIVERED',(r.total*r.price),'0') AS revenue FROM (SELECT DATE(msgtimestamp) AS sumdate, 'ISAT' AS operator, 'DAILYPUSH' AS mainsubject, msp.subject as subkeyword, t.service, t.msgstatus, t.closereason, t.price, COUNT(1) AS total FROM {$trx_backup_table} t INNER JOIN msisdn_subject_partner msp ON msp.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE DATE(msgtimestamp) = '{$sumdate}' AND msp.subject IN ('{$partner}') AND LEFT(t.subject, 7) = 'MT;PUSH' AND SPLIT_STRING(t.subject,';',4) = 'DAILYPUSH' GROUP BY sumdate, operator, mainsubject, subkeyword, t.service, t.msgstatus, t.closereason, t.price) AS r;";
				
				$query = mysql_query($sql, $xmp);
				if(mysql_num_rows($query) > 0)
				{
					while($row = mysql_fetch_array($query)){
						
						$data[] = array(
						   "sumdate" => $sumdate
						   ,"operator" => $row['operator']
						   ,"mainsubject" => $row['mainsubject']
						   ,"subkeyword" => $row['subkeyword']
						   ,"service" => $row['service']
						   ,"msgstatus" => $row['msgstatus']
						   ,"closereason" => $row['closereason']
						   ,"price" => $row['price']
						   ,"revenue" => $row['revenue']
						   ,"total" => $row['total']
						   );
					}
					//print_r($data);
				}
			}
		
			if($by == "UNREG")
			{
				//$sql = "SELECT IF(UPPER(o.name)='INDOSAT','ISAT','XL') AS operator, 'UNREG' AS mainsubject, ms.subject As subkeyword, s.name AS service, t.msgstatus AS msgstatus, t.closereason AS closereason, count(distinct t.msisdn) as total FROM reply r INNER JOIN charging c ON r.charging_id = c.id INNER JOIN mechanism m ON m.id = r.mechanism_id INNER JOIN service s ON s.id = m.service_id INNER JOIN tbl_msgtransact t ON t.serviceid = c.charging_id INNER JOIN msisdn_subject ms ON ms.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE t.subject='MO;PULL;SMS;HANDLERCREATOR' and t.msgdata regexp 'UNREG|Unreg|unreg' AND date(msgtimestamp) = '{$sumdate}' GROUP BY operator, mainsubject, subkeyword, service, msgstatus, closereason;";
				
				//$sql = "SELECT DATE(msgtimestamp) AS sumdate, 'ISAT' AS operator, 'UNREG' AS mainsubject, msp.subject AS subkeyword, t.service, t.msgstatus, t.closereason, COUNT(1) AS total FROM {$trx_backup_table} t INNER JOIN msisdn_subject_partner msp ON t.msisdn = msp.msisdn WHERE DATE(t.msgtimestamp) = '{$sumdate}' AND t.subject='MO;PULL;SMS;HANDLERCREATOR' AND t.msgdata regexp 'UNREG|Unreg|unreg' AND msp.subject IN ('{$partner}') GROUP BY sumdate, operator, mainsubject, subkeyword, t.service, t.msgstatus, t.closereason;";
				
				$sql = "SELECT DATE(t.msgtimestamp) AS sumdate, 'ISAT' AS operator, 'UNREG' AS mainsubject, msp.subject AS subkeyword, t.service, t.msgstatus, t.closereason, COUNT(1) AS total FROM {$trx_backup_table} t INNER JOIN msisdn_subject_partner msp ON t.msisdn = msp.msisdn WHERE DATE(t.msgtimestamp) = '{$sumdate}' AND t.subject='MO;PULL;SMS;HANDLERCREATOR' AND t.msgdata regexp 'UNREG|Unreg|unreg' AND msp.subject IN ('{$partner}') GROUP BY sumdate, operator, mainsubject, subkeyword, t.service, t.msgstatus, t.closereason;";
				
				$query = mysql_query($sql, $xmp);
				
				if(mysql_num_rows($query) > 0)
				{
					while($row = mysql_fetch_array($query)){
						
						$data[] = array(
						   "sumdate" => $sumdate
						   ,"operator" => $row['operator']
						   ,"mainsubject" => $row['mainsubject']
						   ,"subkeyword" => $row['subkeyword']
						   ,"service" => $row['service']
						   ,"msgstatus" => $row['msgstatus']
						   ,"closereason" => $row['closereason']
						   ,"price" => 0
						   ,"revenue" => 0
						   ,"total" => $row['total']
						   );
					}
				}
			}
		
			if($by == "LANDING")
			{
				$sql = "SELECT DATE(date_time) AS sumdate, UPPER(partner) AS subkeyword, servicename, UPPER(operator) AS operator, COUNT(id) AS total FROM pixel_storage WHERE DATE(date_time) = '{$sumdate}' AND UPPER(partner) IN ('{$partner}') GROUP BY sumdate, subkeyword, servicename, operator;";
			
				$query = mysql_query($sql, $cmp);
				if(mysql_num_rows($query) > 0)
				{
					while($row = mysql_fetch_array($query)){
						
						$data[] = array(
						   "sumdate" => $sumdate
						   ,"operator" => "ISAT"
						   ,"mainsubject" => $by
						   ,"subkeyword" => $row['subkeyword']
						   ,"service" => $row['servicename']
						   ,"msgstatus" => ''
						   ,"closereason" => ''
						   ,"price" => 0
						   ,"revenue" => 0
						   ,"total" => $row['total']
						   );
					}
				}
			}
		
			break;
		}
		
	break;
}

mysql_close($rpt);
mysql_close($xmp);
mysql_close($cmp);

echo json_encode($data);
exit();