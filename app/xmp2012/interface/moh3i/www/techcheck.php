<?php require 'techcheck/config.php'; ?>
<html>
<head>
<title>Technical Check H3I</title>
<link rel="stylesheet" href="techcheck/css/my-css.css">
<link rel="stylesheet" href="techcheck/css/jquery-ui.css">
<script src="techcheck/js/jquery-1.12.4.js"></script>
<script src="techcheck/js/jquery-ui.js"></script>
<script src="techcheck/js/my-script.js"></script>
<script src="techcheck/js/tab-script.js"></script>
</head>
<body>

<!-- Tab links -->
<div class="tab">
  <button id="defrev" class="tablinks" onclick="openTabs(event, 'rev')">Revenue Checking</button>
  <button id="deftechcheck" class="tablinks" onclick="openTabs(event, 'tech')">Technical Check</button>
  <button id="defchangetpm" class="tablinks" onclick="openTabs(event, 'tpm')">TPM Management</button>
  <button id="defrateandanalysis" class="tablinks" onclick="openTabs(event, 'rateanalysis')">Rate And Analysis</button>
</div>

<!-- Tab content -->
<div id="rev" class="tabcontent"><?php include 'techcheck/revcheck.php'; ?></div>
<div id="tech" class="tabcontent"><?php include 'techcheck/technicalcheck.php'; ?></div>
<div id="tpm" class="tabcontent"><?php include 'techcheck/tpmmanagement.php'; ?></div>
<div id="rateanalysis" class="tabcontent"><?php include 'techcheck/rateandanalysis.php'; ?></div>

<?php
if(isset($_POST['iddeftab']))
{
	$iddeftab = $_POST['iddeftab'];
	
	if($iddeftab == 'defrev')
	{
?>
	<script language="javascript">
	openTabs(document.getElementById("defrev").id, 'rev');
	</script>
<?php
	}
	else if($iddeftab == 'defchangetpm')
	{
?>
	<script language="javascript">
	openTabs(document.getElementById("defchangetpm").id, 'tpm');
	</script>
<?php
	}
	else if($iddeftab == 'defrateandanalysis')
	{
?>
	<script language="javascript">
	openTabs(document.getElementById("defrateandanalysis").id, 'rateanalysis');
	</script>
<?php
	}
	else
	{
?>
	<script language="javascript">
	openTabs(document.getElementById("deftechcheck").id, 'tech');
	</script>
<?php
	}
}
else
{
?>
	<script language="javascript">
	openTabs(document.getElementById("defchangetpm").id, 'tpm');
	</script>
<?php
}
?>
</body>
</html>