<?php
/* error_reporting(1);

require_once '/app/xmp2012/interface/moh3i/xmp.php';

if ($_REQUEST) {
    $moProcessor = new manager_mo_processor ( );
    $response = $moProcessor->saveToFile($_REQUEST);        
    printResponse($_GET['transid']);
	
	$sms = $_REQUEST['sms'];

	if(substr(strtolower($sms), 0, 3) == "reg")
	{
		sendToCmpStorage($_REQUEST);
	}
        
} else {
    printResponse(3, $_GET['transid']);
}

function printResponse($trx_id, $status=0) {
	
	$response_str = array(
		0 => 'Message processed successfully',
	   -1 => 'Parameter incomplete',
		3 => 'System error', 
		
	);
	
	
	$response = '<?xml version="1.0" ?><MO><STATUS>'.$status.'</STATUS><TRANSID>'.$trx_id.'</TRANSID><MSG>'.$response_str[$status].'</MSG></MO>';
	
	header('Content-type: text/xml');
	echo $response;
}

function sendToCmp($GET) {
    list($serv,$servicename) = explode(" ", trim($GET['sms']));
    
    $data = array(
    	 "msisdn"	=> $GET['msisdn']
	,"service"	=> $servicename
	,"operator"	=> "moh3i"	
    );
    
    ob_start();
    print_r($data);
    $log = ob_get_clean();

    error_log("/mo/index.php " . date("Y-m-d H:i:s") . " " . $log . PHP_EOL, 3, "/app/xmp2012/logs/moh3i/cpa/cpa-" . date("Y-m-d"));

    $sPixel = new moh3i_cmp_manager_keyword();
    $sPixel->process_pixel($data);
}

function sendToCmpStorage($GET) {
  if(strpos(strtolower($GET['sms']), "unreg") === false || strpos(strtolower($GET['sms']), "confirm") === false){
    $datasms = explode(" ", trim($GET['sms']));
    if(count($datasms) > 2)
    {
		list($trigger,$servicename,$identifierP) = explode(" ", trim($GET['sms']));

		$data = array(
			 "msisdn"       => $GET['msisdn']
			,"service"      => strtolower($servicename)
			,"operator"     => "moh3i"
			,"pixelStorageID" => substr($identifierP, 1, strlen($identifierP))
		);

		$_GET['sms'] = implode(" ", array($trigger, $servicename));

		$sPixel = new moh3i_cmp_manager_keyword();
		$sPixel->process_pixel_2($data);
    }
  }
} */

error_reporting(1);
require_once '/app/xmp2012/interface/moh3i/xmp.php';
/* FROM Tri
?mobile_no=628978659181&short_code=99876&message=Reg+gemezz
Actual Param
sc=9233&transid=100002200101181121232906715021&trx_time=20181122062906&sms=REG+GEMEZZ&msisdn=62898xxxxxxxx
*/

$params = array();
$params['sc'] = $_REQUEST['short_code'];
$params['msisdn'] = $_REQUEST['mobile_no'];
$params['sms'] = $_REQUEST['message'];
$params['transid'] = date("YmdHis") . time();
$params['channel'] = 'sms';

$moProcessor = new manager_mo_processor ( );
$response = $moProcessor->saveToFile($params);

echo $response;