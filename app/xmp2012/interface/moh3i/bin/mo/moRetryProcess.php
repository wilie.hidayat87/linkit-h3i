#!/usr/bin/php
<?php
require_once '/app/xmp2012/interface/moh3i/xmp.php';

$lockPath = '/tmp/lock_moh3i_mo_retry_process';

if(file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit;
} else {
	touch($lockPath);
}

$config_main = loader_config::getInstance()->getConfig('main');

$bFile = buffer_file::getInstance();
$token_data = json_decode($bFile->get($config_main->path_token));
		
$process = false;
if((int)$token_data->result == 1)
	$process = true;
else
{
	// FORCE REQUEST TOKEN REFRESH & LOGIN
	$forceReqFile = '/tmp/lock_moh3i_request_refresh_n_login_process';
	if(!file_exists($forceReqFile)) touch($forceReqFile);
}

if($process)
{
	$mo = new moh3i_mo_processor ();
	$result = $mo->initprocess (
			array(
				"configname" 	=> "moretry", 
				"logname" 		=> "mo_retry_processor",
				"lockname" 		=> "moretry_processor",
				)
			);
}

if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

unlink($lockPath);

