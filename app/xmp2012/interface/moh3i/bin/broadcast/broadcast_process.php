#!/usr/bin/php
<?php
	require_once '/app/xmp2012/interface/moh3i/xmp.php';
	
	$lockPath = '/tmp/lock_moh3i_broadcast_process';
	
	if(file_exists($lockPath)) {
		echo "NOK - Lock File Exist on $lockPath \n";
		exit;
	} else {
		touch($lockPath);
	}
	
	
	$broadcast = new manager_broadcast ();

	$result = $broadcast->execute ();
	if ($result) {
		echo "OK \n";
	} else {
		echo "NOK \n";
	}
	
	unlink($lockPath);
