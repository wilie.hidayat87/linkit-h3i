#!/usr/bin/php
<?php

/*
$params = getopt('a:b:');
$ms = $params['a'];
$spc = $params['b'];
*/

$handle = @fopen("/app/xmp2012/interface/moh3i/config/dpTPM.txt", "r");

if ($handle) {
    while (($buffer = fgets($handle, 4096)) !== false) {
        $buffer = trim($buffer);
		
		if(substr($buffer,0,1) <> "#")
		{
			if(strpos($buffer,"RM;") !== FALSE)
			{
				list($init,$value) = explode("=", $buffer);
				
				switch($init)
				{
					case "RM;MS" : $ms = (int)$value; break;
					case "RM;SPC" : $spc = (int)$value; break;
					case "RM;FORKING" : $forking = (int)$value; break;
				}
			}
		}
	}
		
    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
    }
    fclose($handle);
}

function push($arrData)
{
	for($i=0;$i<10;$i++) shell_exec("nohup php /app/xmp2012/interface/moh3i/bin/broadcast/broadcast_queue_sb.php -s gemezz -p 2000.00 -t ".$i." -n ".$arrData['ms']." -w retry_monthly > /dev/null 2>&1");

	for($i=0;$i<10;$i++) shell_exec("nohup php /app/xmp2012/interface/moh3i/bin/broadcast/broadcast_queue_sb.php -s gemezz -p 1000.00 -t ".$i." -n ".$arrData['spc']." -w retry_monthly > /dev/null 2>&1");
}

// SET DEFAULT AND ASSIGN 2 PROCESSOR
$arrData = array(
	"ms" => (!empty($ms) ? $ms : 5),
	"spc" => (!empty($spc) ? $spc : 5)
);

$forking = (($forking == 1) ? true : false);

if ($forking) 
{
	switch ($pid = pcntl_fork ()) {
		case - 1 :
			//$log->write(array('level' => 'error', 'message' => "Forking failed"));
			die('Forking failed');
			break;
		case 0 :
			push($arrData);
			exit ();
			break;
		default :
			//pcntl_waitpid ( $pid, $status );
			break;
	}
} else {
	push($arrData);
}

