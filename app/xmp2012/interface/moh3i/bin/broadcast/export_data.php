#!/usr/bin/php
<?php
$lockPath = '/tmp/lock_moh3i_erase_mo_lock';
	
if(file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit;
} else {
	touch($lockPath);
}
	
$date = date("Ymd");
$dir = "/DATA/app/xmp2012/logs/moh3i/lock/{$date}";

$cdir = scandir($dir); 
foreach ($cdir as $key => $value) 
{ 
	if($value <> '.' && $value <> '..')
	{
		$thefile = $dir . DIRECTORY_SEPARATOR . $value;
		$thefiletime = filemtime($thefile);
		if(time() - $thefiletime >= 30)
		{
			unlink($thefile);
		}
	}
}

unlink($lockPath);