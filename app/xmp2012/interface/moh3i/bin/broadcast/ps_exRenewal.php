<?php
class pp
{
	// push project
	public $pid;
	public $msg;
	public $price;
	
	public function setPP($pid, $message, $price)
	{
		$this->pid = $pid;
		$this->msg = $message;
		$this->price = $price;
	}	
}

class s
{
	// subs
	public $time_updated;
	public $subscribed_until;
	public $msisdn;
	public $service;
	public $subject;
	public $s2;
	public $priority;
	public $success;
	
	public function __construct($time_updated, $subscribed_until, $msisdn, $service, $subject, $s2, $priority, $success)
	{
		$this->time_updated = $time_updated ;
		$this->subscribed_until = $subscribed_until;
		$this->msisdn = $msisdn;
		$this->service = $service;
		$this->subject = $subject;
		$this->s2 = $s2;
		$this->priority = $priority;
		$this->success = $success;
	}
}

$params = getopt('s:');
$subject = isset($params['s']) ? $params['s'] : '';
$subject = strtoupper($subject);

if(isset($subject))
{
	require_once '/app/xmp2012/interface/moh3i/xmp.php';

	$configMain = loader_config::getInstance()->getConfig('main');
	$configDB = loader_config::getInstance()->getConfig('database');
	
	$profile = "connDatabase1";
	$xmp['host'] = $configDB->profile[$profile]['host'];
	$xmp['user'] = $configDB->profile[$profile]['username'];
	$xmp['pass'] = $configDB->profile[$profile]['password'];
	$xmp['db'] = $configDB->profile[$profile]['database'];
	
	$profile = "connBroadcast";
	$dbpush['host'] = $configDB->profile[$profile]['host'];
	$dbpush['user'] = $configDB->profile[$profile]['username'];
	$dbpush['pass'] = $configDB->profile[$profile]['password'];
	$dbpush['db'] = $configDB->profile[$profile]['database'];

	$xmp = new mysqli($xmp['host'], $xmp['user'], $xmp['pass'], $xmp['db']);
	$dbpush = new mysqli($dbpush['host'], $dbpush['user'], $dbpush['pass'], $dbpush['db']);
	
	/* $xmp = mysql_connect($host,$user,$pass);
	mysql_select_db('xmp',$xmp);

	$dbpush = mysql_connect($host,$user,$pass);
	mysql_select_db('dbpush',$dbpush); */

	$pp = new pp();
	
	$sql = "SELECT pid,message,price FROM dbpush.push_projects WHERE DATE(created) = CURRENT_DATE AND `subject` = 'MT;CHARGE;SMS;RENEWAL_DAILY' LIMIT 1;";
	
	if ($stmt = $dbpush->prepare($sql)) {

		/* execute statement */
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result($pid, $message, $price);

		/* fetch values */
		while ($stmt->fetch()) {
			$pp->setPP($pid, $message, $price);
		}

		/* close statement */
		$stmt->close();
	}

	if(!empty($pp->pid))
	{
		$sql = "SELECT s.time_updated, s.subscribed_until, s.msisdn, ms.service, ms.subject, ms.s2, s.priority, s.success FROM xmp.subscription s INNER JOIN xmp.msisdn_subject ms ON s.msisdn = ms.msisdn WHERE s.service = 'gemezz' AND s.adn = '99876' AND s.operator = 'moh3i' AND s.active = '1' AND ms.subject IN ('{$subject}') AND CURRENT_DATE < DATE(s.active_until) AND CURRENT_DATE < DATE(s.subscribed_until) AND DATE(s.time_created) <> CURRENT_DATE AND DATE(s.subscribed_from) <> CURRENT_DATE AND DATE(s.renewal_date) = CURRENT_DATE;";
	
		//echo $sql;die;
		if ($stmt = $xmp->prepare($sql)) {

			/* execute statement */
			$stmt->execute();

			/* bind result variables */
			$stmt->bind_result($time_updated, $subscribed_until, $msisdn, $service, $subject, $s2, $priority, $success);

			$x=0; $i=0; 
			$buffer_prio = ""; $buffer_nonprio = "";
			$msisdns_prio = ""; $msisdns_nonprio = "";
			
			while ($stmt->fetch()) 
			{
				$s = new s($time_updated, $subscribed_until, $msisdn, $service, $subject, $s2, $priority, $success);
				
				$mt_data = loader_data::get ( 'mt' );
				$mt_data->inReply = NULL;
				$mt_data->msgId = date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) );
				$mt_data->adn = "99876";
				$mt_data->msisdn_subject = $s->subject;
				$mt_data->msgData = $pp->msg;
				$mt_data->price = $pp->price;
				$mt_data->operatorId = "1";
				$mt_data->channel = "sms";
				$mt_data->service = "gemezz";
				$mt_data->subject = "MT;CHARGE;SMS;RENEWAL_DAILY;".$s->subject;
				$mt_data->operatorName = "moh3i";
				$mt_data->msisdn = $s->msisdn;
				$mt_data->type = "daily"; // daily | monthly
				$mt_data->s2 = $s->s2;
				
				$obj = serialize ( $mt_data );
				$thread_id = substr ( $mt_data->msisdn, strlen ( $mt_data->msisdn ) - 1, 1 );
				
				$success = (int)$s->success;
				$priority = (int)$s->priority;
				
				if($success > 0 && $priority == 1)
				{
					$buffer_prio .= "(DEFAULT,'{$pp->pid}','{$mt_data->adn}','{$mt_data->msisdn}','{$mt_data->operatorId}','{$mt_data->service}','{$mt_data->subject}','{$mt_data->msgData}','{$mt_data->price}','ON_QUEUE',NOW(),'{$mt_data->msgId}','{$obj}','{$thread_id}','1'),";
					
					$msisdns_prio .= $mt_data->msisdn . ",";
				}
				else
				{
					$buffer_nonprio .= "(DEFAULT,'{$pp->pid}','{$mt_data->adn}','{$mt_data->msisdn}','{$mt_data->operatorId}','{$mt_data->service}','{$mt_data->subject}','{$mt_data->msgData}','{$mt_data->price}','ON_QUEUE',NOW(),'{$mt_data->msgId}','{$obj}','{$thread_id}','0'),";
					
					$msisdns_nonprio .= $mt_data->msisdn . ",";
				}
								
				if($i >= 100)
				{
					if(!empty($buffer_prio))
					{
						$buffer_prio = rtrim($buffer_prio, ",");
						
						$sql = "INSERT INTO dbpush.push_buffer_priority (id,pid,src,dest,oprid,service,subject,message,price,stat,created,tid,obj,thread_id,priority) VALUES {$buffer_prio};";
						
						if ($result = $dbpush -> query($sql)) 
						{
							echo $sql." [{$x}]...\n\r\n\r";
						
							$renewal_date = date("Y-m-d H:i:s", strtotime($configMain->custom_renewal));
							$sql2 = "UPDATE xmp.subscription SET renewal_date = '".$renewal_date."' WHERE active = 1 AND msisdn IN ({$msisdns_prio})";
							
							echo $sql2." [{$x}]...\n\r\n\r";
							
							$xmp -> query($sql2);
						
							$buffer_prio = "";
							$msisdns_prio = "";
						}
					}
					
					if(!empty($buffer_nonprio))
					{
						$buffer_nonprio = rtrim($buffer_nonprio, ",");
						
						$sql = "INSERT INTO dbpush.push_buffer_nonpriority (id,pid,src,dest,oprid,service,subject,message,price,stat,created,tid,obj,thread_id,priority) VALUES {$buffer_nonprio};";
						
						if ($result = $dbpush -> query($sql)) 
						{
							echo $sql." [{$x}]...\n\r\n\r";
						
							$renewal_date = date("Y-m-d H:i:s", strtotime($configMain->custom_renewal));
							$xmp -> query("UPDATE xmp.subscription SET renewal_date = '".$renewal_date."' WHERE active = 1 AND msisdn IN ({$msisdns_nonprio})");
							
							$buffer_nonprio = "";
							$msisdns_nonprio = "";
						}
					}
				
					$i=0;
				}
				
				$i++; $x++;
			}

			$buffer_prio = rtrim($buffer_prio, ",");
			$msisdns_prio = rtrim($msisdns_prio, ",");
			$buffer_nonprio = rtrim($buffer_nonprio, ",");
			$msisdns_nonprio = rtrim($msisdns_nonprio, ",");

			if(!empty($buffer_prio))
			{
				$buffer_prio = rtrim($buffer_prio, ",");
				
				$sql = "INSERT INTO dbpush.push_buffer_priority (id,pid,src,dest,oprid,service,subject,message,price,stat,created,tid,obj,thread_id,priority) VALUES {$buffer_prio};";
				
				if ($result = $dbpush -> query($sql)) 
				{
					echo $sql." [{$x}]...\n\r\n\r";
				
					$renewal_date = date("Y-m-d H:i:s", strtotime($configMain->custom_renewal));
					$xmp -> query("UPDATE xmp.subscription SET renewal_date = '".$renewal_date."' WHERE active = 1 AND msisdn IN ({$msisdns_prio})");
							
					$buffer_prio = "";
				}
			}

			if(!empty($buffer_nonprio))
			{
				$buffer_nonprio = rtrim($buffer_nonprio, ",");
				
				$sql = "INSERT INTO dbpush.push_buffer_nonpriority (id,pid,src,dest,oprid,service,subject,message,price,stat,created,tid,obj,thread_id,priority) VALUES {$buffer_nonprio};";
				
				if ($result = $dbpush -> query($sql)) 
				{
					echo $sql." [{$x}]...\n\r\n\r";
				
					$renewal_date = date("Y-m-d H:i:s", strtotime($configMain->custom_renewal));
					$xmp -> query("UPDATE xmp.subscription SET renewal_date = '".$renewal_date."' WHERE active = 1 AND msisdn IN ({$msisdns_nonprio})");
							
					$buffer_nonprio = "";
				}
			}
		}
		
		/* close statement */
		$stmt->close();
	}

	//echo $sql." ...\n\r\n\r";
	$dbpush->close();
	$xmp->close();
}
else
{
	echo "please put an argument, ex: ps_exRenewal.php -s 'PS60'";
}

exit();