#!/usr/bin/php
<?php
$params = getopt('c:p:');
$contentLabel = isset($params['c']) ? $params['c'] : '';
$price = isset($params['p']) ? $params['p'].".00" : '0.00';

require_once '/app/xmp2012/interface/moh3i/xmp.php';

$lockPath = '/tmp/lock_moh3i_broadcast_push_'.$contentLabel.'_'.str_replace(".00","",$price);

if(file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit;
} else {
	touch($lockPath);
}

$broadcast = new moh3i_broadcast_contentlabel ();

$result = $broadcast->execute (array("contentLabel" => $contentLabel, "price" => $price));

if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

unlink($lockPath);
