#!/usr/bin/php
<?php

$params = getopt('s:p:t:n:w:j:u:b:');

if(!isset($params['s']) && !isset($params['t']) && !isset($params['w'])) {
	echo 'Incomplete parameter. Usage' . "\n";
	echo 'broadcast_queue.php -s dg -t 0 w subject' . "\n";
	exit(0);
}

require_once '/app/xmp2012/interface/moh3i/xmp.php';
$broadcast = new manager_broadcast ();
$result = $broadcast->process ();
if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

exit(0);

