#!/usr/bin/php
<?php
require_once '/app/xmp2012/interface/moh3i/xmp.php';

$lockPath = '/tmp/lock_moh3i_broadcast_noreply180d';

if(file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit;
} else {
	touch($lockPath);
}

$broadcast = new moh3i_broadcast_sentcontent ();

$result = $broadcast->processSentNoReplied180D ();
if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

unlink($lockPath);