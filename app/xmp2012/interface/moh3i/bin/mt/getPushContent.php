<?php
$filename = "/app/xmp2012/logs/moh3i/setting_content_gold.txt";

$mysqli = new mysqli('10.10.13.85', 'push', 'pushp455w0rd', 'dbpush');

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$query = "SELECT content FROM push_content WHERE service='gold' ORDER BY modified DESC LIMIT 1";
$content_push="";
if ($stmt = $mysqli->prepare($query)) {

    /* execute statement */
    $stmt->execute();

    /* bind result variables */
    $stmt->bind_result($content);

    /* fetch values */
    while ($stmt->fetch()) {
        $content_push = $content;
    }

    /* close statement */
    $stmt->close();
}

/* close connection */
$mysqli->close();

$write = fopen($filename, "w");
fwrite($write, serialize($content_push));
fclose($write);