#!/usr/bin/php
<?php
	require_once '/app/xmp2012/interface/moh3i/xmp.php';
	
	$lockPath = '/tmp/lock_moh3i_mt_delay_process_retry';
	
	if(file_exists($lockPath)) {
		echo "NOK - Lock File Exist on $lockPath \n";
		exit;
	} else {
		touch($lockPath);
	}
	
	$mt = new moh3i_mt_retry ();
	
	$result = $mt->process();
	
	if ($result) {
		echo "OK \n";
	} else {
		echo "NOK \n";
	}
	
	unlink($lockPath);
