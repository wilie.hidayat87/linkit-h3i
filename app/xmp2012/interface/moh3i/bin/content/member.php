#!/usr/bin/php
<?php
require_once '/app/xmp2012/interface/moh3i/xmp.php';

$lockPath = '/tmp/lock_moh3i_membership_credential';

if (file_exists($lockPath)) {
    echo "NOK - Lock File Exist on $lockPath \n";
    exit;
} else {
    touch($lockPath);
}

$content = new moh3i_broadcast_sentcontent ();

$result = $content->process();

if ($result) {
    echo "OK \n";
} else {
    echo "NOK \n";
}

unlink($lockPath);


