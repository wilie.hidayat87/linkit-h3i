#!/usr/bin/php
<?php
//php /app/xmp2012/interface/moh3i/bin/content/random_mt.php -d 'connBroadcast' -o 'renewal' -s 'gemezz' -f 'success' -t 'renewal_daily'

$params = getopt('d:o:s:f:t:');
$profiledb = $params['d'];
$setfor = $params['o'];
$service = $params['s'];
$flag = $params['f'];
$type = $params['t'];

$lockPath = '/tmp/lock_moh3i_random_mt_'.$profiledb.'_'.$setfor.'_'.$service.'_'.$flag.'_'.$type;

if (file_exists($lockPath)) {
    echo "NOK - Lock File Exist on $lockPath \n";
    exit;
} else {
    touch($lockPath);
}

require_once("/app/xmp2012/interface/moh3i/config/database.php");
			
$dbConf = new config_database();
$profile = $profiledb;
//$profile = "connBroadcast";

$host = $dbConf->profile[$profile]['host'];
$username = $dbConf->profile[$profile]['username'];
$password = $dbConf->profile[$profile]['password'];
$db = $dbConf->profile[$profile]['database'];

$mysqli = new mysqli($host, $username, $password, $db);

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$SQL = sprintf("SELECT id, content FROM {$db}.random_content WHERE setfor = '%s' AND service = '%s' AND flag = '%s' AND `type` = '%s' AND used = 0 ORDER BY RAND() LIMIT 1;", $setfor, $service, $flag, $type);

echo "SQL = " . $SQL . PHP_EOL;

if ($result = $mysqli->query($SQL)) {
    	
	$total_used = (int)$result->num_rows;
	
	if($total_used < 1)
	{
		$update_random_mt = sprintf("UPDATE {$db}.random_content SET used = 0 WHERE setfor = '%s' AND service = '%s' AND flag = '%s' AND `type` = '%s'", $setfor, $service, $flag, $type);
		
		echo "SQL = " . $update_random_mt . PHP_EOL;
		
		$mysqli->query($update_random_mt);
		
		$result->close();
		
		$result = $mysqli->query($SQL);
	}
	
	while ($rec = $result->fetch_row()) {
		$id = $rec[0];
		$content = $rec[1];
		
		echo "ID = " . $id . PHP_EOL;
		echo "Content = " . $content . PHP_EOL;
	}
	
	$update_random_mt = sprintf("UPDATE {$db}.random_content SET used = 1, lastused = NOW() WHERE id = %d", $id);
	
	echo "SQL = " . $update_random_mt . PHP_EOL;
	
	$mysqli->query($update_random_mt);
	
	$SQL = sprintf("UPDATE {$db}.push_content SET content = '%s' WHERE service = '%s' AND content_label = '%s' AND author = '%s' AND notes = '%s'", $content, $service, $type, 'daily', $flag);
	
	echo "SQL = " . $SQL . PHP_EOL;
	
	$mysqli->query($SQL);
	
	$result->close();
}

$mysqli->close();

if ($result) {
    echo "OK \n";
} else {
    echo "NOK \n";
}

unlink($lockPath);


