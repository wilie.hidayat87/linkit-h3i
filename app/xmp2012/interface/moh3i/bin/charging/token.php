#!/usr/bin/php
<?php
$lockPath = '/tmp/lock_moh3i_request_refresh_n_login';
	
if(file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit;
} else {
	touch($lockPath);
}
	
require_once '/app/xmp2012/interface/moh3i/xmp.php';

$forceReqFile = '/tmp/lock_moh3i_request_refresh_n_login_process';

$run = false;
if(file_exists($forceReqFile)) $run = true;

$charging = new moh3i_charging_token ($run);
$result = $charging->run();

if($result || (time() - filemtime($lockPath)) > 5) unlink($lockPath);