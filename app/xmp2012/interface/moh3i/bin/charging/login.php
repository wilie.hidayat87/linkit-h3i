#!/usr/bin/php
<?php
$params = getopt('l:');
$login = isset($params['l']) ? $params['l'] : '';

require_once '/app/xmp2012/interface/moh3i/xmp.php';

$lockPath = '/tmp/lock_moh3i_request_login';

if(file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit;
} else {
	touch($lockPath);
}

$charging = new moh3i_charging_token ();

$result = $charging->login ($login);
if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

unlink($lockPath);