<?php

final class moh3i_service_module_registration implements service_module_interface {

    public function run(mo_data $mo, service_reply_data $reply) {
        $log = manager_logging::getInstance();
        //$log->write(array('level' => 'debug', 'message' => "Start mo : ".serialize($mo).", reply : ".serialize($reply)));
		
        $loader_model = loader_model::getInstance();
		$loaderConfig = loader_config::getInstance();
		
		$configMain = $loaderConfig->getConfig('main');

		# save MO
		$mt_processor = new manager_mt_processor ( );
		if (in_array(strtoupper($mo->customService), $configMain->bundlingSubject)) 
		{
			// Save sub type & product name
			$mo->media = $mo->subsType;
			$mo->inReply = $mt_processor->saveMOToTransact($mo);
		}

		$mt = loader_data::get('mt');
		
        /* @var $user_manager user_manager */
        $user_manager = user_manager::getInstance();
		
		/* if(!$user_manager->isBlacklisted($mo->msisdn))
		{ */
			
			/* @var $user user_data */
			$user = loader_data::get('user');
			$user->msisdn = $mo->msisdn;
			$user->adn = $mo->adn;
			$user->service = $mo->service;
			$user->operator = $mo->operatorName;
			$user->active = '0,1,2,5,6';
			$user->operator_id = $mo->operatorId;
			//$user->subscribed_to = date("Y-m-d H:i:s", strtotime($configMain->subscribed_until));
			$user->subscribed_to = date("Y-m-d H:i:s", strtotime($configMain->subscribed_until));
			$user->active_until = date("Y-m-d H:i:s", strtotime($configMain->active_until));
			$user->renewal_date = date("Y-m-d H:i:s");
			
			$log->write(array('level' => 'debug', 'message' => "GET USER EXCEPTION : " . serialize($user)));
			$subcribe = $user_manager->getUserException($user);
			
			if ($subcribe === FALSE) {
				$log->write(array('level' => 'debug', 'message' => "Not Yet Registered, THEN ADD User"));

				$user->active = '1';
				$user->transaction_id_subscribe = $mo->inReply;
				$user->channel_subscribe = $mo->channel;
				
				$last_insert_id = $user_manager->addUserData($user);
				
				if(!empty($last_insert_id))
				{
					if(strtoupper($mo->customService) == 'PS59')
					{
						$this->wakiIn($mo);
					}
					
					$mt = $this->chargeDataAfterRegistered($mo, $reply, array());
					
					if((int)$mo->bundling == 1 || strtoupper($mo->customService) == 'PS15') // 90 Day date on wording
					{
						/* $log->write(array('level' => 'debug', 'message' => "subscribed_to : " . $user->subscribed_to));

						list($sudate, $sutime) = explode(" ", $user->subscribed_to);
						list($suy, $sum, $sud) = explode("-", $sudate);
						list($suh, $sui, $sus) = explode(":", $sutime);
						
						$val_90day = 90;

						$date=date_create($user->subscribed_to);
						date_add($date,date_interval_create_from_date_string("-{$val_90day} days"));
						$mt->subscribe_until = date_format($date,"Y-m-d");

						$mt->subscribe_until .= " " . $sutime;
						$user->subscribed_to .= " " . $sutime;

						$log->write(array('level' => 'debug', 'message' => "subscribe_until : " . $mt->subscribe_until)); */
						
						$renewal_accumulate = $configMain->listAccumulateRenewal[strtoupper($mo->customService)];
						$user->renewal_date = date("Y-m-d H:i:s", strtotime("+" . $renewal_accumulate . " day"));
							
						$mt->subscribe_until = $user->renewal_date;
					}
					else if(strtoupper($mo->customService) == 'PS60') // Add 10 Day for next renewal
					{
						$user->renewal_date = date("Y-m-d H:i:s", strtotime($configMain->custom_renewal));
						$mt->subscribe_until = $user->subscribed_to;
					}
					else
					{
						$mt->subscribe_until = $user->subscribed_to;
					}

					$mt->active_until = $user->active_until;

					$user->priority = '0';

					if($mt->msgStatus == "DELIVERED")
					{
						if($reply->subject == 'monthly'){
							$user->active = '2';
							//unset($user->active_until);
						}

						// new feature priority revenue user
						$user->priority = '1';
					}

					
					//if(in_array($mo->msisdn, array("62895423478415","6289655913961","62895353170833")))
					//{
						if(trim($mt->closeReason) <> '200_3_109')
						{
							if($mt->msgStatus == 'DELIVERED' && strtoupper($mo->customService) <> 'PS7') 
							{
								$user->success = 1;
							}
							
							// SUCCESS & PRIORITY ALWAYS ON FOR PRIORITY TOMORROW RENEWAL
							// AUTHOR : WILIE, 2020-10-06 10:59
							$user->success = 1;
							$user->priority = '1';
							
							$user_manager->updateActivationByMsisdn($user);
						}
					//}
					//else
					//{
						//$user_manager->updateActivationByMsisdn($user);
					//}
				}
				
			} else {
				
				if ((int) $subcribe->active == 1 || (int) $subcribe->active == 2) {
					$log->write(array('level' => 'debug', 'message' => "User Registered, THEN process dicontinued"));
				}
				else if ((int) $subcribe->active == 5 || (int) $subcribe->active == 6) {
					
					$subject = strtoupper($mo->customService);
					
					list($date, $time) = explode(" ", $subcribe->time_modified);
					list($y,$m,$d) = explode("-", $date);
					list($h,$i,$s) = explode(":", $time);
					
					// RESUB CHECK
					
					$_allow_sub = "60"; // 60 day or 2 month
					$target_date = date("Y-m-d", mktime(0,0,0,$m,($d+(int)$_allow_sub),$y));
					
					$origin = new DateTime($target_date);
					$today = new DateTime(date("Y-m-d"));
					
					$interval = $origin->diff($today);
					$check = $interval->format('%R%a');

					if((double)$check >= 0){
						//echo (double)$check . " | Allow";
						
						$mt = $this->chargeDataAfterRegistered($mo, $reply, array());
						
						$user->id = $subcribe->id;
						$user->active = '1';
						$user->channel_subscribe = $mo->channel;
						$user->priority = '0';
						
						if(strtoupper($mo->customService) == 'PS60') // Add 10 Day for next renewal
						{
							$user->renewal_date = date("Y-m-d H:i:s", strtotime($configMain->custom_renewal));
						}
						
						$mt->subscribe_until = $user->subscribed_to;
						$mt->active_until = $user->active_until;
					
						if(trim($mt->closeReason) <> '200_3_109')
						{
							if($mt->msgStatus == 'DELIVERED') 
							{
								$user->success = 1;
								$user->priority = '1';
							}
							
							$user_manager->updateUserData($user);
						}
						
						$log->write(array('level' => 'info', 'message' => "Allow sub {$user->msisdn}-{$subject}, more {$check} day from today : " . date("Y-m-d")));
					}
					else{
						//echo (double)$check . " | Disallow";
						$log->write(array('level' => 'info', 'message' => "Disallow sub {$user->msisdn}-{$subject} from status active : ".$subcribe->active.", still {$check} day from today : " . date("Y-m-d")));
					}
					
				}
				else if ((int) $subcribe->active == 0) {
					
					list($date, $time) = explode(" ", $subcribe->time_modified);
					list($y,$m,$d) = explode("-", $date);
					list($h,$i,$s) = explode(":", $time);
										
					$time_modified = mktime($h,$i,$s,$m,$d,$y);
					$now = strtotime("+15 seconds");

					$allowed = (int)$now - (int)$time_modified;

					if((int)$allowed > 15)
					{
						if(strtoupper($mo->customService) == 'PS59')
						{
							$this->wakiIn($mo);
						}
						
						$log->write(array('level' => 'debug', 'message' => "UPDATE with user unreg data"));

						$mt = $this->chargeDataAfterRegistered($mo, $reply, array());
						
						$user->id = $subcribe->id;
						$user->active = '1';
						$user->channel_subscribe = $mo->channel;
						$user->priority = '0';

						if($mt->msgStatus == "DELIVERED")
						{						
							if($reply->subject == 'monthly'){
								$user->active = '2';
								//unset($user->active_until);
							}

							// new feature priority revenue user
							$user->priority = '1';
						}

						if((int)$mo->bundling == 1 || strtoupper($mo->customService) == 'PS15') // 90 Day date on wording
						{
							$user->subscribed_to = date("Y-m-d H:i:s", strtotime($configMain->subscribed_until));
							
							//echo ($mo->rereg) ? "1" : "0";
							
							if((int)$mo->rereg == 1){
								list($date, $time) = explode(" ", $subcribe->renewal_date);
								
								//print_r($subcribe->renewal_date);
							} else {
								$date = date("Y-m-d");
								$time = date("H:i:s");
							}
							
							list($y,$m,$d) = explode("-", $date);
							list($h,$i,$s) = explode(":", $time);

							$renewal_accumulate = $configMain->listAccumulateRenewal[strtoupper($mo->customService)];
							$user->renewal_date = date("Y-m-d H:i:s", mktime($h, $i, $s, $m, ((int)$d+$renewal_accumulate), $y));
			
							$mt->subscribe_until = $user->renewal_date;
							/* $log->write(array('level' => 'debug', 'message' => "msisdn : {$user->msisdn}, subcribe->subscribed_to : " . $subcribe->subscribed_to));
							$log->write(array('level' => 'debug', 'message' => "msisdn : {$user->msisdn}, user->subscribed_to : " . $user->subscribed_to));

                            if($mo->rereg == true){
                                list($sudate, $sutime) = explode(" ", $subcribe->subscribed_to);
                                $user->subscribed_to = $subcribe->subscribed_to;
                            }
                            else{
                                list($sudate, $sutime) = explode(" ", $user->subscribed_to);
                            }

							list($suy, $sum, $sud) = explode("-", $sudate);
							list($suh, $sui, $sus) = explode(":", $sutime);

                            $val_90day = 90;

                            $date=date_create($sudate);
							date_add($date,date_interval_create_from_date_string("-{$val_90day} days"));
							$mt->subscribe_until = date_format($date,"Y-m-d") . " " . $sutime;

							$log->write(array('level' => 'debug', 'message' => "msisdn : {$user->msisdn}, subscribe_until : " . $mt->subscribe_until)); */
						}
						else if(strtoupper($mo->customService) == 'PS60') // Add 10 Day for next renewal
						{
							$user->renewal_date = date("Y-m-d H:i:s", strtotime($configMain->custom_renewal));
							$mt->subscribe_until = $user->subscribed_to;
						}
						else
						{
							$mt->subscribe_until = $user->subscribed_to;
						}

						$mt->active_until = $user->active_until;
					
						//if(in_array($mo->msisdn, array("62895423478415","6289655913961","62895353170833")))
						//{
						
							if(trim($mt->closeReason) <> '200_3_109')
							{
								if($mt->msgStatus == 'DELIVERED' && strtoupper($mo->customService) <> 'PS7') 
								{
									$user->success = 1;
								}
								
								// SUCCESS & PRIORITY ALWAYS ON FOR PRIORITY TOMORROW RENEWAL
								// AUTHOR : WILIE, 2020-10-06 10:59
								$user->success = 1;
								$user->priority = '1';
								
								$user_manager->updateUserData($user);
							}
						//}
						//else
						//{
							//$user_manager->updateUserData($user);
						//}
					}
					else
					{
						$log->write(array('level' => 'debug', 'message' => "Too many request for the same msisdn = " . $mo->msisdn));
					}
				}
			}			
		/* } */
		
        return $mt;
    }

    private function getMsgBeforeRegistered($mo, $reply, $replyAttributes) {

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $mt_manager = new manager_mt_processor ( );

        $mt = loader_data::get('mt');

        $mt->msgData = $reply->message;
        $mt->msgId = $mo->msgId;
        $mt->charging = $reply->chargingId;
        $mt->adn = $mo->adn;
        $mt->channel = $mo->channel;
        $mt->msisdn = $mo->msisdn;
        $mt->operatorId = $mo->operatorId;
        $mt->inReply = $mo->inReply;
        $mt->price = $reply->price;
        $mt->operatorName = $mo->operatorName;
        $mt->service = $mo->service;
        $mt->subject = strtoupper('MT;PULL;' . $mo->channel . ';TEXT');
        $mt->type = 'mtpull';
        $mt->mo = $mo;

        $mt_manager->saveToQueue($mt);

        return $mo;
    }

    private function getMsgAfterRegistered($mo, $reply, $replyAttributes) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $mt_manager = new manager_mt_processor ( );

        $mt = loader_data::get('mt');

        $mt->msgData = $reply->message;
        $mt->msgId = $mo->msgId;
        $mt->charging = $reply->chargingId;
        $mt->adn = $mo->adn;
        $mt->channel = $mo->channel;
        $mt->msisdn = $mo->msisdn;
        $mt->operatorId = $mo->operatorId;
        $mt->inReply = $mo->inReply;
        $mt->price = $reply->price;
        $mt->operatorName = $mo->operatorName;
        $mt->service = $mo->service;

        $mt->subject = strtoupper('MT;PULL;' . $mo->channel . ';TEXT');
        $mt->type = 'mtpull';
        $mt->mo = $mo;

        $mt_manager->saveToQueue($mt);

        return $mo;
    }
	
	private function saveDataBeforeRegistered($mo, $reply, $replyAttributes) {

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $mt = loader_data::get('mt');

        $mt->msgData = $reply->message;
        $mt->msgId = $mo->msgId;
        $mt->charging = $reply->chargingId;
        $mt->adn = $mo->adn;
        $mt->channel = $mo->channel;
        $mt->msisdn = $mo->msisdn;
        $mt->operatorId = $mo->operatorId;
        $mt->inReply = $mo->inReply;
        $mt->price = $reply->price;
        $mt->operatorName = $mo->operatorName;
        $mt->service = $mo->service;
        $mt->subject = strtoupper('MT;PULL;' . $mo->channel . ';TEXT;' . strtoupper($mo->customService));
        $mt->type = 'mtpull';
        $mt->mo = $mo;
		$mt->msgLastStatus = 'FAILED';
		$mt->msgStatus = 'FAILED';

        loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1')->saveMT($mt);

        return $mo;
    }

	private function chargeDataAfterRegistered($mo, $reply, $replyAttributes) {

		$configMain = loader_config::getInstance()->getConfig('main');
		
		$loader_model = loader_model::getInstance();
		
		$mechanism = $loader_model->load( 'mechanism', 'connDatabase1' );
		$trx = $loader_model->load( 'tblmsgtransact', 'connDatabase1' );
		$modelay = $loader_model->load( 'modelay', 'connDatabase1' );
		
        $log = manager_logging::getInstance();

        $mt = loader_data::get('mt');

        $mt->msgData = $reply->message;
        $mt->charging = $reply->chargingId;
        $mt->adn = $mo->adn;
        $mt->channel = $mo->channel;
        $mt->msisdn = $mo->msisdn;
        $mt->operatorId = $mo->operatorId;
        $mt->inReply = $mo->inReply;
        $mt->price = $reply->price;
        $mt->operatorName = $mo->operatorName;
        $mt->service = $mo->service;
        $mt->subject = strtoupper('MT;CHARGE;' . $mo->channel . ';TEXT');
		
		if(!empty($mo->customService))
			$mt->subject .= ";" . strtoupper($mo->customService);
		
		// - CHANGELOG - 
		// Date : 2019-07-30 14:53
		// Author : Wilie Wahyu Hidayat
		
		if(!empty($mo->s2))
			$mt->subject .= ";" . strtoupper($mo->s2);
		
        //$mt->type = 'mtpull';
        //$mt->mo = $mo;
		$mt->msgLastStatus = '';
		
		if(!empty($mo->bundling) || $mo->bundling == true || in_array(strtoupper($mo->customService), $configMain->freemiumKeyword))
		{
			$mt->msgStatus = 'DELIVERED';
        	$mt->msgLastStatus = 'DELIVERED';
		}
		else
		{
			$rev = $mechanism->getMoRevByRaw($mo);
			$morev = (int)$rev[0]['morev'];
			$modrev = (int)$rev[0]['modrev'];
			
			if($morev < $modrev)
			{
				$charge = new moh3i_charging_charge();
				$response = $charge->run(
					array(
						'msisdn' => $mo->msisdn, 
						'message_type' => $reply->subject,
						'price'	=> $reply->price
					)
				);
				
				$rc = (int)$response['header_code'];
				$output = json_decode($response['output']);
				
				if($rc == 200)
				{
					//if((int)$output->Result == 1 && !in_array($mo->msisdn, array("62895423478415","6289655913961","62895353170833")))
					if((int)$output->Result == 1)
					{
						$mt->msgId = $output->SessionID;
						$mt->msgStatus = 'DELIVERED';
						$mt->closeReason = '';
						
						$mechanism->updateMoRevByRaw($mo);
					}
					else
					{
						$mt->msgId = $output->SessionID;
						$mt->msgStatus = 'FAILED';
						$mt->closeReason = $output->ReasonCode;
						
						//if(in_array($mo->msisdn, array("62895423478415","6289655913961","62895353170833")))
						//{
							//$mt->closeReason = '200_3_109';

						if(trim($mt->closeReason) == '200_3_109')
						{
							// will no continue to hit mt as /app/xmp2012/system/core/service/creator/handler.php
							$mt->msgStatus = ''; 

							if($modelay->manualUnsub(array(
								 'active' 		=> 0
								,'msisdn' 		=> $mo->msisdn
							)))
							{
								$buffer_file = buffer_file::getInstance();
                                
                                $mo_data = loader_data::get('mo');
                                $mo_data->msisdn = $mo->msisdn;
                                $mo_data->msgId = $mt->msgId;
                                $mo_data->rawSMS = $mo->rawSMS;	
                                $mo_data->adn = $mo->adn;
                                $mo_data->operatorName = $mo->operatorName;
                                $mo_data->incomingDate = $mo->incomingDate;
                                $mo_data->incomingIP = $mo->incomingIP;
                                $mo_data->type = $mo->type;
                                $mo_data->channel = $mo->channel;
                                $mo_data->bundling = $mo->bundling;
                                $mo_data->subsType = $mo->subsType;
                                $mo_data->rereg = $mo->rereg;
                                $mo_data->service = $mo->service;
                                $mo_data->customService = $mo->customService;
                                $mo_data->s2 = $mo->s2;
                                $mo_data->closereason = $mt->closeReason;
                                
                                $path = $buffer_file->generate_file_name($mo_data, 'moretry');
                                $buffer_file->save($path, $mo_data);
							}
						}

						//}
					}
				
					//if(in_array($mo->msisdn, array("62895423478415","6289655913961","62895353170833")))
					//{
						if(trim($mt->closeReason) <> '200_3_109') {
							
							# save MO
							$mt_processor = new manager_mt_processor ( );
							$mo->inReply = $mt_processor->saveMOToTransact($mo);

							$trx->saveMT($mt);
						}
					//}
					//else $trx->saveMT($mt);
				}
				else
				{
					$mt->msgId = date("YmdHis");
					$mt->msgStatus = 'FAILED';
					$mt->closeReason = "400";

					$log->write(array('level' => 'debug', 'message' => "Failed to hit, Header Code : " . $rc));
					$trx->saveMT($mt);
				}
				
				if(strtoupper($mo->customService) == "PS59")
				{
					//DN: http://149.129.252.221:8028/app/api/waki_dr.php?trx_id=53534535350986325264&status=1&statusdesc=SUCCESS&operator=4&msisdn=6281232323&sdc=99876&service=REG+GEMEZZ+PS59
					
					$params = array(
						"trx_id=".$mo->msgId
					   ,"status=". (($mt->msgStatus == "DELIVERED") ? $mt->msgStatus : $mt->closeReason)
					   ,"statusdesc=". (($mt->msgStatus == "DELIVERED") ? "Success" : "Failed")
					   ,"operator=4"
					   ,"msisdn=".$mt->msisdn
					   ,"sdc=".$mt->adn
					   //,"service=".urlencode("REG Gemezz PS59")
					   ,"service=".urlencode("REG ".$mo->service." ".$mo->customService." ".$mo->s2)
					   ,"type=firstpush"
					);

					$fullurl = "http://149.129.252.221:8028/app/api/waki_dr.php?";

					$hit = http_request::requestGet ( $fullurl, implode("&", $params), 10 );
				}
			}
			else
			{
				$mt->msgStatus = 'FAILED';
				$mt->closeReason = 'LIMIT REVENUE EXCEEDED';
			
				$trx->saveMT($mt);
				
				$log->write(array('level' => 'info', 'message' => "Revenue reached the limit : " . $mo->rawSMS));
			}
		}

        return $mt;
    }
	
	private function wakiIn($mo)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		//MO : http://149.129.252.221:8028/app/api/waki_in.php?msisdn=6289536625&operator=4&sdc=99876&sms=REG+Gemezz+PS59&trx_id=628273293273209809809809890&service_type=2&trx_date=20100120103012
			
		$params = array(
			"msisdn=".$mo->msisdn
		   ,"operator=4"
		   ,"sdc=99876"
		   ,"sms=".urlencode("REG ".$mo->service." ".$mo->customService." ".$mo->s2)
		   ,"trx_id=".$mo->msgId
		   ,"service_type=2"
		   ,"trx_date=".date("YmdHis")
		);

		$fullurl = "http://149.129.252.221:8028/app/api/waki_in.php?";

		$hit = http_request::requestGet ( $fullurl, implode("&", $params), 10 );
			
	}
}
