<?php

class moh3i_service_module_textdelay implements service_module_interface {

    public function run(mo_data $mo, service_reply_data $reply) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start mo : ".serialize($mo).", reply : ".serialize($reply)));

        $mt_processor = new manager_mt_processor ( );
        $loader_model = loader_model::getInstance();

        /* @var $replyAttribute model_replyattribute */
        # load reply attributes value
        $replyAttributes = array();
        $log->write(array('level' => 'debug', 'message' => "GET Atribute"));
        $replyAttributeModel = $loader_model->load('replyattribute', 'connDatabase1');
        $attributes = $replyAttributeModel->get($reply->id);
        foreach ($attributes as $attribute) {
            $replyAttributes [$attribute ['name']] = $attribute ['value'];
        }

        $user_manager = user_manager::getInstance();
		
		if(!$user_manager->isBlacklisted($mo->msisdn))
		{
			$mt_processor = new manager_mt_processor ( );

			# save MO
			if ($reply->sequence < 1) // IF empty $mo->inReply (assume Not Yet Save MO ) THEN SAVEMO // empty ( $mo->inReply )
				$mo->inReply = $mt_processor->saveMOToTransact($mo);

			if ($reply->sequence > 0) :
				$log->write(array('level' => 'debug', 'message' => "MTPUSH"));
				if ((int) $replyAttributes ['rereg_content'] = 1) :
					$log->write(array('level' => 'debug', 'message' => "rereg_content = 1"));

					#Send MTPUSH
					$this->getMsg($mo, $reply, $replyAttributes);
					$log->write(array('level' => 'debug', 'message' => "SEND MTPUSH text message : " . $reply->message));


				endif;


			else :
				$log->write(array('level' => 'debug', 'message' => "MTPULL"));
				#check if pull_member or not;
				if ((int) $replyAttributes ['pull_member'] == 1) {
					$log->write(array('level' => 'debug', 'message' => "PULL MEMBER = 1"));

					# init user object for used in getUserException func
					$user = loader_data::get('user');
					$user->msisdn = $mo->msisdn;
					$user->adn = $mo->adn;
					$user->service = $mo->service;
					$user->operator = $mo->operatorName;
					$user->active = '1';

					# get user subscribe data
					$log->write(array('level' => 'debug', 'message' => "SEND MTPULL msg_pull_notregistered : " . $reply->message));
					$user_manager = user_manager::getInstance();
					$subscribe = $user_manager->getUserData($user);

					$mo->userData = $subscribe;

					if ($subscribe === FALSE or (int) $subscribe->active != 1) { // Not Yet Registered or Not Active
						#set reply message 
						$reply->message = $replyAttributes ['msg_pull_notregistered'];

						#Send MT 
						$this->getMsg($mo, $reply, $replyAttributes);
						$log->write(array('level' => 'debug', 'message' => "SEND MTPULL msg_pull_notregistered : " . $reply->message));
					} else { // User is active and Registered
						#Send MT 
						$this->getMsg($mo, $reply, $replyAttributes);
						$log->write(array('level' => 'debug', 'message' => "SEND MT text message : " . $reply->message));
					}
				} else { // NOT PULL_MEMBER
					$log->write(array('level' => 'debug', 'message' => "NOT PULL MEMBER != 1"));
					#Send MT 
					$this->getMsg($mo, $reply, $replyAttributes);
					$log->write(array('level' => 'debug', 'message' => "SEND MT text message : " . $reply->message));
				}


			endif;
		}
		
        return $mo;
    }

    private function getMsg(mo_data $mo, service_reply_data $reply, array $replyAttributes) {

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$loaderConfig = loader_config::getInstance();
		$configMain = $loaderConfig->getConfig('main');
		$configDelay = $loaderConfig->getConfig('delay');
		
		$mUser = loader_model::getInstance()->load('user', 'connDatabase1');
		
        $mt = loader_data::get('mt');

        // Set DELAY
        $log->write(array('level' => 'debug', 'message' => "SET DELAY TRUE"));
        $mt->isDelay = TRUE;
		
		if(in_array($mo->msisdn, $configDelay->superpriority)) 
			$mt->priority = 2;
		else
			$mt->priority = 1;

		if(!empty($mo->replied_180d))
			$mt->msgData = $mo->replied_180d;
		else
			$mt->msgData = $reply->message;
	
        $mt->msgId = $mo->msgId;
        $mt->charging = $reply->chargingId;
        $mt->adn = $mo->adn;
        $mt->channel = $mo->channel;
        $mt->msisdn = $mo->msisdn;
        $mt->operatorId = $mo->operatorId;
        $mt->price = $reply->price;
        $mt->operatorName = $mo->operatorName;
        $mt->service = $mo->service;
		$mt->msgStatus = $mo->msgStatus;
        $mt->msgLastStatus = $mo->msgLastStatus;
		$mt->subscribe_until = $mo->subscribe_until;
		$mt->active_until = $mo->active_until;

        if ($reply->sequence < 1) {
            $mt->subject = strtoupper('MT;PULL;' . $mo->channel . ';TEXT');
            $mt->type = 'mtpull';
            $mt->inReply = $mo->inReply;
        } else {
			
			$channel_subject = "DAILY";
			
			if(!empty($mo->customService))
			{
				$idPStorage = substr($mo->customService, 1, strlen($mo->customService));
				$idPStorage = (int)$idPStorage;
				
				if(is_numeric($idPStorage) && $idPStorage > 0){
					$mt->subject = strtoupper('MT;NOTIF;' . $mo->channel . ';TEXT');
				}else{
					$mt->subject = strtoupper('MT;NOTIF;' . $mo->channel . ';TEXT;' . $mo->customService);
					//$channel_subject = (strtoupper($mo->customService) == 'FS') ? 'MONTHLY' : strtoupper($mo->customService);
					$channel_subject = strtoupper($mo->customService);
				}
				
			}else
				$mt->subject = strtoupper('MT;NOTIF;' . $mo->channel . ';TEXT');
			
			if(!empty($mo->s2))
				$mt->subject .= ";" . strtoupper($mo->s2);
		
			$mt->msisdn_subject = $channel_subject;
			$mt->s2 = strtoupper($mo->s2);
			
			$ms = array("msisdn" => $mt->msisdn, "service" => $mt->service, "subject" => $channel_subject, "s2" => strtoupper($mo->s2));
			
			$checkMsisdnSubject = $mUser->checkMsisdnSubject($ms);
			
			if(count($checkMsisdnSubject) > 0)
				$mUser->updateMsisdnSubject($ms);
			else
				$mUser->insertMsisdnSubject($ms);
		
			$mt->type = $reply->subject;
            $mt->inReply = null;
        }
        $mt->mo = $mo;
		
		/* if(in_array(strtoupper($mo->customService), $configMain->excludeWelcomeMessageBySubkeyword)) 
		{
			//live
			$mt->delayTime = date("Y-m-d", strtotime($configMain->mtDelayHitDay)) . " " . $configMain->mtDelayHitHour;
			//test
			//$mt->delayTime = date("Y-m-d", strtotime($configMain->mtDelayHitDay)) . " " . date("H:i:s", strtotime($configMain->mtDelayHitHour));
		}
		else  */
		$mt->delayTime = date("Y-m-d H:i:s", strtotime("+2 second"));

        // ini kok textdelay insert via mt manager si .. padahal di textdelay module ude insert ke mtdelay .. iqbal, 2013/06/12
        $mt_manager = new manager_mt_processor ( );
        $mt_manager->saveToQueue($mt);

        return $mo;

    }

}
