<?php

class moh3i_mt_forwarder {

    public function process() {
        $log_profile = 'mt_processor';
        $log = manager_logging::getInstance();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$delay_config = loader_config::getInstance()->getConfig('delay');
		
		if($delay_config->burstMechanism == 'sequence')
		{
			$this->forwardmt(0);
		}
		else
		{
			//forking process slot

			for ($i = 0; $i < $delay_config->slot; $i++) {
				if ($delay_config->use_forking) {
					switch ($pid = pcntl_fork ()) {
						case - 1 :
							$log->write(array('level' => 'error', 'message' => "Forking failed"));
							die('Forking failed');
							break;
						case 0 :
							$this->forwardmt($i);
							exit ();
							break;
						default :
							//pcntl_waitpid ( $pid, $status );
							break;
					}
				} else {
					$this->forwardmt($i);
				}
			}
		}
		
        return true;
    }

	public function forwardmt($threadid)
	{
		$lockPath = '/tmp/lock_moh3i_mt_delay_process_thread_' . $threadid;

		if(file_exists($lockPath)) {
			echo "NOK - Lock File Exist on $lockPath \n";
			exit;
		} else {
			touch($lockPath);
		}

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $model_mtdelay = loader_model::getInstance()->load('mtdelay', 'connDatabase1');

        $get_expired = $model_mtdelay->getExpiredByOperator('moh3i', $threadid);
        if ($get_expired) {
            foreach ($get_expired as $data) {

                $delay_data = new mt_delay_data ();
                $delay_data->id = $data ['id'];
                $delay_data->status = '1';
                    
                //$mt_data = unserialize(preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $data['obj']));
                $mt_data = unserialize($data['obj']);

                // Must be set to FALSE.
                $mt_data->isDelay = FALSE;
                
                $mt_processor = new manager_mt_processor ();
                $mt_processor->saveToQueue($mt_data);

                $model_mtdelay->update($delay_data);
                
            }
        }
		
		unlink($lockPath);
		return true;
	}
}
