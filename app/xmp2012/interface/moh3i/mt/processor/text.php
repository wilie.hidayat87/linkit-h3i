<?php

class moh3i_mt_processor_text extends default_mt_processor_text {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance) {
            self::$instance = new self ();
        }
        return self::$instance;
    }

    public function saveToQueue($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt_data)));

		if ($mt_data->charging->messageType == 'mtpush' || (!isset($mt_data->mo->msgId) && empty($mt_data->mo->msgId)) )
        {
            if (empty($mt_data->msgId)) $mt_data->msgId =  date("YmdHis") . str_replace('.', '', microtime(true));
        }
        // Otherwise, take from MO
        else
            $mt_data->msgId = $mt_data->mo->msgId;
		
		return $this->process($mt_data);
	}

	public function process($mt) 
	{	
		$log = manager_logging::getInstance();
		
		if($mt->type == 'unreg')
		{
			$this->portal_notif($mt);
			
			if($mt->disable_mt_unreg <> "OK")
				$this->sms_notif($mt);
		}
		else
		{
			if($mt->msisdn == '62895423478415')
				$log->write(array('level' => 'info', 'message' => "Masuk? : " . serialize($mt)));
			
			if(!empty($mt->msgLastStatus))
			{
				if($mt->msisdn == '62895423478415')
					$log->write(array('level' => 'info', 'message' => "Masuk? (2) : " . serialize($mt)));
				
				if($mt->msgStatus == 'DELIVERED')
				{
					$this->portal_notif($mt);
					
					if($mt->msisdn == '62895423478415')
						$log->write(array('level' => 'info', 'message' => "Masuk? (3) : " . serialize($mt)));
					
					if($mt->msgLastStatus == 'DELIVERED')
					{
						if($mt->msisdn == '62895423478415')
							$log->write(array('level' => 'info', 'message' => "Masuk? (4) : " . serialize($mt)));
					
						$this->sms_notif($mt);
					}
				}
			}
		}

		if($mt->requestType == 'OVERRIDE'){
			// no saving mt 
		}else{
			
			$arrSubject = explode(";", $mt->subject);

			if($arrSubject[3] == 'RENEWAL_MONTHLY' || $arrSubject[3] == 'RETRY_MONTHLY' || $arrSubject[3] == 'RENEWAL_DAILY')
			{
				// no saving for DP on mt_delay process
			}
			else
			{
				if($mt->type <> 'confirm') 
				{
					// Bundling data saved
					$mt->media = $mt->mo->media;
					
					$this->saveMTToTransact($mt);
				}
			}
		}
		
		return true;
	}
	
	public function portal_notif($mt)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt)));

		$buffer_file = buffer_file::getInstance ();
									
		$path = $buffer_file->generate_file_name ( $mt, "contentbuffer", "cb" );
		
		if ($buffer_file->save ( $path, $mt )) {
			$log->write(array('level' => 'debug', 'message' => "Buffer OK, msisdn - " . $mt->msisdn));
		} else {
			$log->write(array('level' => 'debug', 'message' => "Buffer NOK, msisdn - " . $mt->msisdn));
		}

		return true;
		/*
        $loader_config = loader_config::getInstance();
		$configMT = $loader_config->getConfig('mt');
		$profile = 'text';
		
		//TSID=201812031319521543817992&MSISDN=6289526716941&STATUS=SUCCESS&EVENT=FIRSTPUSH&Type=DAILY
		$param = "";
		$param .= '?TSID=' . $mt->msgId;
		$param .= '&MSISDN=' . $mt->msisdn;
		
		if($mt->type == 'unreg'){
			$param .= '&STATUS=SUCCESS';
			$param .= '&EVENT=UNSUB';
		}
		else{
			$param .= '&STATUS=' . (($mt->msgLastStatus == 'DELIVERED') ? 'SUCCESS' : 'FAILED');
			$param .= '&EVENT=' . ((strpos(strtoupper($mt->subject), ';TEXT') !== FALSE) ? 'FIRSTPUSH' : 'RENEWAL');
		}
		
		$param .= '&Type=' . strtoupper($mt->type);
		
		if($mt->channel == 'portal' || $mt->channel == 'lp1' || $mt->channel == 'lp2') $param .= '&SOURCE=' . strtoupper($mt->channel);
		
		$url = $configMT->profile [$profile] ['sendUrl'] [1];
		
		$hit = http_request::requestGet($url, $param, $configMT->profile [$profile] ['SendTimeOut']);
		$log->write ( array ('level' => 'debug', 'message' => "Portal Hit Url:" . $url . $param . ', Result:' . $hit ) );

		*/
	}
	
	private function confirmConf($main, $sub, $cur = 'this')
	{
		$load_config = loader_config::getInstance();
		$configMT = $load_config->getConfig('mt');
		
		return $configMT->profile ['text'] ['mtProcess'] ['mt'] ['reg_unreg'] [$main] [strtolower($sub)] [strtolower($cur)];
	}
	
	public function sms_notif($mt)
	{
		$log = manager_logging::getInstance();
		
		if($mt->msisdn == '62895423478415')
			$log->write(array('level' => 'info', 'message' => "Start : " . serialize($mt)));
		else
			$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt)));

        $loader_config = loader_config::getInstance();
		$config_main = $loader_config->getConfig('main');
		$configMT = $loader_config->getConfig('mt');
		$profile = 'text';
		
		$param = "";
		$param .= '&MOBILENO=' . $mt->msisdn;
		
		$date_word = date("Y-m-d H:i:s");
		
		if($mt->type == 'daily')
			$date_word = $mt->subscribe_until;
		else if($mt->type == 'monthly')
			$date_word = $mt->active_until;	
		
		list($date, $time) = explode(" ", $date_word);
		list($y, $m, $d) = explode("-", $date);
		list($h, $i, $s) = explode("-", $time);
		
		$date = date('d/M/y', mktime($h,$i,$s,$m,$d,$y));
		
		$sms = urlencode(str_replace("#date#", $date, $mt->msgData));

		if(strlen($sms) > 160)
			$param .= '&UDH=1';
		else
			$param .= '&UDH=0';

		$param .= '&MESSAGE=' . $sms;
		
		if($config_main->toggle_env == "PRODUCTION") 
		{
			$url = $configMT->profile [$profile] ['sendUrl'] [0];
		} 
		else if($config_main->toggle_env == "DEVELOPMENT") 
		{
			$url = $configMT->profile [$profile] ['sendUrlDev'] [0];
		}
		
		if($configMT->profile [$profile] ['hit']){
			
			$isSMS = $configMT->profile [$profile] ['mtProcess'] ['mt'] ['reg_unreg'] [$mt->type] ['global'];
			
			if(!empty($mt->msisdn_subject) && strtolower($mt->msisdn_subject) <> 'daily')
			{
				$isSMS = $this->confirmConf($mt->type,strtolower($mt->msisdn_subject),'this');
			}
		
			if(!empty($mt->s2))
			{			
				if(!empty($mt->msisdn_subject) && in_array(strtoupper($mt->msisdn_subject), $config_main->dynamic3rdKeyword))
				{
					$isSMS = $this->confirmConf($mt->type,strtolower($mt->msisdn_subject),'dynamic');
					
					//echo "(".(($isSMS) ? "OK" : "NOK").") - S1 = " . $mt->msisdn_subject . ", S2 = " . $mt->s2;
				}
				else
				{
					$isSMS = $this->confirmConf($mt->type,strtolower($mt->msisdn_subject),strtolower($mt->s2));
					
					//echo "(".(($isSMS) ? "OK" : "NOK").") - S1 = " . $mt->msisdn_subject . ", S2 = " . $mt->s2;
				}
			}
			
			if($mt->type == 'unreg')
			{
				$isSMS = $configMT->profile [$profile] ['mtProcess'] ['mt'] ['reg_unreg'] ['unreg'];
			}
			
			if($mt->msisdn == '62895423478415')
				$log->write ( array ('level' => 'info', 'message' => "isSMS :" . (($isSMS == true) ? 'OK' : 'NOK') . ", type : " . $mt->type ) );
			else
				$log->write ( array ('level' => 'debug', 'message' => "isSMS :" . $isSMS . ", type : " . $mt->type ) );
			
			if($isSMS)
			{
				$hit = http_request::requestGet($url, $param, $configMT->profile [$profile] ['SendTimeOut']);
				$log->write ( array ('level' => 'debug', 'message' => "MT Url:" . $url . $param . ', Result:' . $hit ) );
			}
			
		}else{
			
			$log->write ( array ('level' => 'debug', 'message' => "Disable MT" ) );
			
		}
		
		/* $xml = simplexml_load_string($hit);
		$mt->closeReason = trim($xml->RESPONSE->TEXT); */
		
		/* if($mt->type <> 'confirm') $this->saveMTToTransact($mt); */
	}

	public function send_mt($mt) 
	{
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start" . $slot));

        $loader_config = loader_config::getInstance();
        $configMT = $loader_config->getConfig('mt');
		$configSDM = $loader_config->getConfig('sdmcode');
        $profile = 'text';
        
        //print_r($queue);
        
            
            //echo('start-----');
            //print_r($mt);
            //http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0
            //?uid={UID}&pwd={PWD}&serviceid=%s&msisdn=%s&sms=%s&transid=%s&smstype=0
            
            //var_dump($queuePop, $mt);
            
            $param = 'uid=' . $mt->charging->username;
            $param .= '&pwd=' . $mt->charging->password;
            $param .= '&serviceid=' . $mt->charging->chargingId;
            $param .= '&msisdn=' . $mt->msisdn;
            
			//$param .= '&sms=' . urlencode($mt->msgData);
			//$sms = urlencode($mt->msgData);
			$sms = $mt->msgData;
			
			//******* ISAT CUSTOM SMS **********//
			//******* Dev By : Wilie Wahyu H
			//******* Created: 2017-06-21 && 2017-08-03
			$sms = $this->hitCustomSMS($mt, $sms);
			//******* END ****************************//
			
			$sms = urlencode($sms);
			$param .= '&sms=' . $sms;
			
            $param .= '&transid=' . $mt->msgId;
            $param .= '&smstype=0';


	        if(isset($configSDM->sdm['SPMSERVICE'][$mt->service]))
	    {
			$chargeID = array('99876301034003');
			//, '98876301014004', '98876301014005', '98876301014006', '98876301014007');
			
			if(in_array($mt->charging->chargingId, $chargeID)){ // only for charging fee sdm_code service/opt : gold_99876/moh3i
				$sdmcode=$configSDM->sdm['SPMSERVICE'][$mt->service];
				$param .= '&sdmcode='. $sdmcode;
            }else {
				//$param .= '&sdmcode=';
			}
	    }

			$url = $configMT->profile [$profile] ['sendUrl'] [0];
          
            $hit = http_request::get($url, $param, $configMT->profile [$profile] ['SendTimeOut']);
			$log->write ( array ('level' => 'debug', 'message' => "MT Url:" . $url . '?' . $param . ', Result:' . $hit ) );

            $_hit = trim($hit);
            $smsc_response = simplexml_load_string($_hit);
            
            $configDr = loader_config::getInstance()->getConfig('dr');
            if ($configDr->synchrounous === TRUE) {
                if ($_hit == 1) {
                    $mt->msgStatus = 'DELIVERED';
                } else {
                    $mt->msgStatus = 'FAILED';
                }
                $mt->closeReason = $_hit;
            } else {
            	if ($smsc_response->STATUS == 0) {
			//change from $mt->msgStatus = 'DELIVERED'; because when we don't get DR for spesific message, message will be count as revenue, make like PROXL behaviour 
            		$mt->msgStatus = '';

            	} else {
            		$mt->msgStatus = 'FAILED';
            	}
            	$status = $smsc_response->STATUS;
            	$mt->closeReason = $status."|".$configDr->responseACK[(int)$status];
            	//var_dump($configDr->responseACK[(int)$status], $smsc_response->STATUS, $mt->closeReason);
            }
            
            //var_dump($url, $param, $_hit);

            
            $mt->msgLastStatus = 'DELIVERED';
            $this->saveMTToTransact($mt);
	    /*
	    if(strtoupper($mt->subject) == "MT;PUSH;SMS;TEXT" && strtolower($mt->operatorName) == "moh3i")
	    {
		$class = $mt->operatorName . "_cmp_manager_keyword";
		$CPA = new $class();
		$CPA->process($mt);
	    }
	    */
        return true;
    }

	public function hitCustomSMS($mt, $sms)
	{
		$log = manager_logging::getInstance ();
		
		$loader_config = loader_config::getInstance ();
		$configMT = $loader_config->getConfig ( 'mt' );
		$configMain = $loader_config->getConfig ( 'main' );
		
		if(substr($mt->subject, 0, 2) == "MT")
		{
			if (in_array(strtolower($mt->service), $configMain->serviceCustomContent))
			{
				if($configMain->openCustomSms)
				{
					$loader_model = loader_model::getInstance();
					$user = $loader_model->load('user', 'connDatabase1');
					
					$trxdata = new stdClass();
					$trxdata->msisdn=$mt->msisdn;
					$trxdata->active=0;
					$trxdata->created_date=date("Y-m-d H:i:s");
					$trxdata->modify_date=date("Y-m-d H:i:s");
					$trxdata->operator=$configMain->operator;
					$trxdata->service=$mt->service;
					$trxdata->password=rand(0001, 9999);
					$trxdata->sent=0;
					
					$rec = $user->getMembership($trxdata);
					
					if(count($rec) > 0)
						$user->updatePasswordMembership($trxdata);
					else
						$user->insertMembership($trxdata);
					
					$sms = str_replace("#userpass#", "user:".$trxdata->msisdn.",pwd:".$trxdata->password, $mt->msgData);
					
					//$sms = urlencode($sms);
					
					$log->write(array('level' => 'debug', 'message' => 'Response hit password generator ['.$trxdata->msisdn.']: ' . $trxdata->password));
				}
			}
			
			if(strtolower($mt->service) == "beli")
			{					
				if($configMain->openCustomSms)
				{
					if(strpos($mt->msgData, "#pullurl#") !== false){
						$sms = str_replace("#pullurl#", $mt->mo->slypeeUrl, $mt->msgData);
						
						if(strpos($sms, "#price#") !== false){
							$price = (int)$mt->price;
							$tax = $price / 10;
							$price_with_tax = $price + $tax;
							
							$sms = str_replace("#price#", $price_with_tax, $sms);
						}
					}
					else
						$sms = $mt->msgData;
				}
				
				//$sms = urlencode($sms);
			}
		}
		
		return $sms;
	}
}
