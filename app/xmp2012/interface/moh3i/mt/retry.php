<?php

class moh3i_mt_retry {

    public function process() {
        
		$now = time();
		$check = strtotime("22:00:00");
		
		if($now <= $check)
		{
			$log_profile = 'mt_processor';
			$log = manager_logging::getInstance();
			$log->setProfile($log_profile);
			$log->write(array('level' => 'debug', 'message' => "Start"));
		
			$trx = loader_model::getInstance ()->load ( 'tblmsgtransact', 'connDatabase1' );
			$mtdelay = loader_model::getInstance ()->load ( 'mtdelay', 'connDatabase1' );
			
			$msisdns = $trx->getMsisdnWithSystemError("MT;PUSH;SMS;TEXT%");
			
			if(count($msisdns) > 0){
				
				for($i=0; $i<count($msisdns); $i++)
				{
					$trxid = $msisdns[$i]['ID'];
					$msisdn = $msisdns[$i]['MSISDN'];
					
					$obj_mt_delay = $mtdelay->checkOnMtDelayWaitingListByMsisdn ($msisdn);
					
					$trx->deleteMsisdnWithSystemErrorByID($trxid);
					
					$id = $obj_mt_delay[0]['id'];
					$mtdelay->updateMtDelayWaitingListByID($id);
				}
				
				return true;
			}
			else { return false; }
			
		}else return false;
    }

}
