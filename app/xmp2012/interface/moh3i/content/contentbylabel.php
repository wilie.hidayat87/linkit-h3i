<?php
class moh3i_content_contentbylabel implements content_interface {
	private static $instance;
	
	private function __construct() {
	}
	
	public static function getInstance() {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		if (! self::$instance)
			self::$instance = new self ();
		return self::$instance;
	}
	
	public function get($broadcast_data) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$model_pushcontent = loader_model::getInstance ()->load ( 'pushcontent', 'connBroadcast' );
		$datas = $model_pushcontent->getContentByLabel ( $broadcast_data );
		
		$push_content_data = array();
		
		if(count($datas) > 0)
		{
			foreach($datas as $data)
			{
				/* $push_content = new broadcast_pushdata ();
				$push_content->id = $data ['id'];
				$push_content->service = $data ['service'];
				$push_content->contentLabel = $data ['content_label'];
				$push_content->content = $data ['content'];
				$push_content->author = $data ['author'];
				$push_content->notes = $data ['notes'];
				$push_content->datePublish = $data ['datepublish'];
				$push_content->lastUsed = $data ['lastused'];
				$push_content->created = $data ['created'];
				$push_content->modified = $data ['modified']; */
				
				$push_content_data[$data ['notes']] = $data ['content'];
			}
		}
		return $push_content_data;
	}
}