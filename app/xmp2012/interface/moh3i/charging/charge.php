<?php
class moh3i_charging_charge
{	
	public function run($arr) {
		
        //$log_profile = 'charging';
        $log = manager_logging::getInstance ();
        //$log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($arr)));
		
		$loaderConfig = loader_config::getInstance();
		$config_main = $loaderConfig->getConfig('main');
		$fToken = $config_main->path_token;

		$lockPath = '/tmp/lock_moh3i_request_refresh_token';

		$bFile = buffer_file::getInstance();
		$token_data = json_decode($bFile->get($fToken));
		
		if(file_exists($lockPath)) {
			$log->write(array('level' => 'info', 'message' => "NOK - Lock File Exist on $lockPath, due to proceeding refresh token"));
			sleep(2);
		}
			
		//* ============== REQUEST PAYMENT =============== *//

		$charging_model = loader_model::getInstance()->load('charging', 'connDatabase1');
		
		$charging_data = new charging_data ();
		$charging_data->operator = 1;
		$charging_data->senderType = 'text';
		$charging_data->messageType = $arr['message_type'];
		
		if(!empty($arr['price']))
			$charging_data->gross = $arr['price'];

		if(!empty($arr['price']))
			$charging_data->netto = $arr['price'];
		
		$get_charging = $charging_model->getCharging($charging_data);
		
		$log->write(array('level' => 'debug', 'message' => "charging_data : " . print_r($get_charging, true)));
		
		$req = array();
		//$req['login'] = !empty($get_charging['username']) ? $get_charging['username'] . ":" . $get_charging['password'] : "linkit_cgw_20181121:whN4DBgw";
		$req['login'] = !empty($get_charging['username']) ? $get_charging['username'] . ":" . $get_charging['password'] : "6dCPCd3R:Admin@1234";
		
		if($config_main->toggle_env == "PRODUCTION") 
		{
			$req['url'] = $config_main->charging_url . ":" . $config_main->charging_url_port . "/payment/";
			$req['port'] = $config_main->charging_url_port;
		} 
		else if($config_main->toggle_env == "DEVELOPMENT") 
		{
			$req['url'] = $config_main->charging_url_dev . ":" . $config_main->charging_url_port_dev . "/payment/";
			$req['port'] = $config_main->charging_url_port_dev;
		}
 
		$req['body'] = json_encode(
			array(
				"SessionID" => date("Ymd") . rand(1111111111, 9999999999) . time(),
				"MSISDN" => $arr['msisdn'],
				"ContentID" => $get_charging['charging_id'], // content_id
				"Price" => (int)str_replace(".00","",$get_charging['gross']) //price
				//"TOKEN" => $token_data->access_token
		));

		$req['headers'] = array(
			'Authorization: Bearer ' . $token_data->access_token,
			'Content-Type: application/json',
			'Content-Length: ' . strlen($req['body'])
		);
							  
		$req['ssl'] = false;
		$req['timeout'] = (!empty($arr['req_timeout']) ? $arr['req_timeout'] : $config_main->charging_timeout);

		$resp = http_request::requestPost($req, "Payment");
		
		/*
		{"SessionID" : "2021090783462399401630988756","Result" : "1","ReasonCode" : "00","UserSpecificReserved":"Digital payment success HTTP"}
		*/
		
		//TESTING
		/* $resp = array();
		$resp['header_code'] = 200;
		$resp['output'] = new stdClass;
		$resp['output']->Result = 0;
		$resp['output']->SessionID = '123';
		$resp['output']->ReasonCode = '';
		$resp['output'] = json_encode($resp['output']); */
		
		return $resp;
	}
}
