<?php
class moh3i_charging_token
{
	private $run;
	private $now;
	private $result;
	
	public function __construct($run)
	{ 
		$this->setRun($run); 
		$this->setNow(time());
	}
	
	public function setRun($run){ $this->run = $run; }
	public function getRun(){ return $this->run; }
	
	public function setNow($now){ $this->now = $now; }
	public function getNow(){ return $this->now; }
	
	public function setResult($result){ $this->result = $result; }
	public function getResult(){ return $this->result; }
	
	public function run()
	{
		$forceReqFile = '/tmp/lock_moh3i_request_refresh_n_login_process';
		
		$log_profile = 'default';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$loaderConfig = loader_config::getInstance();
		$config_main = $loaderConfig->getConfig('main');
		$fToken = $config_main->path_token;
		
		$bFile = buffer_file::getInstance();
		$token_data = json_decode($bFile->get($fToken));
		
		if(empty($token_data)) $this->setRun(true);
		if($token_data->result <> 1 || $this->getNow() >= $token_data->expires_time) $this->setRun(true);
		
		while($this->getRun())
		{
			$refresh = $this->refresh();
			
			if($refresh->result <> 1)
			{
				$login = $this->login(1);
				
				if($login->result == 1) 
				{
					$this->refresh();
					
					$this->setRun(false);					
					$this->setResult(true);
					
					if(file_exists($forceReqFile)) unlink($forceReqFile);
				}
			}
			else 
			{
				$this->setRun(false);
				$this->setResult(true);
				
				if(file_exists($forceReqFile)) unlink($forceReqFile);
			}
			
			sleep(5);
		}
		
		$token_data = json_decode($bFile->get($fToken));
		if($token_data->result == 1) $this->setResult(true);
		
		return $this->getResult();
	}
	
	public function login($login) {
		
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		$loaderConfig = loader_config::getInstance();
		$config_main = $loaderConfig->getConfig('main');
		$fToken = $config_main->path_token;

		$bFile = buffer_file::getInstance();
		$token_data = json_decode($bFile->get($fToken));
		
		if(empty($token_data))
		{
			$data = array();
			$data['login'] = '';
			$data['result'] = 0;
			$data['ReasonCode'] = 0;
			$data['UserSpecificReserved']  = "";
			//$data['expires_in'] = 3600; // in seconds / 1 hour
			$data['expires_in'] = $config_main->expire_token; // in seconds / 1 hour
			$data['expires_time'] = strtotime("-" . $data['expires_in'] . " seconds");
			
			$bFile->saveString($fToken, json_encode($data));
		}

		$token_data = json_decode($bFile->get($fToken));
		
		if($login || empty($token_data->login))
		{
			//* ============== REQUEST TOKEN =============== *//

			$charging_model = loader_model::getInstance()->load('charging', 'connDatabase1');
			
			$charging_data = new charging_data ();
			$charging_data->operator = 1;
			$charging_data->senderType = 'text';
			$charging_data->messageType = 'daily';
			
			$get_charging = $charging_model->getCharging($charging_data);
			
			$req = array();
			$req['login'] = !empty($get_charging['username']) ? $get_charging['username'] . ":" . $get_charging['password'] : "6dCPCd3R:Admin@1234";
			
			if($config_main->toggle_env == "PRODUCTION") 
			{
				$req['url'] = $config_main->charging_url . ":" . $config_main->charging_url_port . "/token/";
				$req['port'] = $config_main->charging_url_port;
			}
			else if($config_main->toggle_env == "DEVELOPMENT") 
			{
				$req['url'] = $config_main->charging_url_dev . ":" . $config_main->charging_url_port_dev . "/token/";
				$req['port'] = $config_main->charging_url_port_dev;
			}

			$req['body'] = http_build_query(
				array("grant_type" => "client_credentials")
			);

			$req['headers'] = array(
						'Authorization: Basic ' . base64_encode($req['login']),
						'Content-Type: application/x-www-form-urlencoded',
						'Content-Length: ' . strlen($req['body'])
						);
								  
			$req['ssl'] = false;
			$req['timeout'] = $config_main->charging_timeout;

			$resp = http_request::requestPost($req, "Login");
			$resp = json_decode($resp['output']);
			
			// New Login Token
			/*
			{"access_token":"NmRDUHRzcHotQSoxNjI4MTMwOTU2NTU3","refresh_token":"NmRDUHRzcHotUioxNjI4MTkwOTU2NTU3","scope":"default","token_type":"Bearer","expires_in":1000}
			*/

			$data = array();
			$data['login'] = $req['login'];
			$data['result'] = (!empty($resp->access_token)) ? 1 : $resp->result;
			$data['ReasonCode'] = (!empty($resp->access_token)) ? 1 : $resp->ReasonCode;
			$data['UserSpecificReserved'] = (!empty($resp->access_token)) ? '' : $resp->UserSpecificReserved;
			$data['scope'] = (!empty($resp->scope)) ? $resp->scope : '';
			//$data['token_type'] = (!empty($resp->type)) ? $resp->type : '';
			$data['token_type'] = (!empty($resp->token_type)) ? $resp->token_type : '';
			//$data['expires_in'] = (!empty($resp->expires_in)) ? $resp->expires_in : 3600;
			$data['expires_in'] = $config_main->expire_token;
			//$data['expires_time'] = (!empty($resp->expires_in)) ? strtotime("+" . (int)$resp->expires_in . " seconds") : '';
			$data['expires_time'] = strtotime("+" . $config_main->expire_token . " seconds");
			$data['access_token'] = (!empty($resp->access_token)) ? $resp->access_token : '';
			$data['refresh_token'] = (!empty($resp->refresh_token)) ? $resp->refresh_token : '';
			
			$bFile->saveString($fToken, json_encode($data));
			
			return json_decode($bFile->get($fToken));
		}
		else
		{
			$log->write(array('level' => 'debug', 'message' => "Login Unrequested"));
		}
		
		return $token_data;
	}
	
	public function refresh() {
		
        $log = manager_logging::getInstance ();
		
		$loaderConfig = loader_config::getInstance();
		$config_main = $loaderConfig->getConfig('main');
		$fToken = $config_main->path_token;

		$bFile = buffer_file::getInstance();
		$token_data = json_decode($bFile->get($fToken));
		
		$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($token_data)));
		
		//* ============== REQUEST REFRESH TOKEN =============== *//
	
		$req = array();
		
		$charging_model = loader_model::getInstance()->load('charging', 'connDatabase1');
			
		$charging_data = new charging_data ();
		$charging_data->operator = 1;
		$charging_data->senderType = 'text';
		$charging_data->messageType = 'daily';
		
		$get_charging = $charging_model->getCharging($charging_data);
			
		$req = array();
		//$req['login'] = !empty($get_charging['username']) ? $get_charging['username'] . ":" . $get_charging['password'] : "linkit_cgw_20181121:whN4DBgw";
		$req['login'] = !empty($get_charging['username']) ? $get_charging['username'] . ":" . $get_charging['password'] : "6dCPCd3R:Admin@1234";
		
		if($config_main->toggle_env == "PRODUCTION") 
		{
			$req['url'] = $config_main->charging_url . ":" . $config_main->charging_url_port . "/token/";
			$req['port'] = $config_main->charging_url_port;
		} 
		else if($config_main->toggle_env == "DEVELOPMENT") 
		{
			$req['url'] = $config_main->charging_url_dev . ":" . $config_main->charging_url_port_dev . "/token/";
			$req['port'] = $config_main->charging_url_port_dev;
		}
			
		$req['body'] = http_build_query(
			array("grant_type" => "refresh_token", 
				  "refresh_token" => $token_data->refresh_token)
		);

		$req['headers'] = array(
					'Authorization: Basic ' . base64_encode($req['login']),
					'Content-Type: application/x-www-form-urlencoded',
					'Content-Length: ' . strlen($req['body'])
					);

		$req['ssl'] = false;
		$req['timeout'] = $config_main->charging_timeout;

		$resp = http_request::requestPost($req, "Refresh");
		$resp = json_decode($resp['output']);

		/* New Refresh Token				{"access_token":"NmRDUHRzcHotQSoxNjI4MjgyMzQwMjUz","refresh_token":"NmRDUHRzcHotUioxNjI4MzQyMzQwMjUz","scope":"PRODUCTION","type":"Bearer","ReasonCode":"","UserSpecificReserved":"","expires_in":1000,"Result":"1"}
		*/
		
		$data = array();
		$data['login'] = $req['login'];
		$data['result'] = (!empty($resp->access_token)) ? 1 : $resp->result;
		$data['ReasonCode'] = (!empty($resp->access_token)) ? 1 : $resp->ReasonCode;
		$data['UserSpecificReserved'] = (!empty($resp->access_token)) ? '' : $resp->UserSpecificReserved;
		$data['scope'] = (!empty($resp->scope)) ? $resp->scope : '';
		$data['token_type'] = (!empty($resp->type)) ? $resp->type : '';
		//$data['expires_in'] = (!empty($resp->expires_in)) ? $resp->expires_in : 3600;
		$data['expires_in'] = $config_main->expire_token;
		//$data['expires_time'] = (!empty($resp->expires_in)) ? strtotime("+" . (int)$resp->expires_in . " seconds") : '';
		$data['expires_time'] = strtotime("+" . $config_main->expire_token . " seconds");
		$data['access_token'] = (!empty($resp->access_token)) ? $resp->access_token : '';
		$data['refresh_token'] = (!empty($resp->refresh_token)) ? $resp->refresh_token : '';
		
		$bFile->saveString($fToken, json_encode($data));
		
		//return true;
		return json_decode($bFile->get($fToken));
	}
	
	/* 
	public function login($login) {
		
		$log_profile = 'default';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		$loaderConfig = loader_config::getInstance();
		$config_main = $loaderConfig->getConfig('main');
		$fToken = $config_main->path_token;

		$bFile = buffer_file::getInstance();
		$token_data = json_decode($bFile->get($fToken));
		
		if(empty($token_data))
		{
			$data = array();
			$data['login'] = '';
			$data['result'] = 0;
			$data['ReasonCode'] = 0;
			$data['UserSpecificReserved']  = "";
			//$data['expires_in'] = 3600; // in seconds / 1 hour
			$data['expires_in'] = 1800; // in seconds / 1 hour
			$data['expires_time'] = strtotime("-" . $data['expires_in'] . " seconds");
			
			$bFile->saveString($fToken, json_encode($data));
		}

		$token_data = json_decode($bFile->get($fToken));
		
		if($login || empty($token_data->login))
		{
			// ============== REQUEST TOKEN ===============

			$charging_model = loader_model::getInstance()->load('charging', 'connDatabase1');
			
			$charging_data = new charging_data ();
			$charging_data->operator = 1;
			$charging_data->senderType = 'text';
			$charging_data->messageType = 'daily';
			
			$get_charging = $charging_model->getCharging($charging_data);
			
			$req = array();
			$req['login'] = isset($get_charging['username']) ? $get_charging['username'] . ":" . $get_charging['password'] : "linkit_cgw_20181121:whN4DBgw";
			$req['url'] = $config_main->charging_url . ":" . $config_main->charging_url_port . "/token/";

			$req['body'] = http_build_query(
				array("grant_type" => "client_credentials")
			);

			$req['headers'] = array(
						'Authorization: Basic ' . base64_encode($req['login']),
						'Content-Type: application/x-www-form-urlencoded',
						'Content-Length: ' . strlen($req['body'])
						);
								  
			$req['port'] = $config_main->charging_url_port;
			$req['ssl'] = false;
			$req['timeout'] = $config_main->charging_timeout;

			$resp = http_request::requestPost($req, "Login");
			$resp = json_decode($resp['output']);
			
			$data = array();
			$data['login'] = $req['login'];
			$data['result'] = (isset($resp->access_token)) ? 1 : $resp->result;
			$data['ReasonCode'] = (isset($resp->access_token)) ? 1 : $resp->ReasonCode;
			$data['UserSpecificReserved'] = (isset($resp->access_token)) ? '' : $resp->UserSpecificReserved;
			$data['scope'] = (isset($resp->scope)) ? $resp->scope : '';
			$data['token_type'] = (isset($resp->type)) ? $resp->type : '';
			$data['expires_in'] = (isset($resp->expires_in)) ? $resp->expires_in : 3600;
			$data['expires_time'] = (isset($resp->expires_in)) ? strtotime("+" . (int)$resp->expires_in . " seconds") : '';
			$data['access_token'] = (isset($resp->access_token)) ? $resp->access_token : '';
			$data['refresh_token'] = (isset($resp->refresh_token)) ? $resp->refresh_token : '';
			
			$bFile->saveString($fToken, json_encode($data));
			
			return json_decode($bFile->get($fToken));
		}
		else
		{
			$log->write(array('level' => 'debug', 'message' => "Login Unrequested"));
		}
		
		return true;
	}
	
	public function refresh() {
		
		$log_profile = 'default';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
		
		$loaderConfig = loader_config::getInstance();
		$config_main = $loaderConfig->getConfig('main');
		$fToken = $config_main->path_token;

		$bFile = buffer_file::getInstance();
		$token_data = json_decode($bFile->get($fToken));
		
		$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($token_data)));
		
		// ============== REQUEST REFRESH TOKEN ===============
	
		$req = array();
		
		$charging_model = loader_model::getInstance()->load('charging', 'connDatabase1');
			
		$charging_data = new charging_data ();
		$charging_data->operator = 1;
		$charging_data->senderType = 'text';
		$charging_data->messageType = 'daily';
		
		$get_charging = $charging_model->getCharging($charging_data);
			
		$req = array();
		$req['login'] = isset($get_charging['username']) ? $get_charging['username'] . ":" . $get_charging['password'] : "linkit_cgw_20181121:whN4DBgw";
		$req['url'] = $config_main->charging_url . ":" . $config_main->charging_url_port . "/token/";
			
		$req['body'] = http_build_query(
			array("grant_type" => "refresh_token", 
				  "refresh_token" => $token_data->refresh_token)
		);

		$req['headers'] = array(
					'Authorization: Basic ' . base64_encode($req['login']),
					'Content-Type: application/x-www-form-urlencoded',
					'Content-Length: ' . strlen($req['body'])
					);

		$req['port'] = $config_main->charging_url_port;
		$req['ssl'] = false;
		$req['timeout'] = $config_main->charging_timeout;

		$resp = http_request::requestPost($req, "Refresh");
		$resp = json_decode($resp['output']);

		$data = array();
		$data['login'] = $req['login'];
		$data['result'] = (isset($resp->access_token)) ? 1 : $resp->result;
		$data['ReasonCode'] = (isset($resp->access_token)) ? 1 : $resp->ReasonCode;
		$data['UserSpecificReserved'] = (isset($resp->access_token)) ? '' : $resp->UserSpecificReserved;
		$data['scope'] = (isset($resp->scope)) ? $resp->scope : '';
		$data['token_type'] = (isset($resp->type)) ? $resp->type : '';
		$data['expires_in'] = (isset($resp->expires_in)) ? $resp->expires_in : 3600;
		$data['expires_time'] = (isset($resp->expires_in)) ? strtotime("+" . (int)$resp->expires_in . " seconds") : '';
		$data['access_token'] = (isset($resp->access_token)) ? $resp->access_token : '';
		$data['refresh_token'] = (isset($resp->refresh_token)) ? $resp->refresh_token : '';
		
		$bFile->saveString($fToken, json_encode($data));
		
		return true;
	} 
	*/
}