<?php
class config_broadcast {
	//public $bufferPath = '/app/buffers/moh3i/dailypush';
	public $bufferPath = '/app/xmp2012/buffers/moh3i/dailypush';
	public $dpTPMConfigPath = '/app/xmp2012/interface/moh3i/config/tpmall.json';

	public $lockPath = '/DATA/locking/dailypush';
	public $global_request_timeout = 10;
	public $use_global_timeout = true;
	public $retry_monthly = array(
		'priority' => array(
			array('option_1' => 
				array(
					'timespan' 	=> 2,
					'start' 	=> '05:00:00',
					'end'		=> '19:00:00'
				)
			),
			array('option_2' => 
				array(
					'timespan' 	=> 3,
					'start' 	=> '19:00:00',
					'end'		=> '23:59:59'
				)
			)
		),
		'nonpriority' => array(
			array('option_1' => 
				array(
					'timespan' 	=> 2,
					'start' 	=> '05:00:00',
					'end'		=> '19:00:00'
				)
			),
			array('option_2' => // RETRY SAME DAY && ALL RETRY MONTHLY
				array(
					'timespan' 	=> 1,
					'start' 	=> '19:00:00',
					'end'		=> '23:59:59'
				)
			)
		),
	);

	public $renewal_monthly = array(
		'priority' => array(
			array('option_1' => 
				array(
					'timespan' 	=> 5,
					'start' 	=> '05:00:00',
					'end'		=> '19:00:00'
				)
			),
			array('option_2' => 
				array(
					'timespan' 	=> 10,
					'start' 	=> '19:00:00',
					'end'		=> '23:59:59'
				)
			)
		),
		'nonpriority' => array(
			array('option_1' => 
				array(
					'timespan' 	=> 3,
					'start' 	=> '05:00:00',
					'end'		=> '19:00:00'
				)
			),
			array('option_2' => // RETRY SAME DAY
				array(
					'timespan' 	=> 1,
					'start' 	=> '19:00:00',
					'end'		=> '23:59:59'
				)
			)
		),
	);

	public $renewal_daily = array(
		'priority' => array(
			array('option_1' => 
				array(
					'timespan' 	=> 5,
					'start' 	=> '05:00:00',
					'end'		=> '19:00:00'
				)
			),
			array('option_2' => 
				array(
					'timespan' 	=> 10,
					'start' 	=> '19:00:00',
					'end'		=> '23:59:59'
				)
			)
		),
		'nonpriority' => array(
			array('option_1' => 
				array(
					'timespan' 	=> 3,
					'start' 	=> '05:00:00',
					'end'		=> '19:00:00'
				)
			),
			array('option_2' => // RETRY SAME DAY
				array(
					'timespan' 	=> 1,
					'start' 	=> '19:00:00',
					'end'		=> '23:59:59'
				)
			)
		),
	);

	// CONFIGURATION TIME GET DATA FOR PRIORITY

	public $get_data = array(
		'retry_same_day' => array(
			array('renewal_daily' => 
				array(
					'start' => '13:00:00',
					'end'	=> '19:00:00'
				)
			),
			array('retry' => 
				array(
					'start' => '13:00:00',
					'end'	=> '19:00:00'
				)
			)
		),
		'normal_dp' => array(
			array('micro_services' => 
				array(
					'start' => '05:00:00',
					'end'	=> '15:00:00'
				)
			),
			array('normal' => 
				array(
					'start' => '13:00:00',
					'end'	=> '18:59:59'
				)
			)
		),
	);
}
?>
