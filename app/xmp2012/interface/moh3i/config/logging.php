<?php

class config_logging {

    public $timeDigit = 8;
    
    public $lineFormat = "{uniqueId} {level} {datetime} {exectime} {class} {function} {message} {response}";
    //public $loglevel = 2; //1; //set to 9 to disable debug level logging
    public $loglevel = 1; //1; //set to 9 to enable info level logging
    
	public $profile = array(
        'default' => array(
            //'path' => '/app/xmp2012/logs/moh3i/default',
            'path' => '/DATA//app/xmp2012/logs/moh3i/default',
            'type' => 'file',
            'filename' => 'default'
        ),
        'mo_receiver' => array(
            //'path' => '/app/xmp2012/logs/moh3i/mo_receiver',
            'path' => '/DATA/app/xmp2012/logs/moh3i/mo_receiver',
            'type' => 'file',
            'filename' => 'mo_receiver'
        ),
        'mo_processor' => array(
            //'path' => '/app/xmp2012/logs/moh3i/mo_processor',
            'path' => '/DATA/app/xmp2012/logs/moh3i/mo_processor',
            'type' => 'file',
            'filename' => 'mo_processor'
        ),
		'mo_retry_processor' => array(
            'path' => '/DATA/app/xmp2012/logs/moh3i/mo_retry_processor',
            'type' => 'file',
            'filename' => 'mo_retry_processor'
        ),
        'mt_processor' => array(
            //'path' => '/app/xmp2012/logs/moh3i/mt_processor',
            'path' => '/DATA/app/xmp2012/logs/moh3i/mt_processor',
            'type' => 'file',
            'filename' => 'mt_processor'
        ),
        'mo_subscriber' => array(
            'path' => '/app/xmp2012/logs/moh3i/mo_subscriber',
            'type' => 'file',
            'filename' => 'mo_subscriber'
        ),
        'broadcast' => array(
//          'path' => '/app/xmp2012/logs/moh3i/broadcast',
			'path' => '/DATA/app/xmp2012/logs/moh3i/broadcast',
            'type' => 'file',
            'filename' => 'broadcast'
        ),
        'call_center' => array(
            'path' => '/app/xmp2012/logs/moh3i/callcenter',
            'type' => 'file',
            'filename' => 'callcenter'
        ),
        'dr_receiver' => array(
            //'path' => '/app/xmp2012/logs/moh3i/dr_receiver',
            'path' => '/DATA/app/xmp2012/logs/moh3i/dr_receiver',
            'type' => 'file',
            'filename' => 'dr_receiver'
        ),
        'dr_processor' => array(
            'path' => '/app/xmp2012/logs/moh3i/dr_processor',
            'type' => 'file',
            'filename' => 'dr_processor'
        ),
        'dr_updater' => array(
            'path' => '/app/xmp2012/logs/moh3i/dr_updater',
            'type' => 'file',
            'filename' => 'dr_updater'
        ),
		'retry_processor' => array(
            'path' => '/app/xmp2012/logs/moh3i/retry_processor',
            'type' => 'file',
            'filename' => 'retry_processor'
        ),
        'cmp_processor' => array(
            //'path' => '/app/xmp2012/logs/moh3i/cmp_processor',
            'path' => '/DATA/app/xmp2012/logs/moh3i/cmp_processor/',
            'type' => 'file',
            'filename' => 'cmp_processor'
        ),
		'charging' => array(
            'path' => '/app/xmp2012/logs/moh3i/charging',
            'type' => 'file',
            'filename' => 'charging'
        )
    );

}
