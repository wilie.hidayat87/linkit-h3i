<?php
/*
 * "http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0";
 */
class config_mt {
    /* server config */
    public $profile = array(
        'default' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'indosatMT',
            'slot' => '3',
            'retry' => '5',
		    //'sendUrl' => array('http://127.0.0.1:8053/indosat/simulate/operator/smspull.php'),
		    //'sendUrl' => array('http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0'),
		    'sendUrl' => array('http://202.152.162.163:8329/'),
    		'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'text' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'indosatText',
            'slot' => '10',
            'retry' => '5',
		    //'sendUrl' => array('http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0'),
		    'sendUrl' => array(
				
				//'http://180.214.234.52:8080/PUSH?USERNAME=lnkit99876mt0&PASSWORD=password&REG_DELIVERY=1&ORIGIN_ADDR=998760&TYPE=0&udh=1', // MT SMS
				/*Update endpoint MT 20181213*/
			    //'http://180.214.234.112:9003/PUSH/?USERNAME=linkit&PASSWORD=L1nK17b1&REG_DELIVERY=1&ORIGIN_ADDR=998760&TYPE=0', // MT SMS
				//'http://116.206.10.132:9003/PUSH/?USERNAME=linkit&PASSWORD=L1nK17b1&REG_DELIVERY=1&ORIGIN_ADDR=998760&TYPE=0', // PRODUCTION
				'http://116.206.10.180:9003/push?USERNAME=SD_210906_0165&PASSWORD=L1nK17b1&REG_DELIVERY=1&ORIGIN_ADDR=998760&TYPE=0', // PRODUCTION
				'http://notify.tri.gemezz.mobi/notify/notify.php', // Portal Notify PRODUCTION
				'Pulsa kamu tidak cukup untuk layanan Gemezz (tarif data berlaku). Silakan isi ulang pulsa kamu dan klik http://tri.gemezz.mobi  mainkan kompetisinya lalu tukarkan hadiah menarik dari LinkIT'
				),
            'sendUrlDev' => array(
				'http://127.0.0.1/payment/mt.php?test=1', // DEVELOPMENT
				'http://127.0.0.1/payment/notify.php', // Portal Notify DEVELOPMENT
			),
            'SendTimeOut' => '30',
            'throttle' => 2000,
            'priority' => 6,
            'hit' => true,
			'mtProcess' => array
			(
				'charging' => true,
				'confirmation' => array
				(
					'global' => true,
					'daily' => array
					(
						'subject' => array('ps','ps1','ps2','ps3','ps4','ps5','ps6','ps7','ps8','ps9','ps10','ps11','ps12','ps13','ps14','ps15','ps16','ps17','ps18','ps50','ps51','ps52','ps53','ps54','ps55','ps56','ps57','ps58','ps59','ps60'),
						's2' => array('spc','spc2'),
						'ps' => array
						(
							'this' => false,
							'spc' => false
						),
						'ps1' => array
						(
							'this' => false
						),
						'ps2' => array
						(
							'this' => false
						),
						'ps3' => array
						(
							'this' => false
						),
						'ps4' => array
						(
							'this' => false
						),
						'ps5' => array
						(
							'this' => false
						),
						'ps6' => array
						(
							'this' => false
						),
						'ps7' => array
						(
							'this' => false
						),
						'ps8' => array
						(
							'this' => false
						),
						'ps9' => array
						(
							'this' => false
						),
						'ps10' => array
						(
							'this' => false
						),
						'ps11' => array
						(
							'this' => false
						),
						'ps12' => array
						(
							'this' => false
						),
						'ps13' => array
						(
							'this' => false
						),
						'ps14' => array
						(
							'this' => false
						),
						'ps15' => array
						(
							'this' => false
						),
						'ps16' => array
						(
							'this' => false
						),
						'ps17' => array
						(
							'this' => false
						),
						'ps18' => array
						(
							'this' => false
						),
						'ps50' => array
						(
							'this' => true
						),
						'ps51' => array
						(
							'this' => true
						),
						'ps52' => array
						(
							'this' => true
						),
						'ps53' => array
						(
							'this' => true
						),
						'ps54' => array
						(
							'this' => true
						),
						'ps55' => array
						(
							'this' => true
						),
						'ps56' => array
						(
							'this' => true
						),
						'ps57' => array
						(
							'this' => true
						),
						'ps58' => array
						(
							'this' => true
						),
						'ps59' => array
						(
							'this' => false
						),
						'ps60' => array
						(
							'this' => false
						)
					),
					'monthly' => array
					(
						'subject' => array('fs','ms','ms1','ms2','ms4','ms5','spc3','spc4','spc5','spc6','spc7','spc8','spc9','spc10','spc11','spc12'),
						's2' => array('spc','spc2'),
						'fs' => array
						(
							'this' => false
						),
						'ms' => array
						(
							'this' => false,
							'spc' => false,
							'spc2' => false
						),
						'ms1' => array
						(
							'this' => false
						),
						'ms2' => array
						(
							'this' => false
						),
						'ms4' => array
						(
							'this' => false
						),
						'ms5' => array
						(
							'this' => false
						),
						'spc3' => array
						(
							'this' => false
						),
						'spc4' => array
						(
							'this' => false
						),
						'spc5' => array
						(
							'this' => false
						),
						'spc6' => array
						(
							'this' => false
						),
						'spc7' => array
						(
							'this' => false
						),
						'spc8' => array
						(
							'this' => false
						),
						'spc9' => array
						(
							'this' => true
						),
						'spc10' => array
						(
							'this' => true
						),
						'spc11' => array
						(
							'this' => false
						),
						'spc12' => array
						(
							'this' => false
						)
					)
				),
				'mt' => array
				(
					'reg_unreg' => array
					(
						'global' => true,
						'daily' => array
						(
							'subject' => array('ps','ps1','ps2','ps3','ps4','ps5','ps6','ps7','ps8','ps9','ps10','ps11','ps12','ps13','ps14','ps15','ps16','ps17','ps18','ps50','ps51','ps52','ps53','ps54','ps55','ps56','ps57','ps58','ps59','ps60'),
							'global' => true,
							'ps' => array
							(
								'this' => true,
								'spc' => false
							),
							'ps1' => array
							(
								'this' => true,
								'spc' => false
							),
							'ps2' => array
							(
								'this' => true
							),
							'ps3' => array
							(
								'this' => true,
								'dynamic' => true,
							),
							'ps4' => array
							(
								'this' => true,
								'dynamic' => true,
							),
							'ps5' => array
							(
								'this' => true,
								'dynamic' => true,
							),
							'ps6' => array
							(
								'this' => true,
								'dynamic' => true,
							),
							'ps7' => array
							(
								'this' => true
							),
							'ps8' => array
							(
								'this' => true
							),
							'ps9' => array
							(
								'this' => true
							),
							'ps10' => array
							(
								'this' => true
							),
							'ps11' => array
							(
								'this' => true
							),
							'ps12' => array
							(
								'this' => true
							),
							'ps13' => array
							(
								'this' => true
							),
							'ps14' => array
							(
								'this' => true
							),
							'ps15' => array
							(
								'this' => true
							),
							'ps16' => array
							(
								'this' => true
							),
							'ps17' => array
							(
								'this' => true,
								'dynamic' => true,
							),
							'ps18' => array
							(
								'this' => true,
								'dynamic' => true,
							),
							'ps50' => array
							(
								'this' => true
							),
							'ps51' => array
							(
								'this' => true
							),
							'ps52' => array
							(
								'this' => true
							),
							'ps53' => array
							(
								'this' => true
							),
							'ps54' => array
							(
								'this' => true
							),
							'ps55' => array
							(
								'this' => true
							),
							'ps56' => array
							(
								'this' => true
							),
							'ps57' => array
							(
								'this' => true
							),
							'ps58' => array
							(
								'this' => true
							),
							'ps59' => array
							(
								'this' => true,
								'dynamic' => true,
							),
							'ps60' => array
							(
								'this' => true
							)
						),
						'monthly' => array
						(
							'subject' => array('fs','ms','ms1','ms2','ms4','ms5','spc3','spc4','spc5','spc6','spc7','spc8','spc9','spc10','spc11','spc12'),
							'global' => true,
							'ms' => array
							(
								'this' => true,
								'spc' => false,
								'spc2' => false
							),
							'ms1' => array
							(
								'this' => true
							),
							'ms2' => array
							(
								'this' => true
							),
							'ms4' => array
							(
								'this' => true
							),
							'ms5' => array
							(
								'this' => true
							),
							'spc3' => array
							(
								'this' => false
							),
							'spc4' => array
							(
								'this' => false
							),
							'spc5' => array
							(
								'this' => false
							),
							'spc6' => array
							(
								'this' => false
							),
							'spc7' => array
							(
								'this' => false
							),
							'spc8' => array
							(
								'this' => false
							),
							'spc9' => array
							(
								'this' => false
							),
							'spc10' => array
							(
								'this' => true
							),
							'spc11' => array
							(
								'this' => false
							),
							'spc12' => array
							(
								'this' => true
							)
						),
						'unreg' => true
					),
					'broadcast' => array
					(
						'renewal_daily' => 
						array
						(
							'subject' => array('ps','ps1','ps2','ps3','ps4','ps5','ps6','ps7','ps8','ps9','ps10','ps11','ps12','ps13','ps14','ps15','ps16','ps17','ps18','ps50','ps51','ps52','ps53','ps54','ps55','ps56','ps57','ps58','ps59','ps60'),
							'global' => true,
							'ps' => array
							(
								'this' => true,
								'spc' => false
							),
							'ps1' => array
							(
								'this' => true
							),
							'ps2' => array
							(
								'this' => true
							),
							'ps3' => array
							(
								'this' => true
							),
							'ps4' => array
							(
								'this' => true
							),
							'ps5' => array
							(
								'this' => true
							),
							'ps6' => array
							(
								'this' => true
							),
							'ps7' => array
							(
								'this' => true
							),
							'ps8' => array
							(
								'this' => true
							),
							'ps9' => array
							(
								'this' => true
							),
							'ps10' => array
							(
								'this' => true
							),
							'ps11' => array
							(
								'this' => true
							),
							'ps12' => array
							(
								'this' => true
							),
							'ps13' => array
							(
								'this' => true
							),
							'ps14' => array
							(
								'this' => true
							),
							'ps15' => array
							(
								'this' => true
							),
							'ps16' => array
							(
								'this' => true
							),
							'ps17' => array
							(
								'this' => true
							),
							'ps18' => array
							(
								'this' => true
							),
							'ps50' => array
							(
								'this' => true
							),
							'ps51' => array
							(
								'this' => true
							),
							'ps52' => array
							(
								'this' => true
							),
							'ps53' => array
							(
								'this' => true
							),
							'ps54' => array
							(
								'this' => true
							),
							'ps55' => array
							(
								'this' => true
							),
							'ps56' => array
							(
								'this' => true
							),
							'ps57' => array
							(
								'this' => true
							),
							'ps58' => array
							(
								'this' => true
							),
							'ps59' => array
							(
								'this' => true
							),
							'ps60' => array
							(
								'this' => true
							)
						),
						'renewal_monthly' => 
						array
						(
							'subject' => array('fs','ms','ms1','ms2','ms4','ms5','spc3','spc4','spc5','spc6','spc7','spc8','spc9','spc10','spc11','spc12'),
							'global' => true,
							'ms' => array
							(
								'this' => true,
								'spc' => false,
								'spc2' => false
							),
							'ms1' => array
							(
								'this' => true
							),
							'ms2' => array
							(
								'this' => true
							),
							'ms4' => array
							(
								'this' => true
							),
							'ms5' => array
							(
								'this' => true
							),
							'spc3' => array
							(
								'this' => false
							),
							'spc4' => array
							(
								'this' => false
							),
							'spc5' => array
							(
								'this' => false
							),
							'spc6' => array
							(
								'this' => false
							),
							'spc7' => array
							(
								'this' => false
							),
							'spc8' => array
							(
								'this' => false
							),
							'spc9' => array
							(
								'this' => false
							),
							'spc10' => array
							(
								'this' => false
							),
							'spc11' => array
							(
								'this' => false
							),
							'spc12' => array
							(
								'this' => false
							)
						),
						'retry_monthly' => 
						array
						(
							'subject' => array('fs','ms','ms1','ms2','ms4','ms5','spc3','spc4','spc5','spc6','spc7','spc8','spc9','spc10','spc11','spc12'),
							'global' => true,
							'ms' => array
							(
								'this' => true,
								'spc' => false,
								'spc2' => false
							),
							'ms1' => array
							(
								'this' => true
							),
							'ms2' => array
							(
								'this' => true
							),
							'ms4' => array
							(
								'this' => true
							),
							'ms5' => array
							(
								'this' => true
							),
							'spc3' => array
							(
								'this' => false
							),
							'spc4' => array
							(
								'this' => false
							),
							'spc5' => array
							(
								'this' => false
							),
							'spc6' => array
							(
								'this' => false
							),
							'spc7' => array
							(
								'this' => false
							),
							'spc8' => array
							(
								'this' => false
							),
							'spc9' => array
							(
								'this' => false
							),
							'spc10' => array
							(
								'this' => false
							),
							'spc11' => array
							(
								'this' => false
							),
							'spc12' => array
							(
								'this' => false
							)
						),
						'30D' => true
					),
					'180D' => true
				)
			)
        ),
        'push' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'indosatMTPush',
            'slot' => '2',
            'retry' => '0',
		    //'sendUrl' => array('http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0'),
		    'sendUrl' => array('http://202.152.162.163:8329/'),
        	//'sendUrl' => array('http://127.0.0.1:8053/indosat/simulate/operator/smspull.php'),
            'SendTimeOut' => '10',
            'throttle' => 2000,
            'priority' => 5
        ),
        'wappush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'indosatMTWapPush',
            'slot' => '3',
            'retry' => '5',
		    //'sendUrl' => array('http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0'),
		    'sendUrl' => array('http://202.152.162.163:8329/'),
            //'sendUrl' => array('http://127.0.0.1:8053/indosat/simulate/operator/smspull.php'),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'optin' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'indosatMTOptin',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://127.0.0.1/'),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'dailypush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'indosatMTDailyPush',
            'slot' => '10',
            'retry' => '5',
		    //'sendUrl' => array('http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0'),
		    'sendUrl' => array('http://202.152.162.163:8329/'),
            //'sendUrl' => array('http://127.0.0.1:8053/indosat/sendurl/apa.php'),
            'SendTimeOut' => '10',
            'throttle' => 500
        ),
        'delaypush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'indosatMTDelayPush',
            'slot' => '3',
            'retry' => '5',
		    //'sendUrl' => array('http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0'),
		    'sendUrl' => array('http://202.152.162.163:8329/'),        
		    //'sendUrl' => array('http://127.0.0.1:8053/indosat/simulate/operator/smspull.php'),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'delaywappush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'indosatDelayWapPush',
            'slot' => '3',
            'retry' => '5',
		    //'sendUrl' => array('http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0'),
		    'sendUrl' => array('http://202.152.162.163:8329/'),      
            //'sendUrl' => array('http://127.0.0.1/'),
            'SendTimeOut' => '10',
            'throttle' => 3
        )
    );
}
