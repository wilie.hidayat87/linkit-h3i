<?php
class config_mospc {
	public $bufferPath = '/DATA/buffer/mospc';
	public $bufferSlot = 10;
	public $returnCode = array ('OK' => 'OK', 'NOK' => 'NOK' );
	public $bufferThrottle = 300;

	public $lockPath = '/DATA/locking/mospc';
}
