<?php

class config_main {

    public $operator = 'moh3i';
    public $adn = '99876';
    public $channel = array('sms', 'web', 'wap', 'umb');
    public $partner = array('1');
    public $prefix = 'moh3i';
    public $request_type = array(
        'reg' => array('reg'),
        'unreg' => array('unreg')
    );
    public $use_forking = true;

	public $path_token = "/app/xmp2012/interface/moh3i/config/token.json";

	public $toggle_env = "PRODUCTION"; // PRODUCTION OR DEVELOPMENT
	//public $toggle_env = "DEVELOPMENT"; // PRODUCTION OR DEVELOPMENT

	//DEVELOPMENT
	public $charging_url_dev = "http://147.139.169.26";
	public $charging_url_port_dev = 80;
	
	//PRODUCTION
	//public $charging_url = "https://116.206.10.132"; // NEW
	//public $charging_url_port = 9443;
	
	public $charging_url = "https://116.206.10.180"; // NEW
	public $charging_url_port = 9004;

	//PRODUCTION OLD
	//public $charging_url = "https://180.214.234.112"; // OLD
	//public $charging_url_port = 9443;

	public $charging_timeout = 10;
	public $expire_token = 900; // 30 minutes
	//public $expire_token = 20; // 20 seconds
	
	public $subscribed_until = "+180 day";
	public $active_until = "+90 day";
	public $expire_confirm_180d = "+1 day";

	public $listSubsType = array(
		'S1'	=> 1,
		'S3'	=> 3,
		'S7'	=> 7,
		'S7T'	=> 7,
		'S14'	=> 14,
		'S30'	=> 30,
		'S30T'	=> 30,
		'S7F'	=> 7
	);
	
	public $listAccumulateRenewal = array(
		'PS7'	=> 1,
		'PS8'	=> 3,
		'PS9'	=> 7,
		'PS10'	=> 7,
		'PS11'	=> 14,
		'PS12'	=> 30,
		'PS13'	=> 30,
		'PS14'	=> 7
	);
	
	public $dynamic3rdKeyword = array('PS59');

	public $nonBundlingAllSubject = array('DAILY','PS','PS1','PS2','PS3','PS4','PS5','PS6','PS15','PS16','PS17','PS18','PS50','PS51','PS52','PS53','PS54','PS55','PS56','PS57','PS58','PS59','PS60','FS','MS','MS1','MS2','MS4','MS5','SPC3','SPC4','SPC5','SPC6','SPC7','SPC8','SPC9','SPC10','SPC11','SPC12');
	public $nonBundlingDailySubject = array('DAILY','PS','PS1','PS2','PS3','PS4','PS5','PS6','PS15','PS16','PS17','PS18','PS50','PS51','PS52','PS53','PS54','PS55','PS56','PS57','PS58','PS59','PS60');
	public $bundlingSubject = array('PS7','PS8','PS9','PS10','PS11','PS12','PS13','PS14');

	public $freemiumKeyword = array('PS');
}
