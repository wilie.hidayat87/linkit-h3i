<?php
 class config_delay{
     public $bufferPath = '/app/xmp2012/buffers/moh3i/mtBuffer';
     public $delayTime  = 2;//second
     public $throttle   = 10; 
     public $mtDelay = 20;
     public $burstMechanism = 'parallel';
     public $slot = 10;
     public $use_forking = true;
	 public $superpriority = array(628978659181,6289655913547,6289655913961,62895423478415,628990437031,6289688045564,628990351731);
 }
