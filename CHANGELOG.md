---------------------------
Version 1.0.1 (2021-02-24)
---------------------------

Author : Wilie

Changes:
1. Add postback to kbtools for keyword PS50 - PS60

Commit Path:
1. git add CHANGELOG.md
2. git add app/xmp2012/interface/moh3i/config/main.php
3. git add app/xmp2012/interface/moh3i/config/mt.php
4. git add app/xmp2012/interface/moh3i/mo/processor.php
5. git add app/xmp2012/interface/moh3i/service/module/registration.php
6. git add app/xmp2021/system/core/http/request.php
7. git add app/xmp2021/interface/moh3i/config/tpmall.json
8. git add app/xmp2012/interface/moh3i/charging/charge.php
9. git add app/xmp2012/interface/moh3i/charging/revlimit.php
10. git add app/xmp2012/interface/moh3i/charging/token.php
11. git add app/xmp2012/interface/moh3i/broadcast/sentcontent.php
12. git add app/xmp2012/interface/moh3i/mt/processor/text.php

Commiting Trackid: 

Bugfixes: 
1. Activate timeout data history for first push
2. Add toggle between for developement or productoin environment

---------------------------
Version 1.1.2 (2021-03-17)
---------------------------

Author : Wilie

Changes:
1. Update non bundling keyword
2. Update purge interval from 30 to 90 day

Commit Path:
1. git add CHANGELOG.md
2. git add app/xmp2012/interface/moh3i/config/main.php
3. git add app/xmp2012/interface/moh3i/mo/bundling.php
4. git add app/xmp2012/interface/moh3i/broadcast/base.php

Commiting Trackid: 

Bugfixes: 
1. - fixing when purge activity update, added priority column = 0

---------------------------
Version 1.2.2 (2021-03-22)
---------------------------

Author : Wilie

Changes:
1. Make this keyword PS50 - PS53 using confirmation sms

Commit Path:
1. git add CHANGELOG.md
2. git add app/xmp2012/interface/moh3i/config/mt.php

Commiting Trackid: 870771f9

Bugfixes: 
1. - 

---------------------------
Version 1.3.2 (2021-03-23)
---------------------------

Author : Wilie

Changes:
1. Make default TPM in 3 session default TPM (4 am - 5 am, 6 am - 9 am, 10 am - 11 pm)

Commit Path:
1. git add CHANGELOG.md
2. git add app/xmp2012/interface/moh3i/config/tpmall.json.master_1
3. git add app/xmp2012/interface/moh3i/config/tpmall.json.master_2
4. git add app/xmp2012/interface/moh3i/config/tpmall.json.master_3
5. git add app/xmp2012/interface/moh3i/bin/broadcast/broadcast_retrypush_sesi_1.sh
6. git add app/xmp2012/interface/moh3i/bin/broadcast/broadcast_retrypush_sesi_2.sh
7. git add app/xmp2012/interface/moh3i/bin/broadcast/broadcast_retrypush_sesi_3.sh
8. git add app/xmp2012/interface/moh3i/bin/broadcast/broadcast_retrypush_sesi_4.sh
9. git add app/xmp2012/interface/moh3i/bin/broadcast/broadcast_retrypush_sesi_5.sh

* cron modified

#*** Reset TPM Config ***#

#10 00 * * *      sh /home/production/default_tpm.sh

#Default TPM 04.00 - 05.59

10 00 * * *      sh /home/production/default_tpm_sesi1.sh

#Default TPM 06.00 - 09.59

00 06 * * *      sh /home/production/default_tpm_sesi2.sh

#Default TPM 10.00 - 23.59

00 10 * * *      sh /home/production/default_tpm_sesi3.sh

Commiting Trackid: 7aa8038

Bugfixes: 
1. - Add PS50 - PS60 for retry sd

---------------------------
Version 1.2.3 (2021-04-17)
---------------------------

Author : Wilie

Changes:
1. Update tpmmanagement 

Commit Path:
1. git add CHANGELOG.md
2. git add app/xmp2012/interface/moh3i/www/techcheck/tpmmanagement.php

Commiting Trackid:

Bugfixes: 
1. -

---------------------------
Version 1.3.3 (2021-06-08)
---------------------------

Author : Wilie

Changes:
1. Postback PS59

Commit Path:
1. git add CHANGELOG.md
2. git add app/xmp2012/interface/moh3i/broadcast/base.php
3. git add app/xmp2012/interface/moh3i/mo/processor.php
4. git add app/xmp2012/interface/moh3i/service/module/registration.php

Commiting Trackid:

Bugfixes:
1. -
 
---------------------------
Version 1.4.3 (2021-06-08)
---------------------------

Author : Wilie

Changes:
1. Postback unreg PS59

Commit Path:
1. git add CHANGELOG.md
2. git add app/xmp2012/interface/moh3i/service/module/unregistration.php
3. git add app/xmp2012/interface/moh3i/user/manager.php

Commiting Trackid:

Bugfixes:
1. -